---
description: Watch our Co-Founder Aaluxx speak about Maya with various hosts!
---

# 📽️ Aaluxx Interviews

**Aug 23, 2024**

{% embed url="https://www.youtube.com/watch?t=9247s&v=q0H9bWdIh24" %}

<details>

<summary>Key Points </summary>

* On the first day of Rare Evo, Aaluxx, co-founder of Maya Protocol, took the stage alongside H Payne from Shapeshifter and Reza Shahi from Nine Realms to discuss the multi-chain decentralized future.&#x20;
* The panel delved into the importance of a multi-chain ecosystem and how Maya Protocol is actively working to integrate widely-used chains, reduce transaction fees, and seamlessly connect them with the same assets. Together, they explore the solutions to interact with other ecosystems, highlighting the path forward for a more interconnected and efficient DeFi landscape.

</details>

#### Aug 22, 2024.

{% embed url="https://www.youtube.com/live/hWipg0pLTPE?t=12005s" %}

<details>

<summary>Key Points </summary>

* At Rare Evo, Aalux, co-founder of Maya Protocol, is joined by Zey from Moka and Giga Thad from El Dorado to talk about accelerating and innovating within the DeFi space through collaboration.&#x20;
* Alongside Baokba, Aaluxx discusses the role of strategic partnerships in shaping the future of decentralized finance. Get ready for a deep dive into how collaboration is fueling the growth of DeFi.

</details>

**Aug 22, 2024**

{% embed url="https://www.youtube.com/watch?v=0mqUtNWlpss" %}

<details>

<summary>Key Points </summary>

* The collaboration between the Maya protocol and Maestro, a leading infrastructure provider for the Cardano ecosystem, focuses on bringing seamless interoperability and bridgeless swaps to Cardano.
* The integration process for including Cardano on the Maya protocol is expected to take two to three months, possibly extending to four to six months to address any unique challenges. The development team is incentivized to expedite the integration to benefit from increased transaction volume and to contribute to Cardano's ecosystem expansion.

</details>

**March 10, 2024**

{% embed url="https://www.youtube.com/watch?t=638s&v=p0qIxHexxg8" %}

<details>

<summary>Key Points </summary>

* In this episode of The Deep Dive, Aaluxx sits down with Zeb to explore the inner workings of Maya Protocol. They dive deep into key details such as Cacao, the Maya token, the fair launch, and the overall mission of Maya Protocol.&#x20;
* They also discuss what sets Maya apart from other projects like Thorchain, shedding light on its unique approach to decentralization and the future of cross-chain interoperability. Tune in for an in-depth look at the goals behind Maya Protocol and how it’s shaping the future of DeFi!

</details>

**Jan 27, 2024**

<details>

<summary>Key Points</summary>

* Discussions were held regarding cross-chain swaps for Cardano using Maya protocol and Maestro team. The teams' representatives introduced themselves, including Marvin (co-founder and CEO of Maestro), Vas (co-founder and CTO of Maestro), and Aluxx (co-founder and lead of Maya protocol). The Maya protocol allows for cross-chain swaps by utilizing pooled liquidity from multiple chains, minimizing risks compared to bridges. The discussion revolved around Cardano proposals to accelerate Maya's capabilities on the platform for improved user experience and interoperability.
* Maya is a fork of Thorchain that expands its capabilities to more blockchain networks, with plans of interoperability. It implements changes such as using liquidity node validators and a non-inflationary token for improved efficiency and yield. It functions as a cross-chain decentralized exchange with trustless swaps and incentivized liquidity providers.
* Adding assets to liquidity pools on decentralized exchanges, specifically on Maya, where assets are paired with cacao. Maya also offers a "Savers" feature for single asset exposure, but with lower yields.&#x20;
* Integrating Cardano requires MAYANode operators to run full Cardano nodes and make necessary protocol upgrades. This process incurs additional costs but also brings in new volume and fees. Two Catalyst proposals aim to make the protocol compatible and provide support for testing and running a new Cardano cluster.
* The main limitation in Cosmos chains is the Tendermint consensus technology, which currently only allows for 120-150 nodes due to its requirement for instant finality. This requires multiple rounds of voting, causing delays due to network latency. To improve throughput, there are plans to increase the node limit to 200-250 and enhance network performance. Instant finality is crucial for cross-chain services like Maya, while large collateral is needed for permissionless validation. As such, trustlessness is achieved through game theory incentives instead of a large number of nodes.
* Maya determines valid blocks on different blockchain networks using bifrost, which has an observer and signer client. Maya stores incoming blocks using a block scanner for each chain. Other chains like Kujira have instant finality, while with Cardano, there is collaboration with researchers to determine confirmation mechanisms and refund management based on Ouroboros. This process involves testing and iterative improvements.
* The benefits of streaming swaps between Cardano (ADA) and Bitcoin are discussed, as they break large swaps into smaller sub-swaps executed over multiple blocks, decreasing slippage and fees. This has resulted in a 20x increase in daily trading volumes on other chains, with the largest swap being $10 million.
* Integration of Cardano would require two weeks for initial work, followed by testing, audits, and bootstrapping liquidity through node-created vaults. A funding proposal would help complete the integration efficiently. This integration plan, first planned for 2021, will enhance cross-chain functionality for decentralized exchanges and wallets.
* Discusses cross-chain swaps between Cardano and other protocols through Maya, a decentralized exchange, potentially increasing capital inflow and new users. Wallets facilitating swaps may receive affiliate fees as a form of monetization. Highlighted as an important goal is the fulfillment of trustless peer-to-peer transactions.&#x20;

</details>

{% embed url="https://www.youtube.com/watch?v=WdfPsAhSDGs" %}

**Jan 5, 2024**

{% embed url="https://youtu.be/-8SZLj-jwz8?si=SY0LObCIrkBuJTm5" %}

<details>

<summary>Key Points </summary>

* In this interview, Aaluxx shares the journey behind Maya Protocol, a decentralized cross-chain DeFi solution born from Thorchain. He discusses Maya’s mission to support blockchains not yet integrated by Thorchain, including Cardano, Zcash, Solana,  and more.&#x20;
* Aaluxx explores how Maya empowers users with decentralization, the challenges with centralized exchanges, and the team’s vision for the future. Learn about Maya’s

</details>

**Oct 6, 2023**

{% embed url="https://www.youtube.com/watch?v=E7NmqiGU-48" %}

<details>

<summary>Key Points</summary>

* Recent drama in the Thorchain ecosystem, particularly ThorSwap, has sparked mainstream news coverage. Legal pressure in certain jurisdictions, such as the US, is difficult for DEX teams. DEXs are better for users than centralized exchanges, even with legal compliance changes. Privacy coin concerns have shifted to focus on DEX operations.
* Aditya historically used decentralized swap services for cryptocurrency privacy, wanting to add Zcash and Dash to Thorchain. But Thorchain didn't add them, so they looked into Maya protocol. Nighthawk has been integrating Zcash into Thorchain for over a year, funded by a Zcash grant. However, integration has taken longer than expected due to vulnerabilities in Thorchain and network upgrades. Explores importance of integration for Zcash and current status.
* Decentralized exchange protocols, like THORChain and Maya Protocol, offer an improved experience for privacy minded users. They enable single-click swapping between coins, and the privacy of funds is greater than with centralized exchanges, which require personal info. Decentralized Protocols also reduce risks like insolvency and regulation, making privacy coins more accessible and secure.
* Maya Protocol offers value through arbitrage, redundancy, assets not available on THORChain, and helps with regulatory issues especially with the delisting of privacy coins on CEXs.
* Privacy is a human right being eroded by tech. While some crypto users don't value it, it remains of high importance
* Neutral infrastructure shouldn't be blamed for illicit funds. A gradual approach should be taken, with optional privacy. People don't understand that bitcoin transactions are traceable.

</details>

**Oct 4, 2023**

{% embed url="https://www.youtube.com/watch?v=_ix0GCr6y90" %}

<details>

<summary>Key Points</summary>

• Aaluxx expresses happiness at the two-year effort and commends the community support. Liquidity has exceeded expectations, but the roadmap has been slower. Interest from other platforms to integrate Maya has been high, but competing priorities have slowed progress.

• Issues with Dash transactions getting stuck on Maya blockchain due to node validators not scanning Dash history and volatile gas prices led to a backlog of 50-60 stuck transactions. Version 107 is being released to remove all transactions from queue and manually pay back users. 21 unique addresses were affected, with 16 paid and 5 remaining.

• Thorchain developers have more resources and are breaking new ground, while Maya follows behind. Maya is seen as higher risk, higher reward, with developers improving over time, though long-term success remains uncertain.

• Aaluxx hopes community participation in their project will grow and improve alongside core team. Invites more involvement and need for more hands.

• Starting liquidity pools can be tough due to price volatility, so streaming swaps are an option to help.

• Maya differs from THORChain by not using inflationary tokens and having a different tokenomic structure. To enable more development and use cases, Maya plans to create a smart contract layer on a Cosmos-based chain using the same Cacao token. This would increase total value locked in Cacao and attract more developers, allowing for greater flexibility.

• AZTECChain is a smart contract layer built on Maya Protocol. It uses $CACAO token, providing stability, and supports native synths

• CosmWasm contracts are easier to develop than Solidity. Aztec doesn't need to be the best, just better than average. 90% of fees go to Maya node operators, 10% to Aztec token holders.

• $Maya token revenue sharing model proposed by someone in the Terra ecosystem as compensation for dev/investors. Initial challenge was how investors could exit without obligation to new owners. Solution was Maya tokens representing revenue sharing, with 10% of protocol fees distributed to token holders every 24hrs automatically.

• $MAYA model incentivizes long-term thinking over short-term profits, and provides alternative to ICOs. It also better aligns interests of speculators and users.

• Revenue sharing token approach will be adopted by more & more protocols in the future.&#x20;

• Maya's goal is to separate money from state.

• Regulators are targeting crypto projects, raising questions over whether tokens like XRP and Filecoin are securities. The revenue share model of Maya tokens, however, is not a concern as the team is based in Mexico and utility tokens were given away rather than sold. Some private sales have occurred but not via public ICO.

• It is believed that SEC would have no jurisdiction and attitudes may change in five years as crypto adoption increases. This is seen as beginning of a monetary revolution away from state control.

Maya seeks decentralization to protect against state threats. No user interfaces or customer-facing systems exist, eliminating legal action by having no central control/failure.

• Countries like El Salvador & Switzerland, and their cryptocurrency adoption, attract users leaving jurisdictions with increasing governmental control, and in a few years, we could witness a transfer of wealth.

• Maya team currently has authority and the community is rallied around them, this could change over time as more people get involved with differing opinions.

• The nodes have full governance rights similar to Thorchain.

• It's believed Maya has a sustainable future as something generating economic value through features like savings, lending, and integrating more blockchain networks.

• Maya's focus for the next year is bringing more features and innovations while expanding more safely and quickly, and provides links for people to learn more about and start using Maya.

</details>

**Sep 11, 2023**

What Is Maya Protocol: Cross-Chain Paradigm, Integrating Cardano!

{% embed url="https://www.youtube.com/watch?v=XyDJ6Yl4gQo&t=6s" %}

<details>

<summary>Key Points</summary>

* Maya Protocol, a cross-chain protocol, was ceated to simplify crypto trading. This was motivated by their dissatisfaction and limited crypto options in their native Mexico. In 2021, Aaluxx joined THORChain as a liquidity provider and began working to solve this issue, eventually resulting in Maya Protocol.
* Maya Protocol, launched two years ago, seeks to be an aggregator and arbitrager between blockchain networks. Named after Latin pride, Cacao tokens were distributed with no inflation. Development and launch were completed in 18 months, and currently supports Bitcoin, Ethereum, and stablecoins. Future plans include supporting more coins and networks.&#x20;
* Maya Protocol enables cross-chain trading of cryptocurrencies, providing easy access to different blockchain ecosystems without needing centralized exchanges or complicated transactions. Thus, it furthers the goal of trustless, peer-to-peer interactions across various blockchains.
* DEXs such as Uniswap and Curve are becoming more popular due to lower costs & simpler trading of ERC-20 tokens. It is expected that the next bull run will strengthen this trend with platforms like Maya providing improved user experiences, and more chains.
* Governance is enabled by 17 validator nodes, so far, and can go up to 120, which are permissionless and anonymous, posting collateral to run. TSS signatures secure the vaults. Mimir allows for quick response to issues, approved by validators and transparently recorded; intended to phase out as team and validators mature. Team is incentivized by network fee percentage.&#x20;
* Maya's cross-chain asset transfer system allows users to swap assets from one chain to another. Transactions require two-thirds of nodes to sign, incentivized by a bond that can be slashed for improper behavior.&#x20;
* Maya, though a separate team from THORChain, considers them friendly forks, aiming to add to the THORChain ecosystem.
* Maya & THORChain collaborate to enable one-click asset transfers. They pay for bug bounties, provide ideas for implementations, & advise on security. Their efforts aim to decentralize exchanges & move away from centralized entities. Maya has current infrastructure & dAPP integrations & dedicates resources to expand them. It is a "one plus one equals three" situation.
* Maya is integrating with different blockchain ecosystems and platforms, with progress made in three categories: wallet connections like Ledger, existing third-party chains, and ecosystem-specific integrations. Integration with Cardano, one of the top 5 ecosystems, is planned, with the opportunity to connect several large ecosystems.
* Maya Protocol is integrating with Cardano blockchain, which is complex and requires extra work. Submitted Catalyst funding proposal for $600,000 to support pool and reduce slippage. Looking to launch by year-end.
* Hopes to have aggregations and integrations with Polkadot, Ledger, Trust Wallet, Cardano & other chains in a year's time. Features like savers, streaming swaps, memoless txs also planned. Roadmap for next year achievable to be ready for bull run.&#x20;

</details>

**July 1, 2023**

Dash Podcast 206 with Aaluxx: Dash Activation on the Maya Protocol

{% embed url="https://www.youtube.com/watch?v=FjPOam9Ray4" %}
Dash Podcast 206 with Aaluxx: Dash Activation on the Maya Protocol
{% endembed %}

<details>

<summary>Key Points</summary>

* Dash integration into Maya is on StageNet, with finalization ongoing. Release of the final version is imminent.
* Dash Team has been making modifications to the Dash network, including chain locks & instant send, to boost security & liquidity.
* Dash is unique in that it is focused on being a payment system. In addition, Dash Evolution and GroveDB are significant improvements to the chain that will enable new layers and use cases.
* Institutions need to be more open and inclusive to crypto, otherwise they risk driving away innovation.
* MAYAChain is a decentralized exchange that uses a Threshold Signature Scheme to ensure that only authorized users can access funds.
* MAYAChain doesn't use or need oracle, market forces and arbitrage opportunities make sure the prices are accurate.
* MAYANodes protect the network using economic security.
* Similarly Masternodes secure Dash, incentivized to maintain honest behavior, or risk losing their investment.
* MAYAChain is a friendly fork of THORChain. THORChain's code is battle tested, with past setbacks making it stronger.
* THORChain and MAYAChain employ a unique approach to governance that is consensus-based and verifiable, with emphasis on decentralization.
* THORChain and MAYAChain have both employed several safeguards such as solvency tracker, and chain halts to enhance chain security.
* Dash integration to MAYAChain will make Dash available to trade on mulitple interfaces.
* THORWallet is making strides beyond just being a swapping interface.
* To maximize Dash's success on MAYAChain, the Dash community must&#x20;
  * utilize the decentralized exchange,&#x20;
  * offer liquidity to reduce costs of swaps,&#x20;
  * and request the feature (decentralized swaps) from wallets and interfaces.

</details>

**June 20, 2023:**

Update on Dash-Maya Integration | Incubator WEEKLY

{% embed url="https://www.youtube.com/watch?v=eWFV3-ytN8o" %}
Update on Dash-Maya Integration | Incubator WEEKLY
{% endembed %}

<details>

<summary>Key Points</summary>

* Despite successful launch and liquidity, maintaining network stability was difficult due to ETH Shanghai update, BTC & BRC-20 mempool failure, and Thorchain updates. Striking a balance between keeping the chain running and adding new integrations was challenging. Previous versions focused on stability; v105 introduces Dash integration, and Liquidity Nodes update.
* Dash is currently in Stagenet, undergoing comprehensive testing before Mainnet launch.
* v105 will witness MAYAChain transition from Genesis to Liquidity Nodes. GN and LN will run simultaneously until economic security is achieved, then GN will be retired with time.
* Maya Protocol seeks to integrate with a variety of wallets, enabling users to access Maya without having to be brought to it. As decentralization values choice, multiple front ends with varying fee structures are available.
* The benefits of THORChain & MAYAChain being similar yet distinct offer the opportunity to assess different ideas, features, and implementations to determine the optimal solution. Example, some of MAYAChain's propositions regarding lending have been incorporated into THORChain, of which we are proud. We are optimistic about THORChain's upcoming lending feature and look forward to its launch to evaluate its suitability for MAYAChain.
* We are integrating Dash, Kujira, Arbitrum and Cardano, and a smart contract layer, with swaps and volume being our primary focus. Dash is on Stage Net with no major issues, and we are still testing before releasing. We have confidence but must consider real-life parameters.
* MAYAChain seeks Dash community support during the current CEX and crypto restrictions, including offering swap volume, liquidity, and promotion. Aaluxx invites the Dash community to make their requests heard.
* Running a Liquidity Node on MAYAChain is estimated to cost $100k.
* The Dash Incubator team will develop a product that will utilize Dash CoinJoin feature combined with Maya’s cross-chain swaps. This allows private and anonymous transactions between BTC, ETH, and other assets supported by Maya Protocol.

</details>

**March 13, 2023:**&#x20;

Maya Protocol - Non-custodial Cross-chain swaps on Kujira

{% embed url="https://www.youtube.com/watch?v=vDGwNS4fZZw" %}
"Maya Protocol - Non-custodial Cross-chain swaps on Kujira" by Just Crypto
{% endembed %}

**March 6, 2023**

Will DASH Plus MAYA Equal DEX Success? | Incubator WEEKLY

{% embed url="https://www.youtube.com/watch?v=PYZzfBQhfXI" %}
"Will DASH Plus MAYA Equal DEX Success? | Incubator WEEKLY" by Dash Incubator
{% endembed %}

**March 3, 2023**

Halborn Flash Videos with Aaluxx Myth of Maya Protocol

{% embed url="https://www.youtube.com/watch?v=TPFPMlG8uvw" %}
"Halborn Flash Videos with Aaluxx Myth of Maya Protocol" by _Halborn Security_&#x20;
{% endembed %}

**February 27, 2023**

Maya Protocol Launch - Interview With AaluxxMyth About The Launch And Long-Term Plans

{% embed url="https://www.youtube.com/watch?v=AJbU2fP7BEg" %}
"Maya Protocol Launch - Interview With AaluxxMyth About The Launch And Long-Term Plans" _by Kyle Krason_
{% endembed %}

**February 21, 2023**

Citizen Cosmos: A citizen odyssey, ep. XVI, special mission - Maya Protocol

{% embed url="https://www.youtube.com/watch?v=buYzwPmynOA" %}
"Citizen Cosmos: A citizen odyssey, ep. XVI, special mission - Maya Protocol" by Cosmos Citizen
{% endembed %}

**February 8, 2023**

An Intro To... Maya Protocol!

{% embed url="https://www.youtube.com/watch?v=hKs52EB6N9M" %}
"An Intro To... Maya Protocol!" by _Kuji Kast_
{% endembed %}

**January 18, 2023**

Aaluxx of the Maya Protocol on Expanding THORChain's Vision of Cross-Chain Swaps and Liquidity

{% embed url="https://www.youtube.com/watch?v=grLlQo5JdyY" %}
"Aaluxx of the Maya Protocol on Expanding THORChain's Vision of Cross-Chain Swaps and Liquidity" _by Digital Cash Network_
{% endembed %}

**October 13, 2022**

What is Maya Protocol?

{% embed url="https://www.youtube.com/watch?v=khpcqM2mLO0" %}
**"**&#x57;hat is Maya Protocol?" _by Not Another Crypto Show_
{% endembed %}

**December 19th, 2022**

CROSS CHAIN CRYPTO EXPLOSION!? With Maya Protocol

{% embed url="https://www.youtube.com/watch?v=2tG7l4UUUt8" %}
"CROSS CHAIN CRYPTO EXPLOSION!? With Maya Protocol" _by Degens for a Brighter Future_
{% endembed %}
