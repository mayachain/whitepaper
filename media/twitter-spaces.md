# 🐦 Twitter Spaces

## **Mondays with Maya**

**#102 Jan 27, 2025**\
\
**Title: Asgardex and Maya Protocol updates.**\
\
[🔗 Listen here](https://x.com/Maya_Protocol/status/1884013590648443121)

<details>

<summary>Key Points </summary>

#### **MWM #102 Key Points**

**For Docs**

**Special Guest:**

* We had the pleasure of hosting the **Asgardex team** in this episode!

**About Asgardex:**

* [Asgardex](https://www.asgardex.com) has launched a **brand-new website** with a sleek design and improved user experience—be sure to check it out!&#x20;
* It's one of the few tools that makes sending **custom memos** and **custom deposits** easy, particularly useful for node operators.
* Our partnership with Maya Protocol opens access to **different chains and tokens** like **Radix** and **Dash**, providing new opportunities for users.\

* **What’s Next for Asgardex and Maya Protocol?**
* Enhancing **node operations** for better efficiency.
* Integrating **Zcash** and **Cardano** with Maya Protocol.
* Supporting more of Maya Protocol’s advanced features.\

* **What’s Coming Next for Asgardex?**
* **Chainflip integration**: Expanding cross-chain capabilities.
* **Vultisig integration**: Boosting security and asset management.
* **Secure assets.**

**Maya Protocol Updates:**

* **Zcash integration**: Smoke testing begins in just a few days.
* **Cardano integration**: Testing is further out due to ECDSA development.
* **Version 115**: Improvements, especially for affiliate features.
* Second server: Itzamna will be installing it this week, boosting reliability and performance.

</details>

**#101 Jan 20, 2025**\
\
**Title: Aaluxx and Itzamna 1-on-1 Recap**\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1881360262147821589)

<details>

<summary>Key Points </summary>

**ECC Summit with Zcash Team:**

* **Warm welcome:** Zcash is one of the  most welcoming and supportive community we've encountered for an integration. Their team has been very supportive and collaborative in  every step of the way.
* **Maya Protocol spotlight:** Aaluxx and Itzamna presented Maya Protocol, explained how it works, and outlined the integration process. Excitement is high, and we're working hard to finalize this integration ASAP.
* **Liquidity and roadmap:** A roadmap was created for the integration, starting with basic support for Zcash and gradually adding more layers until full support is achieved.&#x20;

**Technical Updates on Zcash Integration:**

* **Tackling the Challenge:** Zcash’s technical nature brings its challenges, especially with shielded transactions. Our team is fully focused on developing these features to ensure top-notch privacy and functionality.
* **Developer allocation:** Two developers from 13Heavens are actively working on Zcash integration alongside our team.

**Other Key Technical Progress:**

1. **Affiliate features:** These are being developed in parallel with Zcash efforts.
2. **Node expansion:** 13Heavens is expected to onboard a significant number of nodes soon which will boost the network’s security and scalability.
3. **Asgardex sharding:** Major strides here will boost network security and reliability.
4. **Cardano integration:** Two developers are focused on this integration, ensuring code aligns with historical updates and future plans. Data is being handled via a local database in Bifrost for querying and transaction processing.

**Pipeline Evolution:**

* **Smoke tests:** Efforts are ongoing to reduce test duration.
* **Assimilation tests:** Modeled after Thorchain's process.
* **Sync tests:** These ensure new versions function seamlessly with previous ones, a time-intensive but essential step.
* **Launcher:** This tool will allow us to manage nodes efficiently and eventually run Thorchain and Maya nodes symbiotically.



</details>

**#100 Jan 13, 2025**\
\
**Title: MWM #100 Recap: Celebrating a hundred episodes!** \
\
🔗 [Listen here ](https://x.com/Maya_Protocol/status/1878819388213895294)

<details>

<summary>Key Points </summary>

For our 100th episode of Maya Weekly Meeting (MWM), we welcomed two special guests: Omecayan, Head of 13 Heavens, and Crypto XZ from THORWallet, our launch partner. Here’s a full breakdown of the highlights:

**Maya Protocol Updates:**

* **Zcash & Cardano Integration**: Our dev team is making significant progress toward integrating these networks.
* **Zcash Conference**: Aaluxx and Itzamna attended as guests to  the event, they will be part of a talk and will participate in a workshop. They’re thrilled to build closer connections with the Zcash community and team.
* **TokenPocket Integration**: Coming soon.

**Reflecting on 100 Episodes:**

* We’re incredibly grateful to the tribe for making this milestone possible. Weekly MWMs provide an open platform for communication, encouraging community participation beyond our Discord platform everybody is welcome to join the conversation.
* From a small, team to a growing network supported by 13 Heavens with Omecayan at the front (born as an inspiration from 9 Realms and THORChain) Maya Protocol continues to evolve with a bigger and stronger team.

**Omecayan’s Vision for the Future:**

* **Aztec Expansion**: Excited to bring a new product that is in development to position Aztec as a core value generator for the network.
* **Cacao Staking Pool**: A design is in the works for a staking pool that bonds cacao without impermanent loss, boosting network robustness.
* **Basket of Stablecoins**: inspired in our whitepaper and roadmap, the implementation is under review, but this be a key ecosystem feature.
* **Node Access for the Community**: 13 Heavens is now running its own nodes and will soon open access to the tribe.

Omecayan also called on the community to spread the word, promote the brand, and actively contribute to Maya’s journey.

**Discussing Savers:**

* Savers, while a fantastic product to bring more users into the ecosystem,  but it requires careful planning and significant design changes. It’s not a current priority but remains under consideration for the future.

**THORWallet Highlights:**

* Crypto XZ shared updates on the **Titan Token**, which offers exciting benefits like 17% cashback and enhanced rewards for users. Stay tuned for their launch!

**Celebrating Our Achievements:**

* We’ve grown from $162k to over $1 million in volume in less than two years—a 100x increase! While our goal is reaching $22 billion, we’re focused on consistent volume, ensuring sustainable value for liquidity providers, Maya token holders, and the ecosystem.

Thank you, tribe, for your incredible support. Here’s to the next 100 episodes and beyond! 🚀

</details>

**#99 Jan 6, 2025**&#x20;

**Title: Maya Updates & El Dorito**\
\
🔗[ Listen here ](https://x.com/i/spaces/1OwxWNzrXkwJQ)

<details>

<summary>Key Points </summary>



**Special Guest: El Dorito**

* **Rebrand**: El Dorito shared their rebrand aimed at being more fun, unique, and relatable to their community.
* **Platform Vision**: They're building a platform designed for everyday use, allowing users to:
  * Swap assets.
  * Manage their portfolio.
  * Earn yield—all in a self-custody manner.
* **Token Launch**:
  * Feeless swaps available if you hold a specific amount of their token.
  * Focused on reducing user friction, making DeFi more accessible.
* **Infrastructure Development**:
  * Revamping their UI/UX to deliver a standout experience.
  * Building their own router and SDK to create infrastructure for other apps and backends.
* **Upcoming Integrations**:
  * Ready to support **Cardano** and **Zcash** when Maya goes live with the integration.

***

**Maya Protocol Updates**

* **Ecosystem Health**:
  * Volume is back to healthy levels, and everything is operating smoothly.
  * The ThorWallet refund issue has been resolved.
* **Development Progress**:
  * Slow but steady progress on the Maya node, with clarity on version 118.
  * Teams working in parallel on **Zcash** and **Cardano** integrations.
  * Wallet integrations are on the horizon.
* **Alpha Alert**:
  * **Token Pocket** feature coming soon to Maya Protocol!

</details>

**#98 Dec 30, 2024**\
\
**Title:  V115 and future plans** \
\
🔗 [Listen here ](https://x.com/i/spaces/1ynJODBkvykxR)

<details>

<summary>Key Points </summary>



* **Version 115 Released:**
  * Version 115 launched last Monday with updates required for Thorchain.
  * Thorchain has been successfully unhalted, and stability has been restored.
* **XRD Unhalted:**
  * XRD is now operational, with good stability across the platform.
  * While everything is running smoothly, the team remains ready to make adjustments if needed.
* **ETH Halt and Upcoming Version 116:**
  * Following an issue reported by ThorWallet on a large swap, ETH was halted last Friday as a precautionary measure.
  * Our team fully supports the node's decision and is finalizing version 116 to unhalt ETH.
  * Version 116 is expected to roll out tomorrow.
* **Future Integrations:**
  * Development for Cardano and Zcash integrations is underway, with promising progress.
  * We are thrilled to participate in the upcoming Zcash Convention this January. Aluxx and Itzmana will represent the team and engage with the Zcash community.
* **Node Churn Progress:**
  * A new node churn occurred after a while, and we’re working towards having consistent churns every 7-8 days moving forward.

</details>

\
**#97 Dec 23, 2024**\
\
**Title:** KeepKey’s Innovations & Maya Protocol Updates\
\
🔗 [Listen here ](https://x.com/Maya_Protocol/status/1871211715662151704)

<details>

<summary>Key Points </summary>

Mondays with Maya #97 brought exciting developments from **KeepKey**, a hardware wallet championing privacy and innovation, as well as key updates on Maya Protocol’s progress. Here's a breakdown of what you need to know.\
\
**KeepKey Highlights**

* **Privacy-Centric Design:** KeepKey ensures seamless **multi-chain asset management** without requiring KYC.
* **Open Source Commitment:** Their entire stack is open source, fostering transparency and community-driven development.
* **Thorchain Ecosystem Integration:** Designed to connect assets across Thorchain, KeepKey is enhancing the decentralized finance (DeFi) experience.
* **Browser Extension:**
  * A new **browser extension** simplifies integrations.
  * Minimal dependencies required—use the **window function** to enable transfers, swaps, and other multi-chain actions.
  * This tool reduces friction for developers and expands functionality for users.

👉 **Take Action:**

*   Explore the **KeepKey browser extension** and provide feedback—it’s essential as they prepare for the big **announcement in January**\
    \


    **Maya Protocol Updates**

    * **Version 115:**
      * The next release is designed to **align with the Thorchain hard fork**.
      * Smoke and synth tests were completed; the team is now conducting staging tests.
      * Minor issues with messaging arose, and updates will follow via Discord.
    * **Radix Integration:**
      * Stuck transactions have been resolved, but integration is temporarily **halted until version 115.1** to address node synchronization issues.
      * The **Radix team** is actively collaborating on the fix.
    * **Future Integrations:**
      * The Maya team is actively working on **new integrations**, paving the way for broader adoption and enhanced functionality



\




</details>

\
**#96 Dec 16, 2024**\
\
**Title:** Ctrl Wallet and Maya Protocol updates.\
\
🔗[Listen Here](https://x.com/i/spaces/1dRJZdLXMzgKB)

<details>

<summary>Key Points </summary>



Welcome to another **Mondays with Maya**, where we bring you the latest from the Maya Protocol and beyond! Today’s session was packed with excitement:

* A special guest appearance by **Ctrl Wallet**, revealing their bold rebrand and innovative features.
* Key updates from Maya Protocol, including **bug fixes**, upcoming **version 1.15**, and exciting **new integrations**.
* Plus, reminders about our ongoing **Advent Calendar** challenges!\


**Special Guest Spotlight: Ctrl Wallet**

* **Bold New Look**: Ctrl Wallet unveiled their rebranded identity with a sleek, innovative design that focuses on boldness and user experience.
* **Multi-Chain Extension**: The **first-ever multi-chain wallet extension**, designed for seamless navigation, allows users to see their total crypto balance and its breakdown by wallet and account—empowering them with a clear understanding of their holdings.
* **Custody Equals Freedom**: Ctrl Wallet champions the idea that true user freedom lies in custody, redefining the DeFi wallet experience.
* **Comprehensive Features**: Tribe members can use Ctrl Wallet to **send, receive**, and connect with other interfaces that support Maya Protocol.
* **Future Innovations**: Exciting features are planned for 2025, hinting at even more dynamic functionality.
* **Action Step**: Explore the **Ctrl extension** and their **mobile app** on iOS and Android to experience the future of wallet management.

**Maya Updates: Key Highlights**

* **Cacao Price Fix on CoinGecko**: A bug affecting the Cacao price display on CoinGecko has been resolved. Maya apologizes for any inconvenience caused.
* **Version 1.15 in the Works**: The next major release will include:
  * Updates to align with **Thorchain’s v3 hard fork** (Rune will remain halted until further notice).
  * **Refunds** as part of the version’s rollout.\

* **New integrations underway: Zcash and Cardano** integrations are progressing, with great advancements being made.
*   The **Advent Calendar Dynamic** runs until **December 24th**.

    Challenges and riddles will **increase in difficulty** each week, offering **even better prizes** for the tribe’s participation.



    \


</details>

\
**#95 Dec 9th, 2024**\
\
**Title:**  Maya Protocol and Junction\
\
🔗[Listen Here](https://x.com/Maya_Protocol/status/1866135842240253965)

<details>

<summary>Key Points </summary>

Today at MWM, we were happy to host Junction as a special guest to discuss our  upcoming integration. Junction was created to address the need for a unified DeFi platform, offering comprehensive on-chain access to all DeFi tools in one place. Their mission empowers users by giving them full control

* **Junction Highlights:**
  * **Unified DeFi Access:** Junction’s API supports seamless cross-chain interactions across 100+ blockchains, simplifying multi-chain transactions.
  * **Enhanced User Experience:** Features like gas fee abstraction and an intuitive UI make DeFi more accessible and engaging.
  * **Maya Integration Benefits:** Integrating with Maya Protocol enables native swaps in other ecosystems that nobody else support, expanding asset accessibility and improving transaction efficiency.
  * **Future Developments:** Junction will launch with all chains supported by Maya and continue adding new ones we integrated.
  * Visit Junction’s website and subscribe to thei[r newsletter](https://junction.exchange) to stay updated on the latest developments. &#x20;
* **Maya Protocol Updates:**
  * Progress on integrating Zcash and Cardano is underway—stay tuned for updates.
  * Node synchronization issues from V114 are being resolved, with enhanced redundancies in development.
  * We are very excited for 2025 with plans for new products, increased volume, and broader ecosystem support.\


\
\
\




</details>

\
**#94 Dec 2nd, 2024**\
\
**Title:** V112 and V113\
\
🔗[Listen Here](https://x.com/i/spaces/1OyKAZpqdkbGb)

<details>

<summary>Key Points</summary>

* **Version 1.12**
*

    * &#x20;**XRD Bonding**: XRD is now bondable.
    * **Queue Optimizations**: Adjustments in Ethereum and Arbitrum Bifrost to prevent the queue from getting stuck.
    * **Zcash Refund Addresses**: Refund addresses will be required we are paving the way for our Zcash integration.
    * **Fee Collector Module**
    * **Multiple affiliates** perfect for upcoming integrations and UIS
    * **Clear Pending Liquidity**: Implements Thorchain’s solution to manage incomplete asymmetrical liquidity adds.
    * **Whitelisted several DEX agregation**: l Radix tokens, refund vaults, and smart contracts.


*   **Version 1.13**

    * **Fixes**: Bug fixes for Kujira and Arbitrum functionality.
    * &#x20;**XRD Testing**: Continued testing of Radix functionality.
    * **New Integrations**: InstaSwap and Ctrl Wallet are now integrated go and check them out.


*   **What’s Next?**

    &#x20;**Zcash Integration**:

    * We're actively working with the side by side with Zcash team to have this integration as soon as possible.

    Stay tuned for more exciting developments!&#x20;

</details>

**#93 Nov 25, 2025**\
\
**Title:** Cross-Chain Milestones: Dash, InLeo, and Maya Protocol.\
\
🔗[Listen Here: ](https://x.com/i/spaces/1gqxvNXARmexB)

<details>

<summary>Key Points</summary>

*   #### **Building Bridges: Dash, InLeo, and Maya Protocol in Action**

    This week on **Mondays with Maya**, we had the pleasure of hosting **special guests from Dash and InLeo** for an insightful conversation about their projects and how they connect with Maya Protocol.\


    From the game-changing **Dash Evolution upgrade** to **InLeo’s decentralized platform for content creators**, and Maya Protocol’s role as the bridge between ecosystems, this session was all about milestones, innovation, and collaboration.

    Here’s a quick recap: \


    #### **Dash Evolution: A Decade of Progress**

    * **9 Years in the Making:** Dash’s Evolution upgrade launched in July 2024 and was activated in September, marking a significant leap forward in usability and functionality.
    * **Breaking Records:** Over **1,200 transactions in a single day**, pushing Dash closer to becoming a real-world, data-focused blockchain.
    * **Next-Gen Wallet Features:**
      * Register unique usernames for simplified payments.
      * Privacy-focused design conceals actual addresses for secure, discreet transactions.
      * A new dash wallet makes private, global transactions user-friendly.

    #### **InLeo: Decentralized Content for Creators**

    * **Blockchain-Powered Creator Platform:**
      * Combines microblogging, long-form posts, videos, and premium content creation.
      * Built on the Hive blockchain, ensuring user ownership and decentralized storage of all content.
    * **Own Your Data:**
      * All content is secured with private keys—no intermediaries involved.
    * **Dual Revenue Streams:**
      * Earn Hive dollars and Leo tokens through upvotes from the rewards pool.
      * Instant payouts reward both creators and curators.

    #### **Dash x InLeo x Maya: Seamless Integration**

    * **Dash Meets Hive Ecosystem:**
      * Dash users log into InLeo with their wallets and gain access to Hive’s creator-driven ecosystem.
      * Activities on the platform automatically earn Hive dollars and Leo tokens.
    * **Effortless Swapping via Maya Protocol:**
      * Hive and Leo tokens are auto-swapped into Dash, enabling seamless cross-chain transactions.
    * **Expanding Utility:**
      * Dash users explore Hive’s decentralized content economy.
      * Hive users gain opportunities to transact using Dash’s private digital cash.

    ***

    #### **Why It Matters**

    * **Cross-Chain Interoperability:** Maya Protocol bridges ecosystems, ensuring low fees, seamless swaps, and enhanced utility for tokens.
    * **Real-World Impact:**
      * Dash empowers sovereign digital cash for everyday use.
      * InLeo redefines decentralized content creation and monetization.
    * **Enhanced Ecosystem Benefits:**
      * Users enjoy greater token utility and opportunities across Dash, Hive, and Maya.

    **Learn More:**\
    Catch the full discussion on **Mondays with Maya**, featuring Dash, InLeo, and Maya Protocol

    \
    \


</details>

**#92 Nov 18, 2024**\
\
**Title:** Mondays with Maya x Vultisig\
\
[🔗 Listen here](https://x.com/i/spaces/1gqGvNpkXowGB)

<details>

<summary>Key Points </summary>

*

    This week’s **Mondays with Maya** featured a special guest, **Vultisig**, the next standard for crypto wallets. In this session, we explored how Vultisig is redefining wallet security and simplicity, its ambitious roadmap, and its partnership with Maya Protocol. We also shared important updates on Maya’s progress.



    #### **Vultisig Highlights:**

    * **Enhanced Security**:
      * Two-factor authentication (2FA) using two devices (Fast Vaults).
      * Public key exposure only, with no seed phrases to steal.
    * **Versatile Use Cases**:
      * Replace MetaMask for daily users.
      * Replace Ledger for secure cold storage.
      * Replace institutional services with a scalable, robust solution.
    * **Comprehensive Product Suite**:
      * iOS/Android: VultiSigner & VultiWallet.
      * Windows/Mac: Desktop wallet support.
      * VultiConnect: Seamless integration with apps and DEXs.
    * **Token & Airdrop Updates**:
      * Token not launched yet, but airdrop details for early adopters are coming soon.
      * Stay updated via Vultisig’s official channels.
    * **Maya x Vultisig Partnership**:
      * Enabling secure cross-chain transactions.
      * Thorwallet rebranding to VultiWallet is just the beginning they are bringing full DeFi functionalities like LPs and Savers.
      * Exciting new features like plugins are in development.

    #### **Maya Protocol Updates:**

    * **Airdrop Countdown**:
      * Only **11 churns** left before the airdrop deadline—get ready!
    * **Version 1.12 Updates**:
      * **StageNet testing ongoing** with pools for LPs, custom refund addresses (useful for Zcash), an affiliate collector module, and pending liquidity features.
      * Significant improvements for **Arbitrum** to enhance user experience.
      * **Churns remain halted** until version 1.12 is finalized, ensuring a smoother rollout.



</details>

**#91: Nov 11, 2024**

**Title:** Exploring the Future of DeFi with Edge Wallet and Maya Protocol\
\
🔗 [Listen here](https://x.com/i/spaces/1BRKjwLjvjwGw)

<details>

<summary>Key Points </summary>

* We welcomed **Edge Wallet** for an in-depth conversation on their recent integration with Maya Protocol. Paul, from Edge Wallet, shared the evolution of the project since its start in 2014, highlighting a 2018 revamp that introduced new features like buy, sell, and trade options.
* Edge Wallet stands out for its **privacy-preserving** approach and a **user-friendly interface** designed to feel as accessible as a centralized exchange while maintaining decentralized security and **key management**.
*   **What's Next for Maya and Edge**\
    The discussion covered exciting upcoming features, including **Savers** and a highly anticipated **Zcash integration**. Paul and Aaluxx shared insights on the future of DeFi.



</details>

**#90: Nov 4, 2024**\
\
**Title:** Reactivation & Validator Success\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1850917381981544672)

<details>

<summary>Key Points</summary>

* Trading is back! Join us for an update on the successful churn and validator updates. Find out what’s in store for our next steps and roadmap.&#x20;

</details>

**#89: Oct 28, 2024**\
\
**Title:** Trading Halted & Milestone Records\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1845842989194584285)

<details>

<summary>Key Points</summary>

* Trading is temporarily paused for validator maintenance, but we’re celebrating huge milestones: $200 million monthly and $8 million daily records. Thanks to all of you for being a part of this!

</details>

**#88: Oct 14, 2024**\
\
**Title:  Dive into Maya Names**\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1842986931962933310)

<details>

<summary>Key Points</summary>

* We’re on the verge of breaking our highest volume record! Plus dive into Maya names.

</details>

**#87: Oct 07, 2024**\
\
**Title:** New Volume Record & Node Testing\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1840595482139066720)

<details>

<summary>Key Points</summary>

* This week, we hit a new volume record in just three days! Learn about our testing and improvements for enhanced operations.

</details>

**#86: Sep 29, 2024**\
\
**Title:** Tier 1 Airdrop Verification & Team Growing\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1835695217971712278)

<details>

<summary>Key Points</summary>

* Tier 1 list verification is now available on Discord! We’re also expanding our team—apply now and join our tribe.

</details>

**#85: Sep 16, 2024**\
\
**Title:** Version 112 Final Testing \
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1833143392957767881)

<details>

<summary>Key Points</summary>

* We’re polishing the final details for version 1.12! With the final merge in sight, learn about the last steps before launch.

</details>

**#84: Sep 9, 2024**\
\
**Title:** Astrolecent Partnership\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1830434380893868237)

<details>

<summary>Key Points</summary>

* We welcomed our guest from Astrolecent to discuss the project, its roadmap, and our upcoming integration. Get to know what's in store!

</details>



**#83: Sep 1, 2024**\
\
**Title:** Snapshot & Version 1.12 Updates\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1828085087050867088)

<details>

<summary>Key Points</summary>

* The Tier 1 snapshot has been paid out! Plus, we’re working on version 1.12 and introducing the Cross-Chain Crusaders. Find out what’s happening on Discord and Telegram.

</details>

**#82: Aug 05, 2024**\
\
**Title:** CacaoSwap & Radix Guests\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1827739019976860089)

<details>

<summary>Key Points</summary>

* In this edition of _Mondays with Maya_, we’re joined by CacaoSwap and Radix! Learn more about the projects, milestones, and exciting upcoming integrations.

</details>

**#81: Jul 29, 2024**\
\
**Title:** Radix Goes Live\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1825540515443744882)

<details>

<summary>Key Points</summary>

* Exciting news! Radix is live, and our team is seeding the pool. Learn about this new integration and its potential as we dive into how Radix supports additional tokens.

</details>

**#80: Jul 22, 2024**\
\
**Title:** Post-Rare Evo Updates & XRD\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1822860216628601041)

<details>

<summary>Key Points</summary>

* Back from Rare Evo with all the details! Get the scoop on our latest updates and everything XRD. Dive into the highlights from the convention and find out what's next.

</details>

**#79: Aug 05, 2024**\
\
**Title:** Version 1.11 Preview & New Partnerships\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1820475017513316862)

<details>

<summary>Key Points</summary>

* Sneak peek at version at version 111

</details>

**#78: Jul 29, 2024**\
\
**Title:** New Integrations & Project Goals\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1817632805947375937)

<details>

<summary>Key Points</summary>

* We share updates on our latest integrations and discuss our roadmap for the year ahead.

</details>

**#77: Jul 22, 2024**\
\
**Title:** Project Roadmap & Tokenomics Discussion\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1815043304347799604)

<details>

<summary>Key Points </summary>

* We explore our roadmap for the rest of the year and discuss the project’s unique tokenomics.

</details>

**#76: Jul 15, 2024**\
\
**Title:** Mainnet Testing & New Bondable Assets\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1812891926808682541)

<details>

<summary>Key Points</summary>

* Streaming swaps are tested on mainnet, with new bondable assets on the way.

</details>

**#75: Jul 08, 2024**\
\
**Title:** Last Testing for Streaming Swaps\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1810019292152307964)

<details>

<summary>Key Points</summary>

* Version 1.10 is here! Get the latest on final tests for streaming swaps.

</details>

**#74: Jul 01, 2024**\
\
**Title:** Finalizing v1.10\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1807527333831520444)

<details>

<summary>Key Points</summary>

* We’re close to the finish line! Testing and polishing continue as v1.10 nears completion.

</details>

**#73: Jun 24, 2024**\
\
**Title:** Streaming Swap Testing & New Integrations\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1805260867627348045)

<details>

<summary>Key Points </summary>

* The streaming swap code is done! We’re conducting over 200 tests and making progress on Zcash, Radix, and Cardano integrations. Plus, El Dorado joins to discuss their project.

</details>

**#72: Jun 17, 2024**\
\
**Title:** Roadmap Growth & Streaming Swaps Strategy\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1802501699472396654)

<details>

<summary>Key Points</summary>

* We dive into upcoming integrations and share how our approach to streaming swaps stands out from Thorchain’s.

</details>

**#71: Jun 10, 2024**\
\
**Title:** Validator Node Churn & Streaming Swap Update\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1799510296526249994)

<details>

<summary>Key Points </summary>

* Exciting progress as our second Thorchain validator node churns, plus news on streaming swaps.

</details>

**#70: Jun 03, 2024**\
\
**Title:** Team Growth & Streaming Swaps Progress\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1797644436861759558)

<details>

<summary>Key Points</summary>

* New team members join as we advance streaming swaps and prepare for Radix integration.

</details>

**#69: May 27, 2024**\
\
**Title:** XDEFI Integration & Project Roadmap\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1794738832497328469)

<details>

<summary>Key Points</summary>

* Guests from XDEFI join to share details about their project, achievements, and our upcoming integration.

</details>

**#68: May 20, 2024**\
\
**Title:** Cardano Progress & Bifrost Setup\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1792571241053991006)

<details>

<summary>Key Points </summary>

* Get the latest on Cardano’s Node Launcher and the Bifrost setup, plus updates on the push for the Catalyst grant.

</details>

**#67: May 13, 2024**\
\
**Title:** Rango Exchange Integration & Airdrop Update\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1790034407182434657)

<details>

<summary>Key Points</summary>

* Rango Exchange guests discuss their project and roadmap. Plus, the Tier 1 airdrop payout and eligible node and bond provider list are now available on Discord.

</details>

**#66: May 06, 2024**\
\
**Title:** ThorSwap Launch & Streaming Swaps Sneak Peek\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1787499289184195044)

<details>

<summary>Key Points</summary>

* We celebrate our smooth integration with ThorSwap, updates on streaming swaps, and share progress on Radix, Zcash, and other exciting updates.

</details>

**#65: Apr 29, 2024**\
\
**Title:** What’s Next for ThorSwap & v1.10 Updates\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1784960979371004228)

<details>

<summary>Key Points</summary>

* ThorSwap guests cover their latest features, including Swap Kit, and we share progress on v1.10.

</details>

**#64: Apr 22, 2024**\
\
**Title:** Exploring Zcash Integration\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1781998578300227943)

<details>

<summary>Key Points</summary>

* A special episode with Zcash, discussing the upcoming integration, its importance, and what to expect.

</details>

**#63: Apr 15, 2024**\
\
**Title:** Arbitrum Live & New Pools on MayaScan\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1779629114753728954)

<details>

<summary>Key Points </summary>

* Arbitrum integration is now live! Explore 16 new pools available on MayaScan.

</details>

**#62: Apr 08, 2024**\
\
**Title:** The Future of DeFi with JP Thor & Chad B\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1777350830435307923)

<details>

<summary>Key Points </summary>

* In one of our biggest Spaces yet, JP Thor and Chad B discuss the future of DeFi, diving deep into Thorchain, Maya, and the evolution of multi-chain.

</details>

**#61: Apr 01, 2024**\
\
**Title:** Turbo Trade Integration & KeepKey News\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1774844291011027455)

<details>

<summary>Key Points</summary>

* Learn about our new integration with Turbo Trade, plus exciting news about MayaChain now live on KeepKey.

</details>

**#60: Mar 25, 2024**\
\
**Title:** Node Launcher & Arbitrum Sync Update\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1772277367982997789)

<details>

<summary>Key Points</summary>

* The Node Launcher is live, and node synchronization is underway. Catch the latest on Arbitrum progress and more.

</details>

**#59: Mar 18, 2024**\
\
**Title:** Future of DeFi with ThorWallet & MayaScan\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1769740750957220172)\


<details>

<summary><strong>Key Points</strong></summary>

* Special guests from ThorWallet and MayaScan dive into the future of DeFi. Aaluxx shares the latest on v1.09 and upcoming Arbitrum integration.

</details>

**#58: Mar 11, 2024**\
\
**Title: Key Points**: CacaoSwap Crew, Savers, and Arbitrum Integration.\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1766888438907768841)

<details>

<summary><strong>Key Points</strong>:</summary>

* &#x20;In this special episode, we hosted the CacaoSwap team, who shared their latest alpha, roadmap, and exciting future plans. We also covered updates on our next big release featuring Savers and our upcoming Arbitrum integration.

</details>

**#57: Mar 04, 2024**\
\
Title: CacaoSwap & Upcoming Release Highlights\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1764667229524701679)

<details>

<summary>Key Points</summary>

* Join us for alpha on CacaoSwap’s roadmap, plus key updates on the next release for savers and exciting plans ahead.

</details>

**#56 | Feb 26, 2024**\
\
Title: Deep Dive into Fees with ThorChain University\
\
🔗 [Listen here](https://x.com/i/spaces/1MYGNoEZOnOJw)

<details>

<summary>Key Points</summary>

* Special guests from Thorchain University and ThorWallet cover everything you need to know about inbound, outbound, and affiliate fees in DeFi.

</details>

**#55 | Feb 19, 2024** \
\
Title: Kenso Finance Integration & v1.09 Updates\
\
🔗 [Listen here](https://x.com/Maya_Protocol/status/1759593793450295335)

<details>

<summary>Key Points </summary>

* Guests from Kenso Finance join to discuss their upcoming integration, what to expect, and development updates as we polish v1.09 for launch.

</details>

**#54 | Feb 12, 2024**&#x20;

Title: Early Node Snapshots & New Integrations\
\
🔗 [Listen here](https://x.com/i/spaces/1YpKkwpgnNdKj)

<details>

<summary>Key Points</summary>

* The latest on early node snapshot payouts, Cardano proposal voting updates, and KeepKey testing. Plus, news on upcoming integrations with Kenosha Finance and CacaoSwap.

</details>

**#53: Feb 05, 2024**\
\
Title: Dev Updates & DeFiSpot Insights

[https://x.com/i/spaces/1vOxwjPWNzPJB](https://x.com/i/spaces/1vOxwjPWNzPJB)

**#52: Jan 29, 2024.**

Title: XDEFI Integration Takes Maya to the Next Level - Get Ready for the Epic Launch!

[https://twitter.com/i/spaces/1vOGwjBZmqEKB?s=20](https://twitter.com/i/spaces/1vOGwjBZmqEKB?s=20)

<details>

<summary>Key Points</summary>



* Exciting updates from the XDEFI integration! We've successfully integrated XDEFI to Maya we are in the final testing phase before the official launch, so stay tuned for the big reveal.
* Our team has been actively working on version 109 which includes Arbitrum and some minor fixes. We are happy to announce that we will be back to the normal routine of node cycling every Monday and Thursday.

- We are preparing Arbitrum for some smoke testing, with over one hundred simulated transactions covering various scenarios. If the initial smoke test goes well, we'll proceed to the next step: stage testing
- In terms of new integrations, Keepkey is on the horizon, undergoing final adjustments before becoming the first hardware wallet to support Maya. More integrations are in the pipeline, so expect exciting developments soon.&#x20;
- If you're interested in helping us grow our ecosystem, consider participating in our catalyst proposals and spreading the word about Maya's value. Voting is open until February 8th. Check them out here: [https://cardano.ideascale.com/c/idea/112584](https://t.co/OaM4eLz7A3)[ https://cardano.ideascale.com/c/idea/11252](https://t.co/s1VFFJhaby)
- Don't forget to join the Zealy quest – an opportunity to earn $CACAO and $Aztec while actively contributing to our ecosystem's growth. Your involvement is crucial, so let's get started!

</details>

**#51: Jan 22, 2024.**

Title: Churn, Arbitrum, & XDEFI

[https://twitter.com/Maya\_Protocol/status/1749446939387854954](https://twitter.com/Maya_Protocol/status/1749446939387854954)

<details>

<summary>Key Points</summary>

* Hey, great news on the Rune front! Our team has sorted out the outbound transaction fees and has been on a manual refund spree, especially for those transaction wizards out there. If you're more of a one or two-transactions kind of person, don't worry – you'll get your share automatically in version 109.

- All our chains are doing their thing without a hitch. The integration with Asgardex is making leaps and bounds, and we're in the final testing stretch for swaps. Midgard is also getting some fine-tuning love from our end.

* Big news – get ready for a churn! We've done the test bonding and unbinding dance, and everything seems to be grooving nicely. The churn is on the horizon, so if you’ve got pre-churn preparations, bond providers and node operators, now's the time. Remember, this churn and the next one put you in the running for the second early node Maya airdrop. It's a good chance to grab some extra goodies!

- What's on the horizon? The launch of ARBITRUM I and the router is coming up. We're mirroring the router from Ethereum, which is not just a time-saver but a game-changer.

* Get ready to cast your votes in the Cardano proposal, starting this Friday. We'll hang out in Twitter spaces with Maestro – big things on the horizon.

- Next week, the ALPHA Maya chain is making its debut on XDEFIi. Keep your eyes peeled for more updates.

* The Mids are minted, and there's a lot more goodness to come. Stay tuned for some extra perks and cool info.

- And guess what? We're dropping a new version of Maya Quest with a fresh prize pool of Maya and Aztec. We've revamped the rules to keep it fair and square, so everyone's got an equal shot at winning. We'd love to see you in the game!

\


</details>

**#50: Jan 15, 2024.**

Title: Arbitrum Addition, Refunds for Overpaid Fees, and Rare Shrimp Collection.

[https://twitter.com/Maya\_Protocol/status/1746910372463009950](https://twitter.com/Maya_Protocol/status/1746910372463009950)

<details>

<summary>Key Points</summary>

* Our team has been focused on addressing mainnet issues, such as resolving the fee concern with rune transactions and fixing an Ethereum-related glitch.&#x20;
* These matters have been successfully sorted out, and everything is functioning smoothly. For users who experienced higher fees while swapping rune, rest assured that we will be refunding the difference in version 109—be sure to keep an eye out for this update.
* Now that these minor issues are resolved, we are gearing up to restart our nodes and prepare for the early node snapshot. Stay tuned for more exciting developments!
* Speaking of version 109, it will not only contain crucial fixes but also pave the way for the addition of ARBITRUM.
* In other news, the Shrimp Collection Mids minting event is just around the corner! Join us this Friday, January 19th, at 3 pm UTC. The mint price is set at $USK, and you'll need $USK on @StargazeZone to participate.
* Furthermore, we extend an invitation to our tribe to join the Rare Evo: A Blockchain Event next summer. Consider buying a ticket and visiting us—our team will be there, and we would love to meet you in person. Exciting times ahead!

</details>

**#49: Jan 8, 2024.**

Title: Get Ready for the NFT Mids Collection.

[https://twitter.com/Maya\_Protocol/status/1744373674470253006](https://twitter.com/Maya_Protocol/status/1744373674470253006)

<details>

<summary>Key Points</summary>

* Great news Version 108 was realized yesterday and adopted by 100% of nodes. The chain is in good health, although we have to do some checks on features discovered that we will do today.&#x20;

- We encountered a minor issue: 100% of validators adopted v108 during the update. One of our full nodes that is used as infra for the endpoints that people typically use was corrupted. We needed to reset and resync; we analyzed how the version deploys in the sense that all the features work correctly. We will do this later today to ensure everything is good and excellent.

* Savers is just around the corner and churning regularly as well. We expect to have Savers and churns by the end of the week, so stay tuned.&#x20;

- What's on the horizon? Brace yourselves for the upcoming NFT Mids collection! We'll release later this week a blog post with all the essential details, including dates, benefits, and more.

* In the coming week, Cardano's proposal is set to go live for voting. Join us in a Twitter space with Maestro as we collaborate with him on this exciting proposal.

- Meanwhile, our team is  working on version 109, featuring Arbitrum. Making great progress and we're optimistic about a swift release if we don’t have any significant issues on mainnet or in the bifrost , the roadmap remains unchanged, maintaining a clear.Anticipate new interfaces arriving soon – stay connected for further updates.

</details>

**#48: Jan 2, 2024.**

Title: Breakthroughs from Maya Protocol's Latest Meeting! Secrets, Surprises, and More Unveiled!

[https://twitter.com/Maya\_Protocol/status/1741897158104616990](https://twitter.com/Maya_Protocol/status/1741897158104616990)

<details>

<summary>Key Points</summary>

* Maya will have a booth along with THORChain in [⁠@RareEvo⁠](https://twitter.com/RareEvo) in LA in August. Welcome to join, if you’re near.&#x20;

- [⁠@AaluxxMyth⁠](https://twitter.com/AaluxxMyth) and Gima plan to travel to Europe before the upcoming event to improve their presence and seize business opportunities through networking.&#x20;

* Aaluxx will also attend Denver and possibly consensus but without a booth, for the same purpose.&#x20;

- Maya Ledger Integration is in final stages.&#x20;

* A very good release Candidate for savers (Version 108) is being tested. MAYANode issues to be fixed, and will also have data pass-through, which is the first step for THORChain aggregation&#x20;

- An NFT collection "Mids" in collaboration with [⁠@ShrimpClubNFT⁠](https://twitter.com/ShrimpClubNFT) will launch within the next week. They will be priced at [⁠$5⁠](https://twitter.com/search?q=%245\&src=cashtag_click) each on the Stargaze platform. The purpose is to expand the community, with a chance to earn [⁠$CACAO⁠](https://twitter.com/search?q=%24CACAO\&src=cashtag_click) as well.&#x20;

* Version 109, will have the Arbitrum integration, which will be implemented shortly after version 108.&#x20;

- Version 110 will have streaming swap. After that we will focus on fine-tuning and making adjustments based on the current state.&#x20;

* After Version 110, will work on Cardano and other chains.&#x20;

- Team is expanding, and all versions are being worked on at the same time, as well as the Ethereum Router to enable different interactions with ERC-20s, that will open many new possibilities.

</details>

**#47: Dec 18, 2023.**

Title: Happy Holidays! WE appreciate your Support This Year.

[https://twitter.com/Maya\_Protocol/status/1736763373369393233](https://twitter.com/Maya_Protocol/status/1736763373369393233)

<details>

<summary>Key Points</summary>

1. The Shrimp Club is thrilled to announce the launch of an NFT (Non-Fungible Token) collection of1,000 unique NFTs with cool rewards for users. This initiative connects chains and fosters a sense of community across ecosystems, bridging the Kujira and Maya protocols for increased awareness.
2. We are very happy to announce that our team has been making great process with the work of KeepKey and Ledger, thanks to our new developers. Stay tuned for forthcoming updates.
3. We successfully migrated from our Genesis nodes to an entire network of liquidity nodes, node operators, and bond providers. This collaborative effort is paramount to ensuring the security of our network.
4. Version 108 introduces a substantial fix: a store migration to address dropped transactions. Be vigilant for this update, as it contributes to a more seamless user experience.
5. Exciting developments are underway for Version 109, which includes the implementation of Arbirturm and Exchange AS. Node Launcher received its first green light, and our team is working hard to have this version as soon as possible.
6. Asgardex continues to perform exceptionally well, and we are saticefised  with its positive results.
7. If you would like to learn more about Cardano integration, make sure to join tomorrow's Twitter Spaces "coffee with Cardano" and get to know more details about this integration.
8. Participate in our latest edition of Maya Quest! Explore,participate and earn rewards.

</details>



**#46: Dec 11, 2023.**

Title: $1 Billion Transactions, Exciting Partnerships, and More!

[**https://twitter.com/Maya\_Protocol/status/1734226620741796030**](https://twitter.com/Maya_Protocol/status/1734226620741796030)\
[**https://twitter.com/Maya\_Protocol/status/1734232460643258430**](https://twitter.com/Maya_Protocol/status/1734232460643258430)

<details>

<summary>Key Points</summary>

1\. Tier1 Airdrop:

* Tier 1 members, check your wallets! The first new Maya is ready for you to print some chocolate. Big thanks for your support from March till now!
* Two more airdrops are on the way, so stay tuned for the upcoming airdrops.

2\. Early Node Airdrop:

* Airdrop alert! We will be having a  final churn and after that churn, we'll take a snapshot and initiate payouts. Keep an eye out for the next two airdrops.!

3\. Milestones Achieved:

* Celebrating $1 billion $CACAO transacted!
* Record-breaking 110,000 swaps and 10,000 wallets – a fantastic achievement. Exciting times ahead!

4\. Welcome to New Dev Team:

* The 1st dev, a returning talent, has already made a significant impact on Maya Protocol..
* The 2nd dev brings expertise in node development, contributing to liquidity nodes format.
* The 3rd dev will kick off effective and safe code reviews starting in January.

5\. Exciting Partnerships:

* Shoutout to the Shrimp Club NFT – an exciting partnership in the works. Stay tuned for details of this NFT collection it will be dope and it aims to attract the Kujira community.
* Dash community reached 1 million in TVL. Thank you for your support!

6\. Maya Interfaces and Integration:

* Anticipate more Maya interfaces using Exchange AS.
* ASGARDEX integration is a huge step. It's now open source, downloadable, and supports various functions, including sending, receiving, and storing $Cacao and $Maya tokens.

7\. Bare-Metal Migration:

* Maya is now fully migrated to bare metal. Exciting developments on Stagenet and Mainnet nodes, testing, and access to impressive APYs.
* 80% of validators are now running on bare metal, ensuring a safer and more resilient infrastructure.

8\. Maya Swap Improvements:

* The Maya Swap feature now allows trading $CACAO and $Maya in an order book system. Experience the enhanced speed and functionality on Maya's scan!

9\. Arbitrum:

* We are currently working with Arbitrum Foundation to launch a proposal for the new chain integration. Look forward to Arbitrum on version 109!\
  \
  Stay connected for more updates, and thank you for being part of this exciting journey!

</details>

**#45: Dec 4, 2023.**

Title: 10,000 Followers, New Integrations, and Special Airdrops Await!

[**https://twitter.com/Maya\_Protocol/status/1731689903228883340**](https://twitter.com/Maya_Protocol/status/1731689903228883340)

<details>

<summary>Key Points</summary>

* Our team is actively working on Version 108, and while we've faced some delays, primarily due to the ongoing bare-metal migration for our Genesis nodes, we're optimistic about completing it by the end of the week. With the help of our new two developers.
* Progress on the exchange front has been commendable. A big shout-out to our community developers for their awesome work, the integration with this exchange will also make the implementation of Asgardex smoother and faster.
* Exciting developments on the Asgardex front! Soon, the first version related to Maya activity involving $cacao and Maya transactions will be out. This will enable users to send custom memos and other assets through Asgardex.
* This Tuesday the distribution of Airdrops and Ambassador will be payout. Additionally, the Tier 1 LP Airdrop is scheduled for the upcoming Tuesday. An early node airdrop is also expected this Monday, catering to the third to last Monday, the second to last Monday, and the previous churn from the snapshot."
* For those considering maximizing their Maya position, an enticing opportunity awaits. A special airdrop, allocating 17,500 Maya tokens, will be distributed. Eligibility includes anyone churning on Monday, Thursday, or the following Monday. This airdrop is split evenly between node operators and bond providers.
* We are very excited to announce the integration of Maya to LeapWallet, a non-custodial gateway to the COSMOS universe! Now you can Seamlessly send, receive, or HODL your $CACAO and $MAYA tokens through Leap Wallet.
* Exciting news! Our proposal for Maya integration with XDEFI has received the green light, thanks to the support of our voters. We're thrilled about the enthusiasm from the XDEFI community. This integration promises to simplify transactions significantly.
* We are celebrating a significant milestone – surpassing 10,000 followers! We're thrilled to welcome new members into our tribe. If you're new to Maya Protocol, explore our website for tools, articles, guides, and our white paper. Join our Discord for any queries; our team is here to assist you.

</details>

**#44: Nov 28, 2023.**

Title: New Cardano Catalyst Proposal, Churn Routine Resuming, with Upgrades and Exciting Features

[**https://twitter.com/Maya\_Protocol/status/1729183376383631605**](https://twitter.com/Maya_Protocol/status/1729183376383631605)

<details>

<summary>Key Points</summary>

* Churn scheduled in a few hours; nodes and bond provider
* Excited to resume churn routine next week after resolving delays due to Kujira issues with TSS (Threshold Signature Scheme).
* Some $Kuji outbound transactions were stuck.
* Assuring users that some were paid, others had to be dropped, but all will be resolved.
* Urgent fund needs can be addressed through a help desk ticket for manual refunds.
* Version 108 will include fixes to prevent similar bugs in the future.
* Ongoing votes in ShapeShift and XDEFI to prioritize adding Maya to their interfaces.
* We encourage users of Fox and XDEFI to vote, comment, and spread the word.
* Preparing a new Catalyst proposal for Cardano in collaboration with the Cardano team we are very optimistic about the chances we have of success.
* Maya Quest ending on November 30th with a reward of 6,000 Maya tokens, don’t miss your chanse and participate.
* Despite small errors, airdrop for our Tier1 will be sent this weekend. Thanks for your continued support.

</details>

**#43: Nov 20, 2023.**

Title: "Maya's Tier1 LP Airdrop is Just a Stop - Major Updates and Airdrops Coming Soon!"

[**https://twitter.com/Maya\_Protocol/status/1726616426159784051**](https://twitter.com/Maya_Protocol/status/1726616426159784051)

<details>

<summary>Key Points</summary>

* Exciting news! The Tier 1 LP airdrop is coming soon, with a slight delay due to the huge number of addresses being reviewed. But trust us, it will be worth the wait! Keep your eyes peeled for the airdrop.
* We are pleased to see a rise in active Discord users and increased interest from projects wishing to collaborate with Maya.
* Leap Wallet integration is almost here. Final updates and polishing are happening for the last bits of data. Launching any day now!
* Maya has reached 1,000 active users, 8,000 wallets, and we have a strong following of 10,000 on Twitter.&#x20;
* You can now find Maya Protocol on Defilama.
* Future developments: expanded chains, improved UI, increased security measures and dev team.&#x20;
* Addressing a payout issue in progress for version 108 and enabling bonding wstETH.
* Our focus is on users rather than CEX listings. Onboarding new users is a simple process for any token supported by Maya. Purchases of $CACAO occur on chain, increasing the number of wallets, usage, volume, and revenue for LPS.
* THORChain's streaming swaps feature is planned to be incorporated in Maya Protocol's version 110 to increase trade volume. New UIs will also be launched to capture this momentum.
* Maya will stand out with innovative technology and exclusive swaps not found on other DEXes. Look out for early node and LP airdrops, as well as GLD airdrops in January and March. Visit Maya Scan for more information.

</details>

**#42: Nov 13, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1724079763449389333**](https://twitter.com/Maya_Protocol/status/1724079763449389333)

<details>

<summary>Key Points.</summary>

* Maya reached record-breaking achievements last week in the decentralized cross-chain space. The efforts of Maya and Thorchain are revolutionary.
* Our tribe is expanding rapidly, with daily growth and significant activity, such as $17 million in volume and payments increasing for Maya holders. Integration plans are in place for the future.
* We appreciate our community and their support, camaraderie, and helpful vibes. Together, we are addressing questions and promoting the Maya protocol.
* Maya Protocol's goal is to address market gaps and expand the cross-chain DEX space. We are broadening our reach by including assets such as Dash, Arbitrum, Kuji, and others.
* Expect greatness with the upcoming version 108, and each coming version. Stay tuned! 🚀

</details>

**#41: Nov 06, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1721543464892891363**](https://twitter.com/Maya_Protocol/status/1721543464892891363)

<details>

<summary>Key Points</summary>

* Our latest Tier 1 unlock has ended and all current system assets are voluntary, with many users choosing to remain in holding. There is an increasing interest in long-term holding of Bonded LP, particularly in $CACAO. We look forward to the results of this shift and greatly appreciate your ongoing support.
* Our team promptly addressed a consensus failure during a churn caused by a provider having 0 LP. The error temporarily halted block generation, but our developers quickly fixed it by modifying the formula, allowing normal operations to resume.
* Sync issues with validators and Kujira are being addressed by our team and will be resolved by the end of the week.
* Our chains have consistently delivered impressive results this week, with a total volume of $1.95 million and reaching a milestone of 4,000 wallets.
* Tier 1 snapshot is on the horizon, and we'll be sharing an Excel document with the corresponding addresses on Discord.
* The highly awaited Version 108 is coming soon with housekeeping features and the long-awaited Savers. Updates to follow.
* MayaScan introduced Maya Perps, a perpetual contract trading platform, with improved accessibility, transparency, and usability.&#x20;
* Unlike traditional centralized exchanges, Maya provides transparent and visible processes for user monitoring.

</details>

**#40: Oct 23, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1716469734294839509**](https://twitter.com/Maya_Protocol/status/1716469734294839509)

<details>

<summary>Key Points</summary>

* Our team is finalizing KUJIRA's launch and incorporating USK as the third stablecoin in our asset selection. We are excited about this integration and will provide updates on Discord.
* Our team will operate a node on Thorchain, a significant milestone for us as one of the largest Rune third-party custodians. We are pleased to be running a node and welcome any Rune holders who wish to delegate to our validator. Please contact us for more information.
* There are now eight liquidity nodes and bond providers will be affected by Kujira's unhaulting. May Protocol's next airdrop will occur in nine churns and will be divided equally for bond providers and validators.
* Tier 1 unlocks are coming soon and we are optimistic about the future due to our increasing daily trading volume. The Tier 1 airdrop snapshot is scheduled for November 3, remember to watch out for it.&#x20;
* Additionally, Maya quests are back with new missions and attractive rewards.

</details>

**#39: Oct 16, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1713947967395160501**](https://twitter.com/Maya_Protocol/status/1713947967395160501)

<details>

<summary>Key Points</summary>

* Kujira was launched successfully last week, but an unexpected update the following day forced us to temporarily halt the chain. Our team is working on fixing the changes and will inform you promptly when the Kujira chain is resumed.
* Local Money is a peer-to-peer protocol allowing individuals to trade using local currencies. The protocol is inviting its community to form a Decentralized Autonomous Organization (DAO).&#x20;
* Trades are conducted on-chain with escrow for both fiat and crypto assets, ensuring prompt payment. Communication is secured off-chain with public key access. Oracle pools maintain accurate prices against USD without user price-setting. Transaction confirmations are easily sent and received.
* Local Money integration with Thorchain and Maya allows for smooth transitions and reduces reliance on centralized exchanges, benefitting users in regions with strict government regulations. Transaction confirmations are simple to send and receive.
* Our integration offers a unique peer-to-peer experience with lower fees based on user tiers, transforming the industry. Users can access a wide variety of assets for exchange.
* Local Money allows for converting fiat money to Bitcoin using THOR/MAYAChain assets, creating numerous opportunities.&#x20;
* At Maya Protocol, we are excited about the diverse chains that add value, such as Dash for privacy and Local Money for off-ramping fiat, along with Arbitrum for converting crypto to USD via ThorWallet credit cards.&#x20;
* Cross-chain technology is undoubtedly the future and we are dedicated to advancing it with optimism and enthusiasm.

</details>

**#38: Oct 9, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1711396163323388248**](https://twitter.com/Maya_Protocol/status/1711396163323388248)

<details>

<summary>Key Points</summary>

**Liquify:**

* Liquify now supports Maya Protocol, offering a comprehensive dashboard network, allowing users to monitor network health and access historical data.

- Soon, Bond providers can subscribe for updates that reports insights into node statistics, rewards, and protocol health.

* They are also operating a node, contributing to decentralization.\

* Thanks to their dashboard, node churn process has been improved, benefiting bond providers and node operators.

**El Dorado:**

* A Maya-exclusive swapping platform, is improving UX further.

- They are integrating $KUJI and soon #Savers on #MAYAChain.

* With volume increasing you can still partake in our Vision of the city of gold with our seed raise still open.

- DEX aggregation feature with Manta DAO assets. Allowing users to swap any Kujira asset in one click.

**MayaScan:**

* After M-Tokens & M-NFTs MayaScan now offers p2p messaging, allowing users to select which address to communicate with, enhancing interaction with nodes & monitoring of messages.

**General:**

* Node operator rewards increasing. Perfect time to consider running a node and boost your $CACAO rewards.

- You can now bond $DASH LP and soon $KUJI.&#x20;

* V107 automates reward payouts.

- Retiring first Genesis Node as we move forward.

* $DASH is operating seamlessly without any issues.

- Kujira is live; announcing availability & enabling swaps after 24hrs of LPing. UIs has tested Kuji on stagenet & in own environments, so integration

</details>



**#37: Oct 2, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1708859529591464011**](https://twitter.com/Maya_Protocol/status/1708859529591464011)

<details>

<summary>Key Points</summary>

* Our team was dedicatedly working on Version 107 last week. The launch is imminent; developers are refining the final details to guarantee a smooth release.
* We have decided to not include Savers in Version 107, opting for a rapid rollout of Version 108 dedicated to Savers after Version 107's release. Version 107 will contain essential fixes for Dash and the Kujira addition.
* Our team is addressing the 21 addresses that have yet to receive their funds; 16 have been resolved and we are working to resolve the remaining 5. Updates to follow.
* Introducing MayaScan, a fast, simple, and user-friendly blockchain explorer acclaimed for its UI & efficiency. This weekend we launch the MRC-20 Standard & M-NFTs, well-received by the Maya community, with future features such as gaming multipliers. Open source & user-friendly, try it & explore!&#x20;

</details>

**#36: Sep 25, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1706322760174010536**](https://twitter.com/Maya_Protocol/status/1706322760174010536)&#x20;

<details>

<summary>Key Points</summary>

* Kujira integration into MAYAChain, allows users the access to Kujira's top of the line DeFi solutions and ecosystem, permissionless & without the use of bridges or wrapped assets.
* "Savers" feature, allowing users to earn yield on their favorite assets (BTC, ETH, RUNE, DASH, KUJI) without exposure to CACAO.
* Reward logic to be fixed for Node payouts where rewards will be distributed to Node Operators, after each churn, according to the fees they set, and proportionally among Bond Providers according to their LP share.
* $Dash and $KUJI will gain the ability be bonded to Liquidity Nodes in v107, enabling $DASH & $KUJI LPers to get a bigger share of MAYAChain fees.
* A known bug regarding quoting synths on MAYAChain will be fixed. Users can now swap synths to any asset.
* Finally, Node Operator fees are now adjustable at any time to ensure more flexibility and ease of use to our Node Operators.

</details>

**#35: Sep 18, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1703786253618159926**](https://twitter.com/Maya_Protocol/status/1703786253618159926)

<details>

<summary>Key Points</summary>

* We're pleased to announce Maya Protocol's integration into Pulsar Finance, providing users effortless monitoring of CACAO/MAYA tokens, LP positions, and Savers positions. Pulsar also added support for the Dash chain.
* The release of version 106 marked a significant milestone for Maya Protocol. Here are some noteworthy updates:
  * Dynamic Outbound Fee Multiplier (DOFM): Our commitment to enhancing user experience includes more flexible fee structures.
  * Node Rewards Fix: To further enhance stability, we've resolved issues related to node rewards.
  * Whitelist of Ethereum Tokens: Safeguard your swaps with an enhanced security measure against scam tokens.
  * First Dex Aggregation Contracts for Ethereum: Seamlessly swap BTC, ETH, and more across Ethereum's decentralized exchanges.
  * Improved MAYANode Sync: We've fixed bugs to ensure accurate chain synchronization status.
* Version 106 had synchrony issues; Fixes, Savers & Kujira to launch version 107 by week's end with changes to operator fee & more. Further details to follow.
*   We believe in the power of community collaboration. If you possess knowledge of JavaScript and would like to contribute to our project, please reach out!&#x20;

    \


</details>

**#34: Sep 11, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1701249471193952542**](https://twitter.com/Maya_Protocol/status/1701249471193952542)

<details>

<summary>Key Points</summary>

* $KUJI integration in MAYAChain broadens Kujira capabilities, by providing $KUJI holders access to THORChain and MAYAChain backed assets e.g. Bitcoin, Ethereum, Dash, THORChain, without KYC and transparently.&#x20;
* At the same time, #Kujira will be the gateway for Maya Protocol into the broader Cosmos ecosystem, ushering in exciting new possibilities in all the relevant Cosmos chains.&#x20;
* THORWalletDEX recently teased the introduction of $KUJI in their wallet, allowing for sending/receiving assets on the Android test flight. To access these features, update the APP and explore the world of Kujira. &#x20;
* MAYANodes airdrop successfully distributed last Friday, rewarding all contributors to MAYAChain security. &#x20;
* Our team is preparing to release an update with bug fixes, clearing the $DASH transaction queue, and making fees cheaper (improving the Outbound Fee Multiplier).

</details>

**#33: Sep 4, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1698712993121366120**](https://twitter.com/Maya_Protocol/status/1698712993121366120)

<details>

<summary>Key Points</summary>

* Airdrop to nodes to be distributed this week; allocation details to be shared later today. Bond operators/nodes with doubts should notify us; payments to be made by Wednesday/Thursday.
* We have halted liquidity node churns for a few days due to a bug in Dash. We are diligently working to fix the issue and will provide updates by week's end.
* Regarding Dash, We've made changes to Bifröst to collect more data and logs. Two fixes are planned; one may not be ready this week, while the other may be resolved by the end of the week. Keep an eye on Discord for updates.
* Our team is progressing and performing admirably. Preparations for the Kujira launch are underway; Kujira has been quite helpful. We plan to launch Dash version 106 shortly and a potentially major Maya update is being considered to expedite the process.
* We may need to submit a second proposal with further clarity is necessary for the Cardano funding proposal. We thank the Cardano community for their positive feedback.
* On the development front, our contributors and community have been actively involved, we thank you and really appreciate your feedback and new ideas.
* Participants in the Ambassadors Program must complete the form soon; announcements will be made on Discord and Twitter.
* We've implemented new security measures to ensure Discord server safety and are actively discussing ways to further improve security.

</details>

**#32: Aug 28, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1696176009110802887**](https://twitter.com/Maya_Protocol/status/1696176009110802887)

<details>

<summary>Key Points</summary>

* Thorwalled and ZK Discord were compromised despite 2Fa and security measures due to Discord's structure. Hacker gained access, booted moderators, and claimed an airdrop with associated wallets compromised. Accounts regained today with further security measures encouraged. Social engineering, not only code, and 2FA should be taken into account for Maya Protocol security.
* Maya Protocol never rushes airdrops; announcements are made way in advance to ensure careful claiming processes. Limited times or spots are usually indicative of a scam.
* Maya Protocol will never send/redirect to an unofficial website; official websites will be used exclusively.
* We encourage you to use new wallets to claim Airdrops; it's fast and easy to setup, not to mention, much safer.
* Always verify information from multiple sources; consult our mods/admins, docs, & website; be skeptical.
* Hackers plan meticulously; stay vigilant and report suspicious activity. Help protect the community.

**Business side**

* Kujira's arrival is imminent; integration of Maya protocol into their ecosystem has been confirmed by multiple teams.
* Our team is developing v106, featuring Kujira, savers, outbound fee multiplier, and improvements for node operators to enhance churns. Dash core wallets, ledger, and trust wallets are in the pipeline.
* Our team achieved the first early node airdrop last week! Visit our Discord for more info. Interested in contributing? Check out the Data passthrough and join the conversation. [https://gitlab.com/mayachain/mayanode/-/issues/16](https://gitlab.com/mayachain/mayanode/-/issues/16)

</details>

**#31: Aug 21, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1693639214410105310**](https://twitter.com/Maya_Protocol/status/1693639214410105310)

<details>

<summary>Key Points</summary>

* Our team is focusing on v.106 & ledger integration, which will include features like $Kujira, Dynamic fee multiplier, & savers, plus relevant fixes.
* Integration with Ledger expected in early September, followed by Trust wallet and Swap kit.
* Last week saw our first churn post-Tss updates, raising our nodes from 13 to 15. Active validator set bond eclipsed 2M, totaling 4.6M with active and standby bonds.
* One last churn before the first airdrop of Maya tokens, split 50/50 between node operators and bond providers, is the final opportunity to join the active validator set. Following 12 churns, a second airdrop will be delivered, after 18 churns a third, and after 36 churns, the last 17,500 $Maya tokens, totalling 70,000 $Maya tokens.
* We recommend our community use MayaScan and the team is working diligently to have its features match the old explorer.
* Record of $15.4K on $Dash pool in 24 hrs. DAO approved cacao addition, pool soon to exceed $200K.
* We invite our community to aid us in advancing our gitbook, webpage, and discord by contributing and/ or identifying areas for improvement.

</details>

**#30: Aug 14, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1691103455421636608**](https://twitter.com/Maya_Protocol/status/1691103455421636608)

<details>

<summary>Key Points</summary>

* Executed a successful churn, allocating $4M LP Bonds evenly by validators and stand-bys.
* Maintenance on Binance TSS will delay the upcoming churn. We remain committed to emulating their approach and await the necessary adjustments. Three more churns are expected before the early node airdrop is fully allocated.
* Our team is creating version 106 and essential housekeeping updates to speed up our development process.
* We have achieved a major accomplishment in our partnership with the Dash Investment Foundation and will soon be adding Lp to the pool, offering great potential.
* High priority is given to Ledger support. Testing and refining are being carried out, with Ledger team providing evaluation to ensure integration.
* Our ecosystem achieved a noteworthy milestone over the weekend, with a record-breaking volume of $700,000 and total liquidity rising from $15.6 million to $18 million.
* Join Maya Protocol and actively participate, share experiences, and provide feedback to our team. Engage with our community and help spread awareness of ThorChain. Together, we can elevate the ecosystem.

</details>

**#29: Aug 7, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1688565705711489024**](https://twitter.com/Maya_Protocol/status/1688565705711489024)

<details>

<summary>Key Points</summary>

* Last week our team focused on fixing the threshold when signing transactions and finding bugs.
* v106 nearing completion; Kujira, savers, dynamic fee multiplier implementation are getting closer.
* We achieved a successful churn after 13 attempts; to reduce it to 2 attempts, key agent fixes will be implemented in version 106.
* Security has risen to 3M dollars (20% of network security) with active validators at 1.6M and standby bonds at 1.2M. Impressive numbers.
* Ledger support coming soon; testing rounds underway; priority for Dev team; similar to TC; should go smoothly.
* Bond providers 4-6 to ensure continuous growth. Gain $MAYA airdrop by bonding or running a node.
* Dash LP swaps are working well; we must focus on increasing Lp. We have a few things prepared to make it happen.
* If you want to help us, you can start by pushing integrations for Dash x Maya to have this integration everywhere.
* What is next? New chains. We invite you to engage, join the conversation in our Discord and let us know what you think about new chains integration.&#x20;
* Some of you participated in the THORWallet referrals program. 4 or 5 people haven't yet linked their wallet, so please get in touch.&#x20;

</details>

**#28: July 31, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1686029238544011265**](https://twitter.com/Maya_Protocol/status/1686029238544011265)

<details>

<summary>Key Points</summary>

* Last week's launch of version 105 was successful, with four active validators and nine standby liquidity nodes with a combined $1.9 million in bonds. Interested in running a node? Instructions are available on Discord.
* Dash integration is live and functioning, with add, withdraw, and swap operations. ThorWallet and El Dorado are polishing final details; watch out for them.
* Dash pool has reached $113K, aiming to exceed $1M with further support.
* v106 is upcoming, introducing Kujira, savers, and dynamic fee multipliers. v107 will introduce Arbitrum and streaming swaps.

</details>

**#27: July 24, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1683492356643868678**](https://twitter.com/Maya_Protocol/status/1683492356643868678)

<details>

<summary>Key Points</summary>

* We are delighted to launch v105, our biggest update yet, with major changes to our codebase. Tested for reliability.
* v105 of our system features updated Liquidity Nodes and Dash integration. Stage net is live today and tomorrow, followed by bond unhalt. In a few days, Liquidity Nodes will be churned, allowing community members to add liquidity and trade.
* Emphasizing safety, we urge all to take the time necessary to ensure security.
* Liquidity nodes & Dash integration pave the way for future chain integration.
* Our team will be rewarding Thor Wallet referrals with Maya tokens this week. Stay tuned of further information.
* v106 is in development, with either Arbitrum or Kujira. It will also address any issues detected during v105 testing.
* Look out for the new streaming swap features coming soon after rigorous testing on THORChain for reliability.

</details>

**#26: July 17, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1680947009866575874**](https://twitter.com/Maya_Protocol/status/1680947009866575874)

<details>

<summary>Key Points</summary>

* Version 105 of MAYAChain and Dash integration is imminent. For updates, follow our Discord.
* Version 106 with features Kuji & Savers coming soon after 105. Progress made by team is impressive.
* Tier 2 unlocked. Withdrawing available on testflight, android, and web app. Apple users can expect new version soon with added features. Thanks for feedback.
* Introducing MayaScan, a cutting-edge blockchain explorer designed for speed and user-friendliness. Check your transactions and data effortlessly. Keep an eye out for upcoming features that will enhance your experience further.
* Exciting integrations are in progress! Maya will soon be integrated into Dash core wallets.
* Kuji Devs have also confirmed their integration with Maya. If you are a Dev in the Kuji ecosystem, we want to hear from you! Reach out to us on Discord to explore opportunities.

</details>

**#25: July 3, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1675882220748144640**](https://twitter.com/Maya_Protocol/status/1675882220748144640)

<details>

<summary>Key Points</summary>

* Our team is testing Dash's chain locks with the Dash incubators team to create a consensus layer alternative. This will take a few more days; stay tuned.
* We are refining, testing, and confirming the operation of liquidity nodes. Tests are progressing well, revealing minor issues, facilitating quicker fixes, and enabling seamless mainnet operation.
* Maya Mask Airdrop distributed. Check wallet & Discord for updates.
* Defispot offers an exchange experience with DEFI, replicating the functions of centralized exchanges. Defispot is a cross-chain aggregator for token swaps, and Maya protocol adds support for cross-chain native tokens.
* Integrating MAYAChain is essential to increase options and create new liquidity pools, routes and chains (e.g. Cardano, Dash and Kujira). MAYAChain will also add new features and enhance the DeFi user experience.
* Devs are nearing completion of MAYAChain integration; stay updated on our socials for news.

</details>

**#24: June 26, 2023.**\
\
[**https://twitter.com/Maya\_Protocol/status/1673346009626206209**](https://twitter.com/Maya_Protocol/status/1673346009626206209)

<details>

<summary>Key Points</summary>

* Testing of Liquidity Nodes (LN) feature revealed compatibility issue due to logic changes between v104 and v105; adjustments needed and will be completed in a few days.
* Dash is operational, nodes ready, Bifrost scanning; integration imminent, stay alert.
* For Savers & Kuji anticipate a swift update following v.105, with a rapid release cycle.
* We invite our tech-oriented members to explore our Gitbook for updates and innovations, along with engaging content.
* DEX Aggregation enabled: Code present, interface paused; soft launch in few weeks, hard launch later; feature arriving soon.
* Maya Mask holders have 48 hours to review the list to published on Discord. If issues arise, contact the team. Airdrop to follow.
* Our team is compiling and organizing referral code data; cashbacks will be distributed next week.
* Quests are active; join for rewards without the Maya mask requirement.
* We'll host our first Twitter Spaces with Ambassadors to discuss Maya Protocol. Further info to come later this week.

</details>

**#23: June 19, 2023.**\
\
[**https://twitter.com/Maya\_Protocol/status/1670808614317981696**](https://twitter.com/Maya_Protocol/status/1670808614317981696)

<details>

<summary>Key Points</summary>

* v105, containing Dash and LN, is performing better. A Dash issue was stuck on Monday, which necessitated API updates, and was resolved with assistance from the Dash Core Group & Dash Incubator.\

* We updated the smoke tests for Dash & Maya, pushed the integration to Stagenet, and resolved liquidity nodes issues; however, there is still a bond issue pending.\

* Testing is imperative for a successful launch to Mainnet; Dash & LN should be open to bonds this week if testing is successful. Updates will be given on Discord.\

* Dynamic Outbound Fees Multiplier (which reduces swap fees) postponed to v106 due to compatibility issues on Stagenet. Resolution will take a couple of weeks, so the update was postponed to avoid delaying Dash & LN.\

* We are fixing Midgard and CACAO prices on CoinGecko, as well as developing v106 savers.\

* Regarding Maya Masks: 20th of June (Today) is the last day to link your wallets, we will have a list showing the addresses that were eligible on Friday the 16th.\

* Caution advised when seeking support; scammers WILL pose as support. Never provide your seed phrase & always verify information from multiple sources.\

* There is a lot of interest in running MAYANodes especially from Bare Metal Nodes operators.\

* We will finally get to see the tradeoffs of Liquidity Nodes vs Pure Bonds tested in real life.\

* New Maya Quests will conclude on July 17th; all are welcome, Mask not required.\

* Ambassadors selected; additional spots available shortly. Ambassadors Twitter Spaces being planned for end of week.

</details>

**#22:** **June 12, 2023**

[**https://twitter.com/Maya\_Protocol/status/1666987304785973249**](https://twitter.com/Maya_Protocol/status/1666987304785973249)

<details>

<summary>Key Points</summary>

* Dash integration experienced a compatibility issue with the RPC connecting nodes to send/receive transactions. Observations are working, however, signing transactions has encountered the issue. The Dash Core team is currently addressing and both teams are striving to resolve it promptly.
* Version 105 is nearly ready with its exciting features, including the Dynamic fee multiplier reducing fees from 3X to 1.5X. The Maya Mask airdrop will have an eligible address list released in the coming days. If any doubts arise, contact a team member for assistance.
* We are pleased to share that the Swapper DEX wallet will soon integrate Maya. Finalizing the update will enable us to welcome new community members, unique users, and increased volume to the tribe.
* We are delighted to welcome our first Maya Ambassadors: Mr.Mohawk, Team Ocebot, Anouluwalove, brawncrypto\_eth and shelleymaeph! Welcome to the program.

</details>

**#21: June 5, 2023**

[**https://twitter.com/Maya\_Protocol/status/1665535307163525121**](https://twitter.com/Maya_Protocol/status/1665535307163525121)

<details>

<summary>Key Points</summary>

* Version 105, featuring Dash and Liquidity Nodes, is soon to be released. These additions are anticipated to expand the user base, boost volume and create stronger security. Additionally, Liquidity Nodes are expected to improve liquidity stability.
* Claim your Maya Mask in two weeks before the snapshot! Connect your ETH wallet holding Maya Mask to the Maya Protocol via swap or liquidity.
* Our team has selected people for the first round of the Maya MaAmbassador's program; a new form will be available on our website for the next round. Claim your Maya mask before the snapshot taken this month by connecting your eth wallet to Maya Protocol (via swap or liquidity provision). Two more weeks remain.
* Maya Academy's marketing team is releasing beginner-friendly, 101 crypto info articles, as well as sophisticated material for experienced users.
* Participants have one week remaining to compete and reach the summit of Maya Quests' rankings!

</details>

**#20: May 29, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1663198707486015490**](https://twitter.com/Maya_Protocol/status/1663198707486015490)

<details>

<summary>Key Points</summary>

* Back on schedule and team has resumed work on v105 updates, including DASH, Liquidity Nodes, and Savers.
* Liquidity Nodes feature is essentially ready; undergoing review and minor changes. Should be added to v105 updates.
* Dash is our top priority; the rebase is now complete. We are conducting a small test with Dash; a giant simulation to visualize & catch problems quickly. Once these tests are run, we will be ready for Dash integration.
* Our team is making progress in adjusting savers' yield percentage based on synth utilization; the changes are straightforward and will be ready soon.
* After Dash, we're focusing on Kujira, then Airbitrum; integrating one chain at a time to guarantee functionality and identify problems quickly.
* Expect the Maya Mask airdrop soon. Refer to the guide and #MayaMask channel on Discord for details. If uncertain, contact a team member for assistance.
* We love to hear about our community; reach out on our discord, make your case, and let us know which chain you want us to integrate.

</details>

**#19: May 23, 2023.**

[**https://twitter.com/i/spaces/1ZkKzXrrNVyJv?s=20**](https://twitter.com/i/spaces/1ZkKzXrrNVyJv?s=20)

<details>

<summary>Key Points</summary>

* Team finishing version 104 polishing & testing; trading to be enabled soon.
* Making significant and smooth progress with the Liquidity nodes feature; expect news soon.
* Dash and Savers up next; 104 version rebase in the works; exciting & noticeable upcoming updates for users.
* Maya now integrates with Wallet Connect, streamlining the connection to Maya Chain and enabling swapping, sending, & receiving.

</details>

**#18: May 16, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1658140343668441089**](https://twitter.com/Maya_Protocol/status/1658140343668441089)

<details>

<summary>Key Points</summary>

1. $MAYA airdrop to $RUNE holders is imminent. We appreciate your patience. We are double checking to make sure all data are correct.
2. Team is working hard on the housekeeping update, which will make easier and faster to notice on-chain issues. This update will help us to unhalt BTC chain.
3. Next on the schedule is $DASH and savers, which will bring more liquidity to the protocol.
4. &#x20;We're excited to launch our Ambassador program! Content creators and influencers can apply to join and get rewarded with Maya tokens.
5. Maya Mask holders receive exclusive quests this month as per community vote and in line with the vision that the Maya-Masks are utility NFTs.
6. Starting next month Non-holders will also have opportunities to earn $MAYA.
7. Do you believe you or someone you know can contribute to Maya Protocol, reach out on our Discord.

</details>

**#17: May 9, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1655603671210639365**](https://twitter.com/Maya_Protocol/status/1655603671210639365)

<details>

<summary>Key points</summary>

* Numerous UIs have declared integration of Maya Protocol. Wallets, Dexes, and Portfolio managers will be included, with some already in the process. Anticipate amazing news soon.
* BTC chain is halted as a precaution to prevent double transactions caused by fee fights.
* Snapshots for $Rune holders taken May 3; team gathering all data points for distribution. A document containing addresses/points will be available soon; tickets on Discord for inquiries.
* Rewards for the Maya quests were distributed over the weekend; check wallets.
* The Maya Tribe is an NFT media, marketing & ambassador community for creators that want to contribute to the Maya ecosystem. Visit their Twitter and Discord for more info.
* We welcome feedback and collaboration; join our Discord and reach out.

</details>

**#16: May 3, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1653444528949305345**](https://twitter.com/Maya_Protocol/status/1653444528949305345)

<details>

<summary>Key points</summary>

* Chain has been performing well, with swaps, synths, and withdrawals functioning as intended. A minor update is scheduled for mid-week, allowing for further refinements.
* Our team is working on a liquidity nodes feature requested by node operators, which enables bond providers to select the number of LP units to bond with a node, aiding node operator operations.
* Integration of Dash & Savers is a top priority; end of week target for testing Dash-ETH, Dash-Rune swaps on stage net. Mainnet deployment imminent.
* We are excited to be negotiating with multiple wallets and UI to integrate Maya, with a new integration expected every 1-2 weeks.
* Rewards for Maya Quests to be sent by week's end; awaiting info from ThorWallet; announcement once ready.
* The claiming period for the $MAYA Airdrop has been extended to May 3rd, 23:59 CST. Subsequent to this date, claiming will be closed and data collected to compute points. An official announcement will be made once the airdrop is completed.

</details>

**#15: April 25, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1650545334320345107**](https://twitter.com/Maya_Protocol/status/1650545334320345107)

<details>

<summary>Key points</summary>

* We've finally achieved a stable version of the network with an incredible 2,000 swaps and an astounding $1 million of accumulated volume in just 4 days! This is only the beginning, and we're already looking forward to our next milestone: 10 million!
* Right now, we're focused on increasing the number of liquidity nodes - the more nodes we have, the more decentralized we become, and the deeper liquidity we have. This will also provide $CACAO with greater stability against any potential withdrawal activity, which has been surprisingly low - a true testament to the confidence in the protocol!
* Maya will integrate $DASH in the next major update, which will create swap routes that are unique to Maya Protocol. This integration will provide more use cases and higher volume, making Maya a more attractive option.
* Mid-term, we will work on other integrations such as KUJI, ADA (might happen earlier than expected), and $AZTEC.
* ThorWallet now enables you to swap all assets in the Web App, and you can now make swaps on mobile via Test Flight and Android.
* The Thorwallet team is working hard to enable LP. We expect this by the end of the week. We are extending the $MAYA AirDrop claiming process for $RUNE holders to four days after ThorWallet enables LP.
* Listing soon: We filling all applications for the CoinGecko listing team for review. We have already listed ourselves on Coinmarketcap. We just need to link the price info.



</details>

**#14: April 17, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1647978356644651008**](https://twitter.com/Maya_Protocol/status/1647978356644651008)

<details>

<summary>Key points</summary>

* We are thrilled to announce that we have successfully donated CACAO! We have an exciting week ahead of us as we eagerly await the activation of trading. We will quickly churn and then we will be ready to unhault the trading.
* MAYA will concentrate on three pillars: Deep Liquidity, Routes (Chains), and Availability. Once trading is enabled, we will focus on onboarding integrations and partnerships, increasing swaps, and boosting volume. These three pillars must be of the highest quality.
* The team expects Rango to smoothly integrate Maya once trading is enabled, as ThorChain is already integrated into Rango. Rango is a Crosschain Dex/ Bridge aggregator that enables interoperability between 53 different blockchains.
* Until 20 April, $Rune holders can still claim the $Maya Airdrop. Once ThorChain reopens the pools, the claiming process will be simplified and the alpha airdrop will occur 10 days later.

</details>

**#13: April 10,2023.**

[**https://twitter.com/Maya\_Protocol/status/1645449202141364224**](https://twitter.com/Maya_Protocol/status/1645449202141364224)

<details>

<summary>Key points</summary>

* We expect the final stage net to be available on Tuesday, which will include an update, churn, $CACAO donation, and trading activation. Keep yourself updated by following our [Discord](https://discord.gg/mayaprotocol) & [Twitter](https://twitter.com/Maya_Protocol).
* We're developing a Community Ambassador initiative aimed at expanding our presence in untapped markets and communities, including Dash and Kuji.
* We delayed the donation event due to incomplete and error-filled memos. The issues were resolved promptly, but we urge our community to handle funds with care and responsibility.
* Genesis Nodes protect with trust, Liquidity Nodes with incentives. Liquidity Nodes can boost their capital efficiency by earning both Validator and Liquidity Provider rewards.
* Your ideas and feedback are invaluable to us. To contribute to Maya, review our Gitbook, or if you are a senior cosmos developer, contact us. More eyes on the code mean better outcomes.
* Check the complete Maya [episode](https://twitter.com/i/spaces/1OyJAVNvZabxb?s=20), and stay updated on our latest news by joining our [Discord](https://discord.gg/mayaprotocol).

</details>

**#12: April 3, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1642894658803232769**](https://twitter.com/Maya_Protocol/status/1642894658803232769)

<details>

<summary>Key points</summary>

* Maya Protocol raised $11,421,176.19 and will start operations with approximately $23 million TVL.
* We're preparing for the $CACAO Donation and will enable trading as soon as possible with minimal slippage.
* %81 of Liquidity was allocated to Tier 1 which exceeded expectations.
* Having unique chain integrations, such as Dash & Kuji, is a top priority to have attractive swap routes.
* 20k $MAYA has been airdropped and THORWalletDEX is working to make them visible with correct balances on both iOS (testflight) and the web interface.
* Maya Protocol is working on a non-reflexive pool during the summer. Stay tuned for more details.

</details>

**#11:** **March 27, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1640368305969016833**](https://twitter.com/Maya_Protocol/status/1640368305969016833)

<details>

<summary>Key points</summary>

* We have raised over $6 million with 3 days left to go! Maya Protocol is now officially viable, and we will be launching $CACAO on the 31 of March.
* If you're Cosmos SDK savvy and want to help us to grow the ecosystem, reach out! We are looking for more hands and ideas.
* A few days ago, Thorwallet figured out a way to add liquidity through Xdefi with Ledger.&#x20;
* We are polishing the last details for the first batch of Maya Rewards for Tier 1 liquidity providers, so stay tuned as the airdrops are about to begin.
* El Dorado intends to join Maya's ecosystem by introducing a swap platform to provide users with more options and increase exposure. The platform will go live following the $CACAO event and activation of Trading.

</details>

**#10: March 21, 2023.**

&#x20;[**https://twitter.com/Maya\_Protocol/status/1637869579471839235**](https://twitter.com/Maya_Protocol/status/1637869579471839235)

<details>

<summary>Key points</summary>

* After opening up the $Rune pool, we went from 800,000 to 4.2m 48 hours later!
* 93% of the liquidity raised this far is Tier 1, meaning that for 200 days, $CACAO will have a stable price at launch.
* What's next? 10M, this is an important number because after we open up the savers and with a fifty percent sync cap 30 to 40 dollar mark on TVL would be already 1/3 of the Thorchain pool.
* After that, our goal is 20M. Pools would be competitive; why is this number so important? We will be taken more seriously, and other interfaces and wallets can't help to notice us.

</details>

**#9: March 14, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1635355237258248192**](https://twitter.com/Maya_Protocol/status/1635355237258248192)&#x20;

<details>

<summary>Key points</summary>

* Tested churning with our own nodes in another Stagenet. Everything worked perfectly. On March 13th at 4:00 p.m. CST we had a smooth and successful churning event of our genesis nodes to Stagenet.
* We will churn Genesis nodes to the Mainnet today and complete a system check. You can follow our progress more closely on the #genesis-nodes channel on discord [https://bit.ly/3LnnPQx](https://t.co/44rvV88wzO)
* The extra $MAYA token rewards will be happening from Wednesday at 10 a.m. CST to Sunday at 10 a.m.

</details>

**#8:** **March 7, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1632908951804157952**](https://twitter.com/Maya_Protocol/status/1632908951804157952)&#x20;

<details>

<summary>Key points</summary>

* Minting the first block, having all genesis nodes churn in, and opening up THORWallet and inbound addresses publicly for all to add funds will not happen simultaneously.

-   Here's a more realistic timeline:

    -Tuesday (07/03): Mainnet launched, and the first block minted.

    -Wednesday (08/03): Sanity checks and internal testing.

    -Thursday (09/03): Churn once the last Genesis Node reaches the Ethereum Network block tip.

    -Friday (10/03): Significant liquidity can be added if everything looks good.
- MAYA allocation model is changing to be time-dependent instead of being dependent on the first inflows to pools. The first 75% of the award per pool will be given to people adding T1 liquidity from Friday, March 10th to Tuesday, 14th.

</details>

**#7:** **February 28, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1630236344772448256**](https://twitter.com/Maya_Protocol/status/1630236344772448256)

**#6: Maya & Thorwallet - February 21, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1627700335836790787**](https://twitter.com/Maya_Protocol/status/1627700335836790787)

<details>

<summary>Key points</summary>

* THORWallet is currently in the last steps of testing. The Liquidity Auction is completely integrated into the mobile app and currently working on the previous stages of the web app integration.
* Halborn is currently working on the DASH bifröst audit. Our team prioritized this, and the final report will be available soon. Next on our list is [$AZTEC](https://twitter.com/search?q=%24AZTEC\&src=cashtag_click), so stay tuned for more updates.
* **NEW:** Maya's cash-back program. Provide liquidity through a wallet and receive a link to share with friends. If they use your link to provide liquidity, you earn a percentage of their liquidity.

</details>

**#5: Maya & Halborn** - **February 13, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1625132975607418880**](https://twitter.com/Maya_Protocol/status/1625132975607418880)

<details>

<summary>Key points</summary>

* Halborn is giving Maya an audit of the code and a 360º analysis of new security measures in the market and economic security assessments.
* &#x20;Once Maya’s code audits are done, Aztec will be the next in line.&#x20;
* Maya’s first stage was to learn and research about THORChan: how and why it was implemented that way and how they Maya could manage to cover the same issues, and more. The actual stage consists in implementing.&#x20;

</details>

**#4: February 7, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1622746989724110848**](https://twitter.com/Maya_Protocol/status/1622746989724110848)&#x20;

<details>

<summary>Key points</summary>

* The team is working on updating Maya to the TC version that supports savers, V101, and savers will be available at launch.
* Stage testing will be started when V101 is ready, and a stage liquidity auction will be conducted around Feb 20th.
* The THORWallet app has the tiers part of the liquidity auction integrated, and an affiliate program will be launched with THORWallet, rewarding $Maya tokens.
* There are plans to reward the first Tier 1 liquidity providers under 5M USD with 1% of $MAYA tokens to incentivize joining the LA early.
* Any wallet with a custom memo function can send funds to the LA, and every integrated chain affects the difficulty of integration.

</details>

**#3: January 31, 2023.**

**Maya & Thorstarter** [**https://twitter.com/Maya\_Protocol/status/1620210390641659912**](https://twitter.com/Maya_Protocol/status/1620210390641659912)&#x20;

<details>

<summary>Key points </summary>

* Thorwallet is looking for ways to incentivize people to participate in the Liquidity Auction, and more details will be shared later.
* Halborn Security has performed six security audits for Maya, and they are waiting for the final report to end the process.
* Aztec is coming soon, and CosmWasm and IBC will be enabled from the start for powerful composability.

</details>

**#2:** **January 24, 2023.**

[**https://twitter.com/Maya\_Protocol/status/1617673467599687680**](https://twitter.com/Maya_Protocol/status/1617673467599687680)

<details>

<summary>Key points </summary>

* Liquidity nodes have been improved by bonding LP units with profound intentions, resulting in higher yield, more people adding liquidity, more pool depth, and more affordability.
* Genesis nodes are motivated by a desire to protect users and are open to more than six nodes willing to put their reputations on the line.
* Economic incentives for nodes will be significant from the start, and genesis nodes will eventually be churned to make the system more secure and sustainable.

</details>

**#1:** **January 17, 2023.**

[**https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20**](https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20)

<details>

<summary>Key points</summary>

* The latest Halborn Audits report shows no major vulnerabilities in Rune & Maya pools. Full report will be shared with the community in the following weeks.
* Supported tokens on the liquidity auction: $BTC, $RUNE, $ETH, and some stablecoins, with plans to add $KUJI, $Osmos, $BSC, and $BNB after the auction to reduce risks and increase security.
* The first three Maya Nodes will be run by @THORWalletDEX, @Thorstarter, and @Maya\_protocol, with an opportunity for organizations with a stake worth value to join the genesis nodes.

</details>

## Twitter Spaces with other hosts

**Dash is live on the Maya Protocol:** [**https://twitter.com/Dashpay/status/1684933932645539840**](https://twitter.com/Dashpay/status/1684933932645539840)

<details>

<summary>Key Points</summary>

* We are thrilled to have this Twitter spaces to announce that the Dash integration with Maya is finally live.
* We are attempting to address CEX's large market share, KYC, over-regulation, and misused funds by offering an anonymous, straightforward way to exchange Bitcoin for Ethereum, etc.
* The objective for the coming days is to increase liquidity by connecting with Dash-associated orgs, such as Dash Incubator, DAOS, and Dash Core Group; encouraging those enthusiastic about the integration to network and publicize.
* All passionate thinkers from the Dash & Maya communities can request guidance or contribute to the Maya's ecosystem by joining the Dash & Maya Discords. This project belongs to you.
* El Dorado is a UI providing low-cost, secure asset trading, storage, and growth services linked to the Maya Protocol and Aztec, a central incubator.
* You can now supply liquidity on El Dorado and see the dash balance. They are currently working on swaps to be available as soon as possible.
* We are so glad to find a community like Dash focused on financial freedom because that's why we are all here.
* No central exchange necessary; Maya, a decentralized exchange, enables buying, selling, and swapping of decentralized crypto. A non-custodial solution provides asset autonomy; users determine when assets are needed.
* Dash and more chains offer communities an opportunity to utilize their assets through permissionless/ no-kyc swaps and trading.

</details>

**Cosmos Club & Aaluxx:** [**https://twitter.com/CosmosClub\_/status/1622881702233141249**](https://twitter.com/CosmosClub_/status/1622881702233141249)&#x20;

**LPU & Maya:** [**https://twitter.com/i/spaces/1OwxWwqXXaVxQ**](https://twitter.com/i/spaces/1OwxWwqXXaVxQ)&#x20;

**AMA with Danku:** [**https://twitter.com/Maya\_Protocol/status/1616118122137657345**](https://twitter.com/Maya_Protocol/status/1616118122137657345)

**THORWallet x Maya integration:** [**https://twitter.com/THORWalletDEX/status/1597528829207392256**](https://twitter.com/THORWalletDEX/status/1597528829207392256)&#x20;

**Liquidity Auction Tier ROI calculator with Kyle Krason:** [**https://twitter.com/Maya\_Protocol/status/1630723295254548482**](https://twitter.com/Maya_Protocol/status/1630723295254548482) [\
](https://twitter.com/NACSTWEETS)
