# Philosophical perspective FL

We truly believe that our Fair Launch process is one to be proud of.  Learning from the experience of other protocols and DAOs, we came across what we think is something really open to anybody in the DeFi space to participate in. Compared to an IDO, where investors with large amounts of tokens can manipulate the price and cause disadvantages for the rest, in Maya, there is no minimum entry ticket. There is no previous whitelist, no special allocation for larger investors, and the time range is wide and pre-announced.&#x20;

In the end, we are pushing towards the objective of having one more protocol in a network of decentralized, Layer Zero cross-chain facilitators that dictate prices over the market. We want to concentrate the markets’ liquidity there instead of how it currently concentrates around centralized venues, and that is why we are looking to attract a diverse and wide user base that will become part of a community from the moment they get their first tokens.&#x20;

The process also takes advantage of the built-in Asymmetrical Liquidity functionality from the THORChain codebase. We aim to: &#x20;

A. Reduce Founding Team risks.&#x20;

B. Make a decentralized protocol that is completely owned by its community.&#x20;

C. Create incentives for the Founding Team to continue developing over the long term.&#x20;

D. Bootstrap the largest amount of external asset liquidity possible to secure the sustainable future of our protocol.

How does the Liquidity Auction tackle these issues? Let’s look at its advantages:&#x20;

**01 The community ends up owning the token**&#x20;

So the system governance is decentralized and permissionless. No founding person or investor can pump & dump, rug pull, etc. The team gets only a percentage of the fees, which means we only earn money if the community does. The team simply cannot create sell pressures for the token.

**02 Symmetry of information**&#x20;

Everyone has the same chance to participate during the 21 days duration of the auction. There are no discounts, no privileged information, front running or unfair allocations. Everyone has the same chance to make the same ROI as everyone else during the launch regardless of how much money is raised and what kind of assets they contribute. There are no disincentives to share the liquidity auction details with others since everyone gets the same terms regardless of participation size and depth.&#x20;

**03 Dynamic inflation**&#x20;

To prevent situations where too much $CACAO sits idle outside of our pools, the concept of Dynamic Inflation has been introduced.

Whenever more than 10% of all issued $CACAO is withdrawn from the liquidity pools, new $CACAO emissions will occur at a rate of (1- y) \* 40% + 1%, where y is the total number of $CACAO in our pools divided by the total number of existing $CACAO tokens.

Newly minted $CACAO is then distributed in equal proportions to current depositors (i.e., LPs) and to Node operators (via system income, according to the incentive curve). This has the nice effect of leaving all participants in constant economic conditions, except for those holders of $CACAO outside of the system, who now have inflationary erosion and a bigger incentive to become LPs again.

Here’s an example of how Dynamic Inflation could look under different y scenarios:

<figure><img src="https://lh4.googleusercontent.com/-jcB9KQJnUp2YsPhEpLZ4aFYpd-IvxPkfRgeWbY72mzAqZ7NlV3PfPXocFary7YxJHOuAisROr70DcANDSy30HfDi_dmRrBTq2TfYQ7aIYgVH42L8GVnQDobD5_8N6yo6FM52Vj15skZcobHt69xSbJv7VDxHLYR2cQSygeWNP2G1H6QzK-FpNID2y3CxA" alt=""><figcaption></figcaption></figure>

**04 Large incentives to participate**

Remember, there will not be any other $CACAO issuances, so anybody that wants to own the token will have to acquire it from somebody that got it during this mint. It is very likely that $CACAO’s price will be the cheapest ever (in $BTC terms) right after the auction. This makes it more attractive for people to invest heavily during the Liquidity Auction —which is, of course, what we want, as it leads to deeper pools, reduced slippage and slip fees, attractive arbing opportunities, and overall liquidity depth. Deep liquidity attracts swap volume.

**05 Simplicity**&#x20;

Only one open permissionless cross-chain liquidity event to rule them all. The rules are clear: there are no KYC processes, people will have to understand and use Maya to participate —the Liquidity Auction will serve as a live Demo to our target participants—, the whole thing happens during an extended period of time, and everyone participates under the same conditions. Everything is also managed directly in the Maya Blockchain, so it becomes very secure, and everyone becomes a liquidity provider!

A Liquidity Auction simply makes sense to secure the long-term future of Maya. It keeps us honest as a team, it gives everyone a fair set of rules to participate in, and it will surely raise significant resources to start up a virtuous cycle for our liquidity blackhole. By having only one event, we are ensuring it will be simple, interesting, and even urgent for anyone to participate while helping Maya jump into the big leagues!
