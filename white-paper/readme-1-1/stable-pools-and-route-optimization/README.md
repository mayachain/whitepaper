# ⚖ Stable Pools & Route Optimization

Optimizing can only be done when there’s more than one option. What if we break transactions into smaller trades that always take the path of least resistance? Can we do this while increasing Maya’s internal economic value and creating healthy demand for $USc?

### ELI5

1. Liquidity Pools are piles of cryptocurrencies locked up inside smart contracts, usually in pairs, by people called “Liquidity Providers” (LPs). DeFi users can buy from or sell to these smart contracts at all times, and the price that they can give or ask is balanced out by the ratio between the assets inside them.
2. Assets usually have to be contributed into Liquidity Pools in equal USD denominations. For example, in a typical ETH / $CACAO pool, LPs would need to input $100 USD worth of $CACAO for every $100 USD worth of ETH. This requirement might not be ideal for many investors who do not want exposure to the price fluctuation of $CACAO or who do not want to risk having Impermanent Loss.
3. In addition to our $CACAO pools, we have designed special Liquidity Pools that pair assets against our stablecoin $USc, that enjoy Impermanent Loss Protection, and are enhanced with slip-fees optimizers to attract more institutional and conservative investors. Internally, our systems will route all trades in the most efficient way using both our regular and our Stable Pools to give our users better prices and bigger notional depth. We hope that external DEx User Interfaces like this model of pools connect to them, in combination with THORChain’s.

<figure><img src="../../../.gitbook/assets/WHITE PAPER GRAPHICS -13.png" alt=""><figcaption></figcaption></figure>
