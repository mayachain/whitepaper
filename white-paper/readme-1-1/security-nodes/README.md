---
description: >-
  Exporting security to other Application-Specific-Blockchains and creating a
  Maya Economy.
---

# 🔒 Security Nodes

Maya Protocol is designed to be safe, useful, and solvent to attract Liquidity Providers and facilitate cross-chain exchanges. Due to its economical design and incentives, it has the ability to export this  Security and Solvency to other listed chains while sharing $CACAO tokens. Maya can remain a conservative space even though $CACAO can be used in other, more flexible, or fast-growing environments, creating new use cases and demand for it while bringing back more economic activity to the Maya Economy.

### ELI5&#x20;

1. Maya is, by design, a very solvent, very secure, and very censorship-resistant network. It also has the tradeoff of not supporting some interesting capabilities like smart contracts, DeFi, derivative products,  NFTs, etc.&#x20;
2. Maya could export its security and solvency architecture to other side chains by sharing the same nodes and the same native token —$CACAO. This can be accomplished with triple redundancy by having an IBC bridge, Bifröst equivalent —and what we call a  “Security Nodes” model. In exchange for securing alternative chains, Maya Protocol can earn fees or taxes differently.
3. Alternative chains could have many functionalities and economic activities that benefit $CACAO and the whole Maya ecosystem. As long as it is done within certain limits and parameters, there is little to no downside in having more chains.

<figure><img src="../../../.gitbook/assets/WHITE PAPER GRAPHICS -39.png" alt=""><figcaption></figcaption></figure>
