# Economic overview SN

Let’s see an example of how one of these Application-Specific  Blockchains (ASBCs) could connect to Maya and what the economic implications would look like. What we describe here also holds for other,  consequent chains, although an effort has to be made not to have too many of them just doing the same things and being redundant.&#x20;

For the sake of this exercise, we will call Maya’s chain “Chain A” and a new, arbitrary, cosmos-based utility chain “Chain B”.&#x20;

Both Chain A and Chain B have their own treasury. This treasury is meant to accumulate $CACAO and other assets with time, holding them during economic expansion times and spending them during economic contraction times. They would also generally behave programmatically, according to various parameters set by and modifiable by each chain's set of nodes’ consensus.&#x20;

### Chain A looks like so:&#x20;

All of its native token —$CACAO— was minted and distributed at launch to early liquidity providers who brought external assets with them. Its liquidity is useful and productive, constantly exchanging and generating yield for its bearers.&#x20;

Liquidity Providers can seek higher yield if they upgrade into Liquidity  Nodes by bonding their Liquidity Provider Units and if they are the highest bondholder during the next node churn round. &#x20;

Liquidity Bond Wars emerge where participants try to acquire enough  $CACAO to pair with other native external assets to beat their peers and become active nodes. Nodes have big stakes in Maya, half of which are made up of $CACAO.&#x20;

$CACAO liquidity is very sticky since the unbonding process takes time.&#x20;

### Chain B comes to the stage…&#x20;

Chain B needs $CACAO to work, and it needs to import it from Chain A since they cannot mint it themselves. The easiest to acquire it should be via the Liquidity Pools inside Chain A, “paying” for it with other native assets, such as BTC or ETH. This process increases Chain A’s TVL, generates some nice swap fees (i.e., protocol revenue), and decreases the $CACAO supply inside Chain A. After their acquisition, the tokens can be routed through the IBC into Chain B, which was always programmed to recognize it as its native token.&#x20;

<figure><img src="../../../.gitbook/assets/WHITE PAPER GRAPHICS -44.png" alt=""><figcaption></figcaption></figure>

### Sharing nodes, limits on $CACAO withdrawals…&#x20;

Nodes in Chain A can choose if they want to become nodes for Chain B or not, mainly based on their interest in the $CACAO fees being generated by the economic activity happening there (via Smart Contracts, NFTs, or any other functionality attainable in the  CosmosSDK framework). Becoming a node in Chain B would require them to post a  second bond, denominated in $CACAO, and since we do not support delegation, this bond would be their own skin in the game.&#x20;

We require that all nodes in Chain B already be active validators in Chain A; if you are kicked out of the node count in Chain A, you are also kicked out of Chain B. Without this, Chain A could maliciously choose to cut off Chain B and remove all its liabilities from its balance sheet, harming the ecosystem of Chain B and whoever holds assets there.&#x20;

It is important to have aligned interests between both chains because a massive return of $CACAO from Chain B to Chain A could have very negative, volatile, or inflation-like effects, like the price decreases too much or it is swapped back to external native assets that then leave Chain A’s balance sheet. In fact, we start perceiving any $CACAO outside of Chain A as somewhat of a liability.

If we think of it as a liability, then why permit it? For the same reason, any bank or business issues debt to set itself up for growth. If used productively, it can create synergy for the whole system too.&#x20;

We also believe that we can design a system that handles these debts/productive liabilities and keeps a healthy balance sheet through economic expansions and contractions by balancing out how much $CACAO is allowed to be withdrawn, how the system’s treasury takes profits during economic expansions (in the form of fees or taxes) and how it uses them during economic contractions.&#x20;

These parameters would be taken care of via the “Degrees of Freedom” of the system, modifiable by nodes in Chain A and Chain B.&#x20;

### Degrees of Freedom: &#x20;

1\. Max Debt to Chain B.&#x20;

2\. Dynamic Inflation parameters on Chain A.&#x20;

a. Participation Rate for minimum inflation.&#x20;

b. Participation Rate for maximum inflation.&#x20;

c. Minimum Inflation.&#x20;

d. Maximum Inflation.&#x20;

e. Treasury Cut.&#x20;

3\. Percentage of fees from Liquidity Pools in Chain A that connect to Chain B  assets through Yax Bridge.&#x20;

4\. Virtual Depth for slip fees when exchanging between $CACAO in Chain A  and $CACAO in Chain B.&#x20;

5\. Percentage of transaction fees in Chain B.&#x20;

6\. Exclusivity on positive arbitrage between Chain B and Chain A when the limit is reached, and $CACAO in Chain B reaches a higher price than $CACAO in  Chain B (we call this “Marginal Wealth tax”).&#x20;

Nodes must carefully balance out these parameters for the system treasury  to extract value from the side chain economy in a reasonable manner. Too  little value extraction would mean the Maya Economy would not be  prepared for economic downturns, too much value extraction would  suffocate the sidechain’s economy. &#x20;

Nodes can tweak these parameters on the fly to set things up for any developing economic situation, and they do this with the typical 67% majority consensus model. Nodes hold around 80% to 90% of liquidity in Maya, half of which is in $CACAO, so it is in their best interest to do the best job possible at adjusting these values.

### Example Degrees of Freedom&#x20;

**1. Max Debt = 10%**&#x20;

This means only 10,000,000 $CACAO can leave Chain A in Chain B&#x20;

**2. Dynamic Inflation**&#x20;

a. Min = 0%&#x20;

b. Max = 35%&#x20;

c. Min Participation = 90%&#x20;

d. Max Participation = 50%&#x20;

e. Treasury Cut = 20%&#x20;

This means there will be 0% of inflation at a 90% participation rate, i.e., when more than 90% of the Total Supply of $CACAO is in liquidity pools in Maya —provided by both nodes and LPs. &#x20;

Inflation would appear and increase linearly, up to 35%, at less than a 50% participation rate.&#x20;

20% of any newly minted $CACAO would go to Chain A’s treasury, the rest into  Pools.&#x20;

**3. Pool Tax = 10%**&#x20;

This means the Maya treasury would collect 10% of all yield generated by any  $CACAO\_Chain\_A / $CACAO\_Chain\_B or $CACAO\_Chain\_A / $TOKEN\_Chain\_B  pools.&#x20;

**4. Virtual Depth Tax = 1,000,000 CACAO on either side (could be  asymmetrical)**&#x20;

This means the treasuries would collect slip fees whenever $CACAO is sent from  Chain A into Chain B or vice versa as if there was a liquidity pool with 1,000,000  $CACAO on either side. Since no LPs are involved in this process, all the proceeds go to the correspondent treasury, i.e., They go to Chain A’s Treasury when $CACAO goes from Chain A to Chain B and to Chain B’s treasury when $CACAO goes from Chain B to Chain A.&#x20;

**5. Sidechain Fee Tax = 10%**&#x20;

This means that much of the fees generated in Chain B will be redirected to its own treasury. These fees can include gas fees, transaction fees, swap fees, and all others.

**6. Marginal Wealth Tax = 1 (this parameter is on or off).** &#x20;

Whenever the Max Debt threshold is reached, arbitraging between Chain A  and Chain B becomes impossible, leading to a fragmented market.  Marginal Wealth Tax gives Chain A’s treasury exclusivity over this arbitrage trade by allowing it to surpass the Max Debt Limit.&#x20;

When the price of $CACAO inside Chain B normalizes, these tokens are then exchanged for external assets inside Chain A, and the treasury nets positive returns.&#x20;

Chain B’s treasury can use the newly input $CACAO to buy assets, such as Maya Synths, and sends them back to Chain A’s treasury, which then proceeds to sell the Synths for Chain A $CACAO, closing the loop. Chain A profits some external assets —which it no longer owes to any synth minters— and in $CACAO.&#x20;

This can happen for as long as $CACAO’s price is higher in Chain B than in Chain A. &#x20;

If, for any significant reason, consensual nodes decide that it is necessary,  they can also dramatically decrease the fees charged when sending $CACAO  from Chain B to Chain A while leaving the opposite path untouched —this would repatriate $CACAO slowly over time— or even inflate Chain A’s $CACAO  supply to make Chain B’s $CACAO represent a smaller percentage of the total supply.&#x20;

### The mechanism requires the following simple set of rules:&#x20;

1\. Wallets in Chain B can always send $CACAO back to Chain A.&#x20;

2\. Wallets can only send $CACAO from Chain A into Chain B if the transaction does not contravene the Max Debt limit.&#x20;

3\. Whenever Marginal Wealth Tax is 1, Chain A’s treasury can send  $CACAO to Chain B above the Max Debt limit. Whenever it is 0, no one can.

Some simulated scenarios &#x20;

**Let’s analyze four possible  market conditions:**&#x20;

1\. Chain A grows relative to Chain B.&#x20;

2\. Chain B grows relative to Chain A.&#x20;

3\. Chain A contracts relative to Chain B.&#x20;

4\. Chain B contracts relative to Chain A.&#x20;

**Chain A grows relative to Chain B.**&#x20;

a. This can happen if the overall crypto market cap or TVL increases, for example, or if demand for cross-chain swaps surges suddenly.&#x20;

b. $CACAO would repatriate organically to Chain A, given that its price would be higher there. These repatriated tokens could be either used as a  trading pair or to extract external assets, and, in this regard, repatriated $CACAO would be slowing Chain A’s growth down; the lesser of these tokens that come back, the better for both chains.&#x20;

c. $CACAO in Chain B becomes more scarce gradually, which in turn protects its economy and the security budget held by its Nodes. The dual chain system allocates capital naturally and assures both chains grow as much as they are warranted to grow.&#x20;

d. Chain A should seek to reduce Chain B's taxing proportionally.

**Chain B grows relative to Chain A.**&#x20;

a. This can happen if Chain B’s economy booms, isolated to the rest of the market or to the demand for cross-chain swaps.&#x20;

b. $CACAO would expatriate from Chain A into Chain B given that the price there would be higher. Expatriated $CACAO would catalyze growth inside  Chain B while diminished supply in Chain A would increase its security budget. &#x20;

c. Chain B would attract Chain A derivatives (Synths), and all of the expatriated  $CACAO would have been acquired via swaps from external assets inside  Chain A.&#x20;

d. Both treasuries would be collecting taxes actively out of Chain B’s growth.  $CACAO supply increases in Chain B until reaching Max Debt, after which  Marginal Wealth Taxation is triggered.&#x20;

**Chain A shrinks relative to Chain B.** &#x20;

a. This can happen if the overall crypto market cap plunges or if the demand for cross-chain swap falls while Chain B’s activities thrive.&#x20;

b. Chain B’s $CACAO inflow quickly reaches Max Debt while Chain A’s supply is reduced.&#x20;

c. Chain A’s treasury can start arbing $CACAO with exclusivity into Chain B, which would pocket some profits. This would keep reducing $CACAO’s supply in Chain A, creating incentives for new external capital inflows. &#x20;

d. More fee volume in Chain B would ensure that more nodes try and compete to make it into this chain’s node roster, but since being a node in  Chain B requires a node in Chain A, both chains’ node liquidity and healthy competition are enhanced.&#x20;

e. All of the most representative assets of Chain B would be available to trade inside Chain A’s liquidity pools which would bring external capital and swap volume to Chain A.&#x20;

f. Both Chain A’s and Chain B’s treasuries could use any of their capital resources to stimulate Chain A; they could buy synthetic assets, add LP  positions, donate $CACAO into any pools they find convenient, or execute any other strategies that the nodes may adopt by a supermajority vote. &#x20;

g. Chain A would be strengthened and better prepared to weather any potential economic downturn while the markets recover, so client demand comes back or the Maya team adjusts or delivers any required code or strategy upgrades.&#x20;

**Chain B shrinks relative to Chain A.**&#x20;

a. This can happen if Chain B’s economics dwindle or if the utility or demand for its services wanes o, while Chain A’s thrive.&#x20;

b. Chain B $CACAO would repatriate into Chain A organically, making some slip fees for the latter’s treasury. &#x20;

c. $CACAO supply in Chain A would increase, and overall price and purchasing power would decrease. This could prompt participants to swap back into external assets and exit the Maya ecosystem.&#x20;

d. Depending on the total fees collected and economic activity generated by Chain B, the chain could have still done more good than harm. e. At some point - and if Chain B’s economics still make sense - $CACAO  would stop leaking out because there would be so little of it that its purchasing power would increase considerably.&#x20;

f. Both Chain A’s and Chain B’s treasuries could use any of their capital resources to stimulate Chain B and provide any assistance it could use until its markets recover, demand for its services comes back, or the developing team adjusts or delivers any required code or strategy upgrades.

All in all, we believe that this model of interconnected chains provides a lot of potential upside for the ecosystem with little to negligible downside, as proved by the described four scenarios before, and as long as it's done securely and within certain limits; code for these interconnections would be carefully audited before activating in all cases.&#x20;

**Third-Party Chains** &#x20;

At Maya, we are thrilled with our current roadmap, which already includes a few of these chains planned for development and launch), and yet, any external team can decide to build a chain that is secured by the Maya Protocol architecture.&#x20;

We recommend that they create a $MAYA-like token and a Maya fund-like vehicle that benefits their teams via a protocol fee revenue model since no new $CACAO will be minted with any new chain additions. &#x20;

Launching along these lines, by the way, has several benefits for any developer team,  compared to launching a sovereign chain, including:&#x20;

1\. Bootstrapped liquidity! This is certainly vital and tremendously attractive. 2. Full compatibility and access to the Maya Protocol’s economy. Derivative assets can be included within the Maya pools.&#x20;

3\. Sharing of $CACAO, a token with significant value and an established purchasing power.&#x20;

4\. Solid security, provided by a capable network of highly-invested,  censorship-resistant nodes from block #0.&#x20;

5\. Synergic professional relation with the Maya Protocol team:&#x20;

a. Friendly access to our network and community. &#x20;

b. Support from within our experienced technical team. &#x20;

c. We will support or pay for the necessary code audits that functional projects, successfully tested in our Maya Stagenet, might require.&#x20;

External developers can focus on building with this platform instead of starting a  chain from scratch, which requires many different skills beyond coding.&#x20;

**Long Tail Chains & Assets**&#x20;

If at any time the Yax Bridge connecting other chains into Maya is saturated with too many requests, we could launch a secondary Maya chain (a fork) and connect it using the Security Nodes model to support the long tail assets and chains, simultaneously increasing supported chain capacity, assets, $CACAO  demand, network value and transactions per second.

Said fork would aggregate inbound transactions from “Maya 2” to “Maya 1” through the IBC to facilitate exits of outbound short-tail assets. Likewise, inbound transactions from “Maya 1” could aggregate to “Maya 2”.&#x20;

### On Sovereignty and Independence.&#x20;

A situation where Chain B becomes much more successful than Chain A can happen too… In this case, Maya could become a burden rather than a safety net, and the community could try and vote to separate into an independent project. &#x20;

This scenario would have the following consequences: &#x20;

1\. The IBC would be taken down. $CACAO can no longer be sent interchangeably.&#x20;

2\. Chain B’s $CACAO would be renamed to $BCACAO or whatever other,  different name. &#x20;

3\. Chain B’s assets would remain inside Maya Pools if they were there already. A new pool, $CACAO / $BCACAO, can be added if enough liquidity is behind it.&#x20;

4\. The requirement that a Chain B Node must be an Active Validator Node in Maya is deprecated. Chain B Nodes at that moment remain Nodes in  Chain B.&#x20;

5\. Both chains could jointly decide to have a new IBC, where $CACAO sent from Maya to Chain B is no longer native there. Other Maya derivatives can also be sent through IBC to sovereign Chain B. Maya will not accept  $BCACAO within Maya Chain.&#x20;

Maya would end up erasing all the $CACAO liabilities from its balance sheet - since  $BCACAO would no longer be directly redeemable for external assets that could then leave Maya - and would keep all the revenue raised from taxing Chain B  throughout its history.&#x20;

While possible, this scenario is highly unlikely because it works against network effects and network value, which are very important for blockchain ecosystems.&#x20;

### On Death and Taxes. &#x20;

The opposite scenario could also occur, where Chain A’s nodes become disinterested in protecting Chain B if they don’t find the right economic incentives;  users could also simply not use Chain B, or they would migrate to another better chain. Nodes could trigger a Chain Retirement in any of these cases.&#x20;

An advance notice would be communicated for $CACAO to be recalled into Maya over a determined time period (ex. 10 days), and Chain B would be shut down by the nodes thereafter.&#x20;

Finally, all revenues raised by taxes during this process would be kept by the Maya’s  Treasury, which would end in no way worse o  than it was before Chain B was introduced.

\
