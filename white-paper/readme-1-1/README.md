---
description: A next-gen cross chain DEX.
---

# 📖 Maya Whitepaper 2.0

{% hint style="warning" %}
**Disclaimer:** _This document has been revised from its original version to reflect new ideas and certain recent economic adjustments at the time it was created. These tweaks have been previously announced and set to consideration with the Maya Protocol community in our official_ [_Discord_](https://discord.gg/xZjdXBbu7V) _server. **It is important to note that this document currently serves historical purposes and that some information included in it maybe outdated.**_
{% endhint %}

{% hint style="info" %}
Do you prefer downloading or printing? You can use this link to get the [Whitepaper as a PDF](https://assets.website-files.com/62a14669b65c6aeed054f32e/632358a7a50bee6b8298c1cb\_MAYA%20PROTOCOL%20WHITE%20PAPER.pdf). Just remember this is the most updated version until November 2022. We will update it soon with the new adjustments.
{% endhint %}
