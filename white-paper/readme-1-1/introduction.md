---
description: Maya 1.0 - How we started
---

# Introduction

The impressive feat of first designing and building a technically viable, completely decentralized, permissionless exchange is credited to THORChain and its relentless team of developers. Maya Protocol is nothing more than an effort to create a backup system, an alternative to $RUNE and THORChain, for several fundamental reasons outlined below.

Maya Protocol’s team believes four technological breakthroughs will soon change how our current economic systems work. They are:

* Bitcoin’s Proof of Work
* Ethereum’s Smart Contracts
* Tendermint’s simple BFT PoS - easily programmable with Cosmos SDK
* THORChain sovereign blockchain - open-source and cross-chain

Like THORChain, Maya Protocol is a CosmosSDK-powered, replicated state machine to overwatch and coordinate the movement of digital assets, swaps, or stakes without the need to wrap or peg any of them. In our way, we are validating THORChain’s lead and paying tribute to it. The idea to create Maya Protocol was born when a developer of THORChain publicly mentioned that he expected the market to be filled with at least 3 to 5 similar protocols in the future.

**Maya will be the second to market.**

{% embed url="https://www.youtube.com/watch?v=N9peaLkzmKk" %}

Maya Protocol has its native token, called $CACAO. Why this name? We all know cacao is the main ingredient used for making chocolate nowadays. Still, in antiquity, this seed was also used as a medium of exchange and commerce by the Mayan people in what today is the Yucatán Peninsula and Central America.

Here is an overview of why we think THORChain cannot and should not be the only cross-chain DEX out there:

* They need backup as a universal backend provider.
* THORChain cannot grow fast enough to capture all the addressable markets.
* THORChain will eventually hit its TPS limit.
* The technology needs validation.
* There is currently no competition or collaboration.
* Focus on different target markets.
* Compatibility.
* They should not be dancing alone!
* Version Stability.
* Two minds are better than one.

Let’s go through each one of them:&#x20;

**01 Backup as a universal backend provider.**&#x20;

We believe that Cross-Chain Decentralized Liquidity Protocols will serve as the backend to most of the volume moving across wallets, central exchanges, protocols, and crypto businesses. It is very important to have a  backup to any such system in case any problem could affect it and prevent critical failure across the market. Think of somebody carrying a Visa and a Mastercard; both networks generate loads of transaction volume when people use their debit or credit cards to pay for goods and services. However, if, for any reason, the Visa network stopped working, then all those users could still use their Mastercard instead. If THORChain were Visa, then Maya would be MasterCard.&#x20;

**02 THORChain cannot grow fast enough to capture the market.** &#x20;

This is not a lack of trust in THORChain’s ability to grow but rather a statement that stems from understanding the protocol. THORChain (and Maya)  have some virtuous cycles that cannot be artificially accelerated: their security and liquidity growth. One cannot grow without the other, creating a constant “chicken and egg”  problem. Security scales as more nodes join, bonding bigger amounts of  $RUNE —$CACAO, in our case — but if the bonds grow too much, then the liquidity is provided relative to the bonded capital, then the system becomes riskier. Specialized economic incentives continuously optimize this process, but it takes time. We believe there is more demand for liquidity in the market and people willing to provide the underlying necessary bonds than the speed at which THORChain can currently capture it.

**03 THORChain will eventually hit its TPS limit.**&#x20;

Even when THORChain continuously increases the liquidity in the protocol,  eventually, they will hit the Transactions Per Second limit, which sits around  100 - 500 t/s. At that point, swappers will either start clogging the network or need to rely on another protocol; this is where Maya comes in.&#x20;

**04 Providing validation to the technology.**&#x20;

There are still naysayers about what THORChain has created. Once more protocols, like Maya, enter the picture and continue with the mission THORChain set out to do, we will validate the market and increase confidence in this product. Our mission is clear: for Decentralized Exchanges to manage more liquidity than Centralized ones.  Former smart contract DEXes do not have what it takes. We need a new generation of cross-chain Layer Zero DEXs that ​​handle most of the market’s transaction volume in an efficient, simple, quick, and instantly-final fashion.

**05 Collaboration instead of competition.**&#x20;

Some people might think we are competing with THORChain, and some  THORChain supporters might feel threatened by Maya, but this is completely unfounded: our real competition comes from CEXs and traditional DEXs. Any user we bring from those alleys is a net positive for THORChain and Maya. In other words, this is a game of adoption, and Maya will help drive this adoption forward!&#x20;

Any user that comes from a CEX to Maya and then switches to THORChain for any given reason will still make us very happy. We also believe that increasing THORChain’s market share will help Maya Protocol and that the reverse will hold true!&#x20;

**06 Focus on different target markets.**&#x20;

The Market is huge, and although there might be commonality with some of  THORChain’s users —especially hardcore yield seekers— Maya will be focused on the LATAM market and into much less technically oriented audiences.&#x20;

Maya’s emphasis is geared towards DeFi education, even using marketing channels like TikTok and Instagram to inform a segment of crypto users that have not yet been addressed by THORChain or anybody else.

**07 Compatibility.**&#x20;

We believe big institutional liquidity investors and swappers will take advantage of the compatibility between both protocols and that the same will be true for wallets, exchanges, and other platforms. Having code compatibility —due to the forked nature of Maya— will lead to easy implementation for bigger players that cannot rely on only one option. We believe most end users will eventually use THORChain and Maya interchangeably and unknowingly; how we can use VISA and  MasterCard with the same user experience. Every E-Commerce handles both since coded solutions support both.&#x20;

**08 Becoming price leaders together.**&#x20;

Simply put, today, THORChain is dancing in an empty room. The arbitrage opportunities are constantly big since they must be carried out against centralized exchanges and order books. This, in turn, creates more impermanent loss on THORChain’s books, which, although insured through the economy. &#x20;

Having a second identical twin with whom to dance will create tighter arbitrage, distributed amongst both protocols and creating a smaller percentage of economic capture. We believe an ecosystem of  Thorlikes will eventually exist that will dictate the actual prices of assets in a  decentralized fashion. This would further drive down arbitrage value capture as a percentage of Total Value Locked in the protocols, protecting the liquidity capital of both Maya and THORChain. The objective is to create a  network of Layer Zeroes like Maya, THORChain, and others who become price leaders over CEXs. At that point, impermanent losses would be negligible.&#x20;

**09 Version Stability.**&#x20;

Some users look for new features and opportunities; others look for reliability  and dependability. The first group will probably not choose Maya over  THORChain since we will always lag behind them in updates and versions,  making sure the implemented upgrades have been battle-tested first. These  users will be using THORChain to take advantage of its exciting  opportunities and rapid pace, but there will always be room for both groups.&#x20;

**10. Two minds are better than one.**&#x20;

Our community will grow parallel to THORChain’s and, in turn, bring more developers to both networks. Our teams and driving forces can help increase the rate of improvement of the THORChain ecosystem through both cooperation and competition. Additionally, we have come up with ways to further improve the protocol with an innovative multi-chain approach.  Although we will be followers of the THORChain technology, we want to have a proactive approach as well, creating some cool first-mover advantages with new technologies and ideas we have developed, like Stable Pools and  Liquidity Nodes —more on this later.

### Maya 2.0 - Where we are going

THORChain paved the way with their cleverly designed Proof of Bond protocol using Tendermint's powerful consensus algorithm and excellent economic incentives for an ultra-secure Layer-0 protocol. Maya seeks to go even further by increasing the implied capital efficiency, by having more uses for its native currency, $CACAO, and by using the high security of Maya's node infrastructure for other valuable functionalities. It is important to mention that Maya will be backward compatible with THORChain, which means that Maya will inherit ThorFi - into MayaFi. We have been very selective with the changes we make to our protocol, taking care of this compatibility with any future upgrades to the protocols we are emulating. This is very powerful since Maya can enjoy its improvements plus the improvements of the other protocols included in its network.&#x20;

Maya takes security very seriously, which is why the most sensitive aspects of the THORChain protocol, such as Bifröst, were left untouched. Additionally, audits will be conducted periodically before and after our launch to ensure the security of all funds. The safer a protocol is, the more funds it attracts, especially from institutional entities.&#x20;

Maya is committed not only to security but also to decentralization and censorship resistance, especially by governmental bodies that are ever more involved in how we use cryptocurrencies. Because of this, our team will remain pseudonymous, and our nodes will always be encouraged to remain anonymous.&#x20;

We believe that delegation in protocols does not enhance security but harms it. Delegation creates artificially bonded nodes with a higher incentive to become bad actors and an incentive for the node operators to reveal and promote their identities, decreasing overall censorship resistance. For these reasons, Maya Protocol and its multiple components will always work without delegation.&#x20;

Besides, $CACAO holders will have other better uses for their tokens, like generating yield through liquidity providing or lending.&#x20;

So what are we bringing to the table to achieve this mission?

The team, investors, and key strategic individuals will earn through their share of tokenized fees, which essentially means we only earn if we create value for $CACAO holders, liquidity providers, and nodes. The Maya team then designed a different economic model for Nodes to increase Capital Efficiency without compromising Security called Liquidity Nodes. This Security will be used to secure other algorithms through our Security Node design. This feature will secure a popular Burn & Mint algorithm for truly decentralized and safe Stablecoins and Smart Contract compatibility. Some of these stablecoins can be made eligible as a trading pair in Maya for a more stable investment and to create deeper pools with decreased slippage.&#x20;

First, we will make the **fairest launch** possible by holding a **Liquidity Auction**, where all $CACAO ever to exist is shared in one event at the same discovered price by everyone, from the smallest investor to the largest whale. The team, investors, and key strategic individuals will earn through their share of tokenized fees, which essentially means we only earn if we create value for $CACAO holders, liquidity providers, and nodes. The Maya team then designed a different economic model for Nodes to increase Capital Efficiency without compromising Security called Liquidity Nodes. This Security will secure other algorithms through our Security Node design. This feature will secure a popular Burn & Mint algorithm for truly decentralized and safe Stablecoins and Smart Contract compatibility. Some of these stablecoins can be made eligible as a trading pair in Maya for a more stable investment and to create deeper pools with decreased slippage.&#x20;

We hope that the crypto community throughout the world will understand the huge implications of what we have designed and that they understand the importance and necessity of a decentralized, wide base, and permissionless network of cross-chain alternatives. Centralized services in the cryptocurrency world will remain the cheapest option until a critical mass of adoption is reached. Maya Protocol is our team's shot at making this future more likely. Join us in the mission to topple Centralize Exchanges for good.&#x20;

Read along with each of the chapters of this whitepaper to see what we bring to the table. They cover our six unique features in detail, including sections such as an Introduction, Explain to me like I'm five (ELI5), Philosophica perspective, Economic overview, Technical overview, and Code.
