# 😎 Ambassador Program

We are thrilled to announce the launch of our Maya Ambassador Program! As part of our ongoing mission to empower and educate our community, we are introducing this exciting initiative aimed at promoting Maya and all of its benefits. Applications will be open for a limited time, so don't miss your chance to be part of the program!

<figure><img src="../.gitbook/assets/Twitter_Graphic.png" alt=""><figcaption></figcaption></figure>

### What is the Maya Ambassador Program?

The Maya Ambassador Program is a collaboration between Maya Protocol, talented content creators, and key opinion leaders (KOLs) who are passionate about the DeFi ecosystem. Our goal is to expand our reach, create informative content, and engage with a wider audience. In return, we are offering unique rewards and opportunities for our ambassadors, making it a mutually beneficial partnership.

By producing informative content about Maya Protocol, ambassadors can earn up from $500 to $700 in MAYA tokens per month.

### Who is eligible?

Eligibility for this opportunity is open to creators who fulfill the following criteria:

1. Proven Crypto or Web3 Content Creators: You have established yourself as a credible creator in the realm of cryptocurrency or Web3, and you possess a substantial following on any of the popular social media platforms such as YouTube, TikTok, Twitter, and others.
2. Passion for DeFi: You demonstrate a genuine enthusiasm for Decentralized Finance (DeFi) and actively engage with its concepts and developments.
3. Alignment with Maya DNA: You possess a shared sense of identity and values with the Maya community.

### How does it work?

1. Apply to the program (instructions below).
2. Get selected based on your enthusiasm, engagement, and influence within the community.
3. Create content that informs our community about the technical or social aspects of the project, ensuring it is well-presented, up-to-date, and of high quality.
4. Submit content for review.
5. &#x20;Receive your MAYA tokens at the end of every month!

Our team will carefully review the applications and select the most promising candidates based on their enthusiasm, engagement, and influence within the community. The chosen Maya Ambassadors will then create well-presented, informative content in various formats, such as video and text, to introduce Maya Protocol to new audiences.

You can earn up from $500 to $700 monthly in Maya tokens.

**Eligibility for this opportunity is open to creators who fulfill the following criteria:**&#x20;

* Proven Crypto or Web3 Content Creators.
* You have established yourself as a credible content creator in cryptocurrency or Web3.
* Possess a substantial following on popular social media platforms such as YouTube, TikTok, Twitter, or others, with reasonable engagement.&#x20;
* Minimum Audience in at least one of the following:&#x20;
  * Twitter: 3k followers&#x20;
  * YouTube: 6k subscribers&#x20;
  * TikTok: 15k likes&#x20;
  * Medium, Discord, and Telegram - no minimum range - we will review quality individually.

{% hint style="info" %}
Provided numbers are for reference only. Meeting these numbers is not the sole factor in determining eligibility. The overall quality of your content, audience engagement, and credibility within the crypto and Web3 communities are also likely to be considered.
{% endhint %}

### How to apply?

If you're interested in joining the Maya Ambassador Program, please fill this [form](https://docs.google.com/forms/d/e/1FAIpQLScW6tP4NACGXFQPr-rDk0BWbMwCo6\_C5nxeK4gRningwFqgpw/viewform) and we will get back to you ASAP.

Don't miss this incredible opportunity to be part of our growing community and help spread the word about Maya Protocol. Apply now, and good luck!

\
