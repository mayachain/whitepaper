---
description: Maya Protocol's Visual Identity guide.
---

# 🎨 Content Creators Guide

Whether your creative outlet is a YouTube video, an article, a tweet, or a meme, here are a few simple guidelines that will make your content look more polished and consistent with the visual identity of Maya Protocol's brand.&#x20;

### Quick Definitions:

* **Maya Protocol** is an ecosystem that encompasses MAYAChain (Live) & AZTECChain (in development)
* **MAYAChain** is a cross-chain AMM that facilitates cross-chain swaps without the need for bridging or wrapping assets.
* **AZTECChain** is the smart contract layer for Maya Protocol that will enable suite of DeFi solutions to compliment & leverage MAYAChain cross-chain capabilities.

{% hint style="info" %}
For more information and expanded explanations please visit [**What is Maya Protocol**](https://docs.mayaprotocol.com/introduction/readme) & [**How it works**](https://docs.mayaprotocol.com/deep-dive/how-it-works) sections.
{% endhint %}

### Naming Convention:

Note how the names are written. Make sure to use this conventions.

* Maya Protocol: 2 separate words, with only the first letters capitalized for both words.
* MAYAChain: 1 word, with the word "MAYA" and the first letter of "Chain" capitalized.
* AZTECChain: 1 word, with the word "AZTEC" and the first letter of "Chain" capitalized.

### Color Scheme:

<figure><img src="../.gitbook/assets/Screenshot 2023-06-27 at 22.09.21.png" alt=""><figcaption></figcaption></figure>

### Typography:

<figure><img src="../.gitbook/assets/Screenshot 2023-06-27 at 22.14.46.png" alt=""><figcaption></figcaption></figure>

### Assets:

You can find all the logos in different formats on this [**link**](https://linktr.ee/mayaprotocolassets).

### Links:

If you're going to reference any Maya Protocol social media or websites, please use the official [**links**](https://docs.mayaprotocol.com/media/maya-official-links).

{% hint style="info" %}
If you need anymore information, please contact the team directly on our official [**Discord**](https://discord.gg/mayaprotocol).
{% endhint %}

