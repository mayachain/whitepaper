---
description: Maya Protocol's Official NFT Collection
---

# 🎭 Maya Masks

Maya Masks is the NFT collection that visualizes and unites the Maya Protocol community. The Masks are designed to be wearable assets for your favorite Web3 avatar. Whether you're a Bored Ape, Moonbird, or ThorGuard, you can put a Mask on to show that you're a Mayan!&#x20;

<figure><img src="../.gitbook/assets/d04c4d208b0484f5f8614e3e67e02f0ca49359151c567483217fff7922ef822d.png" alt=""><figcaption></figcaption></figure>

**Only 1,689 Genesis Maya Masks exist in the market.** The remaining 4,371 were taken out of supply after the public mint ended. These Maya Masks currently exist on the Ethereum blockchain, and will migrate to Aztec Chain once ready.&#x20;

Within the collection there are 2 main types of Masks; Golden Masks, and regular Masks. Golden Masks are the rarest pieces in the collection and offer increased utility compared to regular Masks. Maya Masks not only represents the Maya Protocol Community, but it is heavily **utility-focused**.&#x20;

Maya Masks main utility is to:&#x20;

* Stake for daily CACAO rewards from the protocol when Aztec Chain is launched.
* Access pass for community events, channels, and future collections.&#x20;
* Receive a one time airdrop of MAYA and AZTEC (Each Maya mask contains 4.5 MAYA & 4.5 AZTEC, 80% is locked in the Mask and the rest %20 [**airdropped**](https://docs.mayaprotocol.com/airdrop/usdmaya-airdrops#maya-mask-holders)).

{% hint style="info" %}
There are 1,000,000 tokens (MAYA/AZTEC). 10,000 will be shared among 1689 Maya Masks.

* 2,500 tokens allocated to 20 Golden Masks (2,500/ 20 = 125 MAYA).
* 7,500 tokens allocated to 1669 regular Masks (7,500/ 1669 = 4.49 MAYA).
* Same calculations apply to AZTEC.
{% endhint %}

{% hint style="warning" %}
MAYA has already been [**Airdropped**](https://docs.mayaprotocol.com/airdrop/usdmaya-airdrops#maya-mask-holders) to Maya Mask holders. AZTEC will be Airdropped after the launch of AZTECChain .
{% endhint %}

OpenSea: [https://opensea.io/collection/mayamasks](https://opensea.io/collection/mayamasks)&#x20;

Contract: [https://etherscan.io/token/0xe00d8f3dca2ac474f4d7f177570f77de0774e754](https://etherscan.io/token/0xe00d8f3dca2ac474f4d7f177570f77de0774e754)&#x20;

Website: [https://www.mayamask.com/](https://www.mayamask.com/)
