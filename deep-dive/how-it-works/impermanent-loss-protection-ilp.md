---
description: What is Impermanent Loss Protection (ILP) and how does it work?
---

# Impermanent Loss Protection (ILP)

Impermanent Loss refers to the temporary loss that liquidity providers (LPs) may experience when providing liquidity to decentralized exchanges (DEXs). It arises due to the constant fluctuations in the prices of crypto assets. MAYAChain aims to address impermanent loss through its Impermanent Loss Protection mechanism.

### Maya Protocol ILP Rules:

1. Impermanent Loss Protection is funded from the Maya Protocol Reserve, which is 10% of CACAO supply (10m CACAO). Refilled constantly and automatically with 10% of the protocol fees (transaction & swap fees).
2. ILP on Maya starts on the 50th day after depositing liquidity, and coverage is capped at 100%.
   * If ASSET outperforms CACAO, full coverage is after 150 days (50 days after initial deposit + 100 days in LP).&#x20;
   * If CACAO outperforms ASSET, full coverage is after 450 days (50 days after initial deposit + 400 days in LP).&#x20;
3. ILP is calculated at the time the user withdraws liquidity.
   * If the ASSET outperforms CACAO, the ILP is 1% daily. So, if the user withdrew after 150 days, they would get 100% protection. If the user withdrew after 100 days, they would get 50% protection.
   * If CACAO outperforms the ASSET, the ILP is 0.25% daily. So, if the user withdrew after 450 days, they would get 100% protection. If the user withdrew after 250 days, they would get 50% protection.
   * If fees cover the Impermanent Loss, the LP isn’t eligible for ILP.
4. ILP is paid and reset after full withdrawal, not affected by partial withdrawals, and only reset but not paid on top-ups.
5. ILP is always recorded and calculated symmetrically (ASSET-CACAO). So even if you deposited asymmetrically (single asset), the deposit values are NOT the amounts the user deposited. They are the immediate symmetrical (ASSET-CACAO) value of what the user deposited.

#### The impermanent loss protection can be mathematically Calculated using the following process.:

1. Calculate the amount to cover in CACAO.
2. Calculate the percentage of coverage.

**Step 1:** Calculate the amount to cover in CACAO.

Where:

A0 = Initial ASSET amount deposited.

A1 = ASSET amount to be redeemed upon withdrawal.

R0 = Initial CACAO amount deposited.

R1 = CACAO amount to be redeemed upon withdrawal.

P1 = LP ratio of the pool upon withdrawal.

_**Amount to cover in CACAO** = ((A0\*P1)+R0) – ((A1\*P1)+R1)_

Example:

Assuming the ASSET is USDT and the LP initial amounts were (100 USDT + 1,000 CACAO), and (80 USDT + 1100 CACAO) at withdrawal, and the LP ratio of the pool is 1% (0.01).

((100\*0.01)+1000) – ((80\*0.01)+1100) = 1001 – 1100.8 = -99.8 CACAO&#x20;

**Step 2:** Calculate the percentage of coverage.

First we need to determine if CACAO was outperforming (400, after 50, days to full coverage) or underperforming (100, after 50, days to full coverage).

In the above case CACAO has been underperforming. So 100 days to full coverage.

{% hint style="info" %}
As a rule of thumb, if you deposit 2 assets, and at withdrawal you got more of an asset and less of the other compared to the initial deposit, the asset you got more of, is the asset underperforming. Think of it like this; people were giving away the less desirable asset and taking more of the other.
{% endhint %}

Now we need to calculate the length of time liquidity has been deposited, to determine percentage of coverage.

Blockchains don’t have clocks, but they have blocks, and we can derive the time from the number of blocks per second. In Maya’s case it’s around 6 seconds per block, or 14,400 a day.

So,

Percentage of coverage = (block no. at withdrawal – block no. at deposit - 50 days of blocks)/ (number of blocks a day \* number of days to full coverage)

Example:

Block no. at withdrawal = 2,456,000.

Block no. at deposit = 900,000.

Percentage of coverage = (2,456,000 – 900,000 -(14,400 \* 50))/ (14,400 \*100) =  0.58 or 58%.

This means that LP will be protected with (99.8 CACA0 \* 0.58) = 57.88 CACAO.

&#x20;

&#x20;

&#x20;
