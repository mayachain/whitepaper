---
description: >-
  Dynamic Inflation is a feature that can be enabled/disabled by nodes voting on
  it. It is NOT the original state of the chain.
---

# Dynamic Inflation

Maya Protocol is a multichain DEX, with CACAO as its native token. The utility of CACAO lies in its ability to facilitate swaps and trades between different assets (BTC, ETH, Dash, etc...). For this to be possible, CACAO needs to be paired with other assets in Liquidity Pools (LP).

&#x20;

Although Liquidity Pools provide yield, they are subject to Impermanent Loss (IL). This is why some users prefer to hold CACAO on its own, rather than in a LP, even though it is more beneficial for the protocol if these funds are present as liquidity.

&#x20;

Dynamic inflation is an autoregulated mechanism designed to favor Liquidity Providers (LPers) over holders/ speculators. If the amount of CACAO held outside LPs exceeds 10% of the non-reserve supply, Dynamic Inflation is activated in order to protect the Liquidity Providers and reduce the value of CACAO held outside LPs.

&#x20;

This is achieved by injecting CACAO directly into pools and Maya Protocol’s system income (which is distributed to LPs, MAYA, and nodes as yield).

&#x20;

Inflation will continue (inflation increases as CACAO held increases) until balance is restored in the system, and the amount of CACAO held outside LPs is 10% or less of the non-reserve supply.&#x20;

&#x20;

The equation is as follows:

&#x20;

y = CACAO in pools/ CACAO total supply \*100

&#x20;

If y < %90, Dynamic inflation kicks in.

&#x20;

Rate of inflation = (1-y) \* 40% + 1%

&#x20;

Here’s an example table: -

&#x20;

| y    | Inflation |
| ---- | --------- |
| %100 | 0         |
| %90  | 0         |
| %89  | %5.4      |
| %80  | %9        |
| %70  | %13       |
| %60  | %17       |
| %50  | %21       |



{% hint style="warning" %}
Dynamic Inflation is a feature that can be enabled/disabled by nodes voting on it. It is NOT the original state of the chain.&#x20;
{% endhint %}
