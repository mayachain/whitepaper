# What is Threshold Signature Scheme (TSS)?

You've probably read that MAYAChain, similar to THORChain, utilizes Threshold Signature Scheme (TSS) to manage its vaults. But, what exactly is TSS and what advantages does it offer?

### Private Keys, Public Keys, and Wallet Addresses&#x20;

Digital Assets/ Cryptocurrencies are stored, or more accurately, assigned to an Address. This Address is derived from the Public Key, which in turn is derived from the Private key. This process is a one way street and can't be reversed (You cannot figure out the Private Key from the Wallet Address). This is achieved using cryptography.

<figure><img src="../../../.gitbook/assets/k.png" alt=""><figcaption><p>Using cryptography techniques, the wallet address is derived from the public key, and the public key from private key, in way that the reverse can NOT occur.</p></figcaption></figure>

Picture a cryptocurrency private key and signing algorithms as a key to a vault. It is a perfect fit - only this key can open the vault. If attackers manage to access the key, they can steal the victim's money and goods, leaving them with no way to get it back. And, if the key is lost, the safe and its contents are locked away forever. To reduce the risk of loss, the vault user can duplicate the key, but then it increases the risk of theft. It's a tricky situation!

<figure><img src="../../../.gitbook/assets/0_T6t4YpbSKA_ag02N.webp" alt=""><figcaption><p>One Vault, One Key.</p></figcaption></figure>

In MAYAChain's case where larger sums of money are at stake, relying on a single entity to protect the vault is an inadequate solution. It's like trying to carry a heavy burden on your own - it's just too much to handle! So, instead of shifting the problem to another single entity (a custodian), why not share the responsibility between multiple parties (in our case, Nodes)? That way, a more secure unlocking system can be established, so that attackers have to steal multiple independent keys to unlock the vault.

### Multisigs

MultiSig (multi-signature) is like having multiple locks on your vault - it's like having a security guard for each lock! Instead of a single key to open the vault, MultiSig creates a vault with multiple locks and keys and assigns the keys to multiple parties/Nodes. Now even if one of the keys is lost or stolen, the attackers won't be able to open the vault.&#x20;

<figure><img src="../../../.gitbook/assets/0_YQsFWkOPZmCWzcPV.webp" alt=""><figcaption><p>One Vault, Multiple Keys.</p></figcaption></figure>

Of course, this means the vault needs to be bigger to accommodate the multiple locks, and it also means that anyone can see it has an unusual security mechanism, making it easier to track, and with higher fees as the transactions need to be encoded with more bytes for the additional signatures.

### Threshold Signature Scheme (TSS)

With Threshold Signatures, each of the Nodes creates a key independently. Then, they forge the vault’s lock together, in a modular way, so each Node shapes a part of the lock that corresponds to its key. Eventually, at the end of this process, a vault with a single modular lock is created that corresponds to each of the keys.

The unlocking process involves the Nodes taking turns in unlocking the vault. Each key can turn the modular lock a few degrees forward. After a few rounds of partial turning of the different keys, the modular lock is fully unlocked.



<figure><img src="../../../.gitbook/assets/0_RIcxJNiHodRSt7Vq.webp" alt=""><figcaption><p>Each key can unlock the lock partially.</p></figcaption></figure>

This modular lock can be shaped so that even if only a subset of the keys are available, the vault can still be unlocked.

This makes TSS superior to Multisig in several ways:

* Cheaper transactions
* Indistinguishable from a standard private key, from the viewpoint of the blockchain.
* Multisig needs different coding development for different types of blockchain (e.g. UTXO vs EVM vs Cosmos SDK, etc..), unlike TSS which can be implemented for any blockchain.

**Sources:**

* Under the Hood: Asgard Vaults, TSS and Node Churns - LP university: [https://crypto-university.medium.com/under-the-hood-asgard-vaults-tss-and-node-churns-4767f3a5624b](https://crypto-university.medium.com/under-the-hood-asgard-vaults-tss-and-node-churns-4767f3a5624b)
* Threshold Signatures: The Future of Private Keys - Zengo Wallet: [https://medium.com/zengo/threshold-signatures-private-key-the-next-generation-f27b30793b](https://medium.com/zengo/threshold-signatures-private-key-the-next-generation-f27b30793b)
* The Keys to Crypto Kingdom: Wallet Address, Public and Private Keys Explained - Blocktrade: [https://blocktrade.com/wallet-addresses-public-and-private-keys-explained/](https://blocktrade.com/wallet-addresses-public-and-private-keys-explained/)
* Bitcoin stackexchange: [https://bitcoin.stackexchange.com/questions/80708/what-is-the-difference-between-your-address-and-your-public-keys](https://bitcoin.stackexchange.com/questions/80708/what-is-the-difference-between-your-address-and-your-public-keys)
