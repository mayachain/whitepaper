# Liquidity Nodes

### Introduction

The Liquidity Nodes model has been designed to address an issue present in many blockchains. To guarantee network security, Nodes are mandated to purchase and stake/bond a predetermined amount of the network's native asset. This serves as a security precaution, as any misbehaving Nodes will be subject to slashing, resulting in the forfeiture of their stake.

These staked/ bonded funds stay idle, with the rewards being subsidized by inflation. While certain networks attempt to counter the inflation through the implementation of burn mechanisms, MAYAChain has proposed an entirely novel approach.

MAYAChain Nodes bond Liquidity Pairs instead of idle funds, which offers three distinct advantages: security, real yield, and capital efficiency.&#x20;

1. **Security:** the slashing mechanism remains in effect, ensuring that any misbehaving Nodes will have their Liquidity Pairs slashed.&#x20;
2. **Real Yield:** there is no need for inflationary rewards; Nodes can earn fees from swaps and transactions.&#x20;
3. **Capital Efficiency:** the funds used to secure the network are now productive, rather than idle.

<figure><img src="../../.gitbook/assets/Liquidity Node Trifecta.png" alt=""><figcaption></figcaption></figure>

### Security

The security of MAYAChain is of paramount importance, and our nodes must be held accountable to ensure the integrity of our system. To this end, more than 75% of capital should be bonded by our nodes to guarantee trustworthiness. Ideally, the percentage should be closer to 87% to ensure that any potential Sybil attack yields a loss.

To achieve this, Liquidity Providers need to be incentivized to bond their liquidity to MAYANodes. MAYAChain utilizes economic incentives through the redistribution of yield.

### Yield

In MAYAChain, fees earned from swaps and transactions are divided into two types of rewards:

* **Node Exclusive Rewards (NER):** Rewards that are only given to Liquidity Bonded to MAYANodes. They are distributed evenly among all Nodes.
* **Liquidity Pool Rewards (LR):** Distributed to all LPs, including Bonded Liquidity, based on their share of liquidity.

In other words, LR is earned by ALL liquidity provided to MAYAChain (bonded or not), and the NER is only provided to those who Bond their Liquidity.&#x20;

{% hint style="info" %}
While Liquidity Bonded to MAYANodes earn NER, Node operators set a fee for providing their services.&#x20;
{% endhint %}

But what determines the ratios in which these rewards are divided?

#### Incentive Pendulum

As you'd expect, the ratios of Bonded to Unbonded Liquidity will be in constant fluctuation. The incentive Pendulum is the mechanism that algorithmically and periodically rebalances the rewards/fees received by Bonded Liquidity relative to Unbonded Liquidity, according to the network's security needs.

For example, if Bonded Liquidity falls below 75% of total liquidity (unsafe zone) the Incentive Pendulum starts giving 100% of the rewards/fees to the Liquidity Bonded in MAYANodes, and 0% to the Unbonded Liquidity. This incentivizes Unbonded LPs to bond in order to earn Yield.

And, if 100% of Liquidity bonded, users will be incentivized to provide liquidity, as they can earn yield without having to bond to a Node.

### Capital Efficiency

In the Liquidity Node model, Nodes don't have to choose between Node rewards or Liquidity Rewards, as they can earn both. They also have less risk to bond, as even if they fail to churn-in and be in a standby state, as they will still be earning Liquidity rewards. It also creates deeper pools while still maintaining the network security.

Liquidity Nodes earning both NER and LR means more yield, more yield attracts more liquidity, more liquidity reduces swap fees, less swap fees bring more trade volume, and more trade volume means more yield. A liquidity flywheel.



<figure><img src="../../.gitbook/assets/Capital Efficiency Flywheel.png" alt=""><figcaption></figcaption></figure>

### Calculations

The Exact calculations and yield distribution are as follows:

<figure><img src="../../.gitbook/assets/WHITE PAPER ecuacion (1).jpeg" alt=""><figcaption></figcaption></figure>

The calibrations of the economics and incentives that manage the system are algorithmic and code-driven. When the total network's liquidity is disproportionate in either direction (excessive Bonded Liquidity versus excessive Unbonded Liquidity), the incentive mechanism responds by adjusting the rewards accordingly. Below is a visual representation.

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -43 (1).png" alt=""><figcaption></figcaption></figure>

### Slashing

Whenever a node is slashed, MAYAChain still benefits if their liquidity remains in the pools. It is also possible that the slashing was done in error, so that must be taken into account as well, and only execute the slashing if the Node withdraws its liquidity.&#x20;

Slashed funds are transferred to the Protocol Owned Liquidity, and if the slashing is forgiven, it is given back. The forgiving of slashing can be done manually or automatically.

### Summary in 10 points

1. It is no longer the case that Capital Efficiency is inversely proportional to Network Security. Both can be achieved.
2. All of the TVL is invested in pools and is actively traded, thus making MAYAChain significantly more efficient in terms of capital utilization.
3. An increase in capital efficiency increases the average yield for all the ecosystem participants, which attracts liquidity, making swap fees cheaper, bringing in more volume, and creates a liquidity flywheel.
4. Nodes with lower bonds yield higher returns per dollar invested than Nodes with higher bonds, discouraging too much Liquidity to be bonded to any single Node, ensuring decentralization and bond homogenization.
5. As more Nodes compete to churn-in, they contribute to an increase in liquidity. This increased liquidity lowers the Incentive Curve (increasing NER), making it increasingly attractive to become a victorious Node.
6. As the incentive curve system pulls in any direction, Pool Depth increases.&#x20;
7. Node to LP and LP to Node latency is reduced and very easy to do for  Operators without incurring slip fees.
8. Standby Nodes earn yield while they wait to win the Bond War and churn in, making it less risky to compete.&#x20;
9. Nodes no longer need 100% exposure to CACAO.
10. Node misbehavior causes the slashing of a Node’s LP units that are converted into Protocol Owned LP Units that count towards unbonded liquidity. These Protocol Owned LP units will never exit, staying as a buyer of last resort.
