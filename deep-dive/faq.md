---
description: Please find the most frequently asked questions in the following sections.
---

# ❓ FAQs

## General FAQs

**What can I do on MAYAChain?**

You can swap/trade (buy & sell) Assets, provide Liquidity and earn yield, and/or run a Node to help secure our network in exchange for fees and yield.

#### **How much CACAO do the founders and team have?**

The team and founders have Zero CACAO allocation. If they do have it, they have acquired it by buying it from the market, like everyone else. 90% of CACAO has been donated to early Liquidity Providers during the Liquidity Auction, while the remaining 10% is in the Maya Fund vault to finance pools' Impermanent Loss.

#### How much MAYA do the founders and team have?

Maya Protocol founders have 15.6% of MAYA, but these tokens can't be sold. You can check Maya Protocol's Operations Wallet (that pays the team, marketing, and other expenses) at [**this link**](https://www.mayascan.org/address/maya18z343fsdlav47chtkyp0aawqt6sgxsh3vjy2vz).

#### What are the assets supported by Maya?

For an updated list please check [**MayaScan Pools Page**](https://www.mayascan.org/pools). But as of time of writing this guide, supported coins are: BTC, ETH, RUNE, USDT, USDC, wstETH, KUJI, USK, AETH, XRD & CACAO.

#### **Where can I buy CACAO?**

You can currently buy CACAO on both [**El Dorado**](https://www.eldorado.market/) and [**THORWallet**](https://app.thorwallet.org/).

{% hint style="info" %}
* There is no BEP20 or ERC20 CACAO in existence.
* You can only buy Native CACAO from the market.
* Only native CACAO is used in MAYAChain.
{% endhint %}

#### **How can I buy $MAYA?**

Currently, the only way to buy MAYA is OTC. If you wish to do so, you can head to our [**Discord**](https://discord.gg/mayaprotocol).

#### **What's the difference between Uniswap and MAYAChain?**

Uniswap enables decentralized crypto trading **within** a blockchain (mainly Ethereum), while MAYAChain offers a secure method to trade/exchange native assets **across** blockchains.

#### **What assets can you swap and provide liquidity for?**

* Current Chains — Bitcoin, Ethereum, THORChain, Dash, Kujira, Arbitrum & Radix.
* Tokens such as: ERC-20 (USDT, USDC & wstETH) and Kujira native assets (USK)
* See full list on the [**blockchain explorer**](https://www.mayascan.org/pools).

**Does MAYAChain need Oracles?**

No, it doesn't. MAYAChain's Continuous Liquidity Pool design & arbitrageurs set prices. When pools become imbalanced, arbitrage bots trade to rebalance them. CACAO binds all pools together, allowing arbitrage traders to purchase tokens at a lower price on MAYAChain & sell them for a profit. Arbitrage bots interact with the API 24/7, & anyone can run their own!

#### **Are there withdrawal fees and why is there a 3x withdrawal fee premium?**

Yes, there is a fee for every withdrawal, whether it is an asymmetrical or symmetrical deposit.

TLDR: Transaction Fee = Inbound fee Network Fee = 3x fee charged by network Total = Sum of both

Overview of the process and why:

1. Nodes Pay 1\* Standard Fee from the native pools (gas pools)
2. LPs get paid back 2\* St Fee, they earn >1x in margin due to arbing
3. Reserve gets paid back 3x, it earns 1x in margin which goes to the reserve.

➜ See [**Outbound-Fee**](how-it-works/fees.md#outbound-fee).

➜ Standard Fee [**inbound address**](https://mayanode.mayachain.info/mayachain/inbound_addresses) end point.

➜ [**Watch Fees and Wait Times Explained**](https://www.youtube.com/watch?v=XAdaEXO-Ofg) video

#### **Will all ERC20 assets be supported and listed on MAYAChain by default?**

No. Only short tail assets with high MarketCap, good velocity and economic activity would have chances to win liquidity competition to get listed. Although DEX Aggregators connected to MAYAChain can offer this service. Check [**Governance**](how-it-works/governance.md#chain-listing-delisting) for how tokens are added, and our [**pools**](https://www.mayascan.org/pools) for the full list of supported tokens.

{% hint style="info" %}
Open a ticket in Discord if you would like to see a new ERC-20 (on Ethereum or Arbitrum) or Kujira native asset added to Maya Protocol.
{% endhint %}

#### **Are Synths Interest-bearing Assets?**

No. Synths enables synthetic assets with no IL and with single asset exposure. They do not generate yield.

## Technical FAQs

#### Why use BFT Tendermint?

MAYAChain uses Tendermint which is a classical BFT algorithm. 2/3 of the validators need to agree and the network values safety. The chain has instant finality and this is needed to secure cross-chain bridges.

#### Why does CACAO need to be the settlement asset in every pool?

If each pool is comprised of two assets (e.g. BTC:ETH) then there will be a scaling problem with n\*(n-1)/2 possible connections. By having CACAO on one side of each pool, CACAO becomes a settlement bridging asset allowing swaps between any two other assets. Additionally, having CACAO in a pool ensures that the network can become aware of the value of assets it is securing.

#### Why use 1-way state pegs instead of atomic swaps or 2-way asset pegs?

Simply put, Cross-chain bridges are a better solution than Atomic Swaps. Atomic Swaps involve a complex process of signing cryptographic keys between two parties that require interactivity. You also need a counter-party on all trades. Cross-chain bridges, coupled with continuous liquidity pools means you don't need a designated counter-party, and swaps can happen in less than a second. 1-way state pegs are better than 2-way asset pegs because assets don't need to be wrapped or pegged. Instead of having IOU tokens, real native assets can be used instead.

#### What is the CACAO monetary policy / Tokenomics?

The goal is to have a fixed supply at all times, instead of constantly emitting (infinite supply like Cosmos or Ethereum) or reducing the emission down to zero (Bitcoin). Although, Maya Protocol can elect to use [**dynamic inflation**](https://docs.mayaprotocol.com/maya-terms-glossary/dynamic-inflation).

#### How are new pools added in MAYAChain?

Approximately every three days, the pending pool with the deepest liquidity is churned in (becomes an active pool). This is called a Pool Churn. Note: A pending pool must have a minimum 10K CACAO to be eligible for a churn.

This means there is an open decentralised liquidity competition where the community can vote with their liquidity. N.B. Caps will prevent voting with liquidity.

There is a set max of 100 active pools, and once achieved, deeper pending pools will be able to replace shallowest active pools. See Governance for more information.

#### My transaction failed, I can’t add liquidity or make swaps?

Make sure you have a sufficient amount of native cacao to process transactions. Also be sure to check if the liquidity caps are full. If so, you will not be able to add liquidity at that time. If you see errors like “No UTXOs to send” or “Need to wait for more UTXO confirmation”; likely you are trying to spend UTXO assets (BTC, LTC, BCH) that you have just transferred into your wallet. Please wait for more blockchain confirmations, and try again later!

#### **What is the average number of blocks per day for MAYAChain?**

MAYAChain produces a block every 6 seconds on average. So, MAYAChain produces around 14,400 blocks per day. Sometimes blocks can be produced slightly faster or slower, so these numbers are estimations.&#x20;

#### How can I see the numbers of churns on-chain?

You can check the [**churns here**](https://midgard.mayachain.info/v2/churns)**.** Each entry is a churn.

#### How to convert THORChain Address to MAYAChain Address?

You can convert any Thor address to Maya Address using [**this tool**](https://bech32.scrtlabs.com/), as Thor and Maya share the same hdpath.

#### How Can I check midgard data?

You can by visiting [**this link**](https://midgard.mayachain.info/v2/doc).

## Swaps

#### **Do I need CACAO to swap tokens on Maya?**

You do not need CACAO to interact with MAYAChain. You can perform swaps and add/remove liquidity without directly touching $CACAO or the MAYAChain protocol. You will need to find a MAYAChain-connected wallet.

#### **Is there a Network Fee?**

Yes, the Network Fee is collected in CACAO. If the transaction involves an asset that is not CACAO, the user pays the asset's Network Fee. The equivalent amount is taken from that pool's CACAO supply and added to the Protocol Reserve.

#### **What assets are available to swap?**

Users can swap any assets which are on connected chains and which have been added to the network. Users can swap from any connected asset to any other connected asset. They can also swap from any connected asset to CACAO.

#### **Are swaps decentralized?**

MAYA manages the swaps following the rules of the state machine - which is completely autonomous. Every swap that it observes is finalized, ordered, and processed. Invalid swaps are refunded, and valid swaps are ordered transparently and resistant to front-running. Validators can not influence the order of trades and are punished if they fail to observe a valid swap.

#### **How long does it take to complete a swap?**

Swaps are completed as fast as they can be confirmed, around 5-10 seconds.

#### **Are there any costs to swap?**

The cost of a swap is made up of two parts:

1. Outbound Fee
2. Price Slippage

All swaps are charged a network fee. The network fee is dynamic – calculated by averaging recent gas prices.&#x20;

#### **How are swap fees calculated?**

Swap fees are dependent on the depth of liquidity pools, the deeper the pools (aka the more liquidity) the less the fee is. Swap fee also depends on the size of the deposit. Bigger deposits incur more swap fees.

Note that users who force their swaps through quickly cause large slips and pay larger fees to liquidity providers. Learn more about [**Network Fees**](https://docs.mayaprotocol.com/maya-protocol-docs/how-it-works/fees).

## Liquidity Providers

#### **What is a keystore?**

The Keystore is an encrypted version of a user's private key, protected by a password, and presented in JSON/text format. It is a more secure alternative to the private key, as it requires a password to access.&#x20;

#### **What does it mean to be a Liquidity Provider?**

Liquidity providers deposit their assets in liquidity pools and earn yield in return. They earn tokens in Cacao and the pool's connected asset. For example, someone deposited in the BTC/CACAO pool will receive rewards in BTC and CACAO.

Read our whitepaper for a detailed breakdown of being a Liquidity Provider on Maya.

#### **Can anyone participate in liquidity pools?**

Yes! Depositing assets on MAYAChain is permissionless and non-custodial.

#### **What factors affect how much yield I get from LPing?**

Several factors will affect how much yield you receive, including your ownership % of the pool, swap volume, and size of swaps. The yield comes from fees and rewards from the protocol.

#### **How am I compensated for providing LP?**

Yield is calculated for liquidity providers in every block. Yield is paid out to liquidity providers when they remove assets from the pool. Rewards are calculated according to whether or not the block contains any swap transactions. If the block contains swap transactions, the amount of fees collected per pool sets the number of rewards. If the block doesn't have trades, the amount of assets in the pool determines the rewards.

#### **Is there a lockup period if I provide Liquidity?**

There is no minimum or maximum time or amount. Join and leave whenever you wish. There is however a required confirmation time before MAYAChain credits you with liquidity when adding or swapping to protect against Reorgs.&#x20;

{% hint style="info" %}
If you bonded your liquidity you have to wait for the Node you bonded to to churn out.
{% endhint %}

#### **What is the difference between asymmetric and symmetric LP deposits?**

Symmetrical deposits are where users deposit an equal value of 2 assets to a pool. For example, a user deposits $500 of BTC and $500 of CACAO to the BTC/CACAO pool.

Asymmetrical deposits are where users deposit unequal values of 2 assets to a pool. For example, a user deposits $2000 BTC and $0 CACAO to the BTC/CACAO pool. In the backend, the member is given ownership of the pool that considers the slip created in the price. The liquidity provider will end up with <$1000 in BTC and <$1000 in CACAO. The deeper the pool, the closer to a total of $2000 the member will own.

#### Can I withdraw LP symmetrically after depositing an asset asymmetrically?

No. You cannot withdraw symmetrically. You can withdraw only asymmetrically for LP you deposited asymmetrically.

#### I entered asymmetrically but received less than anticipated?

Yes, because when you pool asymmetrically your asset is swapped into 50% cacao and 50% asset. When swapping you are subject to slippage and fees. There are 2 types charged on asymmetrical deposits/withdrawals:

1. The on-chain deposit transaction fee (inbound tx)
2. The liquidity fee as a function of slip

Upon withdrawal, there will also be a transaction fee (outbound tx)

#### **Are there slippage fees for symmetrical deposits?**

No, there is only the deposit transaction fee.

#### What ways can I withdraw my asymmetrical deposit?

If you deposit asymmetrically you can ONLY withdraw asymmetrically.

#### What ways can I withdraw my symmetrical deposit?

You can withdraw your symmetrical deposit both asymmetrically and symmetrically.

#### Is my asymmetric deposit covered by IL Protection?

Yes, but the IL Protection is applied to the asymmetrical deposit after it has been converted into a symmetrical deposit (also meaning after slippage and fees). This means that the IL Protection will cover both assets. See ILP above.

#### **What is** **Impermanent Loss Protection (ILP), and does Maya offer it?**

Currently, yes. Our ILP guarantees that LPs will either make a profit or at least recoup their investment (in USD value) in comparison to holding them externally after a certain period (currently set to 100 days) and even that they can be partially reimbursed for any impermanent losses incurred before that time. Any potential losses in the LP deposit are subsidized in CACAO.

#### **$CACAO underperforms the external ASSET**

ILP kicks in 50 days after providing liquidity, with a 1% compensation for any realized IL. After that, 1% more is added everyday until , on day 150, 100% of the IL realized is covered. As an example, on day 100 after adding liquidity, your ILP is 50%.

#### **$CACAO performs better than the external ASSET**

ILP kicks in 50 days after providing liquidity, with a .25% compensation for any realized IL. After that, .25% more is added everyday until , on day 450, 100% of the IL realized is covered.

#### **Is there a liquidity cap?**

The Soft Cap has been removed, so there should be no limit to the amount of liquidity entered. There is a hard cap in place to ensure the total pooled cannot exceed the total bonded, making the network unsafe however the [**Incentive Pendulum**](how-it-works/incentive-pendulum.md) makes this cap near impossible to reach.

#### **Are** **there any strategies for Liquidity Providers?**

There are two main strategies; active and passive.

**Passive** liquidity providers should seek out pools with deep liquidity to minimize risk and maximize returns.

**Active** liquidity providers can maximize returns by seeking pools with shallow liquidity but high demand. This demand will cause greater slips and higher fees for liquidity providers.

#### How is "bond address" different from the "node address"?

A Node Operator may or may not have their own bond provider address. If they do, node address = bond address. If they don't have it, node address = node operator address.

#### How to calculate a pool's APR manually?

Head to [**MayaScan pools page**](https://www.mayascan.org/pools). Choose the pool of interest. Get the swap fees and annualize them (x365) then divide by the total pool size.

#### How to check your initial liquidity vs. your current liquidity?

1. Go to [**this link**](https://svelte.dev/repl/05d51f51fac7463e91e9327b9347e1d2?version=3.58.0).
2. Choose your pool (BTC, ETH, etc..)
3. Paste in your wallet address.
4. Press "Check Position".
