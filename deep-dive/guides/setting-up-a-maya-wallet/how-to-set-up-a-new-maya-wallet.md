---
description: How to set up a new MAYAChain keystore wallet using THORWallet
---

# Using THORWallet web APP

1\. Go to [ThorWallet ](https://app.thorwallet.org/swap?fromAsset=BTC.BTC\&toAsset=ETH.ETH)and click "Connect."

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.13.51 a.m..png" alt="" width="375"><figcaption></figcaption></figure>

2\. Accept the Terms of Service and Privacy Policy.

3\. Select "Create Keystore."

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-13 a la(s) 12.12.22 p.m..png" alt="" width="375"><figcaption></figcaption></figure>

4\. Create and confirm a password for the keystore file. Make sure to save this password. You'll need it to open the keystore file. Make sure to save the 12-words seed phrase; you can use it to recover your wallet if you lose your keystore.&#x20;

<figure><img src="../../../.gitbook/assets/Screenshot 2023-04-09 at 09.59.11.png" alt=""><figcaption></figcaption></figure>

5\. Click "Download" and save the keystore file somewhere safe on your local drive.

6\. On Connect screen select "Keystore" option to connect using your newly created keystore file.

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.26.21 a.m..png" alt=""><figcaption></figcaption></figure>

7\. Select the new keystore file saved on your drive, input your password and click "Unlock."

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.24.48 a.m..png" alt=""><figcaption></figcaption></figure>

8\. Select "My Assets" from the left sidebar.

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.21.03 a.m..png" alt=""><figcaption></figcaption></figure>

9\. Search for MAYA token in "Search assets" at the top.

10\. Select MAYA.

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.21.55 a.m..png" alt=""><figcaption></figcaption></figure>

11\. Change the tab from "Send" to "Receive" - there, you will be able to copy your new Maya wallet address. It should begin with "maya..."

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-14 a la(s) 10.23.12 a.m..png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Save your new Maya wallet address, and keystore (if you created one), and password in a safe place. You’ll need your Maya address for the next steps.&#x20;

In the example, the Maya wallet created with the keystore file was:

maya1668cguves6x7sfm08pkm2u78exwaq63t9kqwse
{% endhint %}

\
