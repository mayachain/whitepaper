---
description: How to install VultiWallet on your mobile device
---

# Using THORWallet Mobile APP

1. Download VultiWallet on your iOS or Android device.

<figure><img src="../../../.gitbook/assets/WhatsApp Image 2024-12-18 at 18.09.14.jpeg" alt=""><figcaption></figcaption></figure>

2. Clicking on the "Get Started" button, and agree to the Terms of Service and Privacy Policy, after which the wallet setup starts on the following screen:

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2FyLa7EfD0h5m75UmfVTco%2FCreate%20or%20import%20wallet_app.jpg?alt=media&#x26;token=1620e68c-7c3f-4ffa-8c92-a5f0bcae9923" alt=""><figcaption></figcaption></figure>

3. Click create new wallet. After clicking on Create Wallet, you will be asked to write down your unique seed. You can view your seed phrase by clicking on “Backup Wallet”.

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2FkhIU3OYuxY2hWVxIAqQ8%2FBackup%20wallet_app.jpg?alt=media&#x26;token=54ce7a10-f1a7-43aa-be79-7ca6f3b12a28" alt=""><figcaption></figcaption></figure>

4. Click Backup Wallet. You will see the following screen with your seed phrase.&#x20;

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2F7GQsOaBn1uALNnFMsrw4%2FSeed%20Phrase_app.jpg?alt=media&#x26;token=c637c902-a312-4213-b8b8-6723c7025a19" alt=""><figcaption></figcaption></figure>

5. Copy the seed to your clipboard and keep it on your phone, or (**highly recommended**) write it down and keep it off-line.&#x20;

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2FPi5KyBeo76CkK7hvQ3lg%2FCreate%20PIN%3APassword_app.jpg?alt=media&#x26;token=47d8e340-da31-4ee1-bbb5-b8505d8d741c" alt=""><figcaption></figcaption></figure>

6. Create a PIN.

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2FnESdt1xylmK5DlXDnpM7%2FConfirm%20PIN%3APassword_app.jpg?alt=media&#x26;token=aa169946-cdcf-47e8-8ebb-9ec40c95addc" alt=""><figcaption></figcaption></figure>

7. Confirm your PIN.&#x20;

<figure><img src="https://894993076-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FctX4O78lmuKNbb22y3kj%2Fuploads%2F7VnJ7oQ8lggz1sJiqixT%2FEnable%20Biometrics_app.jpg?alt=media&#x26;token=e20c2f37-ee55-41db-bc3c-bec41d51a4e4" alt=""><figcaption></figcaption></figure>

8. You have the option to unlock the wallet using biometrics. Simply click on “Enable Biometrics” to activate this feature, or choose “Skip” if you prefer not to enable it.

<figure><img src="../../../.gitbook/assets/Copia de WhatsApp Image 2024-11-14 at 10.40.42 (1).jpeg" alt="" width="370"><figcaption></figcaption></figure>

9. Finally, you can scroll through some information slides about [VultiWallet](https://app.thorwallet.org/swap) functionalities or just click on “Get Started”, which will bring you to the “home page” of the app.

<figure><img src="../../../.gitbook/assets/WhatsApp Image 2024-11-14 at 10.40.42 (2).jpeg" alt="" width="375"><figcaption></figcaption></figure>
