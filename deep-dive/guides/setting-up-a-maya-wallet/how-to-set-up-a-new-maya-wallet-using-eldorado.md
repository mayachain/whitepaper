---
description: How to set up a new MAYAChain keystore wallet using ElDorado
---

# Using El Dorito Club

## Video Guide

Here is the video guide to create a Keystore wallet on El Dorito. If you prefer a written guide, please continue scrolling.

{% embed url="https://www.youtube.com/watch?v=S6IGKgmoosY" %}

## Written/ Text Guide

1- Visit **El Dorito** [**Website**](https://eldorito.club/) and press the "Enter El Dorado" button.

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-13 a la(s) 11.58.03 a.m..png" alt=""><figcaption></figcaption></figure>

2- On the top right corner press the "Connect" button to connect your wallet.

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-13 a la(s) 11.58.18 a.m..png" alt=""><figcaption></figcaption></figure>

3- A pop-up will appear. Choose "Create new keystore".

<figure><img src="../../../.gitbook/assets/Captura de pantalla 2024-11-13 a la(s) 12.04.02 p.m..png" alt=""><figcaption></figcaption></figure>

4- Create a password to encrypt your keystore.&#x20;

{% hint style="info" %}
Make sure it's a safe password that you can REMEMBER.
{% endhint %}

<figure><img src="../../../.gitbook/assets/Screenshot 2023-04-29 at 05.01.40.png" alt=""><figcaption></figcaption></figure>

5- Once you press create you'll be prompted to save your Keystore file on your computer. Choose a safe location and save.

6- You'll be presented with a 12-words seed phrase. Make sure to write it down or save it somewhere safe.

<figure><img src="../../../.gitbook/assets/Screenshot 2023-04-29 at 05.24.51.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
If you lose your keystore or forget your password, the only way to recover your wallet is using the seed phrase. If you lose that too no one will be able to help you, and your funds may be lost forever.&#x20;
{% endhint %}

7- Congratulations! Your wallet has been created and you can now interact with El Dorito and all Maya Protocol's Front Ends using your Keystore.

<figure><img src="../../../.gitbook/assets/El Dorado Connected swap page 2023-07-17" alt=""><figcaption></figcaption></figure>

For further assistance, please visit ElDorito's [**Discord**](https://discord.gg/yZebrxC82s) channel and/ or Maya Protocol's [**Discord**](https://discord.gg/mayaprotocol) channel.
