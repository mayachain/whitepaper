# How to Install Ferz Wallet iPhone

1. Open Safari.
2. Open Mobile browser and go to [ferz.com](https://ferz.com/en/).
3. Press "Get Ferz".

<figure><img src="../../../.gitbook/assets/IMG_7034.jpg" alt="" width="375"><figcaption></figcaption></figure>

3.Press "Let's go".

<figure><img src="../../../.gitbook/assets/IMG_7035.jpg" alt="" width="375"><figcaption></figcaption></figure>

4. Press "Create Wallet".

<figure><img src="../../../.gitbook/assets/IMG_7036.jpg" alt="" width="375"><figcaption></figcaption></figure>

5. Choose "Software Wallet".

<figure><img src="../../../.gitbook/assets/IMG_7061.jpg" alt="" width="375"><figcaption></figcaption></figure>

6. Press the dots, and create your PIN. Make sure it's a combination of numbers you can remember.

<figure><img src="../../../.gitbook/assets/IMG_7037.jpg" alt="" width="375"><figcaption></figcaption></figure>

7. Repeat PIN code.
8. Read the warning carefully and mark the checkbox.
9. Press "Create Wallet".

<figure><img src="../../../.gitbook/assets/IMG_7040.jpg" alt="" width="375"><figcaption></figcaption></figure>

10. &#x20;Please wait patiently till wallet creation is done.

<figure><img src="../../../.gitbook/assets/IMG_7041.jpg" alt="" width="375"><figcaption></figcaption></figure>

11. Press "Make a backup".

<figure><img src="../../../.gitbook/assets/IMG_7042.jpg" alt="" width="375"><figcaption></figcaption></figure>

12. Choose your preferred method of backup.

{% hint style="info" %}
* Google Drive will save your key in the cloud protected by Google encryption. Will require logging in to your Google Account.
* Mnemonic phrase will display the secret 12-words that you need to write down or save somewhere.
* QR code will display a QR code you can download and scan later if you lost access to your wallet.
* File will download a keystore file to your phone, that you can upload later if you lost access to your wallet.
{% endhint %}

<figure><img src="../../../.gitbook/assets/IMG_7044.jpg" alt="" width="375"><figcaption></figcaption></figure>

13. Go back.
14. Read both checkboxes carefully, mark them, and press "Disable".

<figure><img src="../../../.gitbook/assets/IMG_7062.jpg" alt="" width="375"><figcaption></figcaption></figure>

15. Your wallet is now functioning, but one more step remains.
16. At the bottom of the screen press the "share button" circled in red.

<figure><img src="../../../.gitbook/assets/IMG_7046.jpg" alt="" width="375"><figcaption></figcaption></figure>

17. Find "Add to Home Screen" and press it.

<figure><img src="../../../.gitbook/assets/IMG_7047.jpg" alt="" width="375"><figcaption></figcaption></figure>

18. Your wallet icon should be visible on the Home Screen, and you can access it any time.

<figure><img src="../../../.gitbook/assets/IMG_7048.jpg" alt="" width="375"><figcaption></figcaption></figure>

