# Asgardex: Add RUNE

1. Open Asgardex Desktop App.&#x20;

{% hint style="info" %}
You can download Asgardex [here](https://github.com/thorchain/asgardex-electron/releases/).
{% endhint %}

2. Press "Wallet" on the top mid-right.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.01.55.png" alt=""><figcaption></figcaption></figure>

3. Press the "Action" button next to RUNE.
4. From the dropdown menu press "Send".

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.05.00.png" alt=""><figcaption></figcaption></figure>

5. You will be directed to the send page. Note the fields: Address, Amount, and Memo.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-15 at 15.04.57.png" alt=""><figcaption></figcaption></figure>

6. In the Address field paste the RUNE inbound address found [here](https://mayanode.mayachain.info/mayachain/inbound\_addresses).

{% hint style="warning" %}
The inbound addresses change every churn, and you should always check the link above, and refresh to get the most recent address. Failing to do so may result in loss of funds.\
\
The picture below is an example and may not show the most up to date inbound address.
{% endhint %}

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.17.08.png" alt=""><figcaption></figcaption></figure>

7. In the amount field type the amount you want to add.
8. In the memo field, paste the following:
   * &#x20;\+:thor.rune:_**YourMayaAddress**_

{% hint style="warning" %}
Replace the _**YourMayaAddress**_ in the memo with your actual Maya wallet address.
{% endhint %}

9. Press send.
10. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
