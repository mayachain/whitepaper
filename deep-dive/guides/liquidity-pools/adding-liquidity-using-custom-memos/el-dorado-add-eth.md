---
description: Adding ETH Liquidity Asymmetrically using Memos
---

# El Dorado: Add ETH



1. Open [El Dorado WebApp](https://app.eldorado.market/).
2. If prompted, enter your password. If not, press "Connect" on the top right.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.35.23 (1).png" alt=""><figcaption></figcaption></figure>

3. &#x20;Select "Connect existing keystore".&#x20;

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.37.39 (1).png" alt=""><figcaption></figcaption></figure>

4. Upload your keystore and enter your password.
5. Press "View Wallet" on the top right.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 23.47.50.png" alt=""><figcaption></figcaption></figure>

6. Press the "Transfer" button next to BTC.
7. Check the "Make a deposit with custom memo" box.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-08 at 07.30.29 (1).png" alt=""><figcaption></figcaption></figure>

9. In the memo field, paste the following:
   * \+:eth.eth
10. In the amount field type an amount suitable to cover the fees.
11. Press send.
12. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
