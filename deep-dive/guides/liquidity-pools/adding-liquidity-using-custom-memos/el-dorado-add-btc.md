---
description: Adding BTC Liquidity Asymmetrically using Memos
---

# El Dorado: Add BTC



1. Open [El Dorado WebApp](https://app.eldorado.market/).
2. If prompted, enter your password. If not, press "Connect" on the top right.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.35.23 (1).png" alt=""><figcaption></figcaption></figure>

3. &#x20;Select "Connect existing keystore".&#x20;

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.37.39 (1).png" alt=""><figcaption></figcaption></figure>

4. Upload your keystore and enter your password.
5. Press "View Wallet" on the top right.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-06 at 23.47.50.png" alt=""><figcaption></figcaption></figure>

6. Press the "Transfer" button next to BTC.
7. Check the "Make a deposit with custom memo" box.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-07 at 10.40.15 (1).png" alt=""><figcaption></figcaption></figure>

8. In the memo field, paste the following:
   * \+:btc.btc
9. In the amount field type the amount you wish to add.
10. Press send.
11. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
