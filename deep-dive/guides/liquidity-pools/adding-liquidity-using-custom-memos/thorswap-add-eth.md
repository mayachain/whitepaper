# Thorswap: ADD ETH

1. Open [Thorswap Web App](https://app.thorswap.finance/swap).&#x20;

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.21.48.png" alt=""><figcaption></figcaption></figure>

2. Press "connect" on the top right.
3. Connect using keystore or XDefi.

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.25.09.png" alt=""><figcaption></figcaption></figure>

4. From the left panel press send.
5. You will be directed to the send page. Note the 3 fields: Amount, Recipient Address, and Memo.
6. From the dropdown menu press "ETH".

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.33.34.png" alt=""><figcaption></figcaption></figure>

7. In the Recipient Address field paste the ETH inbound address found [here](https://mayanode.mayachain.info/mayachain/inbound\_addresses).

<figure><img src="../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.16.30.png" alt=""><figcaption></figcaption></figure>

8. In the amount field type the amount you wish to add.
9. In the memo field, paste the following:

* \+:eth.eth:_**YourMayaAddress**_

{% hint style="warning" %}
Replace the _**YourMayaAddress**_ in the memo with your actual Maya wallet address.
{% endhint %}

10. Press send.
11. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
