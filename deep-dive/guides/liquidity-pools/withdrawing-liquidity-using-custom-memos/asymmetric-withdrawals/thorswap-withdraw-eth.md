---
description: Withdraw ETH from LP using Thorswap Front-End
---

# Thorswap: Withdraw ETH

1. Open [Thorswap Web App](https://app.thorswap.finance/swap).&#x20;

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.21.48.png" alt=""><figcaption></figcaption></figure>

2. Press "connect" on the top right.
3. Connect using keystore or XDefi.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.25.09.png" alt=""><figcaption></figcaption></figure>

4. From the left panel press send.
5. You will be directed to the send page. Note the 3 fields: Amount, Recipient Address, and Memo.
6. From the dropdown menu press "ETH".

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-09 at 11.33.34.png" alt=""><figcaption></figcaption></figure>

7. In the Recipient Address field paste the ETH inbound address found [here](https://mayanode.mayachain.info/mayachain/inbound\_addresses).

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.16.30.png" alt=""><figcaption></figcaption></figure>

8. In the amount field type an amount that is sufficient to cover the transaction fees.
9. In the memo field, based on your tier, paste the following:

* Tier 1: "-:eth.eth:50:eth.eth:_**YourMayaAddress**_"
* Tier 2: "-:eth.eth:150:eth.eth:_**YourMayaAddress**_"
* Tier 3: "-:eth.eth:450:eth.eth:_**YourMayaAddress**_"

{% hint style="info" %}
50 represents %0.5, 150 represents %1.5, and 450 represents %4.5. As you can see we are using basis points to represent percentages, so if you have no tier and want to withdraw %100 you'd exchange the number in the middle of the memo with 10000.
{% endhint %}

{% hint style="warning" %}
Replace the _**YourMayaAddress**_ in the memo with your actual Maya wallet address.
{% endhint %}

10. Press send.
11. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
