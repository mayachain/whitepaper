# How to Provide Liquidity using ThorWallet Mobile App

Adding liquidity in Maya using THORWallet is a simple process. Find here the necessary steps to do so.

### Step-by-Step Guide

1. Download the THORWallet application from either [Google Play ](https://play.google.com/store/apps/details?id=defisuisse.thorwallet)or the [App Store](https://apps.apple.com/ch/app/thorwallet-defi-wallet/id1592064324).&#x20;
2. [Create a new wallet](https://docs.mayaprotocol.com/guides/how-to-set-up-a-new-maya-wallet) within the app or import an existing one, if you'd prefer. The TW user interface guides you through both processes seamlessly, but they also have this [Getting Started Guide](https://faqs.thorwallet.org/faqs/general/getting-started-guide) if you need additional help.&#x20;

{% hint style="info" %}
If you do generate a new wallet, you will need to write down your new seed phrase somewhere safe. Remember, this is all non-custodial!
{% endhint %}

3. Explore the app and get used to it. Notice how the app has created several different addresses for you, in various different blockchains. You can send a small amount of crypto to any of those to ensure everything works fine.
4. Send your assets to their corresponding address. You can use their QR code representation if it is convenient. Remember these should all be native assets, you can’t provide liquidity in Maya with wrapped tokens (e.g. wBTC).
5. Find the Earn icon in the main main, on the bottom of the screen. Tap on Liquidity Pooling and then on Available Pools Add Liquidity.

<figure><img src="../../../.gitbook/assets/Screenshot 2023-06-02 at 10.14.55.png" alt=""><figcaption></figcaption></figure>

6. Choose whether you want to provide liquidity symmetrically or asymmetrically. If you are not sure about this one, and you only have thought of adding one asset (for example ETH) you are probably looking to add liquidity symmetrically.

{% hint style="info" %}
Even when choosing to add liquidity symmetrically, you end up having an LP position where your initial asset plus $CACAO total the initial USD notional amount provided. You can read more about symmetric vs asymmetric provisions in [this article](https://www.mayaprotocol.com/latest-post/liquidity-provider).
{% endhint %}

7. Filter the pools using the Maya button.

<figure><img src="../../../.gitbook/assets/Screenshot 2023-06-02 at 10.31.19.png" alt=""><figcaption></figcaption></figure>

8. Select your preferred pool, for example, the BTC/CACAO pool.

<figure><img src="../../../.gitbook/assets/Screenshot 2023-06-02 at 10.31.29.png" alt=""><figcaption></figcaption></figure>

9. Select the amount of tokens you will provide as liquidity into Maya and click Continue.
10. Review your transaction and tap on Add Liquidity Now if everything is correct.
11. That’s it! Wait for the necessary transactions to be broadcasted and for your confirmation!
