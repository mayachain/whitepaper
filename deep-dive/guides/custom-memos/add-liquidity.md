---
description: Add Liquidity Through Custom Memos.
---

# Add Liquidity

{% hint style="warning" %}
This guide should only be used by Advanced Users. It is highly recommended to use [**Maya UIs** ](https://docs.mayaprotocol.com/introduction/integrations)to transact. A mistake in any step CAN cause **loss of funds**.
{% endhint %}

## Memo Format

For Symmetric Adds:  **`ADD:POOL:PAIREDADDR`**

For Asymmetric Adds: **`ADD:POOL`**

{% hint style="info" %}
For a full list of memo formats and abbreviations check this [page](https://docs.mayaprotocol.com/mayachain-dev-docs/concepts/transaction-memos).
{% endhint %}

## Procedure

### Through a MAYAChain UI (using MsgDeposit, eg. El Dorado)

### Symmetric Add (ASSET + CACAO)

1. Connect/Create your wallet.
2. Make sure your wallets are funded with ASSET & CACAO and they have to be of equal values.
3.  Transfer an amount of ASSET, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC:yourMAYAaddress`
      * **Example:** `ADD:BTC.BTC:maya1nxjvgxqxe488jyl8wrmhm9ulsfthgp4rh5u5tw`

    or you can abreviate it to:

    * `+:BTC.BTC:yourMAYAaddress`
4. In the amount field, type the ASSET amount you wish to add (make sure you have enough amount left for fees).
5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).
7.  Transfer an equivalent amount (in value) of CACAO, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC:yourBTCaddress`
      * **Example:** `ADD:BTC.BTC:bc1p7tm6t2m6678cke92cw5r9gddpz4td80p45vu7v0v23604p6vd2us5jdtt2`

    or you can abreviate it to:

    * `+:BTC.BTC:yourBTCaddress`
8. In the amount field, type the CACAO amount you wish to add (make sure you have enough amount left for fees).
9. Press send.
10. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Add&#x20;

{% hint style="info" %}
Assymmetric Adds are made for convenience. Instead of swapping half of the Asset amount to CACAO, then adding liquidity, the chain does the swap part Automatically for you.\
\
Bear in mind, this will cause some limitations:

1. In the future, you will only be able to withdraw liquidity Asymmetrically, and the same side as well. So if you deposit BTC asymmetrically, you can only withdraw BTC asymmetrically.
2. Slippage. The chain will swap half of the Asset for you, which may cause unfavorable slippage fees, during deposit and withdrawal.
{% endhint %}

1. Connect/Create your wallet.
2. Make sure your wallets are funded.
3.  Transfer an amount of ASSET/CACAO, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC`

    or you can abreviate it to:

    * `+:BTC.BTC`

{% hint style="info" %}
You cannot use "+:MAYA.CACAO" memo.&#x20;
{% endhint %}

4. In the amount field, type the amount you wish to add (make sure you have enough amount left for fees).
5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Through a Non-CACAO UI (using Normal Send tx + MsgDeposit, eg. Asgardex)

### Symmetric Add (ASSET + CACAO)

1. Connect/Create your wallet.
2.  Transfer an amount of ASSET, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC:yourMAYAaddress`
      * **Example:** `ADD:BTC.BTC:maya1nxjvgxqxe488jyl8wrmhm9ulsfthgp4rh5u5tw`

    or you can abreviate it to:

    * `+:BTC.BTC:yourMAYAaddress`
3. In the amount field, type the ASSET amount you wish to add (make sure you have enough amount left for fees).
4. In the address field paste ASSET [**Inbound Address**](https://www.mayascan.org/network) on MAYAChai&#x6E;**.**&#x20;

{% hint style="danger" %}
Make sure to refresh the page and NEVER use an old inbound address, as it they change every churn.
{% endhint %}

5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

{% hint style="info" %}
The following steps need to be done through a Maya UI that supports MsgDeposit.
{% endhint %}

7.  Transfer an equivalent amount (in value) of CACAO, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC:yourBTCaddress`
      * **Example:** `ADD:BTC.BTC:bc1p7tm6t2m6678cke92cw5r9gddpz4td80p45vu7v0v23604p6vd2us5jdtt2`

    or you can abreviate it to:

    * `+:BTC.BTC:yourBTCaddress`
8. In the amount field, type the CACAO amount you wish to add (make sure you have enough amount left for fees).
9. Press send.
10. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Add&#x20;

{% hint style="info" %}
Asymmetric Adds are made for convenience. Instead of swapping half of the Asset amount to CACAO, then adding liquidity, the chain does the swap part Automatically for you.\
\
Bear in mind, this will cause some limitations:

1. In the future, you will only be able to withdraw liquidity Asymmetrically, and the same side as well. So if you deposit BTC asymmetrically, you can only withdraw BTC asymmetrically.
2. Slippage. The chain will swap half of the Asset for you, which may cause unfavorable slippage fees, during deposit and withdrawal.
{% endhint %}

1. Connect/Create your wallet.
2.  Transfer an amount of ASSET/CACAO, by substituting the format with actual values. Example:

    * `ADD:BTC.BTC`

    or you can abreviate it to:

    * `+:BTC.BTC`

{% hint style="info" %}
You cannot use "+:MAYA.CACAO" memo.&#x20;
{% endhint %}

4. In the amount field, type the amount you wish to add (make sure you have enough amount left for fees).
5. In the address field paste ASSET [**Inbound Address**](https://www.mayascan.org/network) on MAYAChai&#x6E;**.**&#x20;

{% hint style="danger" %}
Make sure to refresh the page and NEVER use an old inbound address, as it they change every churn.
{% endhint %}

6. Press send.
7. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).
