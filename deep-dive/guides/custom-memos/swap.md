---
description: Swap Assets Using Custom Memos.
---

# Swap

{% hint style="warning" %}
This guide should only be used by Advanced Users. It is highly recommended to use [**Maya UIs** ](https://docs.mayaprotocol.com/introduction/integrations)to transact. A mistake in any step CAN cause **loss of funds**.
{% endhint %}

## Memo Format

**`SWAP:ASSET:DESTADDR`**

{% hint style="info" %}
For a full list of memo formats and abbreviations check this [page](https://docs.mayaprotocol.com/mayachain-dev-docs/concepts/transaction-memos).
{% endhint %}

## Procedure

### Through a MAYAChain UI (eg. El Dorito Swap, Asgardex)

1. Connect/Create wallet.
2. Transfer from the ASSET you want to swap from (eg. ETH).
3.  In the memo field substitute the format for the actual values. Example:

    * `SWAP:MAYA.CACAO:maya1x5979k5wqgq58f4864glr7w2rtgyuqqm6l2zhy`

    or you can abbreviate it to:

    * `=:MAYA.CACAO:maya1x5979k5wqgq58f4864glr7w2rtgyuqqm6l2zhy`
4. In the amount field, type the amount you wish to swap

{% hint style="info" %}
Make sure you have an amount left in your (from) wallet to cover the transaction fees.\
\
Check the following for [**BTC fees**](https://bitinfocharts.com/comparison/bitcoin-transactionfees.html#3m), [**ETH fees**](https://bitinfocharts.com/comparison/ethereum-transactionfees.html#3m), RUNE fees (0.02 $RUNE), [**DASH fees**](https://bitinfocharts.com/comparison/dash-transactionfees.html#3m), KUJI fees (around 0.003 $KUJI, but confirm through a Kujira Wallet).
{% endhint %}

5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).
