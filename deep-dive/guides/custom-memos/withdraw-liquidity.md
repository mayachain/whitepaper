---
description: Withdrawing Liquidity Using Custom Memos.
---

# Withdraw Liquidity

{% hint style="warning" %}
This guide should only be used by Advanced Users. It is highly recommended to use [**Maya UIs** ](https://docs.mayaprotocol.com/introduction/integrations)to transact. A mistake in any step CAN cause **loss of funds**.
{% endhint %}

## Memo Format

For Symmetric Withdrawal:  **`WD:POOL:BASISPOINTS`**

For Asymmetric Withdrawal: **`WD:POOL:BASISPOINTS:ASSET`**

{% hint style="warning" %}
**Liquidity Withdrawal Rules:**

1. If you deposited symmetrically, you can withdraw symmetrically, ASSET asymmetrically  side, or CACAO asymmetrically.
2. If you deposited asymmetrically, you can ONLY withdraw the side you deposited (ASSET/CACAO) asymmetrically.
{% endhint %}

{% hint style="info" %}
For a full list of memo formats and abbreviations check this [page](https://docs.mayaprotocol.com/mayachain-dev-docs/concepts/transaction-memos).
{% endhint %}

## Procedure

### Through a MAYAChain UI (eg. El Dorado, Asgardex)

### Symmetric withdraw (ASSET + CACAO)

1. Connect/Create your wallet.
2.  If you want to pay withdrawal fees in CACAO, do the transfer/memo from the CACAO side. If you want to pay withdrawal fees in ASSET, do the transfer/memo from the ASSET side. In all cases the memo will be the same. **Example**:

    * `WD:BTC.BTC:10000`

    or you can abreviate it to:

    * `-:BTC.BTC:10000`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of ASSET/CACAO observed by nodes. The values are as follows:
   * CACAO (1e-10): `0.0000000001`
   * BTC (10,001 sats): `0.00010001`
   * ETH (1e-8): `0.00000001`
   * RUNE (1e-8): `0.00000001`
   * DASH (10,001 duffs): `0.00010001`
   * KUJI (1e-6): `0.000001`
4. Press send.
5. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Withdraw ASSET side

1. Connect wallet.
2.  Transfer an amount of ASSET, by substituting the format with actual values. **Example:**

    * `WD:BTC.BTC:10000:BTC.BTC`

    or you can abbreviate it to:

    * `-:BTC.BTC:10000:BTC.BTC`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of ASSET observed by nodes. The values are as follows:
   * BTC (10,001 sats): `0.00010001`
   * ETH (1e-8): `0.00000001`
   * RUNE (1e-8): `0.00000001`
   * DASH (10,001 duffs): `0.00010001`
   * KUJI (1e-6): `0.000001`
4. Press send.
5. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Withdraw CACAO side

1. Connect wallet.
2.  Transfer an amount of CACAO, by substituting the format with actual values. Example:

    * `WD:BTC.BTC:10000:MAYA.CACAO`

    or you can abbreviate it to:

    * `-:BTC.BTC:10000:MAYA.CACAO`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of CACAO observed by nodes. The value is as follows:
   * CACAO (1e-10): `0.0000000001`
4. Press send.
5. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Through a Non-CACAO UI (using Normal Send tx + MsgDeposit, eg. Asgardex)

### Symmetric withdraw (ASSET + CACAO)

1. Connect/Create your wallet.
2.  If you want to pay withdrawal fees in CACAO, do the transfer/memo from the CACAO side. If you want to pay withdrawal fees in ASSET, do the transfer/memo from the ASSET side. In all cases the memo will be the same. **Example**:

    * `WD:BTC.BTC:10000`

    or you can abreviate it to:

    * `-:BTC.BTC:10000`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of ASSET/CACAO observed by nodes. The values are as follows:
   * CACAO (1e-10): `0.0000000001`
   * BTC (10,001 sats): `0.00010001`
   * ETH (1e-8): `0.00000001`
   * RUNE (1e-8): `0.00000001`
   * DASH (10,001 duffs): `0.00010001`
   * KUJI (1e-6): `0.000001`
4.  In the address field paste ASSET [**Inbound Address**](https://www.mayascan.org/network) on MAYAChai&#x6E;**.**&#x20;

    {% hint style="danger" %}
    Make sure to refresh the page and NEVER use an old inbound address, as it they change every churn.
    {% endhint %}
5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Withdraw ASSET side

1. Connect wallet.
2.  Transfer an amount of ASSET, by substituting the format with actual values. **Example:**

    * `WD:BTC.BTC:10000:BTC.BTC`

    or you can abbreviate it to:

    * `-:BTC.BTC:10000:BTC.BTC`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of ASSET observed by nodes. The values are as follows:
   * BTC (10,001 sats): `0.00010001`
   * ETH (1e-8): `0.00000001`
   * RUNE (1e-8): `0.00000001`
   * DASH (10,001 duffs): `0.00010001`
   * KUJI (1e-6): `0.000001`
4. In the address field paste ASSET [**Inbound Address**](https://www.mayascan.org/network) on MAYAChai&#x6E;**.**&#x20;

{% hint style="danger" %}
Make sure to refresh the page and NEVER use an old inbound address, as it they change every churn.
{% endhint %}

5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).

### Asymmetric Withdraw CACAO side

1. Connect wallet.
2.  Transfer an amount of CACAO, by substituting the format with actual values. Example:

    * `WD:BTC.BTC:10000:MAYA.CACAO`

    or you can abbreviate it to:

    * `-:BTC.BTC:10000:MAYA.CACAO`

{% hint style="info" %}
MAYAChain uses basis points for percentages. Example:

* 10000 = 100%
* 5000 = 50%
* 100 = 1%
{% endhint %}

3. In the amount field, type the minimum amount of CACAO observed by nodes. The value is as follows:
   * CACAO (1e-10): `0.0000000001`
4. In the address field paste ASSET [**Inbound Address**](https://www.mayascan.org/network) on MAYAChai&#x6E;**.**&#x20;

{% hint style="danger" %}
Make sure to refresh the page and NEVER use an old inbound address, as it they change every churn.
{% endhint %}

5. Press send.
6. Check your transaction/Maya Address on [**MayaScan**](https://www.mayascan.org/).
