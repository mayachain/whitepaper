---
description: Withdraw RUNE & CACAO Symmetrically from LP using Asgardex Front-End
---

# Asgardex: Withdraw RUNE & CACAO

1. Open Asgardex Desktop App.&#x20;

{% hint style="info" %}
You can download Asgardex [here](https://github.com/thorchain/asgardex-electron/releases/).
{% endhint %}

2. Press "Wallet" on the top mid-right.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.01.55.png" alt=""><figcaption></figcaption></figure>

3. Press the "Action" button next to RUNE.
4. From the dropdown menu press "Send".

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.05.00.png" alt=""><figcaption></figcaption></figure>

5. You will be directed to the send page. Note the fields: Address, Amount, and Memo.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-15 at 15.04.57.png" alt=""><figcaption></figcaption></figure>

6. In the Address field paste the RUNE inbound address found [here](https://mayanode.mayachain.info/mayachain/inbound\_addresses).

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-08 at 09.17.08.png" alt=""><figcaption></figcaption></figure>

7. In the amount field type an amount that is sufficient to cover the transaction fees.
8. In the memo field, based on your tier, paste the following:
   * Tier 1: "-:thor.rune:50:_**YourMayaAddress**_"
   * Tier 2: "-:thor.rune:150:_**YourMayaAddress**_"
   * Tier 3: "-:thor.rune:450:_**YourMayaAddress**_"

{% hint style="info" %}
50 represents %0.5, 150 represents %1.5, and 450 represents %4.5. As you can see we are using basis points to represent percentages, so if you have no tier and want to withdraw %100 you'd exchange the number in the middle of the memo with 10000.
{% endhint %}

{% hint style="warning" %}
Replace the _**YourMayaAddress**_ in the memo with your actual Maya wallet address.
{% endhint %}

9. Press send.
10. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
