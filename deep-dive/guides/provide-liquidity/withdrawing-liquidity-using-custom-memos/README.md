# Withdrawing liquidity using custom Memos

{% hint style="info" %}
As of writing this guide, the most affordable and accessible method to withdraw liquidity with memos is via the Maya Protocol side, which is currently supported by [El Dorado](https://www.eldorado.market/). By using [El Dorado](https://www.eldorado.market/), withdrawing funds only incurs a cost of 0.2 $CACAO, as opposed to the considerably higher transaction fees if you withdraw via Bitcoin or Ethereum side.\
\
**TLDR**; for cheaper and easier withdrawals use [El Dorado](https://www.eldorado.market/), but make sure you have $CACAO to pay fees.
{% endhint %}

{% embed url="https://docs.mayaprotocol.com/guides/withdrawing-liquidity-using-custom-memos/symmetric-withdrawals" %}
If you want to withdraw both assets equally from the liquidity pool (Example: BTC & CACAO, ETH & CACAO, RUNE & CACAO, etc..)
{% endembed %}

{% embed url="https://docs.mayaprotocol.com/guides/withdrawing-liquidity-using-custom-memos/asymmetric-withdrawals" %}
If you want to withdraw **only one** asset from the liquidity pool (Example: BTC, ETH, RUNE, CACAO, etc..). Note that this causes imbalance in the pool and a slip fee is charged to regain balance.
{% endembed %}
