---
description: Withdraw ETH from LP using El Dorado Front-End
---

# El Dorado: Withdraw ETH



1. Open [El Dorado WebApp](https://app.eldorado.market/).
2. If prompted, enter your password. If not, press "Connect" on the top right.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.35.23 (1).png" alt=""><figcaption></figcaption></figure>

3. &#x20;Select "Connect existing keystore".&#x20;

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.37.39 (1).png" alt=""><figcaption></figcaption></figure>

4. Upload your keystore and enter your password.
5. Press "View Wallet" on the top right.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 23.47.50.png" alt=""><figcaption></figcaption></figure>

6. Press the "Transfer" button next to CACAO

<figure><img src="../../../../../.gitbook/assets/Screenshot_2023-05-28_at_00.59.19.png" alt=""><figcaption></figcaption></figure>

7. Check the "Make a deposit with custom memo" box.

<figure><img src="../../../../../.gitbook/assets/Screenshot_2023-05-28_at_01.02.15.png" alt=""><figcaption></figcaption></figure>

8. In the memo field, based on your tier, paste the following:
   * Tier 1: "-:eth.eth:50:eth.eth"
   * Tier 2: "-:eth.eth:150:eth.eth"
   * Tier 3: "-:eth.eth:450:eth.eth"

{% hint style="info" %}
50 represents %0.5, 150 represents %1.5, and 450 represents %4.5. As you can see we are using basis points to represent percentages, so if you have no tier and want to withdraw %100 you'd exchange the number in the middle of the memo with 10000.
{% endhint %}

{% hint style="warning" %}
Remove all brackets and spaces from the memo, or it won't work.
{% endhint %}

9. In the amount field type 0.2 (this amount is denominated in $CACAO) to cover the transaction fees.
10. Press send.
11. Check your address on [the block explorer](https://www.explorer.mayachain.info/dashboard)  to view your transaction.
