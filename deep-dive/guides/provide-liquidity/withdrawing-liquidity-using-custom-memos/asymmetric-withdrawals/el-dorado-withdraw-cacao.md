# El Dorado: Withdraw CACAO

1. Open [El Dorado WebApp](https://app.eldorado.market/).
2. If prompted, enter your password. If not, press "Connect" on the top right.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.35.23 (1).png" alt=""><figcaption></figcaption></figure>

3. Select "Connect existing keystore".&#x20;

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 22.37.39 (1).png" alt=""><figcaption></figcaption></figure>

4. Upload your keystore and enter your password.

<figure><img src="../../../../../.gitbook/assets/Screenshot 2023-05-06 at 23.47.50.png" alt=""><figcaption></figcaption></figure>

5. Press "View Wallet" on the top right.

<details>

<summary>If your liquidity pair is BTC/ CACAO</summary>

1. Press the "Transfer" button next to BTC.
2. Check the "Make a deposit with custom memo" box.
3. In the memo field, based on your tier, paste the following:
   * Tier 1: "-:btc.btc:50:maya.cacao"
   * Tier 2: "-:btc.btc:150:maya.cacao"
   * Tier 3: "-:btc.btc:450:maya.cacao"
4. In the amount field type suitable amount to cover the transaction fees.

</details>

<details>

<summary>If your liquidity pair is ETH/ CACAO</summary>

1. Press the "Transfer" button next to ETH.
2. Check the "Make a deposit with custom memo" box.
3. In the memo field, based on your tier, paste the following:
   * Tier 1: "-:eth.eth:50:maya.cacao"
   * Tier 2: "-:eth.eth:150:maya.cacao"
   * Tier 3: "-:eth.eth:450:maya.cacao"
4. In the amount field type suitable amount to cover the transaction fees.

</details>

<details>

<summary>If your liquidity pair is RUNE/ CACAO</summary>

1. Press the "Transfer" button next to ETH.
2. Check the "Make a deposit with custom memo" box.
3. In the memo field, based on your tier, paste the following:
   * Tier 1: "-:eth.eth:50:maya.cacao"
   * Tier 2: "-:eth.eth:150:maya.cacao"
   * Tier 3: "-:eth.eth:450:maya.cacao"
4. In the amount field type suitable amount to cover the transaction fees.

</details>

{% hint style="info" %}
50 represents %0.5, 150 represents %1.5, and 450 represents %4.5. As you can see we are using basis points to represent percentages, so if you have no tier and want to withdraw %100 you'd exchange the number in the middle of the memo with 10000.
{% endhint %}

{% hint style="warning" %}
Remove all brackets, quotation marks and spaces from the memo, or it won't work.
{% endhint %}

9. In the amount field type 0.2 (this amount is denominated in $CACAO) to cover the transaction fees.
10. Press send.
11. Check your address on the [blockchain explorer](https://www.explorer.mayachain.info/dashboard) to see your transaction.
