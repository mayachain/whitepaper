---
description: Find our guides in the following sections.
---

# 🚶♂ Step-by-Step Guides

Here you'll find step-by-step guides on how to [**Setup Wallets**](https://docs.mayaprotocol.com/deep-dive/guides/setting-up-a-maya-wallet), [**Swap Assets**](https://docs.mayaprotocol.com/deep-dive/guides/swapping-assets) using custom memos, and [**Providing**](https://docs.mayaprotocol.com/deep-dive/guides/provide-liquidity) & [**Withdrawing**](https://docs.mayaprotocol.com/deep-dive/guides/withdraw-liquidity) Liquidity.
