---
description: A list of all $MAYA Airdrops, conditions and expected dates.
---

# 🪂 $MAYA Airdrops Guide

{% hint style="danger" %}
Beware of scammers! Maya Protocol will NEVER announce a surprise or emergency Airdrop. ALL airdrops will be announced well in advance on our Discord, Twitter, Reddit, and other Social Media. \
\
Make sure to check multiple sources of information before claiming any Airdrop.
{% endhint %}

{% hint style="info" %}
All future Airdrop dates are estimates.
{% endhint %}

## Tier 1 Liquidity Providers

On the 2nd of April, 2023, 2% of $MAYA token total supply (20,000 $MAYA) was Airdropped to _early_ LPs. 5% still remain to be distributed 200, 350, 500 days after the Liquidity Auction $CACAO distribution (17th of April, 2023). The 5% will be divided to thirds and Airdropped to Tier 1 LPs that stayed in the pools. Here are the dates:

* [x] 3rd of November, 2023.
* [x] 1st of April, 2024.
* [x] 29th of August, 2024.

{% hint style="info" %}
Liquidity Providers Airdrops are calculated based on liquidity left, not liquidity initially provided.
{% endhint %}

## $RUNE holders & Liquidity Providers

On the 17th of May, 2023, 7% of $MAYA tokens total supply (70,000 $MAYA) was Airdropped To $RUNE holders & $RUNE liquidity providers. The conditions were:&#x20;

**A.** Having $RUNE bonded in a Node,

**B.** Having $RUNE locked in a symmetrical LP position,

**C.** Having a $RUNE asymmetrical LP position,

**D.** Holding $RUNE in a wallet and/or,

**E.** Holding $RUNE in Maya pools during the auction.

**Cases A and B had a 4x boost. Cases C and D had a 2x boost. Case E had a 1x boost.**

## Maya Mask Holders

On the 1st of July, 2023, 1% of the total $MAYA tokens supply (10,000 $MAYA) was Airdropped to [**Maya Mask**](https://docs.mayaprotocol.com/deep-dive/maya-masks) Holders.

* 20% of $MAYA has been airdropped to the Maya Mask holders **who claimed** the Airdrop.
* 80% of $MAYA stays inside the masks to stake later on AZTECChain's staking module.
* The remainder of the unclaimed 20% will also be added to the staking module on AZTECChain.&#x20;
* Bear in mind there will be an $AZTEC airdrop as well for Maya Mask holders in the future.
* The $AZTEC amount for Airdrop and staking is identical to $MAYA.

## Early Node Operators and Liquidity Bonders

7% of the $MAYA total supply will be rewarded to early Liquidity Node operators.

* [x] 1.75% of all the $MAYA tokens (17,500 $MAYA) will be Airdropped to the active Liquidity Nodes and Liquidity Bonders **one** month after the first churn-in (After completion of 6 successful Churns).&#x20;
* [x] 1.75% of all the $MAYA tokens (17,500 $MAYA) will be Airdropped to the active Liquidity Nodes and Liquidity Bonders **three** months after the first churn-in (After completion of 18 successful Churns).&#x20;
* [x] 1.75% of all the $MAYA tokens (17,500 $MAYA) will be Airdropped to the active Liquidity Nodes and Liquidity Bonders **six** months after the first churn-in (After completion of 36 successful Churns).&#x20;
* [ ] 1.75% of all the $MAYA tokens (17,500 $MAYA) will be Airdropped to the active Liquidity Nodes and Liquidity Bonders **twelve** months after the first churn-in (After completion of 72 successful Churns).&#x20;

{% hint style="info" %}
**How will the $MAYA Airdrop be distributed among Liquidity Node operators and Liquidity Bonders?**

50% of the Airdrop will go to the LN operator regardless of their liquidity contribution, and 50% will go to the LBs (including the LN operator, if they have liquidity bonded as well).
{% endhint %}
