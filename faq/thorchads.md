---
description: $MAYA Airdrop to $RUNE holders.
---

# THORChads Airdrop

**Was there a $CACAO airdrop for $RUNE holders?**

No, the only way to get $CACAO was through the Liquidity Auction, but $RUNE owners qualify for $MAYA token airdrop. We are a friendly fork of THORChain and have no interest in vampire away any of their capital or users. We plan to share 7% of the total $MAYA token supply with them to acknowledge their support for THORChain, which makes Maya Protocol possible. Find all information related to Thorchads airdrop [here](https://www.mayaprotocol.com/latest-post/the-maya-airdrop-for-rune-holders).

**How do you qualify for the $MAYA Airdrop for Thorchads?**

7% of the total $Maya supply will go to RUNE owners simply by:

**A.** Having $RUNE bonded in a Node,

**B.** Having $RUNE locked in a symmetrical LP position,

**C.** Having a $RUNE asymmetrical LP position,

**D.** Holding $RUNE in a wallet and/or,

**E.** Holding $RUNE in Maya pools during the auction.

{% hint style="info" %}
RUNE address had to be connected to MAYA address to reward active wallets, prevent dilution of the Airdrop and the loss of a limited $MAYA supply.\
\
Snapshots were taken daily to prevent the gaming of the Airdrop.
{% endhint %}

**What wallets qualified for Rune owner's $Maya airdrop?**&#x20;

Non-custodial wallets such as Trust Wallet, XDEFI, Ledger, ThorWallet DEX.&#x20;

{% hint style="warning" %}
Custodial wallets such as Binance and Coinbase did’t qualify.
{% endhint %}

**What date would sending $RUNE to a Thorwallet have qualified the wallet?**&#x20;

All RUNE sent to a Thorwallet before March 6 would qualify.

**When did the $MAYA Airdrop take place?**&#x20;

The $MAYA Airdrop was completed on the 17th of May, 2023.

**How were $MAYA tokens claimed?**&#x20;

To claim $MAYA tokens, you will have to create a Maya wallet (THORWallet can do this for your) and send a minimum of 1 $RUNE to the Maya pool. Detailed instruction were also mentioned in the docs, our official Twitter, and our #Announcements channels on Discord.

