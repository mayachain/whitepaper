# Liquidity Auction

**What is the Maya Liquidity Auction?**

A Liquidity Auction is a fair launch combined with an airdrop. Users may provide external assets and are airdropped $CACAO proportional to their contribution. LP their tokens and get matched amounts with the offered token, in this case, $CACAO. The process will help us bootstrap the initial liquidity that the protocol needs to function and provide a superior user experience to our swappers with less slippage for swaps. Tier 1 participants will also get an allocation of $Maya tokens. [Read more](https://mayaprotocol.gitbook.io/whitepaper/readme/fair-launch) in our Whitepaper.

**Can I buy $CACAO after the Liquidity Auction?**

Remember, there will not be any other $CACAO issuances, so anybody that wants to own the token will have to acquire it from somebody that got it during this mint.

$CACAO’s price will likely be the cheapest ever (in $BTC terms) right after the auction. This makes it more attractive for people to invest heavily during the Liquidity Auction.

**When is the Maya Liquidity Auction?**

Maya Liquidity Auction started on March 16th, 2023 and ended on April 1st.

**What assets were used to provide Liquidity?**

Anybody could contribute supported assets, such as $BTC, $ETH, $USDT, $USDC and $RUNE, to the auction during the timeframe by sending them to a specified address.

**Was there a minimum entry?**

No, anyone could participate with as little as one satoshi! Excluding transaction fees of course.

There was also a $MAYA token airdrop for early liquidity providers and $RUNE holders.

**Could users withdraw during the Liquidity Auction?**&#x20;

Withdrawals during the Liquidity Auction period (21 days) were only allowed for Tier 2 & Tier 3 LPs without any limits. They can exit at any point if they no longer want to participate in the auction. Tier 1 LPs are our most committed LPS. Therefore withdrawals were not allowed during the Liquidity Auction period.

<figure><img src="../.gitbook/assets/WechatIMG95.jpeg" alt=""><figcaption></figcaption></figure>

**Could I withdraw my assets after the Liquidity Auction ends?**&#x20;

Once the LA ends, participants will receive their $CACAO. Maya designed a Liquidity Auction Withdrawals Tiers model to prevent rapid withdrawals and $CACAO dumping. Each Tier has a different lock-up period and daily withdrawal limit.&#x20;

**What safety precautions were taken during the auction?**

We’ve undergone thorough audits with [HalbornSecurity](https://halborn.com/) to ensure no vulnerabilities were found. We will also ran bug bounties on[https://immunefi.com/](https://immunefi.com/).&#x20;
