---
description: Learn about the vision and goals for Maya’s future!
---

# Roadmap 2023

<figure><img src="../.gitbook/assets/March.png" alt=""><figcaption></figcaption></figure>

### Q1

* **March 8th:** Maya Protocol launch, with the chain functioning optimally and producing blocks.
* **March 16th:** Liquidity Auction goes live, allowing users to deposit their assets and receive its equivalent in $CACAO (on a later date), donated and added to their LP.

### Q2

* **April 1st :** Liquidity Auction completion, with a raise of $11.5 million _(Block no.: 573,000)_.
* **April 17th:** $CACAO donation to Liquidity Auction Participants complete.
* **April 20th:** Trading on the Maya Protocol goes live.\

* **May 17th:** Tier 3 Liquidity unlock _(Block no.: 979,311)_.\

* **June 1st:** Initiation of Impermanent Loss Protection (ILP) for all pools.

### Q3

* **July 12th:** Tier 2 Liquidity unlock _(Block no.: 1,869,000)_.
* **July 28th:** Dash integrated & Liquidity-Nodes launches successfully on MAYAChain .

### Q4

* **Oct 12th:** Kujira LP live.&#x20;
* **TBD:** Savers feature go live.
* **Nov 3rd:** Tier 1 Liquidity Snapshot for $MAYA [**Airdrop**](https://docs.mayaprotocol.com/airdrop/usdmaya-airdrops#tier-1-liquidity-providers).&#x20;
  * Unlock at _Block no.: 3,453,000_.
* **TBD:** Arbitrum integration & Streaming Swaps go live.
* **TBD:** Aztec Chain launches.
* **TBD:** Cardano integration goes live.

{% hint style="info" %}
Future dates are estimations.
{% endhint %}
