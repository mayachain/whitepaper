# Maya Integration Guide

A small guide to show what you would need to integrate Maya.&#x20;

Maya Protocol can perform an action when a specific instruction is sent to Maya’s address for a particular chain.&#x20;

If you want to swap BTC to KUJI, you send a BTC transaction to Maya’s vault for Bitcoin, along with the memo: e.g. `=:KUJI.KUJI:kujira1q6g84pxu4tpr26dwp39fdrrwtpcnak44sf0x7f`&#x20;

This memo specifies the asset you want to swap to, and the destination address. You don’t have to manually look for these vaults, we have an endpoint, from which you can fetch the inbound vaults for Maya, and the memo’s you need. An example. You want to swap 0.001 BTC to KUJI. Your Kujira address is: `kujira1cd5kzygxfzylez8q4vak55pwj2z7a6hweat0zw`.&#x20;

Entering this in the quoting endpoint: [https://mayanode.mayachain.info/mayachain/quote/swap?from\_asset=BTC.BTC\&to\_asset=KUJI.KUJI\&amount=100000\&destination=kujira1cd5kzygxfzylez8q4vak55pwj2z7a6hweat0zw](https://mayanode.mayachain.info/mayachain/quote/swap?from\_asset=BTC.BTC\&to\_asset=KUJI.KUJI\&amount=100000\&destination=kujira1cd5kzygxfzylez8q4vak55pwj2z7a6hweat0zw)&#x20;

Gives:

```
{
  "expected_amount_out": "2352067700",
  "fees": {
    "affiliate": "0",
    "asset": "KUJI.KUJI",
    "outbound": "450000"
  },
  "inbound_address": "bc1q6u3d8780tytlrcrr69gj96gxhr9m24558lxee2",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:KUJI.KUJI:kujira1cd5kzygxfzylez8q4vak55pwj2z7a6hweat0zw",
  "outbound_delay_blocks": 1,
  "outbound_delay_seconds": 3,
  "slippage_bps": 2
}
```

You find the expected amount of KUJI you’ll get in 1e8 (in this case 23.52 KUJI), the address (`inbound_address`) you need to send your BTC to, and the memo you need to attack to the BTC transaction. That is everything you need, to swap using Maya Protocol.

{% hint style="info" %}
You can add limits, affiliate fees and more to this endpoint as well.
{% endhint %}

You can find all available and active pools in the Pools endpoint: [https://mayanode.mayachain.info/mayachain/pools](https://mayanode.mayachain.info/mayachain/pools).&#x20;

Bitcoin is `BTC.BTC`, USK is `KUJI.USK` (KUJI is the chain, USK the asset).&#x20;

Exceptions: It can happen that a chain is halted.&#x20;

This can be checked by looking at the Mimir endpoint: [https://mayanode.mayachain.info/mayachain/mimir](https://mayanode.mayachain.info/mayachain/mimir)&#x20;

If `HALTBTCTRADING = 1`, trading is halted. Trading is not halted when the parameter is set to 0. Currently we have the following chains: `BTC`, `ETH`, `THOR`, `DASH` & `KUJI`.&#x20;

You can find all endpoints here: [https://mayanode.mayachain.info/mayachain/doc](https://mayanode.mayachain.info/mayachain/doc) Hopefully this explains well how you can swap and integrate Maya’s cross-chain swaps! (edited)

