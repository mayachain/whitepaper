---
description: How to claim the $MAYA Airdrop as a Maya Mask Holder?
---

# $MAYA Airdrop for Maya Mask Holders

{% hint style="warning" %}
If you added BTC or RUNE liquidity during the Liquidity Auction, make sure to read the guide to the end. If not, please ignore this message.
{% endhint %}

<figure><img src="../.gitbook/assets/snapshot.png" alt=""><figcaption></figcaption></figure>

To be eligible for the $MAYA airdrop as a [Maya Mask](https://docs.mayaprotocol.com/maya-masks) holder, you need to have **either**:

* Added ETH/ USDT/ USDC liquidity **symmetrically** on Maya Protocol (whether during Liquidity Auction or later).

&#x20; **OR**

* Swapped ETH/ USDT/ USDC using Maya Protocol.

**AND**

* Have your [Maya Masks](https://docs.mayaprotocol.com/maya-masks) on the same ETH address you did either transactions from.

If you already satisfy both conditions, **you're eligible** and all you have to do is wait for the airdrop. The snapshot will be taken on June 20th.

The exact date of the airdrop will be announced on a later date on our Twitter & Discord, and will be updated here in the docs as well.

## What if I don't satisfy the conditions?

### Solution 1:

1. **If you want to add liquidity symmetrically**, currently you can only add ETH/ USDC/ USDT liquidity on Maya Protocol using ThorWallet.
2. **If you don't want to add liquidity, you can swap** ETH/ USDT/ USDC on Maya Protocol using either:
   * ThorWallet [WebApp](https://docs.mayaprotocol.com/guides/swapping-assets/how-to-swap-on-maya-using-thorwallet-web-app) or [Mobile App](https://docs.mayaprotocol.com/guides/swapping-assets/how-to-swap-on-maya-using-thorwallet-mobile-app).
   * [El Dorado](https://docs.mayaprotocol.com/guides/swapping-assets/how-to-swap-using-eldorado-webapp)
3. **Send your Maya Masks to your ETH address** that you did either transactions from.&#x20;

### **Solution 2:**&#x20;

You can import the wallet (seed phrase) that contains the Maya Masks to [ThorWallet](https://www.thorwallet.org/), then:

* Add ETH/ USDT/ USDC liquidity **symmetrically**.

&#x20; **OR**

* Swap ETH/ USDT/ USDC.

You can also import the wallet that contains the Maya Masks to [El Dorado](https://www.eldorado.market/), then:

* &#x20;Swap ETH/ USDT/ USDC.

{% hint style="info" %}
You can't add liquidity using [El Dorado](https://www.eldorado.market/) at the time this guide was written. If you want to add ETH/ USDT/ USDC liquidity use [ThorWallet](https://www.thorwallet.org/).
{% endhint %}

{% hint style="info" %}
ETH/ USDT/ USDC swaps MUST BE  **from** ETH **to** CACAO.
{% endhint %}

{% hint style="warning" %}
Your Maya Masks can NOT be on sale during the snapshot.
{% endhint %}

{% hint style="warning" %}
If you added BTC or RUNE liquidity during Liquidity Auction, **DO NOT** add ETH/ USDT/ USDC liquidity from the same wallet you participated with in the Liquidity Auction or your liquidity will be locked for the same period your LA is locked.\
\
You can create a new wallet and follow **solution 1,** or just follow **solution 2.**
{% endhint %}
