# What is the difference between Uniswap and MAYAChain?

Uniswap, like **MAYAChain**, offers a decentralized way to exchange cryptocurrencies. However, it only allows trading inside the Ethereum chain among ERC-20 tokens (i.e., you can't trade native assets from other blockchains, only representations of them called "wrapped" or "synthetic" tokens).

MAYAChain allows exchanges between native currencies in their respective blockchains, reducing attack vectors and counterparty risk for users and liquidity providers.
