---
description: List of all wallets, UIs and dAPP integrations with Maya Protocol.
---

# 📱 Maya dApps

{% hint style="info" %}
Maya Protocol is permissionless. Everyone is free to integrate Maya into their dapp, wallet or protocol. We support all interfaces integrating Maya. Get in touch with our marketing team on Discord to discuss co-marketing opportunities.&#x20;
{% endhint %}

{% hint style="danger" %}
**Disclaimer:** Maya Protocol will not support swapping interfaces and wallets containing ‘Maya’ in its name. We’ve learned from other protocols that this could have negative side effects. Hence, in the case an interface does choose to have ‘Maya’ in its name, Maya Protocol will not engage in co-marketing, not mention the interface on its website, docs, or Discord.
{% endhint %}

### Wallets:

#### [THORWallet Web App](https://app.thorwallet.org/)&#x20;

#### [THORWallet IOS Mobile App](https://apps.apple.com/app/thorwallet-defi-wallet/id1592064324)

#### [THORWallet Android Mobile App](https://play.google.com/store/apps/details?id=defisuisse.thorwallet\&pli=1)

#### [Leap Wallet](https://www.leapwallet.io/)

### User Interfaces:

#### &#x20;[El Dorado Market](https://www.eldorado.market/) (Supports MsgDeposit for Assets & CACAO)

#### [Asgardex](https://twitter.com/asgardex) (Supports MsgDeposit for CACAO)

#### [MayaSwap](https://www.mayaswap.org/) (Orderbook for Trading $MAYA)

#### [Rango Exchange](https://app.rango.exchange/) &#x20;

#### [Swapper](https://app.swapper.market/)&#x20;

#### [Defispot](https://app.defispot.com/trade?from=BTC.BTC\&to=ETH.ETH) ($CACAO is not supported, but MAYAChain routes are utilized)

### Portfolio Manager:

[**Pulsar**](https://app.pulsar.finance/)

### Tax Services:&#x20;

#### [Coinpanda](https://app.coinpanda.io/)&#x20;

### Blockchain Explorer

[**MayaScan**](https://www.mayascan.org/)

### Node Monitoring

[**MAYANode.Network**](https://mayanode.network/) powered by Liquify
