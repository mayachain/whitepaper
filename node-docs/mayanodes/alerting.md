---
description: Telegram Notification Service
---

# Alerting

## Community alert channels

To listen for update announcements, join the following channels:

- Telegram **MAYANode Announcements** [Join here](https://t.me/MayaProtocolOfficial)
- Discord **MAYAChain Community Devs** [Join here](https://discord.gg/mayaprotocol) especially #mayanode-mccn

{% hint style="info" %}
Be sure to use a pseudonym in order to prevent doxxing yourself. Node operators should remain anonymous for the health of the network and your personal/node security.
{% endhint %}
