---
description: How to join MAYAChain as an Node.
---

# Joining

## **Joining MAYAChain**

Now that you have a MAYANode deployed in your Kubernetes cluster, you need to start operating your node to join the network.

There are a couple of steps to follow to do so.

{% hint style="danger" %}
The first wallet to bond their liquidity to a node will be the operator address (i.e. the address that controls the node).
{% endhint %}

### 1. Add Liquidity with Node Operator wallet

MAYAChain nodes are backed up by added liquidity, just as a normal liquidity provider to one of the supported bonding pools: ARB, BTC, DASH, ETH, KUJI or THOR at the moment.

### 2. Bond for the first time using the Node Operator wallet

For now, the only ways to send custom deposit memos are using [custom memo deposit on El Dorado](https://app.tango.us/app/workflow/Deposit-with-custom-memo-in-El-Dorado-493be0ab5cd24e3da417b54eee871216) or with your `node-launcher` setup:

{% hint style="info" %}
The amount specified in the bond transaction will be sent to your node, which will then be used to set your ip address, version and node keys.
{% endhint %}

```bash
$ make shell # Select mayanode
=> Select MAYANode service
   mayanode
   bifrost
   midgard
   gateway
   thornode-daemon
   binance-daemon
   dogecoin-daemon
   gaia-daemon
   avalanche-daemon
   dash-daemon
   ethereum-daemon
   bitcoin-daemon
   litecoin-daemon
   bitcoin-cash-daemon
   midgard-timescaledb

Defaulted container "mayanode" out of: mayanode, init-external-ip (init)
/ # mayanode keys add [name for you key] --recover --keyring-backend file
> Enter your bip39 mnemonic
[paste your mnemonic]
Enter keyring passphrase:[Enter you password used for `node-launcher`]
/ # mayanode tx mayachain deposit 2000000 cacao bond:[pool]:[amount of LP units to bond]:[maya address] --from [name of your key] --keyring-backend file --yes --chain-id mayachain-mainnet-v1 --gas auto --node tcp://localhost:27147
```

### 3. Check your current node status

Next, verify your node is running correctly. \_\*\*\_To check the current status of your node, you can run the command status from the `node-launcher` repository on your terminal:

```
make status
```

You will get an output along those lines, the example below is for a mainnet node:

```
   __  ________  _____   _  __        __
  /  |/  /   \ \/ /   | / |/ /__  ___/ /__
 / /|_/ / /| |\  / /| |/    / __\/ __ / -_)
/_/  /_/_/ |_|/_/_/ |_/_/|_/\___/\_,_/\__/

ADDRESS     maya1v7gqc98d7d2sugsw5p4pshv0mm24mfmzgmj64n
IP
VERSION
STATUS      Active
BOND        100
REWARDS     0.00
SLASH       0
PREFLIGHT   {
  "status": "Standby",
  "reason": "node account has invalid registered IP address",
  "code": 1
}

API         http://18.217.85.10:1317/mayachain/doc/
RPC         http://18.217.85.10:27147
MIDGARD     http://18.217.85.10:8080/v2/doc

CHAIN      SYNC       BEHIND         TIP
MAYA       100.000%   0              2,086,031
THOR       100.000%   0              11,866,225
ETH        100.000%   0              17,773,085
ETH (beacon slot)  100.000%   0              6,958,344
BTC        100.000%   0              800,261
DASH       100.000%   0              1,910,014
```

Your node is running but as you can see in the \`Preflight\` section, your node is not yet ready to be churned in and currently is in standby status, since your node has no IP address setup yet.

But to be able to set up the node IP address, you first need to get it registered in the chain by sending your BOND.

{% hint style="warning" %}
Before sending the BOND, verify that your MAYANode is **fully synced** with connected chains. Connected chains such as Ethereum & Bitcoin may take a day to sync. If your node is fully bonded and is selected to churn into MAYAChain as ACTIVE without fully syncing all connected chains, you will immediately get slashed for missing observations and lose money. It is normal to see Ethereum sit on 99.999% for many hours - be patient.
{% endhint %}

### 4. Setup Node IP Address

{% hint style="warning" %}
If you get an error of not having enough balance. Send some cacao to your node
{% endhint %}

You must tell MAYAChain your IP-Address for its address book and seed-service to run properly:

```
make set-ip-address
```

If you run the status command again, you should now see a different message for the Preflight section saying you need to set your node keys.

{% hint style="info" %}
Once your IP address has been registered for discovery, you can use your own host for queries.
{% endhint %}

### 5. Setup Node keys

{% hint style="warning" %}
If you get an error of not having enough balance. Send some cacao to your node
{% endhint %}

Tell MAYAChain about your public keys for signing sessions:

```
make set-node-keys
```

If you run the `make status` command again, you should now see that your node is in status “ready” and is now ready to be churned in the next rotation.

### 6. Set Version

{% hint style="warning" %}
If you get an error of not having enough balance. Send some cacao to your node
{% endhint %}

Make sure your node broadcasts its latest version, else you won't churn in since MAYAChain enforces a version requirement. This version will appear in your `make status`. If you are on `0.0.0` then you haven't set your version:

```
make set-version
```

## Bonding The Right Amount

Although your node is ready to be churned in, it doesn’t mean it will be the next one to be selected, since someone else could have posted a higher bond than you. To maximize chances of a quick entry, monitor Midgard to see what everyone else is bonding and try to outbid them. Keep an eye on `maximumStandbyBond` and make sure you are bonding higher than that amount.

```
curl http://midgard.mayanode.mayachain.info/v2/network

resp:
 "bondMetrics" : {
      "minimumActiveBond" : "10001000000000",
      "medianStandbyBond" : "1010000000000",
      "medianActiveBond" : "15001000000000",
      "averageStandbyBond" : "1010000000000",
      "maximumActiveBond" : "15001000000000",
      "averageActiveBond" : "12006800000000",
      "maximumStandbyBond" : "1010000000000",
      "totalStandbyBond" : "1010000000000",
      "totalActiveBond" : "60034000000000",
      "minimumStandbyBond" : "1010000000000"
   }
```

The endpoint will show data on average, median, total, minimum and maximum bond amounts. For fastest entry, bond higher than the current maximum.

{% hint style="info" %}
CACAO is always displayed in 1e8 format, 100000000 = 1 CACAO
{% endhint %}

### Bonding More

At any time during standby or active, you can bond more by adding more liquidity to any LP position of the whitelisted bond providers (including the `bond_address`)
