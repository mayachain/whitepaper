---
description: Checklist of items for Node Operators
---

# ✔️ CHECKLIST

## Deploying

- [ ] I have set up a local `mayanode` repository with a `cluster-launcher` and `node-launcher` git cloned into the right sub-folders
- [ ] I have installed all the pre-requisites
- [ ] I have a AWS/DO account ready with credentials if using a service provider
- [ ] I have deployed a cluster and saved my node details (name of node, region)
- [ ] I have installed tools via `make tools`
- [ ] I have deployed a node by running `make <provider>`
- [ ] I have securely saved my MAYANode Password and MAYANode Mnemonic

{% content-ref url="kubernetes/" %}
[kubernetes](kubernetes/)
{% endcontent-ref %}

{% content-ref url="deploying.md" %}
[deploying.md](deploying.md)
{% endcontent-ref %}

## Joining

- [ ] I have added liquidity to one of the supported pools
- [ ] I have bonded my liquidity to my corresponding node
- [ ] I have confirmed my node has been credited the small bond and I am the `bond_address` of the node
- [ ] (Optional) I have confirmed I can unbond a small amount
- [ ] (Optional) I have whitelisted any bond providers for my node and set the node operator fee
- [ ] I have sent the final bond, higher than the MinimumBond
- [ ] I have run `make set-ip-address` to set my Node's IP address
- [ ] I have run `make set-node-keys` to set my Node's public keys
- [ ] I have run `make set-version` to set my Node's version
- [ ] I have verfied in `make status` that my node is ready to churn in

{% content-ref url="joining.md" %}
[joining.md](joining.md)
{% endcontent-ref %}

## Upgrading

- [ ] I have run `make status` to verify the current state of my node
- [ ] I have run `make pods` to verify all my node's services are running properly
- [ ] I have run `make update`
- [ ] I have run `make set-version` to set my new Node version
- [ ] I have run `make status` to verify the final state of my node

## Unbonding

- [ ] I have waited until my node is churned out and is in `standby`
- [ ] I have sent an UNBOND transaction from the same wallet registered to my node
- [ ] I have verified that my liquidity is no longer bonded

{% content-ref url="leaving.md" %}
[leaving.md](leaving.md)
{% endcontent-ref %}

## Leaving Whilst Active

- [ ] I have confirmed my node is active and I wish to leave before waiting to churn naturally
- [ ] I have sent the first LEAVE transaction and verified my node has "requested to leave".
- [ ] I have verified that a churn has taken place and my node is in STANDBY
- [ ] I have sent the final LEAVE transaction and my liquidity is no longer bonded

{% content-ref url="leaving.md" %}
[leaving.md](leaving.md)
{% endcontent-ref %}

## Leaving Whilst Standby

- [ ] I have verified that a churn has taken place and my node is in STANDBY
- [ ] I have verified that my node has returned all hot funds by checking my node's yggdrasil vault on the explorer
- [ ] I have sent the final LEAVE transaction and my liquidity is no longer bonded

{% content-ref url="leaving.md" %}
[leaving.md](leaving.md)
{% endcontent-ref %}

## Destroying a Node

- [ ] I have verified that I have either UNBONDED my entire BOND or LEFT and received my BOND back.
- [ ] I have run `make destroy destroy-tools` to destroy my node from `node-launcher`
- [ ] I have run `make destroy-<provider>` to destroy my cluster from `cluster-launcher`

{% content-ref url="leaving.md" %}
[leaving.md](leaving.md)
{% endcontent-ref %}
