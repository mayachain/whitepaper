---
description: Deploying a MAYANode and its associated services.
---

# Deploying

## **Deploy MAYANode services**

Now you have a Kubernetes cluster ready to use, you can install the MAYANode services.

{% hint style="info" %}
Helm charts are the defacto and currently easiest and simple way to package and deploy Kubernetes application. The team created different Helm charts to help to deploy all the necessary services. Please retrieve the source files from the Git repository here to follow the instructions below:

\*\*\*\*[ **https://gitlab.com/mayachain/devops/node-launcher**](https://gitlab.com/mayachain/devops/node-launcher)\*\*\*\*
{% endhint %}

### Requirements

* Running Kubernetes cluster
* Kubectl configured, ready and connected to running cluster

{% hint style="info" %}
If you came here from the Setup page, you are already good to go.
{% endhint %}

## Steps

Clone the `node-launcher` repo. All commands in this section are to be run inside of this repo.

```
git clone https://gitlab.com/mayachain/devops/node-launcher
cd node-launcher
git checkout master
```

### Install Helm 3

Install Helm 3 if not already available on your current machine:

```
make helm
make helm-plugins
```

### Tools

{% tabs %}
{% tab title="Deploy" %}
To deploy all tools, metrics, logs management, Kubernetes Dashboard, run the command below.

```
make tools
```
{% endtab %}

{% tab title="Destroy" %}
To destroy all those resources run the command below.

```
make destroy-tools
```
{% endtab %}
{% endtabs %}

If you are successful, you will see the following message:

![](../../maya-protocol-docs/.gitbook/assets/image%20\(23\)%20\(1\).png)

If there are any errors, they are typically fixed by running the command again.

## Deploy MAYANode

{% hint style="danger" %}
It is important to deploy the tools first before deploying the MAYANode services as some services will have metrics configuration that would fail and stop the MAYANode deployment.
{% endhint %}

### Before deploy

Currently MAYAChain supports ARB, BTC, ETH, DASH, KUJI & THOR. For all of the chains that we support a fullnode is used in order to read transactions from them. Sync time varies from chain to chain, but there's a couple of things you can do to speed up some of them:

#### ETH

By default the fullnode of ETH will connect checkpoint node provided by Maya Protocol. If you want to customize the checkpoint node to use you can edit the `checkpoint_url` in `ethereum-daemon/values.yaml` attribute.

#### THOR

In order to sync THOR from a ninerealms snapshot you need to explicitly enable it by setting `recover: true` in `thornode-daemon/values.yaml`.

You have multiple commands available to deploy different configurations of MAYANode. You can deploy mainnet or stagenet (currently not churning in third-party nodes). The commands deploy the umbrella chart `mayanode-stack` in the background in the Kubernetes namespace `mayanode` (or `mayanode-stagenet` for stagenet) by default.

```
make install
```

{% hint style="info" %}
If you are intending to run all chain clients, bond in & earn rewards, you want to choose "validator".
{% endhint %}

### After deploy

Once it's all deployed you'll need to sync from a snapshot.

```
$ make recover-maya
Maya only provides mainnet snapshots. Continue?
:: Are you sure? Confirm [y/n]: y

=> Select snapshot type
   pruned
   full
=> Select snapshot height
   1283017
   1359274
   1690642
   1837687
   1893933
```

We recommend choosing the defaults, but in case you have a specific need you can choose otherwise.

You are now ready to join the network:

{% content-ref url="joining.md" %}
[joining.md](joining.md)
{% endcontent-ref %}

### Debugging

{% hint style="info" %}
Set `mayanode` to be your default namespace so you don't need to type `-n mayanode` each time:

`kubectl config set-context --current --namespace=mayanode`
{% endhint %}

Use the following useful commands to view and debug accordingly. You should see everything running and active. Logs can be retrieved to find errors:

```
kubectl get pods -n mayanode
kubectl get pods --all-namespaces
kubectl logs -f <pod> -n mayanode
```

Kubernetes should automatically restart any service, but you can force a restart by running:

```
kubectl delete pod <pod> -n mayanode
```

{% hint style="warning" %}
Note, to expedite syncing external chains, it is feasible to continually delete the pod that has the slow-syncing chain daemon (eg, binance-daemon-xxx).

Killing it will automatically restart it with free resources and syncing is notably faster. You can check sync status by viewing logs for the client to find the synced chain tip and comparing it with the real-world blockheight, ("xxx" is your unique ID):

```
kubectl logs -f binance-daemon-xxx -n mayanode
```
{% endhint %}

{% hint style="info" %}
Get real-world blockheights on the external blockchain explorers, eg:\
[BTC Explorer](https://blockchain.com/explorer/assets/btc)

[DASH Explorer](https://explorer.dash.org)

[ETH Explorer](https://etherscan.io)

[THOR Explorer](https://viewblock.io/thorchain)
{% endhint %}

## CHART SUMMARY

#### MAYANode full stack umbrella chart

* **mayanode**: Umbrella chart packaging all services needed to run a fullnode or validator MAYANode.

This should be the only chart used to run MAYANode stack unless you know what you are doing and want to run each chart separately (not recommended).

#### MAYANode services:

* **mayanode**: MAYANode daemon
* **gateway**: MAYANode gateway proxy to get a single IP address for multiple deployments
* **bifrost**: Bifrost service
* **midgard**: Midgard API service

#### External services:

* **mayachain-daemon**: Mayachain fullnode daemon
* **arbitrum-daemon:** Arbitrum fullnode daemon
* **bitcoin-daemon**: Bitcoin fullnode daemon
* **dash-daemon**: Dash fullnode daemon
* **ethereum-daemon**: Ethereum fullnode daemon
* **kuji-daemon:** Kujira fullnode daemon
* **thornode-daemon**: Thorchain fullnode daemon
* **chain-daemon**: as required for supported chains

#### Tools

* **prometheus**: Prometheus stack for metrics
* **loki**: Loki stack for logs
* **kubernetes-dashboard**: Kubernetes dashboard
