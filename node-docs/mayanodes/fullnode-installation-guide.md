---
description: 'Chain ID: mayachain-mainnet-v1 | Current Node Version: v1.111.0'
---

# Fullnode Installation Guide

#### Install Go and other requirements

**Install Go**

We will use Go `v1.22.0` as an example here. The code below also cleanly removes any previous Go installation.

```
sudo rm -rvf /usr/local/go/
wget https://golang.org/dl/go1.22.2.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.22.2.linux-amd64.tar.gz
rm go1.22.2.linux-amd64.tar.gz
```

**Configure Go**

Unless you want to configure in a non-standard way, then set these in the `~/.bash_profile` for bash and `~/.zshrc` for zsh.

```
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
```

**Install dependencies protobuf-compiler**

```
apt-get update
apt-get install -y git \
     make \
     protobuf-compiler \
     curl \
     wget \
     jq \
     build-essential \
     musl-tools \
     linux-headers-generic
```

**Install docker and docker compose plugin**

https://docs.docker.com/engine/install/

Install Node

Install the current version of node binary.

\*You can see tags in [mayanode's repo tags](https://gitlab.com/mayachain/mayanode/-/tags)

```
git clone https://gitlab.com/mayachain/mayanode
cd mayanode
git checkout <latest version tag>
make protob
```

**Install binary**

```
TAG=mainnet NET=mainnet make install
```

**Run Node**

```
MAYA_COSMOS_TELEMETRY_ENABLED=true CHAIN_ID=mayachain-mainnet-v1 NET=mainnet SEEDS=15.156.45.237,18.217.85.10 bash ./build/scripts/fullnode.sh
```

**Create Service File**

Create a `mayanode.service` file in the `/etc/systemd/system` folder with the following code snippet. Make sure to replace `USER` with your Linux username. You need sudo privilege to do this step.

```
[Unit]
Description="Mayanode"
After=network-online.target

[Service]
User=USER
ExecStartPre=/home/USER/.go/bin/mayanode render-config
ExecStart=/home/USER/.go/bin/mayanode start
Restart=always
RestartSec=3
LimitNOFILE=4096
Environment="MAYA_COSMOS_TELEMETRY_ENABLED=true"
Environment="CHAIN_ID=mayachain-mainnet-v1"
Environment="NET=mainnet"
Environment="SIGNER_NAME=mayachain"
Environment="SIGNER_PASSWD=password"

[Install]
WantedBy=multi-user.target
```

**Download Snapshot**

Please use a [snapshot](https://public-snapshots-mayanode.s3.amazonaws.com/full/index.html) in order to avoid syncing from start. You will need to install [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

```
mkdir -p ~/.mayanode/data
aws s3 cp --recursive s3://public-snapshots-mayanode/full/<snapshot height> ~/.mayanode/data
```

**Start Node Service**

```
# Enable service
sudo systemctl enable mayanode.service

# Start service
sudo systemctl start mayanode

# Check logs
sudo journalctl -feu mayanode
```

Other Considerations

This installation guide is the bare minimum to get a node started. You should consider the following as you become a more experienced node operator.

* Configure firewall to close most ports while only leaving the p2p port (typically 27146) open
* Use custom ports for each node so you can run multiple nodes on the same server

If you find a bug in this installation guide, please reach out to our Discord Server and let us know.
