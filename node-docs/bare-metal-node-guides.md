---
description: An overview of guides made for bare metal setups
---

# Bare Metal Node Guides

Bare Metal nodes come with several advantages over cloud provider nodes. Although the initial costs for purchasing all node equipment are higher, the monthly costs are eventually much lower, and thus the nett yield is higher. Bare Metal nodes are also more resilient. They can't be taken down by hosting services in the case of governmental bans on crypto infrastructure.

&#x20;Several guides have been made for purchasing and setting up bare metal nodes.

### THORChain Validators Migration **- by @D5Sammy**

{% embed url="https://medium.com/@d5sammy/thorchain-validator-migration-the-exhaustive-guide-2d0b9823857f" %}

### Part 2: Multi-Node using MicroK8s - by @D5Sammy

{% embed url="https://medium.com/@d5sammy/thorchain-bare-metal-validator-part-2-multi-node-using-microk8s-e5f98ec2c82c" %}

### Part 3: Maya’s Chocolate- by @D5Sammy

{% embed url="https://medium.com/@d5sammy/thorchain-bare-metal-validator-part-3-mayas-chocolate-d61cbdfe2d04" %}

### Build your own THORChain Validator Node - by @Scorch

{% embed url="https://medium.com/@scorch_ed/build-your-own-thorchain-validator-node-step-by-step-guide-a516e6c67b9f" %}

### Bare-metal MAYANode - by @Runetard

{% embed url="https://medium.com/@Runetard/bare-metal-mayanode-the-comprehensive-n00b-guide-c0da8af0c941" %}
