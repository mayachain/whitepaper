---
description: A guide on how to bond/unbond LP units on MAYAChain using El Dorado Market
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Bonding & Unbonding Guide

{% hint style="warning" %}
Only proceed with the following steps after you've been whitelisted by your node operator
{% endhint %}

### 1. Connecting your wallet

Visit [https://app.eldorado.market/swap](https://app.eldorado.market/swap) and connect your wallet

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 21.57.57.png" alt=""><figcaption></figcaption></figure>

### 2. Select the keystore file of your wallet and enter your password

This keystore file should also contain the LP position(s) you want to bond.

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.01.45.png" alt=""><figcaption></figcaption></figure>

### 3. Go to your wallet

Click on Wallet on the top right corner of your screen. This will bring you to a page that shows the balances of your wallet

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.03.51.png" alt=""><figcaption></figcaption></figure>

### 4. Go to the $CACAO transfer screen

Click on `Transfer`  button next to your CACAO balance.

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.06.38.png" alt=""><figcaption></figcaption></figure>

### 5. Hit the Custom Memo button

Click on the button right before `Make a deposit with custom memo`.

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.12.14.png" alt=""><figcaption></figcaption></figure>

### 6. Fill in the memo

You should see the following screen:

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.15.58.png" alt=""><figcaption></figcaption></figure>

The memo is the most important of this whole process.&#x20;

The structure for the bonding memo is as follows:

`BOND:ASSET:LPUNITS:NODEADDRESS`

* `ASSET` is one of the assets available to bond. Check the [Pools endpoint](https://mayanode.mayachain.info/mayachain/pools) for all current pools

{% hint style="warning" %}
Not all assets are available for bonding. Check with your node operator or on Discord which assets are bondable
{% endhint %}

* `LPUNITS` specifies how many LP units of given asset you want to bond. You can enter your Maya address in [MayaScan](https://mayascan.org), press on your desired pool, to see the amount of LP units you have on each pool.
* `NODEADDRESS` is the Maya address of the Node you want to bond to.

An example memo would look like this:

`BOND:THOR.RUNE:100000000000:maya13pv4zgyy85lzadjjkyvy3mmmr7l0yx9lzamay4` - _Bond 100000000000 THOR.RUNE LP Units to node maya13pv4zgyy85lzadjjkyvy3mmmr7l0yx9lzamay4_

Now that you have the memo, all that's left is the Amount to fill in. **The Amount is** **always 1 CACAO.**

### **7. Send the transaction**

&#x20;You should have all the boxes filled in now. The final step is sending the transaction.

<figure><img src="../.gitbook/assets/Screenshot 2023-12-06 at 22.37.56.png" alt=""><figcaption></figcaption></figure>

Once you have sent the transaction, check with your node operator went well. You should now be a bond provider to a MAYANode.

## Unbonding

1. First, you'll need to check with your Node operator to check if your node is churned out (you can't unbond otherwise).&#x20;
2. Follow the exact same steps as above. The only change will be the memo which should use UNBOND instead of BOND, with this format `UNBOND:ASSET:LPUNITS:NODEADDRESS`
   * Example: `UNBOND:THOR.RUNE:100000000000:maya13pv4zgyy85lzadjjkyvy3mmmr7l0yx9lzamay4`

{% hint style="info" %}
New solutions are being worked on to make bonding and managing nodes easier.
{% endhint %}
