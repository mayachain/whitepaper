---
description: How to install & interact with MAYACli.
---

# CLI

## Install (Mac)

### Prerequisites

1. `xcode-select xcode-select --install`
2. Homebrew: [https://brew.sh](https://brew.sh)

### GoLang

Install go v1.18.1: [https://go.dev/doc/install](https://go.dev/doc/install)

```shell
# Set PATH
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOROOT:$GOPATH:$GOBIN
```

### Protobuf

```shell
# Install Protobuf
brew install protobuf
brew install protoc-gen-go
go install google.golang.org/protobuf/cmd/protoc-gen-go@latestellina
```

### GNU Utils

```shell
# Install GNU find
brew install findutils

# Set PATH
export PATH=$(brew --prefix)/opt/findutils/libexec/gnubin:$PATH
```

### Docker

```shell
# Install docker
brew install homebrew/cask/docker
```

### MAYANode

```shell
# Clone repo and install dependencies
git clone https://gitlab.com/mayachain/mayanode
# Docker must be started...
make openapi
make protob-docker
make install
```

## Commands

`mayanode --help`

```
MAYAChain Network

Usage:
  MAYAChain [command]

Available Commands:
  add-genesis-account Add a genesis account to genesis.json
  collect-gentxs      Collect genesis txs and output a genesis.json file
  debug               Tool for helping with debugging your application
  ed25519             Generate an ed25519 keys
  export              Export state to JSON
  gentx               Generate a genesis tx carrying a self delegation
  help                Help about any command
  init                Initialize private validator, p2p, genesis, and application configuration files
  keys                Manage your application's keys
  migrate             Migrate genesis to a specified target version
  pubkey              Convert Proto3 JSON encoded pubkey to bech32 format
  query               Querying subcommands
  start               Run the full node
  status              Query remote node for status
  tendermint          Tendermint subcommands
  tx                  Transactions subcommands
  unsafe-reset-all    Resets the blockchain database, removes address book files, and resets data/priv_validator_state.json to the genesis state
  validate-genesis    validates the genesis file at the default location or at the location passed as an arg
  version             Print the application binary version information

Flags:
  -h, --help                help for MAYAChain
      --home string         directory for config and data (default "/Users/dev/.thornode")
      --log_format string   The logging format (json|plain) (default "plain")
      --log_level string    The logging level (trace|debug|info|warn|error|fatal|panic) (default "info")
      --trace               print out full stack trace on errors
```

### Popular Commands

#### Add new account

```
mayanode keys add {accountName}
```

#### Add existing account (via mnemonic)

```
mayanode keys add {accountName} --recover
```

#### List all accounts

```
mayanode keys list
```

## Send Transaction

### Create Transaction

```
# Sender: maya1a894mfpeqc9qzf28jc2ac35ujnuzluf5xwpy87
# Receiver: maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j
# Amount: 1 CACAO (in 1e10 notation)
thorcli tx bank send maya1a894mfpeqc9qzf28jc2ac35ujnuzluf5xwpy87 maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j 10000000000cacao --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 --gas 3000000 --generate-only >> tx_raw.json
```

This will output a file called `tx_raw.json`. Edit this file and change the `@type` field from `/cosmos.bank.v1beta1.MsgSend` to `/types.MsgSend`.

The `tx_raw.json` transaction should look like this:

```
{"body":{"messages":[{"@type":"/types.MsgSend","from_address":"maya1a894mfpeqc9qzf28jc2ac35ujnuzluf5xwpy87",
"to_address":"maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j",
"amount":[{"denom":"cacao","amount":"10000000000"}]}],"memo":"",
"timeout_height":"0","extension_options":[],"non_critical_extension_options":[]},
"auth_info":{"signer_infos":[],
"fee":{"amount":[],"gas_limit":"3000000","payer":"","granter":""}},
"signatures":[]}
```

### Sign Transaction

```
mayanode tx sign tx_raw.json --from {accountName} --sign-mode amino-json --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 >> tx.json
```

This will output a file called `tx.json`.

### Broadcast Transaction

```
mayanode tx broadcast tx.json --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 --gas auto
```
