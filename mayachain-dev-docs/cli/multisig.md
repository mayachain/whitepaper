---
description: How to setup a multisig & send transactions.
---

# Multisig

### Setup Multisig

First, collect the pubkeys that will be part of the multisig. They can be printed using `thorcli`:

```
mayanode keys show person1 --pubkey
```

Then share the pubkey with the other parties. Each party can add these pubkeys:

```
mayanode keys add person2 --pubkey {pubkey}
```

Each party can create the multisig (here a 2/3):

```
mayanode keys add multisig --multisig person1,person2,person3 --multisig-threshold 2
```

### Create Transaction

Any of the parties can create the raw transaction:

```
# Sender: maya1ry7jujk4dwjfcrlr2af0e7xeqkgl3esjdqj5v8
# Receiver: maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j
# Amount: 1 CACAO (in 1e10 notation)
mayacli tx bank send maya1ry7jujk4dwjfcrlr2af0e7xeqkgl3esjdqj5v8 maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j 10000000000cacao --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 --gas 3000000 --generate-only >> tx_raw.json
```

This will output a file called `tx_raw.json`. Edit this file and change the `@type` field from `/cosmos.bank.v1beta1.MsgSend` to `/types.MsgSend`.

The `tx_raw.json` transaction should look like this:

```
{"body":{"messages":[{"@type":"/types.MsgSend","from_address":"maya1ry7jujk4dwjfcrlr2af0e7xeqkgl3esjdqj5v8",
"to_address":"maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j",
"amount":[{"denom":"cacao","amount":"10000000000"}]}],"memo":"",
"timeout_height":"0","extension_options":[],"non_critical_extension_options":[]},
"auth_info":{"signer_infos":[],
"fee":{"amount":[],"gas_limit":"3000000","payer":"","granter":""}},
"signatures":[]}
```

### Sign Transaction

The transaction needs to be signed by 2 of the 3 parties (as configured above, when setting up the multisig).

#### From Person 1

```
mayanode tx sign --from person1 --multisig multisig tx_raw.json --sign-mode amino-json --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 >> tx_signed_1.json
```

This will output a file called `tx_signed_1.json`.

#### From Person 2

```
mayanode tx sign --from person2 --multisig multisig tx_raw.json --sign-mode amino-json --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 >> tx_signed_2.json
```

This will output a file called `tx_signed_2.json`.

### Build Transaction

#### Gather Signatures

The party, who wants to broadcast the transaction, needs to gather all json signature files from the other parties.

#### Multisig Sign

First, get the sequence and account number for the multisig address:

```
curl https://mayanode.mayachain.info/cosmos/auth/v1beta1/accounts/maya1y28pn7vlalm5rdq98ssjfq43l6ymnyrp69dd0j
```

Then combine the signatures into a single one (make sure to update the account number `-a` and the sequence number `-s`:

```
# Account number: 33401 (see curl output)
# Sequence number: 0 (see curl output)
mayanode tx multisign tx_raw.json multisig tx_signed_1.json tx_signed_2.json -a 33401 -s 0 --from multisig --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 >> tx.json
```

This will output a final file called `tx.json`.

### Broadcast Transaction

```
mayanode tx broadcast tx.json --chain-id mayachain-mainnet-v1 --node https://tendermint.mayachain.info:443 --gas auto
```
