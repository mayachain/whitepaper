---
description: >-
  Each version shows all the Merge Requests that were deployed with that
  version.
---

# THORChain Version Updates I

{% hint style="info" %}
If it's checkmarked, it means we have implemented it. If a version itself has a checkmark, it means we already have all the MRs that we want from that version and we won't add any of the unchecked ones (if any).
{% endhint %}

### v1.127.2

* [x] \[evm] Allow Token transferOut to Burst Over Max Gas PR: [3418](https://gitlab.com/thorchain/thornode/-/merge\_requests/3418)
* [ ] \[utxo] Optimizations to Avoid Ordinal Issues PR: [3416](https://gitlab.com/thorchain/thornode/-/merge\_requests/3416)
* [ ] Skip block if nil when reprocessing tx in eth block scanner PR: [3415](https://gitlab.com/thorchain/thornode/-/merge\_requests/3415)
* [ ] \[Version-unspecific] TxOutStoreItem RetrievalKey field for not needing to rederive key-value pair key for overwrite/deletion PR: [3382](https://gitlab.com/thorchain/thornode/-/merge\_requests/3382)
* [ ] \[cleanup] Remove Deprecated UTXO Clients PR: [3378](https://gitlab.com/thorchain/thornode/-/merge\_requests/3378)

### v1.127.0

* [ ] \[fix] Missing Whitelist Tokens and Add Linter PR: [3395](https://gitlab.com/thorchain/thornode/-/merge\_requests/3395)
* [ ] \[V127-specific] Check only signing transaction period blocks in handler\_common\_outbound PR: [3386](https://gitlab.com/thorchain/thornode/-/merge\_requests/3386)
* [ ] Testing CI change from an external repo PR: [3380](https://gitlab.com/thorchain/thornode/-/merge\_requests/3380)
* [ ] ~~\[AVAX pool creation whitelist] Add WSOL, ALOT tokens PR:~~ [~~3379~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3379)
* [x] \[dex whitelist] Whitelist LiFi contracts PR: [3377](https://gitlab.com/thorchain/thornode/-/merge\_requests/3377)
* [ ] \[ci] Update gitlab-trigger-ci.sh to set upstream in all cases PR: [3374](https://gitlab.com/thorchain/thornode/-/merge\_requests/3374)
* [ ] \[bifrost] Pipeline Signing Routines PR: [3372](https://gitlab.com/thorchain/thornode/-/merge\_requests/3372)
* [ ] \[Version-unspecific] Drop only specific transactions from transaction- specific getThorchainTxIns or GetObservationsStdTx parsing problems PR: [3369](https://gitlab.com/thorchain/thornode/-/merge\_requests/3369)
* [ ] \[Version-unspecific] Instant observations (and their SetSigned) rather than delayed to the end of the processTransactions queue (after any failed transaction RemoveSigned) PR: [3367](https://gitlab.com/thorchain/thornode/-/merge\_requests/3367)
* [ ] \[api] Add CloutSpent to Outbound Responses PR: [3366](https://gitlab.com/thorchain/thornode/-/merge\_requests/3366)
* [ ] \[V127-specific] Deduct pending outbounds to get available funds in LackSigning PR: [3364](https://gitlab.com/thorchain/thornode/-/merge\_requests/3364)
* [ ] \[Version-unspecific] GAIA ReportSolvency balances consistent with specified heights PR: [3360](https://gitlab.com/thorchain/thornode/-/merge\_requests/3360)
* [x] Coalesce Reschedule Heights and Align TSS Timeouts #check-lint-warning PR:[3359](https://gitlab.com/thorchain/thornode/-/merge\_requests/3359)
* [ ] Documentation update PR: [3358](https://gitlab.com/thorchain/thornode/-/merge\_requests/3358)
* [ ] \[querier] Return error from swap quote endpoint if affiliate swap will fail PR: [3357](https://gitlab.com/thorchain/thornode/-/merge\_requests/3357)
* [ ] \[Version-unspecific] GetMimir lint check PR: [3355](https://gitlab.com/thorchain/thornode/-/merge\_requests/3355)
* [ ] remove keeper's dependency on keeper/v1 PR: [3354](https://gitlab.com/thorchain/thornode/-/merge\_requests/3354)
* [ ] \[stagenet] fix tokenlists and bep2 owner address PR: [3353](https://gitlab.com/thorchain/thornode/-/merge\_requests/3353)
* [ ] \[Version-unspecific] Report how many regression tests have been completed so far PR: [3352](https://gitlab.com/thorchain/thornode/-/merge\_requests/3352)
* [ ] \[evm] Prevent Bifrost from Lowering Outbound Gas Rate PR: [3333](https://gitlab.com/thorchain/thornode/-/merge\_requests/3333)
* [ ] Unify max memo handling in querier PR: [3283](https://gitlab.com/thorchain/thornode/-/merge\_requests/3283)

### v1.126.0

* [x] \[query] return \[] instead of null for empty swap queue PR: [3350](https://gitlab.com/thorchain/thornode/-/merge\_requests/3350)
* [ ] \[fix] Error swaps to BNB accounts with memo flag set PR: [3349](https://gitlab.com/thorchain/thornode/-/merge\_requests/3349)
* [ ] \[fix] avoid float math for utxo sats PR: [3348](https://gitlab.com/thorchain/thornode/-/merge\_requests/3348)
* [ ] \[mocknet] BSC 1.3.7 + BCH 27.0.0 PR: [3344](https://gitlab.com/thorchain/thornode/-/merge\_requests/3344)
* [ ] \[ci] Add router contract tests PR: [3342](https://gitlab.com/thorchain/thornode/-/merge\_requests/3342)
* [ ] Update trunk config PR: [3340](https://gitlab.com/thorchain/thornode/-/merge\_requests/3340)
* [ ] \[V126-specific] Random asgard vault selection PR: [3338](https://gitlab.com/thorchain/thornode/-/merge\_requests/3338)
* [ ] \[V126-Specific] Process all POL pools in pol cycle PR: [3335](https://gitlab.com/thorchain/thornode/-/merge\_requests/3335)
* [ ] \[evm] Update Unstuck to Remove from Signer Cache PR: [3334](https://gitlab.com/thorchain/thornode/-/merge\_requests/3334)
* [ ] \[mocknet] Ethereum 1.13.8 PR: [3332](https://gitlab.com/thorchain/thornode/-/merge\_requests/3332)
* [ ] \[evm] Fix Solvency Check on Tokens PR: [3331](https://gitlab.com/thorchain/thornode/-/merge\_requests/3331)
* [ ] \[evm] Limit Pending Nonce Usage PR: [3330](https://gitlab.com/thorchain/thornode/-/merge\_requests/3330)
* [ ] Bifrost scanner height diff PR: [3329](https://gitlab.com/thorchain/thornode/-/merge\_requests/3329)
* [ ] \[mocknet] Image Updates PR: [3325](https://gitlab.com/thorchain/thornode/-/merge\_requests/3325)
* [x] \[evm] Remove Signed Tx on Observation (BSC) PR: [3323](https://gitlab.com/thorchain/thornode/-/merge\_requests/3323)
* [x] \[fix] Bifrost Signing Status Endpoint PR: [3322](https://gitlab.com/thorchain/thornode/-/merge\_requests/3322)
* [ ] \[V126-Specific] Remove Low bond Node accounts PR: [3321](https://gitlab.com/thorchain/thornode/-/merge\_requests/3321)
* [ ] Resolve "FIX: Lending API Improvements" - Replacing PR #3244 PR: [3318](https://gitlab.com/thorchain/thornode/-/merge\_requests/3318)
* [ ] \[V126-specific; performance] GetAddress for (string) Address rather than GetThorAddress for (\[]byte) AccAddress in needsNewVault PR: [3317](https://gitlab.com/thorchain/thornode/-/merge\_requests/3317)
* [ ] \[FIX] Sanitized Swapper Clout init list PR: [3314](https://gitlab.com/thorchain/thornode/-/merge\_requests/3314)
* [ ] \[docs] Refactor for MDBook and Setup Pages PR: [3308](https://gitlab.com/thorchain/thornode/-/merge\_requests/3308)
* [ ] \[docs] Update Vanilla Docker Pruned Snapshot Instructions PR: [3299](https://gitlab.com/thorchain/thornode/-/merge\_requests/3299)
* [ ] \[DEX whitelist] OKX Routers PR: [3298](https://gitlab.com/thorchain/thornode/-/merge\_requests/3298)
* [ ] Remove unused testnet #check-lint-warning PR: [3296](https://gitlab.com/thorchain/thornode/-/merge\_requests/3296)
* [ ] \[DEX Whitelist] Symbiosis PR: [3291](https://gitlab.com/thorchain/thornode/-/merge\_requests/3291)
* [ ] Adds several tokens PR: [3278](https://gitlab.com/thorchain/thornode/-/merge\_requests/3278)

## v1.125.3

* [x] getBlockReceipts fixups PR: [!3320](https://gitlab.com/thorchain/thornode/-/merge\_requests/3320)
* [x] \[evm] Get block receipts rather than batch call PR: [!3319](https://gitlab.com/thorchain/thornode/-/merge\_requests/3319)
* [ ] Make thornode binary build deterministic PR: [!3303](https://gitlab.com/thorchain/thornode/-/merge\_requests/3303)
* [ ] \[Dev Docs] Callout that BTC Taproot is currently not supported PR: [!3302](https://gitlab.com/thorchain/thornode/-/merge\_requests/3302)
* [ ] \[build] Auto-Install Stringer for Generate PR: [!3275](https://gitlab.com/thorchain/thornode/-/merge\_requests/3275)

## v1.125.2

* [x] \[fix] Stuck EVM Outbounds on Broadcast Fail PR: [!3316](https://gitlab.com/thorchain/thornode/-/merge\_requests/3316)

## v1.125.1

* [ ] bump version to 1.125.1 PR: [!3313](https://gitlab.com/thorchain/thornode/-/merge\_requests/3313)
* [ ] \[fix] Keysign Block Signature Mismatch PR: [!3312](https://gitlab.com/thorchain/thornode/-/merge\_requests/3312)
* [x] Skip missing client in signing endpoint PR: [!3309](https://gitlab.com/thorchain/thornode/-/merge\_requests/3309)

## v1.125.0

* [ ] \[lint] Fix Failure PR: [!3306](https://gitlab.com/thorchain/thornode/-/merge\_requests/3306)
* [ ] store migrate to create init swapper clout values PR: [!3300](https://gitlab.com/thorchain/thornode/-/merge\_requests/3300)
* [ ] \[lint] Fix Lint Failure PR: [!3290](https://gitlab.com/thorchain/thornode/-/merge\_requests/3290)
* [ ] mimir v2 key change issue #check-lint-warning PR: [!3286](https://gitlab.com/thorchain/thornode/-/merge\_requests/3286)
* [ ] \[migrate] Avax Double Spend Bond Refunds PR: [!3285](https://gitlab.com/thorchain/thornode/-/merge\_requests/3285)
* [ ] \[test] Regression Test Improvements PR: [!3282](https://gitlab.com/thorchain/thornode/-/merge\_requests/3282)
* [ ] \[utxo] Improve Re-Org Rescan Handling PR: [!3281](https://gitlab.com/thorchain/thornode/-/merge\_requests/3281)
* [ ] \[config] Expose UTXO Min Confirmations PR: [!3279](https://gitlab.com/thorchain/thornode/-/merge\_requests/3279)
* [ ] \[lint] Disallow Variable Shadowing #check-lint-warning PR: [!3276](https://gitlab.com/thorchain/thornode/-/merge\_requests/3276)
* [x] \[Version-unspecific] StreamingSwap endpoints' source\_asset and target\_asset and destination fields PR: [!3274](https://gitlab.com/thorchain/thornode/-/merge\_requests/3274)
* [ ] \[Version-unspecific] For other MRs' lint jobs, make nodeAddressValidatorAddressPair unversioned and move it to manager\_slasher.go #check-lint-warning PR: [!3273](https://gitlab.com/thorchain/thornode/-/merge\_requests/3273)
* [x] \[Version-unspecific] Reallow EVM AddSignedTxItem cancel transactions upon "already known" SendTransaction error PR: [!3269](https://gitlab.com/thorchain/thornode/-/merge\_requests/3269)
* [ ] \[ci] Set upstream in gitlab-trigger-ci.sh PR: [!3267](https://gitlab.com/thorchain/thornode/-/merge\_requests/3267)
* [ ] \[patch] Needs new vault patch #check-lint-warning PR: [!3266](https://gitlab.com/thorchain/thornode/-/merge\_requests/3266)
* [x] Bifrost health api endpoints PR: [!3263](https://gitlab.com/thorchain/thornode/-/merge\_requests/3263)
* [ ] Dev docs merge PR: [!3261](https://gitlab.com/thorchain/thornode/-/merge\_requests/3261)
* [ ] \[Version-unspecific] queryMimirV2IDs: jsonify the map for a deterministic sorted order PR: [!3259](https://gitlab.com/thorchain/thornode/-/merge\_requests/3259)
* [ ] Bitcoin UTXO Client V2 PR: [!3252](https://gitlab.com/thorchain/thornode/-/merge\_requests/3252)
* [x] Add XVG to ERC-20 whitelist PR: [!3250](https://gitlab.com/thorchain/thornode/-/merge\_requests/3250)
* [ ] \[cleanup] Use Stringer for Type Strings PR: [!3249](https://gitlab.com/thorchain/thornode/-/merge\_requests/3249)
* [ ] \[querier] Mimir v2 api tweaks PR: [!3245](https://gitlab.com/thorchain/thornode/-/merge\_requests/3245)
* [ ] \[lint] Add Spell Check and Improve CI Lint Speed #check-lint-warning PR: [!3237](https://gitlab.com/thorchain/thornode/-/merge\_requests/3237)
* [ ] \[config] Allow Enabling of Thornode GRPC PR: [!3235](https://gitlab.com/thorchain/thornode/-/merge\_requests/3235)
* [ ] \[lint] Add Static Analysis to Ensure Immutable Mimir V2 IDs PR: [!3233](https://gitlab.com/thorchain/thornode/-/merge\_requests/3233)
* [x] \[V125-specific] Confirmation value adjustment for chains PR: [!3231](https://gitlab.com/thorchain/thornode/-/merge\_requests/3231)
* [ ] Swapper clout #check-lint-warning PR: [!3224](https://gitlab.com/thorchain/thornode/-/merge\_requests/3224)
* [ ] ~~Resolve "\[feature] add repay percentage to close loan" PR:~~ [~~!3221~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3221)
* [ ] Bump trunk client version PR: [!3054](https://gitlab.com/thorchain/thornode/-/merge\_requests/3054)

## v1.124.2

* [x] \[fix] fix shadowing of fromAddr for evm auto-observe PR: [!3268](https://gitlab.com/thorchain/thornode/-/merge\_requests/3268)
* [ ] \[fix] Mark Lagging Block Scanner Unhealthy PR: [!3260](https://gitlab.com/thorchain/thornode/-/merge\_requests/3260)

## v1.124.1

* [x] \[performance] Batch Fetch EVM Block Transactions + Increase Default Max Block Lag PR: [!3253](https://gitlab.com/thorchain/thornode/-/merge\_requests/3253)

## v1.124.0

* [ ] update mimir v2 regex to allow uppercase characters PR: [!3230](https://gitlab.com/thorchain/thornode/-/merge\_requests/3230)
* [ ] \[test] Fix UTXO Litecoin Signer Test PR: [!3225](https://gitlab.com/thorchain/thornode/-/merge\_requests/3225)
* [ ] \[test] Support Multi-Validator Regression Tests PR: [!3223](https://gitlab.com/thorchain/thornode/-/merge\_requests/3223)
* [ ] ~~ADR005 Amendment1: Permanently Sunset ILP PR:~~ [~~!3222~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3222)
* [ ] ~~ADR006 Amendment2: Lower POL Exit Criteria PR:~~ [~~!3220~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3220)
* [ ] \[test] Reduce Smoke Test Race PR: [!3218](https://gitlab.com/thorchain/thornode/-/merge\_requests/3218)
* [ ] Litecoin UTXO Client V2 PR: [!3217](https://gitlab.com/thorchain/thornode/-/merge\_requests/3217)
* [ ] \[build] Remove Trunk Cache PR: [!3216](https://gitlab.com/thorchain/thornode/-/merge\_requests/3216)
* [ ] Requeue BTC rescue from inactive vault PR: [!3213](https://gitlab.com/thorchain/thornode/-/merge\_requests/3213)
* [ ] \[tss] Reduce Default Party Timeout PR: [!3211](https://gitlab.com/thorchain/thornode/-/merge\_requests/3211)
* [ ] \[V124-specific] semi random asgard vault selection based on tx inhash and block height PR: [!3210](https://gitlab.com/thorchain/thornode/-/merge\_requests/3210)
* [x] \[bifrost] Also Observe Real Transaction for EVM Instant-Observe Outbounds PR: [!3208](https://gitlab.com/thorchain/thornode/-/merge\_requests/3208)
* [ ] Eridanus/fix requeue dangling action PR: [!3207](https://gitlab.com/thorchain/thornode/-/merge\_requests/3207)
* [x] \[bifrost] Favor Resume from Local Scanner Height on Restart PR: [!3206](https://gitlab.com/thorchain/thornode/-/merge\_requests/3206)
* [x] \[querier] remove outbound delay for native chain, fix EVM inbound conf counting PR: [!3205](https://gitlab.com/thorchain/thornode/-/merge\_requests/3205)
* [ ] \[V124-specific] Deduct gas fees from InactiveVault balances too PR: [!3200](https://gitlab.com/thorchain/thornode/-/merge\_requests/3200)
* [ ] Fix duplicated imports, add new lint enforcement PR: [!3199](https://gitlab.com/thorchain/thornode/-/merge\_requests/3199)
* [ ] \[V124-specific] Skip bsc asset anchor for THOR.BNB PR: [!3197](https://gitlab.com/thorchain/thornode/-/merge\_requests/3197)
* [ ] \[V124-specific] MissBlockSignSlashPoints for missing block signatures PR: [!3196](https://gitlab.com/thorchain/thornode/-/merge\_requests/3196)
* [ ] \[V124-specific] manager\_txout: Do nothing and return nil if toi.ToAddress.IsNoop() PR: [!3195](https://gitlab.com/thorchain/thornode/-/merge\_requests/3195)
* [ ] \[mocknet] Image Updates PR: [!3193](https://gitlab.com/thorchain/thornode/-/merge\_requests/3193)
* [ ] \[test] Remove Regression Test Check Descriptions and Support Variables PR: [!3188](https://gitlab.com/thorchain/thornode/-/merge\_requests/3188)
* [ ] \[config] Expose Inactive Vault Cutoff and Increase Default PR: [!3187](https://gitlab.com/thorchain/thornode/-/merge\_requests/3187)
* [ ] \[Version-unspecific] Cache responses for Bifrost get PR: [!3182](https://gitlab.com/thorchain/thornode/-/merge\_requests/3182)
* [ ] A~~DR006 Amendment - Add Saver Pools to PoL PR:~~ [~~!3179~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3179)
* [ ] \[feature] Mimir V2 #check-lint-warning PR: [!3177](https://gitlab.com/thorchain/thornode/-/merge\_requests/3177)
* [ ] ~~\[V124-specific] ATOMAsset line in NewAssetWithShortCodes to match ShortCode #check-lint-warning PR:~~ [~~!3170~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3170)
* [ ] \[V124-specific] Set StatusSince upon change from RetiringVault to InactiveVault PR: [!3164](https://gitlab.com/thorchain/thornode/-/merge\_requests/3164)
* [ ] \[cleanup] remove yggs part 2/2 (thornode) PR: [!3155](https://gitlab.com/thorchain/thornode/-/merge\_requests/3155)
* [x] Custom refund address in swap memo #check-lint-warning PR: [!3151](https://gitlab.com/thorchain/thornode/-/merge\_requests/3151)
* [ ] Remove final portion of the TxOut item local encryption PR: [!3146](https://gitlab.com/thorchain/thornode/-/merge\_requests/3146)
* [ ] Updates TSLedgerAdapter and add MLT token PR: [!3109](https://gitlab.com/thorchain/thornode/-/merge\_requests/3109)
* [x] \[V124-specific] Use refundTx for streaming swap partial refunds PR: [!3016](https://gitlab.com/thorchain/thornode/-/merge\_requests/3016)
* [ ] \[V124-specific] Deduct all pending txout item amounts when selecting outbound vaults PR: [!2816](https://gitlab.com/thorchain/thornode/-/merge\_requests/2816)

## v1.123.0

* [ ] Refund dropped BTC vault rescue tx PR: [!3198](https://gitlab.com/thorchain/thornode/-/merge\_requests/3198)
* [ ] \[bug] reset pool depth between simulated swaps PR: [!3194](https://gitlab.com/thorchain/thornode/-/merge\_requests/3194)
* [ ] fix streaming loan quote PR: [!3192](https://gitlab.com/thorchain/thornode/-/merge\_requests/3192)
* [ ] \[invariant] fix streaming swap false breaks PR: [!3191](https://gitlab.com/thorchain/thornode/-/merge\_requests/3191)
* [ ] Streaming savers querier fixes PR: [!3189](https://gitlab.com/thorchain/thornode/-/merge\_requests/3189)
* [ ] \[api] Fix Long Memo UTXO Synth Quotes and Drop from\_address Requirements PR: [!3186](https://gitlab.com/thorchain/thornode/-/merge\_requests/3186)
* [ ] \[migration] Re-queue dropped outbound for preferred-asset/streaming-swap conflict PR: [!3184](https://gitlab.com/thorchain/thornode/-/merge\_requests/3184)
* [ ] Move Ethereum router contract into thornode repository. PR: [!3183](https://gitlab.com/thorchain/thornode/-/merge\_requests/3183)
* [ ] \[mocknet] Image Updates and Fixes PR: [!3180](https://gitlab.com/thorchain/thornode/-/merge\_requests/3180)
* [x] Move Ethereum chain client to use shared EVM code PR: [!3172](https://gitlab.com/thorchain/thornode/-/merge\_requests/3172)
* [ ] \[V123-specific] Respect ObservationDelayFlexibility Mimir override PR: [!2969](https://gitlab.com/thorchain/thornode/-/merge\_requests/2969)
* [ ] \[V123-specific] Use GetMinJoinLast instead of GetMinJoinVersion for /nodes querier-called NodeAccountPreflightCheck (and churn low-version finding) PR: [!2940](https://gitlab.com/thorchain/thornode/-/merge\_requests/2940)

## v1.122.0

* [ ] \[config] Increase Default Rate Limit PR: [!3181](https://gitlab.com/thorchain/thornode/-/merge\_requests/3181)
* [ ] \[cleanup] remove yggs part 1/2 (bifrost) PR: [!3178](https://gitlab.com/thorchain/thornode/-/merge\_requests/3178)
* [ ] \[tools] p2p-check cli PR: [!3176](https://gitlab.com/thorchain/thornode/-/merge\_requests/3176)
* [x] \[Version-unspecific; bugfix] Correct EVM-insta-observe ToAddress PR: [!3174](https://gitlab.com/thorchain/thornode/-/merge\_requests/3174)
* [ ] \[Version-unspecific] Log and drop GetObservationsStdTx unobservable ObservedTx PR: [!3173](https://gitlab.com/thorchain/thornode/-/merge\_requests/3173)
* [ ] Fix Thorname response PR: [!3171](https://gitlab.com/thorchain/thornode/-/merge\_requests/3171)
* [ ] \[Backwards-compatibility; V122-specific] Resolve block 11320435 sync failure PR: [!3168](https://gitlab.com/thorchain/thornode/-/merge\_requests/3168)
* [ ] \[Version-unspecific] Display InactiveVault status PR: [!3167](https://gitlab.com/thorchain/thornode/-/merge\_requests/3167)
* [ ] Bad RBF Inbound Refunds + Double Spend Bond Slash Refunds PR: [!3162](https://gitlab.com/thorchain/thornode/-/merge\_requests/3162)
* [ ] \[version] 1.122.0 + Remove Version from Generated API Comments PR: [!3161](https://gitlab.com/thorchain/thornode/-/merge\_requests/3161)
* [ ] \[api] Explicitly Disallow Non-Height Parameters PR: [!3154](https://gitlab.com/thorchain/thornode/-/merge\_requests/3154)
* [ ] \[Version-unspecific] Resync unstuck.go loop upon keysign error PR: [!3150](https://gitlab.com/thorchain/thornode/-/merge\_requests/3150)
* [ ] ~~TS tokenlist update 1.122 - BSC update with top100 by volume PR:~~ [~~!3147~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3147)
* [ ] Switch to typesafe sort of cosmos.Uint PR: [!3145](https://gitlab.com/thorchain/thornode/-/merge\_requests/3145)
* [ ] Remove dead code PR: [!3144](https://gitlab.com/thorchain/thornode/-/merge\_requests/3144)
* [ ] Remove unused & dangerous gas method, mark remaining #check-lint-warning PR: [!3143](https://gitlab.com/thorchain/thornode/-/merge\_requests/3143)
* [ ] \[Version-unspecific] Complete deprecation of QueryObservedTx BlockHeight and FinaliseHeight, and rearrange QueryObservedTx fields PR: [!3137](https://gitlab.com/thorchain/thornode/-/merge\_requests/3137)
* [x] \[V122-specific] Remove double-swap intermediate outbound event PR: [!3136](https://gitlab.com/thorchain/thornode/-/merge\_requests/3136)
* [ ] \[Version-unspecific] Ensure that getOutput only returns an output with a vault address as either the sender or receiver PR: [!3128](https://gitlab.com/thorchain/thornode/-/merge\_requests/3128)
* [ ] \[Version-unspecific] NoneEmpty() for observed Gas too, not just Coins PR: [!3126](https://gitlab.com/thorchain/thornode/-/merge\_requests/3126)
* [x] \[Version-unspecific] AsgardInvariant: Explicitly check IsStreaming to not double-count streaming In and Out for affiliate swaps PR: [!3123](https://gitlab.com/thorchain/thornode/-/merge\_requests/3123)
* [ ] \[api] Add /thorchain/block API PR: [!3121](https://gitlab.com/thorchain/thornode/-/merge\_requests/3121)
* [ ] \[V122-specific] Restore the cancelled-out 1e8 in CalcTxOutHeight PR: [!2912](https://gitlab.com/thorchain/thornode/-/merge\_requests/2912)

## v1.121.1

* [x] EVM insta observe at tx broadcast: [!3148](https://gitlab.com/thorchain/thornode/-/merge\_requests/3148)

## v1.121.0

* [ ] \[bug] treat lending module as an account PR: [!3159](https://gitlab.com/thorchain/thornode/-/merge\_requests/3159)
* [ ] Spelling / log is too verbose PR: [!3158](https://gitlab.com/thorchain/thornode/-/merge\_requests/3158)
* [ ] \[bugfix] Generate unique inbound tx for preferred asset swaps + don't allow tx out scheduling in the past #check-lint-warning PR: [!3142](https://gitlab.com/thorchain/thornode/-/merge\_requests/3142)
* [ ] \[ci] Allow Contributors to Manually Trigger External Fork CI PR: [!3135](https://gitlab.com/thorchain/thornode/-/merge\_requests/3135)
* [ ] \[api] Network Endpoint Use Current Fee Values PR: [!3130](https://gitlab.com/thorchain/thornode/-/merge\_requests/3130)
* [ ] \[feature] dynamic max anchor slip PR: [!3129](https://gitlab.com/thorchain/thornode/-/merge\_requests/3129)
* [ ] Feature/ Add Tor price in the network query endpoint PR: [!3127](https://gitlab.com/thorchain/thornode/-/merge\_requests/3127)
* [ ] migrateStoreV121: Only attempt to transfer Coins if at least one Coin to transfer PR: [!3122](https://gitlab.com/thorchain/thornode/-/merge\_requests/3122)
* [x] Fix endpoint 'quote/swap' for streaming swaps PR: [!3119](https://gitlab.com/thorchain/thornode/-/merge\_requests/3119)
* [ ] update calc liq units math in readme PR: [!3118](https://gitlab.com/thorchain/thornode/-/merge\_requests/3118)
* [ ] \[Backwards-compatibility] Resolve block 6130730 sync failure PR: [!3116](https://gitlab.com/thorchain/thornode/-/merge\_requests/3116)
* [x] \[api] Swap Quote Auto Max Quantity, Misc Fee Fixes PR: [!3115](https://gitlab.com/thorchain/thornode/-/merge\_requests/3115)
* [ ] \[add] Bitcoin Cash UTXO Client V2 PR: [!3114](https://gitlab.com/thorchain/thornode/-/merge\_requests/3114)
* [x] \[add] Commit Generated Files PR: [!3113](https://gitlab.com/thorchain/thornode/-/merge\_requests/3113)
* [ ] \[Version-unspecific] Revert !3073's Taproot support for until after hard fork PR: [!3112](https://gitlab.com/thorchain/thornode/-/merge\_requests/3112)
* [ ] \[cleanup] remove dead code from manager\_network PR: [!3111](https://gitlab.com/thorchain/thornode/-/merge\_requests/3111)
* [ ] ADD: add tx hash to Loan open and repayment events #check-lint-warning PR: [!3108](https://gitlab.com/thorchain/thornode/-/merge\_requests/3108)
* [ ] \[V121-specific] Regular timing of pending liquidity auto-commit PR: [!3106](https://gitlab.com/thorchain/thornode/-/merge\_requests/3106)
* [ ] \[V121-specific] Do not allow network modules as memo final destinations PR: [!3104](https://gitlab.com/thorchain/thornode/-/merge\_requests/3104)
* [ ] \[fix] Gaia Auto Solvency Unhalt PR: [!3102](https://gitlab.com/thorchain/thornode/-/merge\_requests/3102)
* [ ] Return default values if dex agg wl not set PR: [!3101](https://gitlab.com/thorchain/thornode/-/merge\_requests/3101)
* [ ] \[test] Regression Tests Check Invariants Every "create-blocks" PR: [!3100](https://gitlab.com/thorchain/thornode/-/merge\_requests/3100)
* [ ] \[feature] Streaming loans PR: [!3100](https://gitlab.com/thorchain/thornode/-/merge\_requests/3100)
* [ ] \[feature] Streaming loans PR: [!3096](https://gitlab.com/thorchain/thornode/-/merge\_requests/3096)
* [x] Streaming swaps ignore tvl/synth cap during swap PR: [!3095](https://gitlab.com/thorchain/thornode/-/merge\_requests/3095)
* [x] \[bifrost] Keygen Retries and ChurnRetryInterval Mimir PR: [!3092](https://gitlab.com/thorchain/thornode/-/merge\_requests/3092)
* [x] \[bifrost] Add P2P Status Endpoint PR: [!3091](https://gitlab.com/thorchain/thornode/-/merge\_requests/3091)
* [ ] \[Version-unspecific] Display pool asset short codes in querier PR: [!3086](https://gitlab.com/thorchain/thornode/-/merge\_requests/3086)
* [ ] \[Version-unspecific] Update btcutil dependency in order to parse BTC Taproot addresses PR: [!3073](https://gitlab.com/thorchain/thornode/-/merge\_requests/3073)
* [ ] \[Version-unspecific] Asgard invariant: Account for streaming swap native (RUNE) output PR: [!3072](https://gitlab.com/thorchain/thornode/-/merge\_requests/3072)
* [ ] \[sec] vault migration sanity checks PR: [!3071](https://gitlab.com/thorchain/thornode/-/merge\_requests/3071)
* [x] \[ADD] Streaming Swaps for Savers adds/withdraws PR: [!3070](https://gitlab.com/thorchain/thornode/-/merge\_requests/3070)
* [ ] Reduce lines of code in querier PR: [!3060](https://gitlab.com/thorchain/thornode/-/merge\_requests/3060)
* [ ] Add explicit deprecation suffixes to dangerous methods #check-lint-warning PR: [!3059](https://gitlab.com/thorchain/thornode/-/merge\_requests/3059)
* [ ] \[V121-specific] Don't emit zero-amount pool reward PR: [!2937](https://gitlab.com/thorchain/thornode/-/merge\_requests/2937)
* [ ] \[V121-specific] Consolidate swap minting into swapOne PR: [!2843](https://gitlab.com/thorchain/thornode/-/merge\_requests/2843)

## v1.120.2

* [x] \[bifrost] EVM Post Network Fee on Interval if Unset PR: [!3132](https://gitlab.com/thorchain/thornode/-/merge\_requests/3132)

## v1.120.1

* [x] \[fix] Quote Fuzzy on Saver Assets PR: [!3105](https://gitlab.com/thorchain/thornode/-/merge\_requests/3105)

## v1.120.0

* [ ] \[Version-unspecific] Restore Stages and Status endpoints' OpenAPI references without alphas PR: [!3098](https://gitlab.com/thorchain/thornode/-/merge\_requests/3098)
* [ ] \[Backwards-compatibility] processOneTxInV120 #check-lint-warning PR: [!3097](https://gitlab.com/thorchain/thornode/-/merge\_requests/3097)
* [ ] \[feature] emit keygen events PR: [!3093](https://gitlab.com/thorchain/thornode/-/merge\_requests/3093)
* [ ] \[V120-specific] Do not allow swaps to a native coin's synthetic PR: [!3087](https://gitlab.com/thorchain/thornode/-/merge\_requests/3087)
* [x] \[bug] if chain or trading is halted, skip streaming swap attempts PR: [!3085](https://gitlab.com/thorchain/thornode/-/merge\_requests/3085)
* [ ] \[V120-specific] Zero-amount POL MsgWithdrawLiquidity PR: [!3084](https://gitlab.com/thorchain/thornode/-/merge\_requests/3084)
* [x] \[bugfix] go nuclear on swap queue during quotes API PR: [!3083](https://gitlab.com/thorchain/thornode/-/merge\_requests/3083)
* [ ] \[bugfix] Preferred asset swap: remove owner = thor alias check PR: [!3082](https://gitlab.com/thorchain/thornode/-/merge\_requests/3082)
* [ ] \[patch] lending fixes #check-lint-warning PR: [!3081](https://gitlab.com/thorchain/thornode/-/merge\_requests/3081)
* [ ] \[fix] POL Withdraw PR: [!3079](https://gitlab.com/thorchain/thornode/-/merge\_requests/3079)
* [x] \[api] Asset Code Quote Memo, Fix Fuzzy Asset, Loan Quote Short Memo PR: [!3078](https://gitlab.com/thorchain/thornode/-/merge\_requests/3078)
* [ ] ~~Aggregator update TS 1.120 PR:~~ [~~!3077~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3077)
* [ ] \[Version-unspecific] Corrections of !3062 PR: [!3076](https://gitlab.com/thorchain/thornode/-/merge\_requests/3076)
* [ ] add derived depth bps to pools endpoint #check-lint-warning PR: [!3065](https://gitlab.com/thorchain/thornode/-/merge\_requests/3065)
* [ ] ~~add rango contracts for v120 PR:~~ [~~!3064~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3064)
* [ ] \[V120-specific] Track same-block-scheduled migrations to send each Asset to all ActiveVaults PR: [!3046](https://gitlab.com/thorchain/thornode/-/merge\_requests/3046)
* [ ] Deprecate one-off encryption of TxOutItems PR: [!2821](https://gitlab.com/thorchain/thornode/-/merge\_requests/2821)

## v1.119.0

* [ ] Move go-tss to v1.6.3 PR: [!3075](https://gitlab.com/thorchain/thornode/-/merge\_requests/3075)
* [ ] \[fix] UTXO V2 Client Re-Confirm PR: [!3067](https://gitlab.com/thorchain/thornode/-/merge\_requests/3067)
* [ ] \[Version-unspecific] Loan quote parameter compatibility PR: [!3063](https://gitlab.com/thorchain/thornode/-/merge\_requests/3063)
* [x] \[Version-unspecific] Stages (and Status) endpoint support for Streaming swaps PR: [!3062](https://gitlab.com/thorchain/thornode/-/merge\_requests/3062)
* [ ] \[V119-specific] Check n.LeaveScore rather than na.LeaveScore for findOldActor and findLowBondActor PR: [!3057](https://gitlab.com/thorchain/thornode/-/merge\_requests/3057)
* [x] \[V119-specific] Use voter.Height rather than voter.FinalisedHeight for streaming swap adjustment PR: [!3044](https://gitlab.com/thorchain/thornode/-/merge\_requests/3044)
* [ ] Allow stable savers (determined by TOR anchor pools) PR: [!3042](https://gitlab.com/thorchain/thornode/-/merge\_requests/3042)
* [ ] \[security] split multi-node operators into different asgards PR: [!3037](https://gitlab.com/thorchain/thornode/-/merge\_requests/3037)

## v1.118.0

* [ ] \[fix] Correct Refund Amount at TVL Cap PR: [!3058](https://gitlab.com/thorchain/thornode/-/merge\_requests/3058)
* [ ] ~~\[ADR] 011 Lending PR:~~ [~~!3055~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3055)
* [ ] \[bugs] misc lending #check-lint-warning PR: [!3053](https://gitlab.com/thorchain/thornode/-/merge\_requests/3053)
* [ ] \[V118-specific] Don't reimburse gas difference for an outbound from an InactiveVault PR: [!3052](https://gitlab.com/thorchain/thornode/-/merge\_requests/3052)
* [ ] \[logs] Avoid Keyshare Backup Noise on Failed Keygen PR: [!3051](https://gitlab.com/thorchain/thornode/-/merge\_requests/3051)
* [ ] Update release instructions PR: [!3045](https://gitlab.com/thorchain/thornode/-/merge\_requests/3045)

## v1.117.0

* [ ] \[docs] Update README Install PR: [!3049](https://gitlab.com/thorchain/thornode/-/merge\_requests/3049)
* [ ] Re-add removed contracts (sorry) PR: [!3040](https://gitlab.com/thorchain/thornode/-/merge\_requests/3040)
* [x] emit streaming swap event PR: [!3039](https://gitlab.com/thorchain/thornode/-/merge\_requests/3039)
* [x] \[bug] include streaming swap quantity and count into a swap event PR: [!3038](https://gitlab.com/thorchain/thornode/-/merge\_requests/3038)
* [ ] \[bug] allow an operator to remove a bond provider with zero bond PR: [!3036](https://gitlab.com/thorchain/thornode/-/merge\_requests/3036)
* [ ] ~~\[ADR] Streaming Swaps PR:~~ [~~!3035~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3035)
* [ ] ~~\[DEX Whitelist] SquidRouter MultiCall contract PR:~~ [~~!3034~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3034)
* [x] \[feature] streaming swap tracks which sub-swap fails and for what reason PR: [!3033](https://gitlab.com/thorchain/thornode/-/merge\_requests/3033)
* [ ] \[V117-specific] Resolve broken invariants of manager\_txout refunds PR: [!3032](https://gitlab.com/thorchain/thornode/-/merge\_requests/3032)
* [ ] \[Version-unspecific] Do not log GetVault error for THORChain outbound (empty VaultPubKey so expected to error) #check-lint-warning PR: [!3031](https://gitlab.com/thorchain/thornode/-/merge\_requests/3031)
* [ ] \[V117-specific; Store migration] SubFunds ETH.ETH dust from the two remaining Yggdrasil vaults PR: [!3030](https://gitlab.com/thorchain/thornode/-/merge\_requests/3030)
* [ ] \[cleanup] remove non-native rune switch logic #check-lint-warning PR: [!3027](https://gitlab.com/thorchain/thornode/-/merge\_requests/3027)
* [ ] \[cleanup] Remove Chaosnet References PR: [!3024](https://gitlab.com/thorchain/thornode/-/merge\_requests/3024)
* [ ] Update links in README PR: [!3023](https://gitlab.com/thorchain/thornode/-/merge\_requests/3023)
* [ ] ~~Add TSLedgerAdapter contract PR:~~ [~~!3022~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3022)
* [ ] ~~Add PEPE and fix Verse image PR:~~ [~~!3021~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3021)
* [x] \[economics] adjusted algo for determining bond/TVL cap #check-lint-warning PR: [!3020](https://gitlab.com/thorchain/thornode/-/merge\_requests/3020)
* [ ] Churn refactor #check-lint-warning PR: [!3019](https://gitlab.com/thorchain/thornode/-/merge\_requests/3019)
* [ ] \[invariant] layer-one pools and liquidity providers PR: [!3018](https://gitlab.com/thorchain/thornode/-/merge\_requests/3018)
* [ ] \[invariant] reg tests and multiple messages PR: [!3015](https://gitlab.com/thorchain/thornode/-/merge\_requests/3015)
* [x] \[invariant] streaming swaps PR: [!3014](https://gitlab.com/thorchain/thornode/-/merge\_requests/3014)
* [ ] Generic UTXO Client PR: [!3013](https://gitlab.com/thorchain/thornode/-/merge\_requests/3013)
* [ ] Upgrade trunk + linters PR: [!3004](https://gitlab.com/thorchain/thornode/-/merge\_requests/3004)
* [ ] \[V117-specific] Equal migration rounds PR: [!2381](https://gitlab.com/thorchain/thornode/-/merge\_requests/2381)

## v1.116.0

* [x] \[fix] Validate Thorname Preferred Asset is Active Pool PR [!3025](https://gitlab.com/thorchain/thornode/-/merge\_requests/3025)
* [ ] ~~Whitelist LZ Executors PR~~ [~~!3017~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3017)
* [x] \[refactor] Use "latest" Token Lists to Improve Review PR [!3012](https://gitlab.com/thorchain/thornode/-/merge\_requests/3012)
* [ ] \[lint] Catch Mainnet Files with Build Flags in Version Lint PR [!3011](https://gitlab.com/thorchain/thornode/-/merge\_requests/3011)
* [ ] r~~emove coinbase from one of the txns for doge PR~~ [~~!3009~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3009)
* [ ] \[change] USD Fee Mimir Names, Round USD Fees to 2 Significant Figures of RUNE PR [!3008](https://gitlab.com/thorchain/thornode/-/merge\_requests/3008)
* [ ] \[cleanup] remove x/gov references (spurious event and test param) PR [!3007](https://gitlab.com/thorchain/thornode/-/merge\_requests/3007)
* [ ] \[test] Update Mocknet Daemons PR [!3006](https://gitlab.com/thorchain/thornode/-/merge\_requests/3006)
* [ ] \[refactor] Stagenet changes PR [!3005](https://gitlab.com/thorchain/thornode/-/merge\_requests/3005)
* [ ] \[ci] Skip Versioned Function Lint on Develop PR [!3002](https://gitlab.com/thorchain/thornode/-/merge\_requests/3002)
* [ ] ~~Update adr-009-reserve-income-fee-overhaul.md PR~~ [~~!3001~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3001)
* [x] \[patches] misc minor fixes to streaming swaps #check-lint-warning PR [!3000](https://gitlab.com/thorchain/thornode/-/merge\_requests/3000)
* [ ] refactor manager versioning to be easier diffs #check-lint-warning PR [!2998](https://gitlab.com/thorchain/thornode/-/merge\_requests/2998)
* [ ] \[feature] allow mimir to call back yggdrasil funds back to asgard PR [!2996](https://gitlab.com/thorchain/thornode/-/merge\_requests/2996)
* [x] \[feature] clear pending liquidity PR [!2995](https://gitlab.com/thorchain/thornode/-/merge\_requests/2995)
* [ ] \[V116-specific] Decrement ObservedTxVoter slash points with GetConsensusSigners PR \[!2994]\(https://gitlab.com/thorchain/thornode/-/merge\_requests/2994)
* [x] \[feature] outbound delays are reduced by streaming swap time PR https [!2991](https://gitlab.com/tthornode/-/merge\_requests/2991)
* [x] \[feature] AffiliateCollector Module + Preferred Asset Swap #check-lint- warning PR [!2978](https://gitlab.com/thorchain/thornode/-/merge\_requests/2978)
* [x] Refactor memo #check-lint-warning PR [!2968](https://gitlab.com/thorchain/thornode/-/merge\_requests/2968)
* [ ] \[fix] migration to fix module invariants PR [!2814](https://gitlab.com/thorchain/thornode/-/merge\_requests/2814)

## v1.115.0

* [ ] Fix manager tx out V113 file names PR [!2993](https://gitlab.com/thorchain/thornode/-/merge\_requests/2993)
* [ ] \[ante] deduct native tx fees during ante #check-lint-warning PR [!2992](https://gitlab.com/thorchain/thornode/-/merge\_requests/2992)
* [ ] \[fix] Skip Auto-Observe on Failed Broadcast PR [!2990](https://gitlab.com/thorchain/thornode/-/merge\_requests/2990)
* [x] bump go-tss to v1.6.2 PR [!2987](https://gitlab.com/thorchain/thornode/-/merge\_requests/2987)
* [x] \[refactor] streaming swaps tweaks #check-lint-warning PR [!2986](https://gitlab.com/thorchain/thornode/-/merge\_requests/2986)
* [x] Refactor aggregators list and add gas limit PR [!2983](https://gitlab.com/thorchain/thornode/-/merge\_requests/2983)
* [ ] Increase maxGasLimit for Aggregation call PR [!2982](https://gitlab.com/thorchain/thornode/-/merge\_requests/2982)
* [ ] \[querier] add total\_effective\_bond to /network endpoint PR [!2981](https://gitlab.com/thorchain/thornode/-/merge\_requests/2981)
* [ ] \[ante] use infinite gas meter for transactions PR [!2976](https://gitlab.com/thorchain/thornode/-/merge\_requests/2976)
* [ ] last migration round only sends non-gas asset PR [!2973](https://gitlab.com/thorchain/thornode/-/merge\_requests/2973)
* [ ] \[V115-specific] Clear Aggregator information on queued TxOutItem vault reassignment PR \[!2947]\(https://gitlab.com/thorchain/thornode/-/merge\_requests/2947)
* [x] \[feature] allow memos to use asset "short codes" PR [!2899](https://gitlab.com/thorchain/thornode/-/merge\_requests/2899)
* [x] \[feature] Streaming swaps #check-lint-warning PR [!2886](https://gitlab.com/thorchain/thornode/-/merge\_requests/2886)
* [x] \[feature] Pass data between two contracts between two chains PR [!2871](https://gitlab.com/thorchain/thornode/-/merge\_requests/2871)

## v1.114.0

* [ ] \[ante] add ante validation to external msg handlers PR [!2979](https://gitlab.com/thorchain/thornode/-/merge\_requests/2979)
* [ ] \[cleanup] use keeper version for fee checks #check-lint-warning PR [!2975](https://gitlab.com/thorchain/thornode/-/merge\_requests/2975)
* [ ] \[stagenet] fix bnb pools and requeue migrate txs PR [!2974](https://gitlab.com/thorchain/thornode/-/merge\_requests/2974)
* [x] add more tss keygen logging #check-lint-warning PR [!2972](https://gitlab.com/thorchain/thornode/-/merge\_requests/2972)
* [x] Fix signature of swapOut in documentation PR [!2971](https://gitlab.com/thorchain/thornode/-/merge\_requests/2971)
* [ ] ~~Add Stargate aggregator contract PR~~ [~~!2970~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2970)
* [ ] ~~Add Verse DEX token (bitcoin.com) PR~~ [~~!2963~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2963)
* [ ] \[~~mocknet] Bitcoin Cash 26.1.0 PR \[!2960]\(https://gitlab.com/thorchain/thornode/- /merge\_requests/2960)~~
* [ ] \[change] Dollar Denominated Fee Mimirs (ADR-009 Part 1) #check-lint-warning PR [!2923](https://gitlab.com/thorchain/thornode/-/merge\_requests/2923)
* [ ] \[Version-unspecific] Use case-insensitive EqualFold to check UTXO destination address roundtrip PR \[!2802]\(https://gitlab.com/thorchain/thornode/- /merge\_requests/2802)

## v1.113.1

* [ ] \[BUG] Limit inactive vaults number PR [!2967](https://gitlab.com/thorchain/thornode/-/merge\_requests/2967)
* [x] \[add] Observe UTXO Outbounds Immediately After Signing PR[!2964](https://gitlab.com/thorchain/thornode/-/merge\_requests/2964)
* [ ] \[fix] Keygen Timeouts PR [!2966](https://gitlab.com/thorchain/thornode/-/merge\_requests/2966)
* [ ] \[tune] Cache PubKey Addresses PR: [!2965](https://gitlab.com/thorchain/thornode/-/merge\_requests/2965)

## 1.113.0

* [ ] \[migrate] Reverse BTC Pool Slash and Corresponding Bond Slashes PR: [!2961](https://gitlab.com/thorchain/thornode/-/merge\_requests/2961)
* [ ] \[fix] Regression Test Concurrent Map Write PR: [!2959](https://gitlab.com/thorchain/thornode/-/merge\_requests/2959)
* [x] \[V113-specific] Only update a TxOutItem's MaxGas from TxOutStorage EndBlock when first entering the outbound queue PR: [!2957](https://gitlab.com/thorchain/thornode/-/merge\_requests/2957)
* [ ] \[BUG] Allow synth to synth swaps at security hard cap PR: [!2956](https://gitlab.com/thorchain/thornode/-/merge\_requests/2956)
* [ ] \[lending] More Regression Tests PR: [!2955](https://gitlab.com/thorchain/thornode/-/merge\_requests/2955)
* [ ] \[BUG] Ignore transaction when the sender address is invalid PR: [!2950](https://gitlab.com/thorchain/thornode/-/merge\_requests/2950)
* [ ] \[cleanup] remove arb whitelist PR: [!2943](https://gitlab.com/thorchain/thornode/-/merge\_requests/2943)
* [ ] \[fix] Loan Quote Fee Basis Points PR: [!2942](https://gitlab.com/thorchain/thornode/-/merge\_requests/2942)
* [ ] \[V113-specific] Set correct ToAddress when loan handlers make a MsgSwap PR: [!2928](https://gitlab.com/thorchain/thornode/-/merge\_requests/2928)
* [ ] \[V113-specific] DollarInRune -> DollarsPerRune PR: [!2832](https://gitlab.com/thorchain/thornode/-/merge\_requests/2832)

## 1.112.0

* [ ] \[revert] incentive pendulum change PR: [!2954](https://gitlab.com/thorchain/thornode/-/merge\_requests/2954)
* [x] Only change maxgas when tx out item scheduled to a new vault PR: [!2951](https://gitlab.com/thorchain/thornode/-/merge\_requests/2951)
* [ ] \[cleanup] version handler uses GetNativeTxFee PR: [!2944](https://gitlab.com/thorchain/thornode/-/merge\_requests/2944)
* [ ] \[cleanup] halts, fees, anchors in keeper PR: [!2941](https://gitlab.com/thorchain/thornode/-/merge\_requests/2941)
* [ ] refactor loan handlers versioning #check-lint-warning PR: [!2939](https://gitlab.com/thorchain/thornode/-/merge\_requests/2939)
* [ ] \[V112-specific] Transfer PendingInboundRune to Reserve upon pool burning PR: [!2938](https://gitlab.com/thorchain/thornode/-/merge\_requests/2938)
* [ ] \[lint] Catch Current Version Duplicates PR: [!2936](https://gitlab.com/thorchain/thornode/-/merge\_requests/2936)
* [ ] ~~\[Mainnet] Whitelist 2 BSC Aggregator Contracts PR:~~ [~~!2934~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2934)
* [ ] \[V112-specific] IsChain validation of add liquidity AssetAddress PR: [!2932](https://gitlab.com/thorchain/thornode/-/merge\_requests/2932)
* [ ] \[config] Default to Keyshare Backup Enabled PR: [!2931](https://gitlab.com/thorchain/thornode/-/merge\_requests/2931)
* [ ] ~~\[mocknet] Bitcoin 25.0 and Gaia 10.0.0 PR:~~ [~~!2929~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2929)
* [ ] \[Version-unspecific] Regression test update: lending/refunds.yaml PR: [!2927](https://gitlab.com/thorchain/thornode/-/merge\_requests/2927)
* [ ] \[V112-specific] Use totalEffectiveBond instead of totalBonded for Incentive Pendulum PR: [!2922](https://gitlab.com/thorchain/thornode/-/merge\_requests/2922)
* [ ] \[API] Swap Quote Endpoint Improvements PR: [!2905](https://gitlab.com/thorchain/thornode/-/merge\_requests/2905)
* [ ] \[api] Promote Changes PR: [!2901](https://gitlab.com/thorchain/thornode/-/merge\_requests/2901)
* [ ] \[feature] Trade Target in Scientific Notation #check-lint-warning PR: [!2897](https://gitlab.com/thorchain/thornode/-/merge\_requests/2897)
* [x] \[feature] make "best efforts" to refund txns sent to inactive vault PR: [!2869](https://gitlab.com/thorchain/thornode/-/merge\_requests/2869)
* [ ] \[test] Extend Lending Regression Tests PR: [!2850](https://gitlab.com/thorchain/thornode/-/merge\_requests/2850)

## 1.111.1

* [ ] \[tune] Bifrost LevelDB Storage (Enable as Default) PR: [!2933](https://gitlab.com/thorchain/thornode/-/merge\_requests/2933)

## 1.111.0

* [ ] ~~\[bsc] use router 4.1 contract addresses PR:~~ [~~!2930~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2930)
* [ ] \[update] Ethereum 1.12.0 PR: [!2925](https://gitlab.com/thorchain/thornode/-/merge\_requests/2925)
* [x] \[add] Saver Yield Scaling PR: [!2924](https://gitlab.com/thorchain/thornode/-/merge\_requests/2924)
* [ ] \[Version-unspecific] Add MaxRuneSupply to Admin deny list PR: [!2919](https://gitlab.com/thorchain/thornode/-/merge\_requests/2919)
* [ ] \[test] Update Mocknet Daemons PR: [!2918](https://gitlab.com/thorchain/thornode/-/merge\_requests/2918)
* [ ] \[test] Fix Regression Export Paths, Mirror Suite Hierarchy PR: [!2917](https://gitlab.com/thorchain/thornode/-/merge\_requests/2917)
* [ ] \[migrate] Fix Missed Bitcoin Last Observation Heights PR: [!2916](https://gitlab.com/thorchain/thornode/-/merge\_requests/2916)
* [ ] \[V111-specific] Validate positive MaxRuneSupply PR: [!2909](https://gitlab.com/thorchain/thornode/-/merge\_requests/2909)
* [ ] \[V111-specific] LendingLever GetUncappedShare -> GetSafeShare PR: [!2908](https://gitlab.com/thorchain/thornode/-/merge\_requests/2908)
* [ ] \[V111-specific] Emit Derived Asset pool pool\_balance\_change events relative to their previous balances PR: [!2893](https://gitlab.com/thorchain/thornode/-/merge\_requests/2893)
* [ ] remove partial loan repayments #check-lint-warning PR: [!2890](https://gitlab.com/thorchain/thornode/-/merge\_requests/2890)
* [x] \[chain] Binance Smart Chain (Bifrost Refactor Avalanche -> EVM) PR: [!2859](https://gitlab.com/thorchain/thornode/-/merge\_requests/2859)
* [ ] \[feature] Circuit Breaker PR: [!2723](https://gitlab.com/thorchain/thornode/-/merge\_requests/2723)

## 1.110.0

* [ ] Mitigate Potential Freeze Block PR: [!2913](https://gitlab.com/thorchain/thornode/-/merge\_requests/2913)
* [ ] \[test] regression for all tx types PR: [!2911](https://gitlab.com/thorchain/thornode/-/merge\_requests/2911)
* [ ] \[fix] Reset Bitcoin Observation Heights PR: [!2903](https://gitlab.com/thorchain/thornode/-/merge\_requests/2903)
* [ ] \[tune] Bifrost LevelDB Storage PR: [!2902](https://gitlab.com/thorchain/thornode/-/merge\_requests/2902)
* [ ] \[bugfix] fix minout for loan repayment #check-lint-warning PR: [!2898](https://gitlab.com/thorchain/thornode/-/merge\_requests/2898)
* [ ] \[fix] ensure addresses are correct network PR: [!2889](https://gitlab.com/thorchain/thornode/-/merge\_requests/2889)
* [ ] \[V110-specific] Add failed-refund external coins to their pools' BalanceAsset PR: [!2888](https://gitlab.com/thorchain/thornode/-/merge\_requests/2888)
* [ ] \[Version-unspecific] Use THORChain block milliseconds for quotes' OutboundDelaySeconds PR: [!2887](https://gitlab.com/thorchain/thornode/-/merge\_requests/2887)
* [ ] \[V110-specific] Modify mark-for-churn-out timing PR: [!2849](https://gitlab.com/thorchain/thornode/-/merge\_requests/2849)
* [ ] \[patch] Dervied asset swaps doesn't mint rune properly PR: [!2842](https://gitlab.com/thorchain/thornode/-/merge\_requests/2842)
* [ ] \[V110-specific] Avoid handler\_solvency redundant SetMimirs and EventSetMimir emissions PR: [!2827](https://gitlab.com/thorchain/thornode/-/merge\_requests/2827)

## 1.109.2

* [ ] take config height if configured PR: [!2904](https://gitlab.com/thorchain/thornode/-/merge\_requests/2904)

## 1.109.1

* [x] \[fix] Avoid Double Spend on Broadcast Error PR: [!2900](https://gitlab.com/thorchain/thornode/-/merge\_requests/2900)

## 1.109.0

* [x] Round7 #check-lint-warning PR: [!2896](https://gitlab.com/thorchain/thornode/-/merge\_requests/2896)
* [ ] \[improvement] Increase Max PoL Movement Granularity PR: [!2891](https://gitlab.com/thorchain/thornode/-/merge\_requests/2891)
* [x] \[Version-unspecific] Ensure blame of NewMsgTssKeysignFail has a FailReason PR: [!2884](https://gitlab.com/thorchain/thornode/-/merge\_requests/2884)
* [ ] \[fix] Develop Lint PR: [!2883](https://gitlab.com/thorchain/thornode/-/merge\_requests/2883)
* [ ] \[Migration] Make fake tx in observations for unobserved, memo-less BTC Inbounds PR: [!2882](https://gitlab.com/thorchain/thornode/-/merge\_requests/2882)
* [x] \[Version-unspecific] Ignore txs for LockTime only when a future height PR: [!2881](https://gitlab.com/thorchain/thornode/-/merge\_requests/2881)
* [ ] ~~Add 2 new aggregator contracts PR:~~ [~~!2880~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2880)
* [ ] \[V109-specific; store migration] Requeue ETH-halt swallowed TxOutItems PR: [!2877](https://gitlab.com/thorchain/thornode/-/merge\_requests/2877)
* [ ] Remove hardcoded addresses in Ethereum chain client PR: [!2873](https://gitlab.com/thorchain/thornode/-/merge\_requests/2873)
* [ ] Expand admin mimir denylist PR: [!2858](https://gitlab.com/thorchain/thornode/-/merge\_requests/2858)
* [x] \[improvement] Allow Bifrost Init on Chain Failures PR: [!2853](https://gitlab.com/thorchain/thornode/-/merge\_requests/2853)
* [ ] \[cleanup] Reduce Log Noise #check-lint-warning PR: [!2852](https://gitlab.com/thorchain/thornode/-/merge\_requests/2852)
* [ ] \[V109-specific] SpawnDerivedAsset: Only emit an EventPoolBalanceChanged if there's a balance change PR: [!2848](https://gitlab.com/thorchain/thornode/-/merge\_requests/2848)
* [ ] \[fix] Drop Redundant Smoke/Bootstrap Volume Flags PR: [!2845](https://gitlab.com/thorchain/thornode/-/merge\_requests/2845)
* [ ] ~~\[adr] 007: Increase Fund Migration Interval PR:~~ [~~!2844~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2844)
* [ ] \[test] CI and Regression Test Optimizations PR: [!2833](https://gitlab.com/thorchain/thornode/-/merge\_requests/2833)
* [ ] \[V109-specific] GetFee DollarInRune-usage correction PR: [!2830](https://gitlab.com/thorchain/thornode/-/merge\_requests/2830)
* [ ] \[improvement] Delay Churn for Halted Chains #check-lint-warning PR: [!2812](https://gitlab.com/thorchain/thornode/-/merge\_requests/2812)
* [x] \[Version-unspecific] NewQueryPool PR: [!2656](https://gitlab.com/thorchain/thornode/-/merge\_requests/2656)

## 1.108.3

* [x] \[revert] go-tss 1.6.0 (revert libp2p update) PR: [!2879](https://gitlab.com/thorchain/thornode/-/merge\_requests/2879)
* [x] collect tss round when there is a failure #check-lint-warning PR: [!2876](https://gitlab.com/thorchain/thornode/-/merge\_requests/2876)

## 1.108.2

* [x] go-tss > 1.5.9, tss-lib => 0.1.4 PR: [!2874](https://gitlab.com/thorchain/thornode/-/merge\_requests/2874)

## 1.108.1

* [x] Bump patch version for 1.108.1 release PR: [!2864](https://gitlab.com/thorchain/thornode/-/merge\_requests/2864)
* [x] \[update] Ethereum 1.11.5 PR: [!2862](https://gitlab.com/thorchain/thornode/-/merge\_requests/2862)
* [x] Update CI to use new builder image PR: [!2861](https://gitlab.com/thorchain/thornode/-/merge\_requests/2861)
* [x] \[update] Golang 1.20 PR: [!2860](https://gitlab.com/thorchain/thornode/-/merge\_requests/2860)
* [x] \[fix] Analyze Go 1.20 Compatibility PR: [!2857](https://gitlab.com/thorchain/thornode/-/merge\_requests/2857)
* [ ] \[api] Loan Quotes PR: [!2840](https://gitlab.com/thorchain/thornode/-/merge\_requests/2840)

## 1.108.0

* [ ] Remove old & unused Binance workaround PR: [!2855](https://gitlab.com/thorchain/thornode/-/merge\_requests/2855)
* [ ] \[fix] Loan Refund TxID Consistency PR: [!2846](https://gitlab.com/thorchain/thornode/-/merge\_requests/2846)
* [x] set min conf count to 2 for eth PR: [!2836](https://gitlab.com/thorchain/thornode/-/merge\_requests/2836)
* [x] \[Feature] Dynamic Outbound Fee Multiplier #check-lint-warning PR: [!2835](https://gitlab.com/thorchain/thornode/-/merge\_requests/2835)
* [ ] fix archive func #check-lint-warning PR: [!2831](https://gitlab.com/thorchain/thornode/-/merge\_requests/2831)
* [ ] ~~OKB add to token list + new THORSwap contracts PR:~~ [~~!2829~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2829)
* [ ] exclude dervied pools from total rune #check-lint-warning PR: [!2828](https://gitlab.com/thorchain/thornode/-/merge\_requests/2828)
* [ ] Lending enforce minout for first swap of loan open or repayment #check-lint-warning PR: [!2826](https://gitlab.com/thorchain/thornode/-/merge\_requests/2826)
* [ ] \[cleanup] remove versioning for constants PR: [!2825](https://gitlab.com/thorchain/thornode/-/merge\_requests/2825)
* [ ] \[test] Extend Refund Regression Tests PR: [!2824](https://gitlab.com/thorchain/thornode/-/merge\_requests/2824)
* [ ] \[update] Ledger Package for Nano S+ Compatibility PR: [!2823](https://gitlab.com/thorchain/thornode/-/merge\_requests/2823)
* [ ] \[V108-specific] Store migration requeue of BCH-halt txout items PR: [!2822](https://gitlab.com/thorchain/thornode/-/merge\_requests/2822)
* [ ] \[midgard] Add Empty Fields to Withdraw Events on Pool Suspend PR: [!2819](https://gitlab.com/thorchain/thornode/-/merge\_requests/2819)
* [ ] \[refactor] keeper constants #check-lint-warning PR: [!2818](https://gitlab.com/thorchain/thornode/-/merge\_requests/2818)
* [ ] \[add] Churn Estimates in Status Output PR: [!2817](https://gitlab.com/thorchain/thornode/-/merge\_requests/2817)
* [ ] Reduce fragility of memo handling #check-lint-warning PR: [!2815](https://gitlab.com/thorchain/thornode/-/merge\_requests/2815)
* [ ] \[Version-unspecific] Debug logs only for expected ParseMemo errors PR: [!2813](https://gitlab.com/thorchain/thornode/-/merge\_requests/2813)
* [ ] \[V108-specific] Sum-invariant Adjust #check-lint-warning PR: [!2794](https://gitlab.com/thorchain/thornode/-/merge\_requests/2794)
* [ ] \[V108-specific] Clean up failed-refund THORChain coins PR: [!2758](https://gitlab.com/thorchain/thornode/-/merge\_requests/2758)
* [ ] \[V108-specific] Replace explicit HaltTHORChain checks with common.THORChain isChainHalted PR: [!2757](https://gitlab.com/thorchain/thornode/-/merge\_requests/2757)
* [ ] \[V108-specific] Record all THORChain OutTxs #check-lint-warning PR: [!2718](https://gitlab.com/thorchain/thornode/-/merge\_requests/2718)
* [ ] \[V108-specific] Update actions GasRate PR: [!2615](https://gitlab.com/thorchain/thornode/-/merge\_requests/2615)

## 1.107.0

* [ ] \[api] Fix Tolerance Quote to RUNE PR: [!2811](https://gitlab.com/thorchain/thornode/-/merge\_requests/2811)
* [x] \[cleanup] Remove Terra PR: [!2810](https://gitlab.com/thorchain/thornode/-/merge\_requests/2810)
* [x] \[improvement] Skip Old Fee and Solvency Observations PR: [!2809](https://gitlab.com/thorchain/thornode/-/merge\_requests/2809)
* [ ] ~~\[test] Mocknet Litecoin 0.21.2 and Smoke Tuning PR:~~ [~~!2808~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2808)
* [ ] \[test] Regression Sequence Overrides and Filter Genesis Export PR: [!2805](https://gitlab.com/thorchain/thornode/-/merge\_requests/2805)
* [ ] \[Version-unspecific] Readme edit for regression testing: Cosmos Coin fields for tx-send PR: [!2801](https://gitlab.com/thorchain/thornode/-/merge\_requests/2801)
* [ ] query module balance invariants PR: [!2793](https://gitlab.com/thorchain/thornode/-/merge\_requests/2793)
* [ ] \[V107-specific] EventVersion emission PR: [!2786](https://gitlab.com/thorchain/thornode/-/merge\_requests/2786)
* [ ] \[Version-unspecific] Display empty RetiringVaults PR: [!2784](https://gitlab.com/thorchain/thornode/-/merge\_requests/2784)
* [ ] \[V107-specific] Add withdrawal transaction Asset to its pool, if existing PR: [!2777](https://gitlab.com/thorchain/thornode/-/merge\_requests/2777)
* [ ] Handler refactor and enforcement #check-lint-warning PR: [!2770](https://gitlab.com/thorchain/thornode/-/merge\_requests/2770)
* [ ] \[Version-unspecific] Add a Refund field to the Status endpoint planned\_out\_txs PR: [!2769](https://gitlab.com/thorchain/thornode/-/merge\_requests/2769)
* [ ] \[Version-unspecific] Move older-version helpers.go functions to helpers\_archive.go PR: [!2729](https://gitlab.com/thorchain/thornode/-/merge\_requests/2729)
* [ ] \[feature] THORFi Lending #check-lint-warning PR: [!2713](https://gitlab.com/thorchain/thornode/-/merge\_requests/2713)
* [x] \[V107-specific] Outbounds from any combination of vault types PR: [!2644](https://gitlab.com/thorchain/thornode/-/merge\_requests/2644)
* [x] \[V107-specific] Set new pool Status when setting new pool Asset PR: [!2625](https://gitlab.com/thorchain/thornode/-/merge\_requests/2625)

## 1.106.0

* [x] Fix Litecoin wallet handling in smoketest PR: [!2807](https://gitlab.com/thorchain/thornode/-/merge\_requests/2807)
* [ ] \[fix] Handle Bad Litecoin Semver PR: [!2806](https://gitlab.com/thorchain/thornode/-/merge\_requests/2806)
* [ ] \[analyze] Verify Function Versions in Switch PR: [!2800](https://gitlab.com/thorchain/thornode/-/merge\_requests/2800)
* [ ] Regression Test and Couple Additions #check-lint-warning PR: [!2799](https://gitlab.com/thorchain/thornode/-/merge\_requests/2799)
* [ ] \[Version-unspecific] Complete [!2797](https://gitlab.com/thorchain/thornode/-/merge\_requests/2797) by ignoring the Kraken in SetMimir too PR: [!2798](https://gitlab.com/thorchain/thornode/-/merge\_requests/2798)
* [ ] Remove Kraken (mimir-killing) functionality PR: [!2797](https://gitlab.com/thorchain/thornode/-/merge\_requests/2797)
* [ ] \[cleanup] Remove Dead Constants PR: [!2795](https://gitlab.com/thorchain/thornode/-/merge\_requests/2795)
* [ ] ~~ADR 006 - Enable POL PR:~~ [~~!2791~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2791)
* [ ] \[cleanup] Remove Dead Seeds Lambda PR: [!2790](https://gitlab.com/thorchain/thornode/-/merge\_requests/2790)
* [ ] \[fix] Versioned Function Analyzer Ignore Comments PR: [!2789](https://gitlab.com/thorchain/thornode/-/merge\_requests/2789)
* [ ] \[test] Regression Line Numbers and Allow Empty Check Op PR: [!2788](https://gitlab.com/thorchain/thornode/-/merge\_requests/2788)
* [ ] \[Version-unspecific] Display Midgard commit hash in THORNode smoke test PR: [!2787](https://gitlab.com/thorchain/thornode/-/merge\_requests/2787)
* [ ] \[test] Add Export and Events APIs in Regression Tests PR: [!2785](https://gitlab.com/thorchain/thornode/-/merge\_requests/2785)
* [ ] \[test] Add Optional Factor to Avoid Races and Skip Committing Coverage PR: [!2783](https://gitlab.com/thorchain/thornode/-/merge\_requests/2783)
* [ ] \[fix] Quote Tolerance with Synth Assets PR: [!2782](https://gitlab.com/thorchain/thornode/-/merge\_requests/2782)
* [ ] Accumulated miscellania PR: [!2781](https://gitlab.com/thorchain/thornode/-/merge\_requests/2781)
* [ ] Make Buildkit usage explicit for regression test image building PR: [!2780](https://gitlab.com/thorchain/thornode/-/merge\_requests/2780)
* [ ] \[fix] Relax Regression Test Race PR: [!2779](https://gitlab.com/thorchain/thornode/-/merge\_requests/2779)
* [ ] Complete dropped refund PR: [!2772](https://gitlab.com/thorchain/thornode/-/merge\_requests/2772)
* [ ] \[add] Regression Test Framework #check-lint-warning PR: [!2767](https://gitlab.com/thorchain/thornode/-/merge\_requests/2767)
* [x] \[Version-unspecific] Debug rather than Error "fail to get TxInItem" logs PR: [!2766](https://gitlab.com/thorchain/thornode/-/merge\_requests/2766)
* [ ] \[ADD] ante handler plumbing PR: [!2765](https://gitlab.com/thorchain/thornode/-/merge\_requests/2765)
* [x] \[Version-unspecific] Swap Queue /queue/swap endpoint PR: [!2764](https://gitlab.com/thorchain/thornode/-/merge\_requests/2764)
* [ ] \[V106-specific] Automatically produce store migration memo-hashed fake TxIDs for Midgard #check-lint-warning PR: [!2763](https://gitlab.com/thorchain/thornode/-/merge\_requests/2763)
* [x] \[Version-unspecific] Display Peer ID in Nodes endpoint PR: [!2761](https://gitlab.com/thorchain/thornode/-/merge\_requests/2761)
* [ ] \[V106-specific] When emitting suspension event for Derived Asset pool, check its status rather than the L1 pool's PR: [!2760](https://gitlab.com/thorchain/thornode/-/merge\_requests/2760)
* [ ] ~~Adding UniswapV3 contracts for XDEFI thorchain dex agg PR:~~ [~~!2759~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2759)
* [ ] \[V106-specific] Only churn out lowest-bond Active node when already at maximum Active nodes PR: [!2754](https://gitlab.com/thorchain/thornode/-/merge\_requests/2754)
* [ ] \[Version-unspecific] Reopened [!2681](https://gitlab.com/thorchain/thornode/-/merge\_requests/2681) (QueryObservedTx and QueryTxSigners) PR: [!2748](https://gitlab.com/thorchain/thornode/-/merge\_requests/2748)
* [x] Add access controls for admin mimir keys #check-lint-warning PR: [!2588](https://gitlab.com/thorchain/thornode/-/merge\_requests/2588)
* [ ] ~~_\[BUG] Update litecoin client in bifrost to support latest 0.21.2 release PR:_~~ [~~_!2278_~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2278)
* [x] \[add] Bifrost Keyshare Backup #check-lint-warning PR: [!2235](https://gitlab.com/thorchain/thornode/-/merge\_requests/2235)

## 1.105.0

* [ ] Dedupe tx id PR: [!2762](https://gitlab.com/thorchain/thornode/-/merge\_requests/2762)
* [ ] \[fix] Quotes from Synth Assets PR: [!2752](https://gitlab.com/thorchain/thornode/-/merge\_requests/2752)
* [ ] Reopened [!2739](https://gitlab.com/thorchain/thornode/-/merge\_requests/2739) (Allow NodeOperatorFee of 10000 basis points) PR: [!2751](https://gitlab.com/thorchain/thornode/-/merge\_requests/2751)
* [x] \[refactor] Avoid NET Environment Variable for Current Network #check-lint-warning PR: [!2740](https://gitlab.com/thorchain/thornode/-/merge\_requests/2740)

## 1.104.0

* [ ] add max aff fee bp mimir #check-lint-warning PR: !2745
* [ ] \[bugfix] Use correct observed tx for refund PR: !2744
* [ ] Complete !2744 by fixing the order book change as well: !2747

## 1.103.0

* [x] \[fix] Update Status to Nine Realms Gaia PR: [!2736](https://gitlab.com/thorchain/thornode/-/merge\_requests/2736)
* [x] \[mocknet] Bitcoin Cash 26.0.0 PR: [!2735](https://gitlab.com/thorchain/thornode/-/merge\_requests/2735)
* [ ] \[mocknet] Reduce Churn Rounds and Migration Interval #check-lint-warning PR: [!2734](https://gitlab.com/thorchain/thornode/-/merge\_requests/2734)
* [ ] \[api] Update synth\_mint\_paused to Respect MintSynths Mimir PR: [!2733](https://gitlab.com/thorchain/thornode/-/merge\_requests/2733)
* [x] \[fix] Consensus Fail on Sync from Genesis PR: [!2731](https://gitlab.com/thorchain/thornode/-/merge\_requests/2731)
* [ ] Quote feelessEmit accounting for RUNE PR: [!2730](https://gitlab.com/thorchain/thornode/-/merge\_requests/2730)
* [ ] Refund swap from received vault PR: [!2728](https://gitlab.com/thorchain/thornode/-/merge\_requests/2728)
* [ ] \[refactor] virtual pool gets generate at the beginning of each swap #check-lint-warning PR: [!2725](https://gitlab.com/thorchain/thornode/-/merge\_requests/2725)
* [ ] SwapFinalised transaction status stage PR: [!2721](https://gitlab.com/thorchain/thornode/-/merge\_requests/2721)
* [ ] \[api] Quote Endpoints V2 PR: [!2720](https://gitlab.com/thorchain/thornode/-/merge\_requests/2720)
* [x] Mocknet Bitcoin 24.0.1 PR: [!2719](https://gitlab.com/thorchain/thornode/-/merge\_requests/2719)
* [ ] \[Store Migration] Refund two dropped swap outs totaling 9,000 $RUNE PR: [!2716](https://gitlab.com/thorchain/thornode/-/merge\_requests/2716)
* [ ] Fuzzy asset match pool liquidity check PR: [!2661](https://gitlab.com/thorchain/thornode/-/merge\_requests/2661)
* [ ] Send node operator reward directly to node operator address (according to NodeOperatorFee) PR: [!2639](https://gitlab.com/thorchain/thornode/-/merge\_requests/2639)

## 1.102.0

* [ ] fix v1.102.0 store migration PR: [!2715](https://gitlab.com/thorchain/thornode/-/merge\_requests/2715)
* [ ] \[api] Update Doc Description PR: [!2712](https://gitlab.com/thorchain/thornode/-/merge\_requests/2712)
* [x] Stage name edits PR: [!2711](https://gitlab.com/thorchain/thornode/-/merge\_requests/2711)
* [ ] Bump trunk version + linters PR: [!2710](https://gitlab.com/thorchain/thornode/-/merge\_requests/2710)
* [ ] Deduplicate querier PR: [!2709](https://gitlab.com/thorchain/thornode/-/merge\_requests/2709)
* [x] \[add] Stages and Status Endpoints PR: [!2708](https://gitlab.com/thorchain/thornode/-/merge\_requests/2708)
* [ ] \[add] ILP Cutoff Mimir PR: [!2707](https://gitlab.com/thorchain/thornode/-/merge\_requests/2707)
* [x] \[openapi] Fix saver(s) responses PR: [!2706](https://gitlab.com/thorchain/thornode/-/merge\_requests/2706)
* [ ] \[add] Pools: Synth Supply Remaining #check-lint-warning PR: [!2704](https://gitlab.com/thorchain/thornode/-/merge\_requests/2704)
* [x] Mocknet Avalanche 1.9.4 PR: [!2703](https://gitlab.com/thorchain/thornode/-/merge\_requests/2703)
* [ ] \[add] Node Relay Endpoint Config PR: [!2701](https://gitlab.com/thorchain/thornode/-/merge\_requests/2701)
* [ ] \[update] ETH Block Milliseconds PR: [!2698](https://gitlab.com/thorchain/thornode/-/merge\_requests/2698)
* [ ] \[fix] Node Status Bond PR: [!2695](https://gitlab.com/thorchain/thornode/-/merge\_requests/2695)
* [x] \[update] Mocknet Bitcoin Cash 25.0 PR: [!2694](https://gitlab.com/thorchain/thornode/-/merge\_requests/2694)
* [ ] \[lint] Change Title Tag to #check-lint-warning PR: [!2693](https://gitlab.com/thorchain/thornode/-/merge\_requests/2693)
* [x] Add EVM Whitelist Procedures PR: [!2687](https://gitlab.com/thorchain/thornode/-/merge\_requests/2687)
* [ ] Derived assets #check-lint-warning PR: [!2658](https://gitlab.com/thorchain/thornode/-/merge\_requests/2658)
* [ ] \[stagenet testing] Refund user from dropped Swap Out txs PR: [!2596](https://gitlab.com/thorchain/thornode/-/merge\_requests/2596)

## 1.101.0

* [ ] \[openapi] Fix property of ConstantsResponse PR: [!2692](https://gitlab.com/thorchain/thornode/-/merge\_requests/2692)
* [ ] \[fix] Version Lint on Fork Repos PR: [!2691](https://gitlab.com/thorchain/thornode/-/merge\_requests/2691)
* [ ] Revert changes of InboundAddressesResponse PR: [!2690](https://gitlab.com/thorchain/thornode/-/merge\_requests/2690)
* [ ] \[A~~DD] Hyve to ETH token list, update THORSwap agg contracts PR:~~ [~~!2686~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2686)
* [ ] ~~Xdefi whitelist PR:~~ [~~!2684~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2684)
* [ ] Remove separate stagenet dex-agg whitelist PR: [!2683](https://gitlab.com/thorchain/thornode/-/merge\_requests/2683)
* [x] \[update] Mocknet Bitcoin 24.0 PR: [!2682](https://gitlab.com/thorchain/thornode/-/merge\_requests/2682)
* [ ] Node addresses querier display adjustment (no /v2/) PR: [!2680](https://gitlab.com/thorchain/thornode/-/merge\_requests/2680)
* [ ] Revert "Update go-tss to 1.5.6" PR: [!2679](https://gitlab.com/thorchain/thornode/-/merge\_requests/2679)
* [ ] vaults\_migrating /network boolean PR: [!2677](https://gitlab.com/thorchain/thornode/-/merge\_requests/2677)
* [x] \[Add] LP and Saver RedeemValues PR: [!2676](https://gitlab.com/thorchain/thornode/-/merge\_requests/2676)
* [ ] BTC/DOGE fully-wrapped keysign errors PR: [!2675](https://gitlab.com/thorchain/thornode/-/merge\_requests/2675)
* [ ] Update go-tss to 1.5.6 PR: [!2655](https://gitlab.com/thorchain/thornode/-/merge\_requests/2655)
* [ ] Round ETH and AVAX observed gas amount up, not down PR: [!2632](https://gitlab.com/thorchain/thornode/-/merge\_requests/2632)
* [ ] \[Refactor] withdraw\_archive.go (and deduplicate SwapHandler-received validateV93) #unsafe PR: [!2600](https://gitlab.com/thorchain/thornode/-/merge\_requests/2600)

## 1.100.0

* [ ] Only one PostKeysignFailure per UTXO-chain SignTx PR: [!2673](https://gitlab.com/thorchain/thornode/-/merge\_requests/2673)
* [x] NewQuerySaver PR: [!2672](https://gitlab.com/thorchain/thornode/-/merge\_requests/2672)
* [ ] \[bugfix] Don't derive chain from asset address PR: [!2671](https://gitlab.com/thorchain/thornode/-/merge\_requests/2671)
* [ ] `make halt-mocknet` command PR: [!2670](https://gitlab.com/thorchain/thornode/-/merge\_requests/2670)

## 1.99.0

* [x] \[add] Quote API for Swap/Deposit/Withdraw #unsafe PR: [!2666](https://gitlab.com/thorchain/thornode/-/merge\_requests/2666)
* [ ] \[ADD] Swap validation: Check if swap to a synth causes synth supply to exceed MaxSynthPerPoolDepth cap PR: [!2665](https://gitlab.com/thorchain/thornode/-/merge\_requests/2665)
* [x] \[refactor] new approach to calculating saver yield PR: [!2663](https://gitlab.com/thorchain/thornode/-/merge\_requests/2663)
* [x] \[PERF] don't store node signers for deposit txs PR: [!2659](https://gitlab.com/thorchain/thornode/-/merge\_requests/2659)
* [ ] calcSynthYield: Skip yield distribution when the layer 1 pool has zero BalanceAsset PR: [!2653](https://gitlab.com/thorchain/thornode/-/merge\_requests/2653)
* [ ] Resolve "ADD: Synth Mint Paused on inbound address" PR: [!2650](https://gitlab.com/thorchain/thornode/-/merge\_requests/2650)
* [ ] Update TxScript dependencies (to allow UTXO-chain keysign failure broadcast) PR: [!2649](https://gitlab.com/thorchain/thornode/-/merge\_requests/2649)
* [x] \[add] API Response Watermark PR: [!2648](https://gitlab.com/thorchain/thornode/-/merge\_requests/2648)
* [x] \[Fix] Savers endpoints PR: [!2647](https://gitlab.com/thorchain/thornode/-/merge\_requests/2647)
* [ ] \[Refactor] No keeper within pool manager struct #unsafe PR: [!2646](https://gitlab.com/thorchain/thornode/-/merge\_requests/2646)
* [x] \[fix] Avoid Failing Init on Bad Seeds PR: [!2645](https://gitlab.com/thorchain/thornode/-/merge\_requests/2645)
* [x] \[lint] Prevent Versioned Handler Changes PR: [!2643](https://gitlab.com/thorchain/thornode/-/merge\_requests/2643)
* [ ] Accumulated linting and minor cleanups PR: [!2642](https://gitlab.com/thorchain/thornode/-/merge\_requests/2642)
* [x] \[ADD] Support memo-less TXs for Savers PR: [!2641](https://gitlab.com/thorchain/thornode/-/merge\_requests/2641)
* [ ] \[openapi] Correct Thorname Spec PR: [!2636](https://gitlab.com/thorchain/thornode/-/merge\_requests/2636)
* [ ] Remove ProcessGas GetNetwork/SetNetwork dead code PR: [!2635](https://gitlab.com/thorchain/thornode/-/merge\_requests/2635)
* [x] \[add] Missing Chain Status on Shared Daemons PR: [!2628](https://gitlab.com/thorchain/thornode/-/merge\_requests/2628)
* [ ] Require an existing gas asset pool for add liquidity (and a non-Ragnaroked status) PR: [!2624](https://gitlab.com/thorchain/thornode/-/merge\_requests/2624)
* [ ] \[fix] Update Default Statesync RPCs PR: [!2623](https://gitlab.com/thorchain/thornode/-/merge\_requests/2623)
* [ ] \[cleanup] Fix Reason Error Message PR: [!2617](https://gitlab.com/thorchain/thornode/-/merge\_requests/2617)

## 1.98.1

* [ ] \[fix] Avoid Consensus Fail on Add Liquidity Swap PR: [!2640](https://gitlab.com/thorchain/thornode/-/merge\_requests/2640)

## 1.98.0

* [ ] \[fix] Avoid AVAX Double Spend on Nonce Error PR: [!2629](https://gitlab.com/thorchain/thornode/-/merge\_requests/2629)
* [x] \[fix] Correct Bifrost MultiAddr Peers PR: [!2627](https://gitlab.com/thorchain/thornode/-/merge\_requests/2627)
* [ ] POLSynthUtilization -> POLTargetSynthPerPoolDepth for consistency PR: [!2620](https://gitlab.com/thorchain/thornode/-/merge\_requests/2620)
* [ ] \[fix] Avoid Consensus Fail on Add Liquidity PR: [!2619](https://gitlab.com/thorchain/thornode/-/merge\_requests/2619)
* [ ] Move back to cosmos-sdk v0.45.1 PR: [!2618](https://gitlab.com/thorchain/thornode/-/merge\_requests/2618)
* [ ] \[fix] Avoid Consensus Fail on Orderbook End Block PR: [!2614](https://gitlab.com/thorchain/thornode/-/merge\_requests/2614)
* [x] \[fix] Use "." in Savers Endpoint Asset PR: [!2613](https://gitlab.com/thorchain/thornode/-/merge\_requests/2613)
* [x] \[bugfix] fix affiliate fee with savers PR: [!2611](https://gitlab.com/thorchain/thornode/-/merge\_requests/2611)
* [ ] \[fix] Resurrect Log Artifact Logic PR: [!2610](https://gitlab.com/thorchain/thornode/-/merge\_requests/2610)
* [x] \[fix] Node Status ETH/THOR Progress PR: [!2608](https://gitlab.com/thorchain/thornode/-/merge\_requests/2608)
* [x] \[improvement] Configurable Fee Parameters (Reduce AVAX Rounding) PR: [!2607](https://gitlab.com/thorchain/thornode/-/merge\_requests/2607)
* [x] \[update] Use Nine Realms Seeds Endpoint PR: [!2606](https://gitlab.com/thorchain/thornode/-/merge\_requests/2606)
* [ ] \[refactor] synth cap based on pool depth rather than asset depth PR: [!2605](https://gitlab.com/thorchain/thornode/-/merge\_requests/2605)
* [ ] GetFee's GetChain from msg.TargetAsset, not msg.Destination PR: [!2604](https://gitlab.com/thorchain/thornode/-/merge\_requests/2604)
* [ ] \[FIX] mint synths on swap+add flow for single-sided liquidity PR: [!2603](https://gitlab.com/thorchain/thornode/-/merge\_requests/2603)
* [ ] \[ci] Build and Push Mocknet Image PR: [!2601](https://gitlab.com/thorchain/thornode/-/merge\_requests/2601)
* [x] \[update] Mocknet Avalanche 1.9.0 PR: [!2598](https://gitlab.com/thorchain/thornode/-/merge\_requests/2598)
* [x] \[fix] Avoid Cycling Saver Pools (buckets) PR: [!2597](https://gitlab.com/thorchain/thornode/-/merge\_requests/2597)
* [x] \[openapi] Fix: Specs breaks open api generators PR: [!2595](https://gitlab.com/thorchain/thornode/-/merge\_requests/2595)
* [ ] POL (and synth yield) memos PR: [!2594](https://gitlab.com/thorchain/thornode/-/merge\_requests/2594)
* [ ] \[test] Add PoL Cycle Unit Test PR: [!2593](https://gitlab.com/thorchain/thornode/-/merge\_requests/2593)
* [x] \[Add] Update Savers thornode endpoints PR: [!2592](https://gitlab.com/thorchain/thornode/-/merge\_requests/2592)
* [ ] Fix comments in pool unit calculations PR: [!2591](https://gitlab.com/thorchain/thornode/-/merge\_requests/2591)
* [ ] ~~\[bugfix] Increase AVAX maxGasLimit for swapOut, and don't emit security even for "fake" evm gas tx PR:~~ [~~!2587~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2587)
* [x] \[add] Support Chains in Node Status on Shared Daemons PR: [!2586](https://gitlab.com/thorchain/thornode/-/merge\_requests/2586)
* [x] Update docs for Mac users PR: [!2583](https://gitlab.com/thorchain/thornode/-/merge\_requests/2583)
* [ ] Fair Merge add liquidity formula (implementation of [!2546](https://gitlab.com/thorchain/thornode/-/merge\_requests/2546)) PR: [!2582](https://gitlab.com/thorchain/thornode/-/merge\_requests/2582)
* [ ] ~~\[fix] Correct Stagenet AVAX Whitelist PR:~~ [~~!2581~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2581)
* [ ] ~~New stagenet node PR:~~ [~~!2575~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2575)
* [ ] \[dev] Environment Checks PR: [!2572](https://gitlab.com/thorchain/thornode/-/merge\_requests/2572)
* [x] \[update] Ethereum 1.10.25 PR: [!2570](https://gitlab.com/thorchain/thornode/-/merge\_requests/2570)
* [ ] \[FIX] ensure POL LPs are asymetric rune PR: [!2569](https://gitlab.com/thorchain/thornode/-/merge\_requests/2569)
* [ ] \[feature] order book wiring PR: [!2531](https://gitlab.com/thorchain/thornode/-/merge\_requests/2531)
* [x] \[ADD] inbound\_addresses additions/fixes PR: [!2584](https://gitlab.com/thorchain/thornode/-/merge\_requests/2584)
* [x] \[add] Add outbound\_fee, gas\_fee\_units and outbound\_tx\_size to /inbound\_addresses endpoint [!2533](https://gitlab.com/thorchain/thornode/-/merge\_requests/2533)

## 1.97.2

* [ ] Revert LibP2P upgrade which cause keysign issues: [!2580](https://gitlab.com/thorchain/thornode/-/merge\_requests/2580)

## 1.97.1

* [ ] Remove dead code: [!2560](https://gitlab.com/thorchain/thornode/-/merge\_requests/2560)
* [ ] Bump go-tss version to 1.5.5: [!2578](https://gitlab.com/thorchain/thornode/-/merge\_requests/2578)

## 1.97.0

* [ ] \[build] Avoid \`gofumpt\` Lint Noise PR: [!2565](https://gitlab.com/thorchain/thornode/-/merge\_requests/2565)
* [ ] ~~\[fix] Reduce Outlier Impact on AVAX Fees PR:~~ [~~!2564~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/2564)
* [x] \[refactor] Move Genesis Fetch to Config Package PR: [!2562](https://gitlab.com/thorchain/thornode/-/merge\_requests/2562)
* [x] \[Add] via Mogarchy(TS): Update THORSwap Dex Agg Contracts PR: [!2561](https://gitlab.com/thorchain/thornode/-/merge\_requests/2561)
* [x] Add VIU and RAZE (staged) back to the whitelist PR: [!2559](https://gitlab.com/thorchain/thornode/-/merge\_requests/2559)
* [ ] No MsgSolvency messages triggered by ETH.RUNE PR: [!2557](https://gitlab.com/thorchain/thornode/-/merge\_requests/2557)
* [ ] \[build] Target Mainnet for Develop Image PR: [!2555](https://gitlab.com/thorchain/thornode/-/merge\_requests/2555)
* [ ] \[update] TSS (libp2p) PR: [!2549](https://gitlab.com/thorchain/thornode/-/merge\_requests/2549)
* [ ] \[update] Mocknet Avalanche 1.8.5 PR: [!2544](https://gitlab.com/thorchain/thornode/-/merge\_requests/2544)
* [ ] \[fix] Generic Shebang for PR Log Script PR: [!2543](https://gitlab.com/thorchain/thornode/-/merge\_requests/2543)
* [ ] \[bugfix]: Only set LUVI for single-sided if it has net increased PR: [!2538](https://gitlab.com/thorchain/thornode/-/merge\_requests/2538)
* [ ] &#x20;\[docs] Docker Persistence and Snapshot Recover PR: [!2535](https://gitlab.com/thorchain/thornode/-/merge\_requests/2535)
* [ ] \[fix] Retiring Vault Outbound Selection PR: [!2534](https://gitlab.com/thorchain/thornode/-/merge\_requests/2534)
* [ ] \[add] Add outbound\_fee, gas\_fee\_units and outbound\_tx\_size to /inbound\_addresses endpoint PR: [!2533](https://gitlab.com/thorchain/thornode/-/merge\_requests/2533)
* [ ] Test Sync Targets, Fix Stagenet Init PR: [!2528](https://gitlab.com/thorchain/thornode/-/merge\_requests/2528)

## 1.96.2✅

* [x] Add HALTSIGNINGGLOBAL and update TSS dependencies [!2547](https://gitlab.com/thorchain/thornode/-/merge\_requests/2547)

## 1.96.1✅

* [x] \[fix] Correct Add/Withdraw Version to Avoid Consensus Fail PR: [!2527](https://gitlab.com/thorchain/thornode/-/merge\_requests/2527)
* [x] Status - Add Avax and Avoid Log Noise PR: [!2526](https://gitlab.com/thorchain/thornode/-/merge\_requests/2526)
* [x] Revert [!2434](https://gitlab.com/thorchain/thornode/-/merge\_requests/2434) (with commentary) PR: [!2520](https://gitlab.com/thorchain/thornode/-/merge\_requests/2520)
* [x] \[fix] Fix docker README.md PR: [!2517](https://gitlab.com/thorchain/thornode/-/merge\_requests/2517)
* [x] \[ADD]: AVAX Mainnet Contracts PR: [!2515](https://gitlab.com/thorchain/thornode/-/merge\_requests/2515)
* [x] Lint API Docs Version PR: [!2512](https://gitlab.com/thorchain/thornode/-/merge\_requests/2512)
* [x] ETH/AVAX Concurrent Tx Lookup PR: [!2511](https://gitlab.com/thorchain/thornode/-/merge\_requests/2511)
* [x] Mocknet Avalanche 1.7.17 and Ethereum 1.10.23 PR: [!2510](https://gitlab.com/thorchain/thornode/-/merge\_requests/2510)
* [x] \[CLEANUP] isSignedByActiveNodeAccounts param change PR: [!2508](https://gitlab.com/thorchain/thornode/-/merge\_requests/2508)
* [x] Update go-tss dependency PR: [!2507](https://gitlab.com/thorchain/thornode/-/merge\_requests/2507)
* [x] Rolling fee addition correction PR: [!2506](https://gitlab.com/thorchain/thornode/-/merge\_requests/2506)
* [x] \[fix] Run CI Template Jobs on PRs PR: [!2503](https://gitlab.com/thorchain/thornode/-/merge\_requests/2503)
* [x] \[CLEANUP] move pool cycle logic to a manager PR: [!2500](https://gitlab.com/thorchain/thornode/-/merge\_requests/2500)
* [x] \[fix] Misc Config Fixes PR: [!2499](https://gitlab.com/thorchain/thornode/-/merge\_requests/2499)
* [x] Add THORSwap Gnosis Safe multisig to whitelisted addresses PR: [!2497](https://gitlab.com/thorchain/thornode/-/merge\_requests/2497)
* [x] Set version before StoreMgr PR: [!2496](https://gitlab.com/thorchain/thornode/-/merge\_requests/2496)
* [x] \[ADD] Allow active node to top up bond PR: [!2494](https://gitlab.com/thorchain/thornode/-/merge\_requests/2494)
* [x] \[ADD] Set default telemetry enabled to false PR: [!2493](https://gitlab.com/thorchain/thornode/-/merge\_requests/2493)
* [x] \[ADD] contextual version PR: [!2476](https://gitlab.com/thorchain/thornode/-/merge\_requests/2476)
* [x] \[add] Bootstrap Mocknet Target PR: [!2473](https://gitlab.com/thorchain/thornode/-/merge\_requests/2473)
* [x] \[feature] Yield for Yield Bearing Synths PR: [!2466](https://gitlab.com/thorchain/thornode/-/merge\_requests/2466)
* [x] \[Feature] Order Book Manager PR: [!2431](https://gitlab.com/thorchain/thornode/-/merge\_requests/2431)
* [x] \[feature] add/remove liquidity from vaults single asset pools PR: [!2404](https://gitlab.com/thorchain/thornode/-/merge\_requests/2404)
