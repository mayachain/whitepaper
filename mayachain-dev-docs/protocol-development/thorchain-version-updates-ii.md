---
description: >-
  Each version shows all the Merge Requests that were deployed with that
  version.
---

# THORChain Version Updates II

{% hint style="info" %}
If it's checkmarked, it means we have implemented it. If a version itself has a checkmark, it means we already have all the MRs that we want from that version and we won't add any of the unchecked ones (if any).
{% endhint %}

### v2.136.0

* [ ] \[Version-unspecific] Allow Go-TSS pipeline job to pass by allowing apk (use\_dind) and Go PR: [!3743](https://gitlab.com/thorchain/thornode/-/merge\_requests/3743)
* [ ] \[V136-specific] Prune TERRA and BNB addresses from clout store migration PR: [!3740](https://gitlab.com/thorchain/thornode/-/merge\_requests/3740)
* [ ] docker login on more stages PR: [!3735](https://gitlab.com/thorchain/thornode/-/merge\_requests/3735)
* [ ] \[V136-specific] Store migrate v1->v2 hardfork clout scores over 1000 RUNE PR: [!3734](https://gitlab.com/thorchain/thornode/-/merge\_requests/3734)
* [ ] \[Fix] Enable taproot signing + observations PR: [!3732](https://gitlab.com/thorchain/thornode/-/merge\_requests/3732)
* [ ] THORChain's App Layer PR: [!3730](https://gitlab.com/thorchain/thornode/-/merge\_requests/3730)
* [ ] \[genesis] fix swapper clout export PR: [!3729](https://gitlab.com/thorchain/thornode/-/merge\_requests/3729)
* [ ] Implement ADR 17 & 18 #check-lint-warning PR: [!3727](https://gitlab.com/thorchain/thornode/-/merge\_requests/3727)
* [ ] Updated Readme PR: [!3726](https://gitlab.com/thorchain/thornode/-/merge\_requests/3726)
* [ ] \[fix] Chunked Genesis Fetch PR: [!3725](https://gitlab.com/thorchain/thornode/-/merge\_requests/3725)
* [ ] \[lending] collateral genesis export, ragnarok, and invariant PR: [!3724](https://gitlab.com/thorchain/thornode/-/merge\_requests/3724)
* [ ] \[V136-specific] Process Affiliate Collector Module and Pool Module oversolvencies #check-lint-warning PR: [!3722](https://gitlab.com/thorchain/thornode/-/merge\_requests/3722)
* [ ] \[Version-unspecific] Only halt after THOR height specified by HaltSigning and HaltSigning%s Mimir keys, like all other Halt keys PR: [!3720](https://gitlab.com/thorchain/thornode/-/merge\_requests/3720)
* [ ] \[Historical querier panics] Return checkInvariants error on endpoint panic, and fix AsgardInvariant panics PR: [!3718](https://gitlab.com/thorchain/thornode/-/merge\_requests/3718)
* [ ] Proposal: Update bugbounty.md PR: [!3716](https://gitlab.com/thorchain/thornode/-/merge\_requests/3716)
* [ ] \[cleanup] Dead Code #check-lint-warning PR: [!3713](https://gitlab.com/thorchain/thornode/-/merge\_requests/3713)
* [ ] \[feat] Validator-scheduled standard cosmos hard fork upgrades PR: [!3708](https://gitlab.com/thorchain/thornode/-/merge\_requests/3708)
* [ ] Added faqs PR: [!3701](https://gitlab.com/thorchain/thornode/-/merge\_requests/3701)
* [ ] \[fix] Pool Asset for Min Slip and Correct Auto Streaming Quantity #check-lint-warning PR: [!3700](https://gitlab.com/thorchain/thornode/-/merge\_requests/3700)
* [ ] \[mocknet] Image Updates PR: [!3697](https://gitlab.com/thorchain/thornode/-/merge\_requests/3697)
* [ ] \[tool] Recover Keyshare Backup PR: [!3683](https://gitlab.com/thorchain/thornode/-/merge\_requests/3683)
* [ ] Updates from discord PR: [!3676](https://gitlab.com/thorchain/thornode/-/merge\_requests/3676)
* [ ] \[V136-specific] Implement (passed) ADR-015 PR: [!3666](https://gitlab.com/thorchain/thornode/-/merge\_requests/3666)
* [ ] Add new TS contracts to whitelist for swapOut support PR: [!3658](https://gitlab.com/thorchain/thornode/-/merge\_requests/3658)
* [ ] ADR-019: AutoBond PR: [!3654](https://gitlab.com/thorchain/thornode/-/merge\_requests/3654)
* [ ] \[V136-specific] Constant to disable manual swaps to synth (in favour of Trade Asset use instead) PR: [!3630](https://gitlab.com/thorchain/thornode/-/merge\_requests/3630)

### v2.135.1

* [ ] &#x20;Isolate tests for `go-tss` module PR: [!3741](https://gitlab.com/thorchain/thornode/-/merge\_requests/3741)
* [ ] \[Version-unspecific] Remove LENDING- check from queryQuoteLoanClose PR: [!3739](https://gitlab.com/thorchain/thornode/-/merge\_requests/3739)
* [ ] \[fix] Miscellaneous for Stagenet Reset PR: [!3717](https://gitlab.com/thorchain/thornode/-/merge\_requests/3717)

### v2.135.0

* [ ] \[Hardfork] v2.x.x restore of initGenesis SetRUNEPool PR: [!3707](https://gitlab.com/thorchain/thornode/-/merge\_requests/3707)
* [ ] \[Hardfork] Remove ERC20RuneAsset and squash IsNativeRune into IsRune #check-lint-warning PR: [!3705](https://gitlab.com/thorchain/thornode/-/merge\_requests/3705)
* [ ] \[refactor] Remove binance-sdk and btcd Module Replace PR: [!3699](https://gitlab.com/thorchain/thornode/-/merge\_requests/3699)
* [ ] \[refactor] Remove BNB and TERRA #check-lint-warning PR: [!3698](https://gitlab.com/thorchain/thornode/-/merge\_requests/3698)
* [ ] Embedded txscript and go-tss Packages PR: [!3696](https://gitlab.com/thorchain/thornode/-/merge\_requests/3696)
* [ ] remove final mimir v2 code #check-lint-warning PR: [!3693](https://gitlab.com/thorchain/thornode/-/merge\_requests/3693)
* [ ] Remove base builder image, inline package retrieval PR: [!3691](https://gitlab.com/thorchain/thornode/-/merge\_requests/3691)
* [ ] Remove a bit more dead code PR: [!3686](https://gitlab.com/thorchain/thornode/-/merge\_requests/3686)
* [x] \[revert] restore x/upgrade PR: [!3681](https://gitlab.com/thorchain/thornode/-/merge\_requests/3681)
* [ ] \[fork] More Dead Code Removal PR: [!3679](https://gitlab.com/thorchain/thornode/-/merge\_requests/3679)
* [ ] \[mocknet] Fix Cluster Bootstrap + Fix Simulation Race PR: [!3678](https://gitlab.com/thorchain/thornode/-/merge\_requests/3678)
* [ ] Enable BTC Taproot address support PR: [!3669](https://gitlab.com/thorchain/thornode/-/merge\_requests/3669)
* [ ] Add script for checking reproducible build PR: [!3668](https://gitlab.com/thorchain/thornode/-/merge\_requests/3668)
* [ ] \[Retroactive event changes] RUNEPool events rune\_amoumt mispelling correction to rune\_amount PR: [!3667](https://gitlab.com/thorchain/thornode/-/merge\_requests/3667)
* [ ] Added RUNEPool and ADR links PR: [!3665](https://gitlab.com/thorchain/thornode/-/merge\_requests/3665)
* [ ] \[fork] Upgrade to cosmos-sdk 45.16 PR: [!3664](https://gitlab.com/thorchain/thornode/-/merge\_requests/3664)
* [ ] Remove a little more dead code PR: [!3662](https://gitlab.com/thorchain/thornode/-/merge\_requests/3662)
* [ ] Fix trade account unit tests PR: [!3649](https://gitlab.com/thorchain/thornode/-/merge\_requests/3649)
* [ ] \[cleanup] remove fetchConfigInt64 #check-lint-warning PR: [!3594](https://gitlab.com/thorchain/thornode/-/merge\_requests/3594)
* [ ] \[fork] remove unnecessary modules from app PR: [!3593](https://gitlab.com/thorchain/thornode/-/merge\_requests/3593)
* [ ] \[fork] remove archived/obsolete code #check-lint-warning PR: [!3539](https://gitlab.com/thorchain/thornode/-/merge\_requests/3539)
* [ ] Fix protobuf package root for GenesisState PR: [!3389](https://gitlab.com/thorchain/thornode/-/merge\_requests/3389)

### v1.134.1

* [ ] \[Backwards-compatibility, v1.x.x-specific] Re-enable thorchain-mainnet-v1 sync from genesis PR: [!3702](https://gitlab.com/thorchain/thornode/-/merge\_requests/3702)
* [ ] node mimirs genesis state PR: [!3694](https://gitlab.com/thorchain/thornode/-/merge\_requests/3694)
* [ ] enforce halt height PR: [!3690](https://gitlab.com/thorchain/thornode/-/merge\_requests/3690)
* [ ] \[genesis] filter non-asgard vaults PR: [!3687](https://gitlab.com/thorchain/thornode/-/merge\_requests/3687)
* [ ] \[v1.x.x-specific] Initialise NewRUNEPool Uint fields to bypass historical-data panic PR: [!3685](https://gitlab.com/thorchain/thornode/-/merge\_requests/3685)
* [ ] \[fork] Support Halt Height Config PR: [!3680](https://gitlab.com/thorchain/thornode/-/merge\_requests/3680)

### v1.134.0

* [ ] &#x20;V134 disclosures PR: [!3663](https://gitlab.com/thorchain/thornode/-/merge\_requests/3663)
* [ ] Add RunePool logs PR: [!3657](https://gitlab.com/thorchain/thornode/-/merge\_requests/3657)
* [ ] Reduce log message on EVM tx parse PR: [!3656](https://gitlab.com/thorchain/thornode/-/merge\_requests/3656)
* [ ] \[update] ADR-018: Core Protocol Sustainability PR: [!3653](https://gitlab.com/thorchain/thornode/-/merge\_requests/3653)
* [ ] Fix and enable TestOutboundRuneRecords PR: [!3650](https://gitlab.com/thorchain/thornode/-/merge\_requests/3650)
* [ ] \[mocknet] Daemon Updates PR: [!3647](https://gitlab.com/thorchain/thornode/-/merge\_requests/3647)
* [ ] \[lint] Fix Build PR: [!3646](https://gitlab.com/thorchain/thornode/-/merge\_requests/3646)
* [ ] \[bsc] Require Confirmations (amended) PR: [!3644](https://gitlab.com/thorchain/thornode/-/merge\_requests/3644)
* [ ] \[bsc] Require Confirmations PR: [!3643](https://gitlab.com/thorchain/thornode/-/merge\_requests/3643)
* [ ] \[events] Fix Build and Format Node Pause PR: [!3642](https://gitlab.com/thorchain/thornode/-/merge\_requests/3642)
* [ ] ADR 15 (authored by rufus.t.firefly) PR: [!3641](https://gitlab.com/thorchain/thornode/-/merge\_requests/3641)
* [x] \[Version-unspecific] .gitlab-ci.yml "thorchain/thornode" -> $CI\_MERGE\_REQUEST\_PROJECT\_PATH PR: [!3639](https://gitlab.com/thorchain/thornode/-/merge\_requests/3639)
* [ ] Add manual deadcode linter script, remove some dead code PR: [!3638](https://gitlab.com/thorchain/thornode/-/merge\_requests/3638)
* [x] \[ci] Update Docker versions PR: [!3637](https://gitlab.com/thorchain/thornode/-/merge\_requests/3637)
* [ ] \[events] Build Image and Add Remaining Alerts PR: [!3636](https://gitlab.com/thorchain/thornode/-/merge\_requests/3636)
* [ ] \[fix] Saver Eject Memo and Simulation Race PR: [!3635](https://gitlab.com/thorchain/thornode/-/merge\_requests/3635)
* [ ] \[cleanup] Saver Eject Regression Test PR: [!3633](https://gitlab.com/thorchain/thornode/-/merge\_requests/3633)
* [ ] \[fix] restore lost treasury lp values PR: [!3632](https://gitlab.com/thorchain/thornode/-/merge\_requests/3632)
* [ ] \[feature] RUNEPool Implementation PR: [!3631](https://gitlab.com/thorchain/thornode/-/merge\_requests/3631)
* [ ] \[V134-specific] Do not deduct second network fee when unbonding PR: [!3629](https://gitlab.com/thorchain/thornode/-/merge\_requests/3629)
* [ ] \[Backwards-compatibility] Revert and archive ParseAffiliateBasisPoints PR: [!3628](https://gitlab.com/thorchain/thornode/-/merge\_requests/3628)
* [ ] \[api] Add Savers Capacity Fields to Pool Endpoints PR: [!3627](https://gitlab.com/thorchain/thornode/-/merge\_requests/3627)
* [ ] \[cleanup] Leave Event Keys PR: [!3626](https://gitlab.com/thorchain/thornode/-/merge\_requests/3626)
* [ ] ADR-018: Core Protocol Sustainability PR: [!3625](https://gitlab.com/thorchain/thornode/-/merge\_requests/3625)
* [ ] \[fix] Handle BSC Reorgs and Reduce ETH Errata Noise PR: [!3619](https://gitlab.com/thorchain/thornode/-/merge\_requests/3619)
* [ ] \[simulation] Optimizations PR: [!3616](https://gitlab.com/thorchain/thornode/-/merge\_requests/3616)
* [ ] \[lint] Fix Build PR: [!3615](https://gitlab.com/thorchain/thornode/-/merge\_requests/3615)
* [ ] \[simulation] Savers and Eject Test #check-lint-warning PR: [!3614](https://gitlab.com/thorchain/thornode/-/merge\_requests/3614)
* [ ] ADR-017 Burn System Income Lever PR: [!3613](https://gitlab.com/thorchain/thornode/-/merge\_requests/3613)
* [ ] \[ADD] RUNEPool PR: [!3612](https://gitlab.com/thorchain/thornode/-/merge\_requests/3612)
* [ ] Remove defunct mimir v2 implementation PR: [!3610](https://gitlab.com/thorchain/thornode/-/merge\_requests/3610)
* [x] Generate EdDSA keys from mnemonic , and make it available to regression test PR: [!3609](https://gitlab.com/thorchain/thornode/-/merge\_requests/3609)
* [ ] \[cli] fix help output of observe-tx-ins PR: [!3605](https://gitlab.com/thorchain/thornode/-/merge\_requests/3605)
* [ ] \[fix] ensure node address is derived from secp256k1 pub key PR: [!3603](https://gitlab.com/thorchain/thornode/-/merge\_requests/3603)
* [ ] \[genesis] filter inactive vault memberships from nodes PR: [!3602](https://gitlab.com/thorchain/thornode/-/merge\_requests/3602)
* [ ] \[aggregator] Whitelist corn contracts PR: [!3601](https://gitlab.com/thorchain/thornode/-/merge\_requests/3601)
* [ ] \[events] Add Alert Logic PR: [!3600](https://gitlab.com/thorchain/thornode/-/merge\_requests/3600)
* [x] \[fix] stricter coin validation on deposits PR: [!3597](https://gitlab.com/thorchain/thornode/-/merge\_requests/3597)
* [ ] \[logging getTxInFromSmartContract => debug PR: [!3596](https://gitlab.com/thorchain/thornode/-/merge\_requests/3596)
* [ ] \[docs] Update links in EVM router documentation PR: [!3595](https://gitlab.com/thorchain/thornode/-/merge\_requests/3595)
* [ ] ADR 016 - Affiliate Rev Share PR: [!3592](https://gitlab.com/thorchain/thornode/-/merge\_requests/3592)
* [x] \[regression] Fixed Future Version Height PR: [!3591](https://gitlab.com/thorchain/thornode/-/merge\_requests/3591)
* [ ] Remove BNB from make status PR: [!3590](https://gitlab.com/thorchain/thornode/-/merge\_requests/3590)
* [ ] \[metrics] Trade Asset Units and Depth PR: [!3589](https://gitlab.com/thorchain/thornode/-/merge\_requests/3589)
* [x] \[lint] Updates and Re-Enable Shadow Lint #check-lint-warning PR: [!3585](https://gitlab.com/thorchain/thornode/-/merge\_requests/3585)
* [ ] \[simulation] Arbitrage Actor and Require Success in CI PR: [!3584](https://gitlab.com/thorchain/thornode/-/merge\_requests/3584)
* [x] \[ci] Fix ERC20 list linter to use latest PR: [!3583](https://gitlab.com/thorchain/thornode/-/merge\_requests/3583)
* [ ] \[ci] Switch from deprecated gitlab CI rules PR: [!3581](https://gitlab.com/thorchain/thornode/-/merge\_requests/3581)
* [ ] \[Version-unspecific] Visible git\_commit in 'make run-mocknet' (make test-simulation) too PR: [!3580](https://gitlab.com/thorchain/thornode/-/merge\_requests/3580)
* [ ] \[Version-unspecific] Include build-mocknet in test-simulation Makefile PR: [!3579](https://gitlab.com/thorchain/thornode/-/merge\_requests/3579)
* [ ] \[V134-specific] inRune respected by GetAssetOutboundFee for Synth/Derived/Trade Assets PR: [!3573](https://gitlab.com/thorchain/thornode/-/merge\_requests/3573)
* [ ] \[docs] Updates PR: [!3572](https://gitlab.com/thorchain/thornode/-/merge\_requests/3572)
* [ ] \[Version-unspecific] Export all pending outbounds in genesis PR: [!3568](https://gitlab.com/thorchain/thornode/-/merge\_requests/3568)
* [ ] \[tool] Events PR: [!3565](https://gitlab.com/thorchain/thornode/-/merge\_requests/3565)
* [ ] \[api] Remove Deprecated Quote Fee Fields PR: [!3563](https://gitlab.com/thorchain/thornode/-/merge\_requests/3563)
* [ ] \[logs] Miscellaneous Cleanup #check-lint-warning PR: [!3560](https://gitlab.com/thorchain/thornode/-/merge\_requests/3560)
* [ ] \[V134-Specific] Synth eject #check-lint-warning PR: [!3544](https://gitlab.com/thorchain/thornode/-/merge\_requests/3544)
* [x] \[V134-specific] Slash points instead of bond slash for HandleDoubleSign PR: [!3163](https://gitlab.com/thorchain/thornode/-/merge\_requests/3163)
* [ ] \[V134-specific] Do not swallow add liquidity to a non-empty zero-units pool PR: [!2865](https://gitlab.com/thorchain/thornode/-/merge\_requests/2865)
* [ ] \[V134-specific] Slash non-signers upon consensus PR: [!2722](https://gitlab.com/thorchain/thornode/-/merge\_requests/2722)

### v1.133.0

* [ ] \[security] Drop EVM Outbounds to Null Addresses PR: [!3587](https://gitlab.com/thorchain/thornode/-/merge\_requests/3587)
* [ ] Disable dexagg clout PR: [!3586](https://gitlab.com/thorchain/thornode/-/merge\_requests/3586)
* [ ] \[test] Fix simulation tests PR: [!3570](https://gitlab.com/thorchain/thornode/-/merge\_requests/3570)
* [ ] ~~\[migration] rescue taproot btc PR:~~ [~~!3569~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3569)
* [ ] \[test] Deprecate Smoke Tests PR: [!3564](https://gitlab.com/thorchain/thornode/-/merge\_requests/3564)
* [ ] Lock Treasury LP in new Module PR: [!3562](https://gitlab.com/thorchain/thornode/-/merge\_requests/3562)
* [x] \[mocknet] Bitcoin 26.1 PR: [!3559](https://gitlab.com/thorchain/thornode/-/merge\_requests/3559)
* [x] \[ADD] LENDS Token Whitelist PR: [!3556](https://gitlab.com/thorchain/thornode/-/merge\_requests/3556)
* [ ] ~~\[bsc] Increase Gas Price Resolution to 10 Gwei PR:~~ [~~!3555~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3555)
* [ ] \[bifrost] Fix gas price metrics PR: [!3554](https://gitlab.com/thorchain/thornode/-/merge\_requests/3554)
* [ ] \[lint] Miscellaneous Fixes PR: [!3553](https://gitlab.com/thorchain/thornode/-/merge\_requests/3553)
* [x] Add new aggregator contracts to 133 PR: [!3551](https://gitlab.com/thorchain/thornode/-/merge\_requests/3551)
* [ ] \[Version-unspecific] Move Trade Account Deposit and Withdraw events from handlers to manager #check-lint-warning PR: [!3550](https://gitlab.com/thorchain/thornode/-/merge\_requests/3550)
* [ ] \[command] Observation Commands: Cleaner Error Handling PR: [!3549](https://gitlab.com/thorchain/thornode/-/merge\_requests/3549)
* [ ] \[command] Manual In/Out Observation Commands PR: [!3548](https://gitlab.com/thorchain/thornode/-/merge\_requests/3548)
* [ ] \[build] Fix Thorscan CI Build PR: [!3547](https://gitlab.com/thorchain/thornode/-/merge\_requests/3547)
* [ ] \[simulation] ERC20 Pools and Contract Support PR: [!3545](https://gitlab.com/thorchain/thornode/-/merge\_requests/3545)
* [ ] \[ci] Make the lint warning bypass apply to the removed handler check PR: [!3541](https://gitlab.com/thorchain/thornode/-/merge\_requests/3541)
* [ ] ~~\[docs] Memo and Trade Account Updates PR:~~ [~~!3540~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3540)
* [ ] ~~\[update] Ethereum 1.14.2 / Gaia 16.0.0 / Avalanche 1.11.5 PR:~~ [~~!3538~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3538)
* [ ] \[ci] Fix linting race with upstream merges to develop PR: [!3536](https://gitlab.com/thorchain/thornode/-/merge\_requests/3536)
* [ ] \[ci] Ensure that EVM contract tests actually run in CI PR: [!3535](https://gitlab.com/thorchain/thornode/-/merge\_requests/3535)
* [ ] \[test] Verify All Regression Test Blocks PR: [!3531](https://gitlab.com/thorchain/thornode/-/merge\_requests/3531)
* [ ] \[Version-unspecific] Querier quotes confirmations not reduced to 2 when (inbound) chain ETH and confirmations greater than 2 PR: [!3516](https://gitlab.com/thorchain/thornode/-/merge\_requests/3516)
* [ ] ~~\[V133-specific] Zero all BNB-chain vault Asset amounts in store migration PR:~~ [~~!3513~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3513)
* [ ] \[tool] Thorscan PR: [!3511](https://gitlab.com/thorchain/thornode/-/merge\_requests/3511)
* [ ] \[api] Add Asset USD Price to Pool Endpoints PR: [!3478](https://gitlab.com/thorchain/thornode/-/merge\_requests/3478)
* [ ] \[bifrost] Remove dex whitelist requirement for deposits PR: [!3448](https://gitlab.com/thorchain/thornode/-/merge\_requests/3448)
* [x] \[V133-specific] As swaps may be streaming sub-swaps, remove the swap handler's outbound fee check PR: [!3428](https://gitlab.com/thorchain/thornode/-/merge\_requests/3428)
* [ ] \[V133-specific] Mimir V1 implementation of OperationalMimirs (no longer including Admin disabling) PR: [!3400](https://gitlab.com/thorchain/thornode/-/merge\_requests/3400)

### v1.132.0

* [ ] \[build] Fix CI Build Tag (again) PR: [!3537](https://gitlab.com/thorchain/thornode/-/merge\_requests/3537)
* [ ] \[fix for 132] Two GetAssetOutboundFee patches #check-lint-warning PR: [!3534](https://gitlab.com/thorchain/thornode/-/merge\_requests/3534)
* [ ] \[build] Fix CI Image Build Tag PR: [!3533](https://gitlab.com/thorchain/thornode/-/merge\_requests/3533)
* [ ] V132 refund retry migrations #check-lint-warning PR: [!3532](https://gitlab.com/thorchain/thornode/-/merge\_requests/3532)
* [ ] \[simulation] Fix Mocknet AVAX/BSC and Enable in Simulation Tests PR: [!3530](https://gitlab.com/thorchain/thornode/-/merge\_requests/3530)
* [ ] ~~\[migrate] v132: Retry BNB Vault Recovery PR:~~ [~~!3529~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3529)
* [ ] \[V132-specific] RollupSwapSlip calls only once per block (in spawnDerivedAssets) PR: [!3528](https://gitlab.com/thorchain/thornode/-/merge\_requests/3528)
* [ ] \[cleanup] Remove Non-Pipelined Signer Logic PR: [!3527](https://gitlab.com/thorchain/thornode/-/merge\_requests/3527)
* [ ] \[V132-specific] NewExternalHandler unredaction of otherwise-redacted errors PR: [!3526](https://gitlab.com/thorchain/thornode/-/merge\_requests/3526)
* [ ] \[build] Optimize Build Context PR: [!3525](https://gitlab.com/thorchain/thornode/-/merge\_requests/3525)
* [x] Upgrade to Go 1.22 PR: [!3524](https://gitlab.com/thorchain/thornode/-/merge\_requests/3524)
* [ ] \[Backwards-compatibility] p.keeper.GetVersion() -> p.version to not panic when from ParseMemo (which has a nil keeper) PR: [!3523](https://gitlab.com/thorchain/thornode/-/merge\_requests/3523)
* [ ] \[test] test current logic for add/withdraw/deposit PR: [!3522](https://gitlab.com/thorchain/thornode/-/merge\_requests/3522)
* [ ] \[test] improve keeper\_halt tests PR: [!3521](https://gitlab.com/thorchain/thornode/-/merge\_requests/3521)
* [x] \[update] Ethereum 1.13.15 PR: [!3520](https://gitlab.com/thorchain/thornode/-/merge\_requests/3520)
* [ ] \[GasManager] Replace GetFee with GetAssetOutboundFee #check-lint-warning PR: [!3519](https://gitlab.com/thorchain/thornode/-/merge\_requests/3519)
* [x] \[add] Recommended Gas Rate for Quotes PR: [!3517](https://gitlab.com/thorchain/thornode/-/merge\_requests/3517)
* [ ] \[cleanup] unarchive active code PR: [!3512](https://gitlab.com/thorchain/thornode/-/merge\_requests/3512)
* [ ] \[V132-specific; second of two] Current network manager tx.Hash\_deprecated() to tx.Hash() PR: [!3510](https://gitlab.com/thorchain/thornode/-/merge\_requests/3510)
* [ ] \[V132-specific; ETH/EVM] Use receipt.GasUsed instead of tx.Gas() gas limit for observed gas in getTxInFromTransaction and getTxInFromFailedTransaction PR: [!3506](https://gitlab.com/thorchain/thornode/-/merge\_requests/3506)
* [ ] \[framework] Simulation Tests PR: [!3502](https://gitlab.com/thorchain/thornode/-/merge\_requests/3502)
* [ ] \[Version-unspecific] /thorchain/slips and /thorchain/slip/{asset} endpoints (with example regression test) PR: [!3496](https://gitlab.com/thorchain/thornode/-/merge\_requests/3496)
* [ ] \[fix] Multi-Anchor Pool Depth PR: [!3450](https://gitlab.com/thorchain/thornode/-/merge\_requests/3450)
* [ ] Affiliate fees for Lending #check-lint-warning PR: [!3425](https://gitlab.com/thorchain/thornode/-/merge\_requests/3425)
* [ ] \[V132-specific] Only spend UTXO unused gas for Migrate outbounds PR: [!3421](https://gitlab.com/thorchain/thornode/-/merge\_requests/3421)

### v1.131.1

* [ ] \[fix] Dropped Aggregator Fields and Signature Mismatch in Keysign Response PR: [!3515](https://gitlab.com/thorchain/thornode/-/merge\_requests/3515)
* [ ] \[utxo] Fix Under-Reported Solvency PR: [!3514](https://gitlab.com/thorchain/thornode/-/merge\_requests/3514)

### v1.131.0

* [ ] [\[V131-specific\] Do not over-assign gas asset outbounds to vaults](https://gitlab.com/thorchain/thornode/-/merge\_requests/3505)
* [x] [Leave coins in vault if fail to process non-native outbound #check-lint-warning](https://gitlab.com/thorchain/thornode/-/merge\_requests/3503)
* [ ] [v131 store migrations](https://gitlab.com/thorchain/thornode/-/merge\_requests/3495)
* [ ] [\[lint\] Analyzer to Prevent Use of Random](https://gitlab.com/thorchain/thornode/-/merge\_requests/3498)
* [ ] [\[fix\] refresh voter record in deposit/txin handler](https://gitlab.com/thorchain/thornode/-/merge\_requests/3491)
* [ ] [\[ragnarok\] Gas Asset Ragnarok Removes Token Pools](https://gitlab.com/thorchain/thornode/-/merge\_requests/3490)
* [ ] [\[cleanup\] remove deposit refunds #check-lint-warning](https://gitlab.com/thorchain/thornode/-/merge\_requests/3489)
* [ ] [\[V131-specific; Ragnarok\] Restore SetPoolRagnarokStart to pool-Ragnarok process to zero synth GetTotalSupply after synth redemption](https://gitlab.com/thorchain/thornode/-/merge\_requests/3487)
* [ ] [\[V131-specific; Ragnarok\] GetPool between redeemSynthAssetToReserve BalanceRune deduction and PoolStaged SetPool](https://gitlab.com/thorchain/thornode/-/merge\_requests/3486)
* [ ] [\[V130-specific\] Consistent updating of MaxGas and GasRate when a TxOutItem joins the outbound queue](https://gitlab.com/thorchain/thornode/-/merge\_requests/3481)
* [ ] [\[V131-specific\] Clean up trade account manager's GetSafeShare arguments to behave consistently when Depth is lower than Units](https://gitlab.com/thorchain/thornode/-/merge\_requests/3470)
* [ ] [\[test\] Improve Regression Test CI Flakiness and Update Mocknet Images](https://gitlab.com/thorchain/thornode/-/merge\_requests/3500)
* [ ] [\[Version-unspecific; first of two\] common.Tx Hash() -> Hash\_deprecated() #check-lint-warning](https://gitlab.com/thorchain/thornode/-/merge\_requests/3499)
* [ ] [\[V131-specific\] Get Ragnarok outbound MaxOutboundAttempts reference height from memo](https://gitlab.com/thorchain/thornode/-/merge\_requests/3497)
* [ ] [\[lint\] Fix Ignore Comments](https://gitlab.com/thorchain/thornode/-/merge\_requests/3492)
* [ ] [\[V131-specific; Backwards-compatibility\] Bump newTxOutStorageVCUR and newGasMgrVCUR versions from 1.130.0 to 1.131.0 #check-lint-warning](https://gitlab.com/thorchain/thornode/-/merge\_requests/3485)
* [ ] [\[V130-specific\] Remove gm.mgr from current-version gas manager](https://gitlab.com/thorchain/thornode/-/merge\_requests/3477)
* [ ] [\[~~V131\] Whitelist BSC Tokens for pool creation~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3471)
* [ ] [~~TS new aggregator contracts~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3467)
* [x] [\[Version-unspecific\] Replace Query structs with OpenAPI structs](https://gitlab.com/thorchain/thornode/-/merge\_requests/3452)
* [ ] [Support scientific notation in dexAgg minAmount](https://gitlab.com/thorchain/thornode/-/merge\_requests/3437)
* [ ] [\[V131-specific\] Asset-specific Dynamic Outbound Fee Multipliers #check-lint-warning](https://gitlab.com/thorchain/thornode/-/merge\_requests/3423)
* [ ] [~~Doc Updates~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3508)
* [ ] [~~ADR 013: Synth Backstop~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3507)
* [ ] [~~ADR 14: Reduce Saver Yield Synth Target to Match POL Target~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3501)
* [ ] [~~Adding docs on Trade Accounts~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3442)
* [x] [\[ethereum\] Improve Dynamic Transaction Fee Handling, Dynamic Fee Outbounds, Allow Gas Limit Buffer](https://gitlab.com/thorchain/thornode/-/merge\_requests/3509)
* [ ] [\[utxo\] Fix Unspent Filtering](https://gitlab.com/thorchain/thornode/-/merge\_requests/3493)
* [ ] [\[V131-specific\] Don't reschedule TxOutItems for future heights when already scheduled for a multiple of RescheduleCoalesceBlocks](https://gitlab.com/thorchain/thornode/-/merge\_requests/3483)
* [ ] [Tendermint v0.34.15](https://gitlab.com/thorchain/thornode/-/merge\_requests/3480)

### v1.130.1

* [ ] ~~\[fix] Gaia v15 Incompatibilities #check-lint-warning PR:~~ [~~!3488~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3488)

### v1.130.0

* [ ] \[security] Prevent Multiple Coins in Send and Deposit: [!3484](https://gitlab.com/thorchain/thornode/-/merge\_requests/3484)

### v1.129.0

* [ ] ~~\[migrate] v129 Recover Taproot Inbound PR:~~ [~~!3476~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3476)
* [ ] Miscellaneous Ragnarok Fixes PR: [!3474](https://gitlab.com/thorchain/thornode/-/merge\_requests/3474)
* [ ] avoid negative slip rollup PR: [!3473](https://gitlab.com/thorchain/thornode/-/merge\_requests/3473)
* [ ] \[V129-specific] Tx Coins to not change vault balances when their FromAddress and ToAddress are the same PR: [!3472](https://gitlab.com/thorchain/thornode/-/merge\_requests/3472)
* [ ] \[V129-specific] Disallow swapping to a trade asset of a native coin PR: [!3468](https://gitlab.com/thorchain/thornode/-/merge\_requests/3468)
* [ ] \[V129-specific] Allow swaps between Trade Assets and RUNE PR: [!3465](https://gitlab.com/thorchain/thornode/-/merge\_requests/3465)
* [ ] \[fix] Reschedule Frozen Vault Outbounds to Same Vault PR: [!3464](https://gitlab.com/thorchain/thornode/-/merge\_requests/3464)
* [ ] Cleanup unused argument + CI fixes PR: [!3462](https://gitlab.com/thorchain/thornode/-/merge\_requests/3462)
* [x] \[lint] Add TokenList Version Lint PR: [!3456](https://gitlab.com/thorchain/thornode/-/merge\_requests/3456)
* [ ] \[V129-specific] Aim Incentive Pendulum to target equilibrium of effective security bond and vault (RUNE value of assets) liquidity, rather than pooled RUNE liquidity PR: [!3451](https://gitlab.com/thorchain/thornode/-/merge\_requests/3451)
* [ ] Upgrade trunk and tooling PR: [!3446](https://gitlab.com/thorchain/thornode/-/merge\_requests/3446)
* [x] Use current managers in unversioned/current tests PR: [!3441](https://gitlab.com/thorchain/thornode/-/merge\_requests/3441)
* [ ] \[Version-unspecific] Deprecate Coins Add, Coins SafeSub, and Gas Add #check-lint-warning PR: [!3440](https://gitlab.com/thorchain/thornode/-/merge\_requests/3440)
* [ ] \[V129-specific] Migrate only funds not spoken for by pending outbounds PR: [!3435](https://gitlab.com/thorchain/thornode/-/merge\_requests/3435)
* [ ] docs/concepts/memos.md - large clean-up PR: [!3433](https://gitlab.com/thorchain/thornode/-/merge\_requests/3433)
* [ ] \[Version-unspecific] Display OpenAPI-struct endpoint responses consistently with their Swagger examples PR: [!3432](https://gitlab.com/thorchain/thornode/-/merge\_requests/3432)
* [ ] \[Version-unspecific] Only report solvent vaults when all vaults are solvent PR: [!3412](https://gitlab.com/thorchain/thornode/-/merge\_requests/3412)
* [x] \[Version-unspecific] Querier: `continue` rather than `break` on queryStreamingSwap/s GetSwapQueueItem error (expected when no MsgSwap set for that index) PR: [!3404](https://gitlab.com/thorchain/thornode/-/merge\_requests/3404)
* [ ] Remove remaining ILP code PR: [!3402](https://gitlab.com/thorchain/thornode/-/merge\_requests/3402)

### v.1.128.2

* [ ] \[fix] Handle Ethereum 1.13.14 Update PR: [!3475](https://gitlab.com/thorchain/thornode/-/merge\_requests/3475)
* [ ] \[utxo] Performance Update to Dogecoin GetBlock with Verbose Transactions PR: [!3459](https://gitlab.com/thorchain/thornode/-/merge\_requests/3459)

### v.1.128.1

* [ ] Revert bug introduced by !3376 PR: [!3469](https://gitlab.com/thorchain/thornode/-/merge\_requests/3469)

### v1.128.0

* [ ] \[utxo] Abort Signing Retries with Spent VINs PR: [!3466](https://gitlab.com/thorchain/thornode/-/merge\_requests/3466)
* [ ] \[fix] Trade Account Events #check-lint-warning PR: [!3458](https://gitlab.com/thorchain/thornode/-/merge\_requests/3458)
* [ ] \[utxo] Expand Retryable Errors to Avoid Missed Observations PR: [!3457](https://gitlab.com/thorchain/thornode/-/merge\_requests/3457)
* [ ] \[Backwards-compatibility] Revert after-V126 addition of ETH.FLIP to V126 whitelist PR: [!3455](https://gitlab.com/thorchain/thornode/-/merge\_requests/3455)
* [ ] Use layer 1 asset of affiliate address PR: [!3449](https://gitlab.com/thorchain/thornode/-/merge\_requests/3449)
* [ ] \[fix] Miscellaneous Fixes for BNB Ragnarok PR: [!3447](https://gitlab.com/thorchain/thornode/-/merge\_requests/3447)
* [ ] \[mocknet] Update Midgard TimescaleDB PR: [!3444](https://gitlab.com/thorchain/thornode/-/merge\_requests/3444)
* [ ] \[fix] use size in network fee voter keys #check-lint-warning PR: [!3443](https://gitlab.com/thorchain/thornode/-/merge\_requests/3443)
* [ ] \[V128-specific] Set InactiveVault StatusSince when set from handler\_observed\_txout as well PR: [!3434](https://gitlab.com/thorchain/thornode/-/merge\_requests/3434)
* [ ] ~~Enact ADR012 PR:~~ [~~!3429~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3429)
* [x] add flip and fix vthor image PR: [!3426](https://gitlab.com/thorchain/thornode/-/merge\_requests/3426)
* [ ] Mocknet Daemon Updates PR: [!3422](https://gitlab.com/thorchain/thornode/-/merge\_requests/3422)
* [ ] \[V128-specific] needsNewVault check of outbound asset, not only outbound vault PR: [!3420](https://gitlab.com/thorchain/thornode/-/merge\_requests/3420)
* [ ] \[V128-specific] Do not act on solvency message consensus when for a height before the last chain height PR: [!3411](https://gitlab.com/thorchain/thornode/-/merge\_requests/3411)
* [ ] \[V128-specific] GetTxOutValue: Sum the value of MaxGas Coins too #check-lint-warning PR: [!3410](https://gitlab.com/thorchain/thornode/-/merge\_requests/3410)
* [ ] \[bugfix] add affiliate bps to recommended\_min\_amount\_in PR: [!3409](https://gitlab.com/thorchain/thornode/-/merge\_requests/3409)
* [ ] ~~Add ADR-012 Scale Lending PR:~~ [~~!3403~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3403)
* [ ] \[V128-specific] mimir v2 ids cleanup #check-lint-warning PR: [!3401](https://gitlab.com/thorchain/thornode/-/merge\_requests/3401)
* [ ] \[ci] One last fix to external pipeline trigger script PR: [!3398](https://gitlab.com/thorchain/thornode/-/merge\_requests/3398)
* [ ] \[Version-unspecific] querier\_quotes: Allow synth address shortening and shortened (or short code) asset parameters PR: [!3396](https://gitlab.com/thorchain/thornode/-/merge\_requests/3396)
* [ ] \[test] Allow Native TxID Template in Regression Check Endpoint PR: [!3393](https://gitlab.com/thorchain/thornode/-/merge\_requests/3393)
* [ ] Remove unused TSS preparams argument to Bifrost PR: [!3391](https://gitlab.com/thorchain/thornode/-/merge\_requests/3391)
* [ ] Removing unused .proto files PR: [!3390](https://gitlab.com/thorchain/thornode/-/merge\_requests/3390)
* [ ] More evm and ethereum package deduplication PR: [!3383](https://gitlab.com/thorchain/thornode/-/merge\_requests/3383)
* [ ] Strip bnb-chain SDK from non-BNB codebase PR: [!3376](https://gitlab.com/thorchain/thornode/-/merge\_requests/3376)
* [ ] ~~\[Version-unspecific] Drop querier GetLoanCollateralRemainingForPool error, since negative MaxRuneSupply error expected before height 12241034 PR:~~ [~~!3375~~](https://gitlab.com/thorchain/thornode/-/merge\_requests/3375)
* [ ] Use a more clear error when deposit fails due to insufficient funds PR: [!3373](https://gitlab.com/thorchain/thornode/-/merge\_requests/3373)
* [ ] Switch ledger-thorchain-go from github to gitlab PR: [!3371](https://gitlab.com/thorchain/thornode/-/merge\_requests/3371)
* [ ] \[V128-specific] Increase THORNode vault balance only after inbound confirmation counting PR: [!3351](https://gitlab.com/thorchain/thornode/-/merge\_requests/3351)
* [ ] \[V128-specific] Use both effectiveSecurityBond and totalEffectiveBond to calculate poolShare PR: [!3347](https://gitlab.com/thorchain/thornode/-/merge\_requests/3347)
* [ ] \[feature] Trade accounts #check-lint-warning PR: [!3326](https://gitlab.com/thorchain/thornode/-/merge\_requests/3326)
* [ ] Dev upates and supported address PR: [!3315](https://gitlab.com/thorchain/thornode/-/merge\_requests/3315)
* [ ] \[code-games] Comments in handler.go PR: [!3297](https://gitlab.com/thorchain/thornode/-/merge\_requests/3297)
* [ ] \[V128-specific] Min swap fee PR: [!3258](https://gitlab.com/thorchain/thornode/-/merge\_requests/3258)
