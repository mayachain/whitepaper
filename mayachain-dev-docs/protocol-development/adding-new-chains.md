# Adding New Chains

Chain Developers should be extremely familiar with how MAYAChain works, and how their own chain works.

There is now a specific process for the addition of new chains, see: ​[https://gitlab.com/mayachain/mayanode/-/blob/develop/docs/chains/README.md](https://gitlab.com/mayachain/mayanode/-/blob/develop/docs/chains/README.md)

#### Process <a href="#process" id="process"></a>

1. Read ​[https://gitlab.com/mayachain/mayanode/-/blob/develop/docs/newchain.md](https://gitlab.com/mayachain/mayanode/-/blob/develop/docs/newchain.md)
2. Bifrost: Start by forking one of the existing [Bifrosts](https://gitlab.com/mayachain/mayanode/-/tree/develop/bifrost/pkg/chainclients?ref\_type=heads) (UTXO, EVM or COSMOS).
3. Daemon: Add the chain daemon to MAYAChain/Node-Launcher: [https://gitlab.com/mayachain/devops/node-launcher](https://gitlab.com/mayachain/devops/node-launcher)
4. Smoke Tests: Build out the smoke tests for the chain. This ensures the connection is robustly tested.
5. [XChainJS](https://github.com/xchainjs/xchainjs-lib): Add a new chain package to XChainJS so the entire ecosystem of wallets can easily support.

Once this is complete, the chain can be added to Stagenet. After some time of demonstrating Stablity on Stagenet, the MAYAChain Node Operator community is polled and if supported, it can be merged to Mainnet.

Once on mainnet, the chain is typically given a period of 12 months to demonstrate uptake and usage. If the chain cannot maintain sufficient demand, it may be removed from the network and all liquidity refunded to LPs.
