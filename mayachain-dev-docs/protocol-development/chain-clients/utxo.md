---
description: How the Bifrost works for UTXO chains like Bitcoin and its forks.
---

# UTXO

## Chain Client

Example for Bitcoin.

#### Observer

{% embed url="https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref_type=heads" %}

#### Signer

{% embed url="https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/signer.go?ref_type=heads" %}

### Scanning Blocks

The block scanner monitors the Asgard Addresses and looks for incoming UTXOs spending to those addresses. When it sees one performs validation on it and witnesses to MAYAChain. For Bitcoin, it looks that at least 1 output is spent to Asgard, and searchs for another output to have an `OP_RETURN`. These two outputs form the `amount` and `memo` witness to MAYAChain.



### Confirmation Counting

The `txValue` is the sum of all transactions received in a block to Asgard vaults. The `blockValue` is the coinbase value, which includes fees and subsidy. If a miner forgets to add a coinbase value (it has happened) a default of 6.25 is used. (This should be updated every 4 years, or use logic to auto-update).

[https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L1161](https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L1161)

### Re-orgs

Bifrost tracks the `BlockCacheSize = 144` blocks of transactions reported in a local KV store. Every time it detects a new block at a previous height it has seen, it checks for the presence of every transaction it has reported. If the transaction is missing then it has been re-orged out. The missing txID is reported to MAYAChain as an `ErrataTx`

[https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L376](https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L376)

### Network Fees

Reported as `sats/byte` where the fee rate is computed over the last block. Reports the highest seen in the last 20 blocks.&#x20;

[https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L725](https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L725)

### Handling Gas

The gas amount for a transaction is just the difference between outputs and inputs.\


[https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L1055](https://gitlab.com/mayachain/mayanode/-/blob/develop/bifrost/pkg/chainclients/bitcoin/client.go?ref\_type=heads#L1055)

### Other Considerations

#### UTXO consolidation

UTXOs consume inputs, and these inputs need to be signed independently. Thus consuming 15 inputs requires 15 times the TSS bandwidth than a single input. To prevent runaway liabilities the client will automatically enter a TSS signing ceremony for Asgard every 15 inputs to consolidate them back to one. This transaction uses the `consolidate` memo and can be seen regularly on MAYAChain vaults.

[https://gitlab.com/thorchain/thornode/-/blob/4bcaf4f80787d0aaee711388578ed453959ef673/bifrost/pkg/chainclients/bitcoin/client.go#L915](https://gitlab.com/thorchain/thornode/-/blob/4bcaf4f80787d0aaee711388578ed453959ef673/bifrost/pkg/chainclients/bitcoin/client.go#L915)

#### ReplaceByFee

RBF transactions allow the spender to double-spend with a higher fee. Users can use RBF transactions to spend to Asgard, but RBF does not need to be used in the THORChain vaults.&#x20;

## Wallet Client

UTXO clients implemented in XChainJS have the following nuances:

### Fees

The wallet client should spend with a fee rate at least equal to what is reported on `inbound_addresses` - if not it risks not being confirmed by the time the vault migrates.

### Pending UTXOS

Do not consume pending transactions when spending to Asgard (with a memo) since it may consume a low-fee tx and get stuck.

### MEMO

The memo is inserted as an OP\_RETURN in an output. It can be any output. The MEMO is limited to 80 bytes, so it should be trimmed and use abbreviated memos or Asset identifiers where possible.
