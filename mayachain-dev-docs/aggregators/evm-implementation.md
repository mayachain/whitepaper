---
description: On-Chain AMM Calls to MAYAChain
---

# EVM Implementation

MAYAChain Aggregator Example

{% embed url="https://gitlab.com/mayachain/ethereum/eth-router/-/blob/master/contracts/MAYAChain_Aggregator.sol?ref_type=heads" %}

Tokens must be on the ETH Whitelist (LINK). The destination address should be a user control address, not a contract address.&#x20;

### SwapIn

The aggregator contract needs a **swapIn** function similar to the one below. First, swap the token via an on-chain AMM, then call into MAYAChain and pass the correct memo to execute the next swap.&#x20;

```go
 function swapIn(
        address tcVault,
        address tcRouter,
        string calldata tcMemo,
        address token,
        uint amount,
        uint amountOutMin,
        uint256 deadline
        ) public nonReentrant {
        uint256 _safeAmount = safeTransferFrom(token, amount); // Transfer asset
        safeApprove(token, address(swapRouter), amount);
        address[] memory path = new address[](2);
        path[0] = token; path[1] = WETH;
        swapRouter.swapExactTokensForETH(_safeAmount, amountOutMin, path, address(this), deadline);
        _safeAmount = address(this).balance;
        iROUTER(tcRouter).depositWithExpiry{value:_safeAmount}(payable(tcVault), ETH, _safeAmount, tcMemo, deadline);
    }
```

Transaction Example (LINK). Note the destination address is not a contract address.&#x20;

### SwapOut

The MAYAChain router uses `transferOutAndCall()` to call the aggregator with a max GasLimit of 400k units.&#x20;

It is a particular function that also handles a swap fail by sending the user the base asset directly (ie, breached AmountOutMin, or could not find the finaltoken). The user will need to do the swap manually.&#x20;

The parameters for this function are passed to MAYAChain by the user's original memo.&#x20;

```go
function transferOutAndCall(address payable target, address finalToken, address to, uint256 amountOutMin, string memory memo) public payable nonReentrant {
        uint256 _safeAmount = msg.value;
        (bool erc20Success, ) = target.call{value:_safeAmount}(abi.encodeWithSignature("swapOut(address,address,uint256)", finalToken, to, amountOutMin));
        if (!erc20Success) {
            bool ethSuccess = payable(to).send(_safeAmount); // If can't swap, just send the recipient the ETH
            if (!ethSuccess) {
                payable(address(msg.sender)).transfer(_safeAmount); // For failure, bounce back to Yggdrasil & continue.
            }
        }
        emit TransferOutAndCall(msg.sender, target, _safeAmount, finalToken, to, amountOutMin, memo);
    }
```

The **swapOut** function will only be passed three parameters from the MAYAChain Router and it must comply with the function signature (name, parameters). It can then call an on-chain AMM to execute the swap. It will only ever be given the base asset (eg ETH).&#x20;

Here is an example to call UniV2 router:

```go
function swapOut(address token, address to, uint256 amountOutMin) public payable nonReentrant {
        address[] memory path = nelw address[](2);
        path[0] = WETH; path[1] = token;
        swapRouter.swapExactETHForTokens{value: msg.value}(amountOutMin, path, to, type(uint).max);
    }
```
