---
description: Understanding THORChain Asset Notation
---

# Asset Notation

MAYAChain uses a CHAIN.ASSET notation for all assets. TICKER and ID are added where required. The asset full notation is pictured.

![L1 Asset Notation](https://docs.google.com/drawings/u/1/d/skidhZPIsMKQ-XWJWb3EJaQ/image?w=698\&h=276\&rev=23\&ac=1\&parent=1ZoJQKvyATQekFbWMk_rqX96K9BSmCArh9e-A_g66wDQ)

There are three kinds of assets within MAYAChain:

1. [Layer 1 Assets](asset-notation.md#layer-1-assets) - CHAIN.ASSET
2. [Synthetic Assets](asset-notation.md#synthetic-assets) - CHAIN/Asset

#### Examples

| Asset    | Notation                                            |
| -------- | --------------------------------------------------- |
| Bitcoin  | BTC.BTC (Native BTC)                                |
| Bitcoin  | BTC/BTC (Synthetic BTC)                             |
| Bitcoin  | MAYA.BTC (Derived BTC)                              |
| Ethereum | ETH.ETH                                             |
| USDT     | ETH.USDT-0xdac17f958d2ee523a2206206994597c13d831ec7 |
| DASH     | DASH.DASH (Native)                                  |
| DASH     | DASH/DASH (Synth)                                   |
| CACAO    | THOR.RUNE                                           |

### Layer 1 Assets

* Layer 1 (L1) chains are always denoted as `CHAIN.ASSET`, e.g. BTC.BTC.&#x20;
* As two tokens can live on different blockchains, the chain can be used to distinguish them. Example: USDC is on the Ethereum Chain and Arbitrum Chain and is denoted as `ETH.USDC` and `ARB.USDC` respectively; note the contract address (ticker) was removed for simplicity.&#x20;
* Tickers are added to denote assets and are required in the full name. For EVM based Chains, the ticker is the ERC20 Contract address, e.g. `ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48`. This ensures the asset is specific to the contract address. The [pools list ](https://midgard.ninerealms.com/v2/pools)shows assets using the full notation.&#x20;
* [Synthetic Assets](../../deep-dive/mayachain-finance/synthetic-asset-model.md) are denoted as `CHAIN/ASSET` instead of `CHAIN.ASSET, e`.g. Synthetic BTC is `BTC/BTC` and Synthetic USDT is `ETH/USDT.` While Synthetic assets live on the MAYAChain blockchain, they retain their CHAIN identifier.
* Synthetic Assets can only be created from MAYAChain-supported L1 assets and are only denoted as `CHAIN/ASSET`, no ticker or ID is required.
* Chain differentiation is also used for Synthetics, e.g. `ETH/USDC` and `ARB/USDC` are different Synthetic assets created and redeemable on different chains.
