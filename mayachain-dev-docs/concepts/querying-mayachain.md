---
description: How to Query MAYAChain
---

# Querying MAYAChain

### Getting the Asgard Vault

Vaults are fetched from the `/inbound_addresses` :

[https://mayanode.mayachain.info/mayachain/inbound\_addresses](https://mayanode.mayachain.info/mayachain/inbound\_addresses)

You need to select the address of the Chain the inbound transaction will go to.&#x20;

The address will be the current active Asgard Address that accepts inbounds. Do not cache these address as they change regularly. Do not delay inbound transactions (e.g. do not use future timeLocks).

Example Output, each connected chain will be displayed.&#x20;

```json
[
  {
		"address": "bc1qqtemwlu9ju3ts3da5l82qejnzdl3xfs3lcl4wg",
		"chain": "BTC",
		"chain_lp_actions_paused": false,
		"chain_trading_paused": false,
		"gas_rate": "390",
		"gas_rate_units": "satsperbyte",
		"global_trading_paused": false,
		"halted": false,
		"outbound_fee": "390000",
		"outbound_tx_size": "1000",
		"pub_key": "mayapub1addwnpepqvz6vkw4nzcp5sl9ktx63kz7v0n754r99uehfnzv42eauc644yzgvmuun3k"
	},
      ...

```

{% hint style="danger" %}
Never cache vault addresses, they churn regularly.&#x20;
{% endhint %}

{% hint style="danger" %}
Inbound transactions should not be delayed for any reason else there is risk funds will be sent to an unreachable address. Use standard transactions, check the `inbound address` before sending and use the recommended `gas rate` to ensure transactions are confirmed in the next block to the latest `Inbound_Address`.
{% endhint %}

{% hint style="danger" %}
Check for the `halted` parameter and never send funds if it is set to true
{% endhint %}

{% hint style="warning" %}
If a chain has a `router` on the inbound address endpoint, then everything must be deposited via the router. The router is a contract that the user first approves, and the deposit call transfers the asset into the network and emits an event to THORChain.&#x20;

\
This is done because "tokens" on protocols don't support memos on-chain, thus need to be wrapped by a router which can force a memo.&#x20;

Note: you can transfer the base asset, eg ETH, directly to the address and skip the router, but it is recommended to deposit everything via the router.&#x20;

```
{
		"address": "0x87a8d8abd8086173e2f15a90a1938d8077e02ecb",
		"chain": "ETH",
		"chain_lp_actions_paused": false,
		"chain_trading_paused": false,
		"gas_rate": "100",
		"gas_rate_units": "gwei",
		"global_trading_paused": false,
		"halted": false,
		"outbound_fee": "840000",
		"outbound_tx_size": "80000",
		"pub_key": "mayapub1addwnpepqvz6vkw4nzcp5sl9ktx63kz7v0n754r99uehfnzv42eauc644yzgvmuun3k",
		"router": "0xe3985E6b61b814F7Cdb188766562ba71b446B46d"
	},
```
{% endhint %}

{% hint style="warning" %}
If you connect to a public Midgard, you must be conscious of the fact that you can be phished and could send money to the WRONG vault. You should do safety checks, i.e. comparing with other nodes, or even inspecting the vault itself for the presence of funds. You should also consider running your own '[fullnode](../../node-docs/mayanodes/overview.md)' instance to query for trusted data.
{% endhint %}

* `Chain`: Chain Name
* `Address`: Asgard Vault inbound address for that chain.,&#x20;
* `Halted`: Boolean, if the chain is halted. This should be monitored.
* `gas_rate`: rate to be used, e.g. in Stats or GWei. See Fees.

### Displaying available pairs

Use the `/pools` [endpoint](https://mayanode.mayachain.info/mayachain/pools) of Midgard to retrieve all swappable assets on MAYAChain. The response will be an array of objects like this:

```json
{
		"annualPercentageRate": "0.1653348126078579",
		"asset": "KUJI.KUJI",
		"assetDepth": "22781179322318",
		"assetPrice": "3.621027920713548",
		"assetPriceUSD": "3.498740619649093",
		"liquidityUnits": "5385277119677037",
		"poolAPY": "0.1653348126078579",
		"runeDepth": "8249128639289561",
		"status": "available",
		"synthSupply": "31040367",
		"synthUnits": "3668841935",
		"units": "5385280788518972",
		"volume24h": "561310885869971"
	}
```

{% hint style="info" %}
Only pools with `"status": "available"` are available to trade
{% endhint %}

{% hint style="info" %}
Make sure to manually add Native $CACAO as a swappable asset.
{% endhint %}

{% hint style="info" %}
`"assetPrice" tells you the asset's price in CACAO (RUNE Depth/AssetDepth ). In the above example`

`1 KUJI.KUJI = 3.62 RUNE`
{% endhint %}

### Decimals and Base Units

All values on MAYAChain (MAYANode and Midgard) are given in 1e8 eg, 100000000 base units (like Bitcoin), and unless postpended by "USD", they are in units of RUNE. Even 1e18 assets, such as ETH.ETH, are shortened to 1e8. 1e6 Assets like ETH.USDC, are padded to 1e8. MAYANode will tell you the decimals for each asset, giving you the opportunity to convert back to native units in your interface.&#x20;

See code examples using the MAYAChain xchain package here [https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain ](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain)

### Finding Chain Status

There are two ways to see if a Chain is halted.&#x20;

1. Looking at the `/inbound_addresses` [endpoint](https://mayanode.mayachain.info/mayachain/inbound\_addresses) and inspecting the halted flag.
2. Looking at Mimir and inspecting the HALT\[Chain]TRADING setting. See [Network Halts](network-halts.md) for more details.
