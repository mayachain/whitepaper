---
description: Understanding how fees are calculated.
---

# Fees

## Overview

There are 4 different fees the user should know about.&#x20;

1. Inbound Fee (sourceChain: gasRate \* txSize)
2. Affiliate Fee (affiliateFee \* swapAmount)
3. Liquidity Fee (swapSlip \* swapAmount)
4. Outbound Fee (destinationChain: gasRate \* txSize)

### **Terms**&#x20;

* **SourceChain**: the chain the user is swapping from&#x20;
* **DestinationChain**: the chain the user is swapping to txSize: the size of the transaction in bytes (or units)&#x20;
* **gasRate**: the current gas rate of the external network&#x20;
* **swapAmount**: the amount the user is swapping swapSlip: the slip created by the&#x20;
* **swapAmount**, as a function of poolDepth&#x20;
* **affiliateFee**: optional fee set by interface in basis points

## Fees Detail

### Inbound Fee

This is the fee the user pays to make a transaction on the source chain, which the user pays directly themselves. The gas rate recommended to use is `fast` where the tx is guaranteed to be committed in the next block. Any longer and the user will be waiting a long time for their swap and their price will be invalid (thus they may get an unnecessary refund).&#x20;

$$
inboundFee = txSize * gasRate
$$

{% hint style="success" %}
MAYAChain calculates and posts fee rates at [`https://mayanode.mayachain.info/mayachain/inbound_addresses`](https://mayanode.mayachain.info/mayachain/inbound\_addresses)
{% endhint %}

{% hint style="warning" %}
Always use a "fast" or "fastest" fee, if the transaction is not confirmed in time, it could be abandoned by the network or failed due to old prices. You should allow your users to cancel or re-try with higher fees.
{% endhint %}

### Liquidity Fee

This is simply the slip created by the transaction multiplied by its amount. It is priced and deducted from the destination amount automatically by the protocol.

$$
slip = \frac{swapAmount}{swapAmount + poolDepth}
$$

$$
fee =slip * swapAmount
$$

### Affiliate Fee

In the swap transaction you build for your users you can include an affiliate fee for your exchange (accepted in $CACAOor a synthetic asset, so you will need a $CACAO address).

* The affiliate fee is in basis points (0-500) and will be deducted from the inbound swap amount from the user.&#x20;
* If the inbound swap asset is a native MAYAChain asset ($CACAO or synth) the affiliate fee amount will be deducted directly from the transaction amount.&#x20;
* If the inbound swap asset is on any other chain the network will submit a swap to $CACAO with the destination address as your affiliate fee address.&#x20;
* If the affiliate is added to an ADDLP tx, then the affiliate is included in the network as an LP.&#x20;

`SWAP:CHAIN.ASSET:DESTINATION:LIMIT:AFFILIATE:FEE`

Read [https://medium.com/thorchain/affiliate-fees-on-thorchain-17cbc176a11b](https://medium.com/thorchain/affiliate-fees-on-thorchain-17cbc176a11b) for more information.&#x20;

$$
affliateFee = \frac{feeInBasisPoints * swapAmount}{10000}
$$

### Preferred Asset for Affiliate Fees

Affiliates can collect their fees in the asset of their choice (choosing from the assets that have a pool on MAYAChain). In order to collect fees in a preferred asset, affiliates must use a [MAYAName](../introduction/mayaname-guide.md) in their swap [memos](transaction-memos.md#swap).&#x20;

#### How it Works

If an affiliate's MAYAName has the proper preferred asset configuration set, the network will begin collecting their affiliate fees in $CACAO in the AffiliateCollector module. Once the accrued CACAO in the module is greater than [`PreferredAssetOutboundFeeMultiplier`](https://gitlab.com/thorchain/thornode/-/blob/develop/constants/constants\_v1.go#L107)`* outbound_fee` of the preferred asset's chain, the network initiates a swap from $CACAO -> Preferred Asset on behalf of the affiliate. At the time of writing, `PreferredAssetOutboundFeeMultiplier` is set to `100`, so the preferred asset swap happens when the outbound fee is 1% of the accrued $CACAO.&#x20;

**Configuring a Preferred Asset for a MAYAName**

1. [**Register a MAYAName**](../introduction/mayaname-guide.md#create-a-mayaname) if not done already. This is done with a `MsgDeposit` posted to the MAYAChain network.&#x20;
2. Set your preferred asset's chain alias (the address you'll be paid out to), and your preferred asset. _Note: your preferred asset must be currently supported by MAYAChain._

For example, if you wanted to be paid out in USDC you would:

1. Grab the full USDC name from the [Pools ](https://mayanode.mayachain.info/mayachain/pools)endpoint: `ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48`
2.  Post a `MsgDeposit` to the MAYAChain network with the appropriate memo to register your MAYAName, set your preferred asset as USDC, and set your Ethereum network address alias. Assuming the following info:

    1. MAYAChain address: `maya1g8dzs4ywxhf8hynaddw4mhwzlwzjfccakkfch7`
    2. MAYAName: `wr`
    3. ETH payout address: `0x6621d872f17109d6601c49edba526ebcfd332d5d`&#x20;

    The full memo would look like:

{% code overflow="wrap" %}
```
~:wr:ETH:0x6621d872f17109d6601c49edba526ebcfd332d5d:maya1g8dzs4ywxhf8hynaddw4mhwzlwzjfccakkfch7:ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48
```
{% endcode %}

{% hint style="info" %}
You can use [Asgardex](https://github.com/asgardex/asgardex-desktop/) to post a MsgDeposit with a custom memo. Load your wallet, then open your MAYAChain wallet page > Deposit > Custom.&#x20;
{% endhint %}

{% hint style="info" %}
You will also need a MAYA alias set to collect affiliate fees. Use another MsgDeposit with memo: `~:<mayaname>:MAYA:<mayachain-address>` to set your MAYA alias. Your MAYA alias address can be the same as your owner address, but won't be used for anything if a preferred asset is set.&#x20;
{% endhint %}

{% hint style="info" %}
You can also set one or more subaffiliates to share revenue from affiliate fees. Use another MsgDeposits with memos:&#x20;

`~:wr:500:SUBA1:2000`

`~:wr:500:SUBA2:1000`
{% endhint %}

Once you successfully post your MsgDeposit you can verify that your MAYAName is configured properly. View your MAYAName info from MAYANode at the following endpoint:\
[https://mayanode.mayachain.info/mayachain/mayaname/wr](https://mayanode.mayachain.info/mayachain/mayaname/wr)

The response should look like:

```
{
  "name": "wr",
  "expire_block_height": 22061405,
  "owner": "maya1g8dzs4ywxhf8hynaddw4mhwzlwzjfccakkfch7",
  "preferred_asset": "ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48",
  "affiliate_collector_cacao": "0",
  "aliases": [
    {
      "address": "0x6621d872f17109d6601c49edba526ebcfd332d5d",
      "chain": "ETH"
    },
    {
      "address": "maya1v7gqc98d7d2sugsw5p4pshv0mm24mfmzgmj64n",
      "chain": "MAYA"
    }
  ],
  "affiliate_bps": 150
  "subaffiliates": [
    {
      "name": "SUBA1",
      "bps": 2000
    },
    {
      "name": "SUBA2",
      "bps": 1000
    }
  ]  
}
```

Your MAYAName is now properly configured and any affiliate fees will begin accruing in the AffiliateCollector module. You can verify that fees are being collected by checking the `affiliate_collector_rune` value of the above endpoint.&#x20;

### Outbound Fee

This is the fee the Network pays on behalf of the user to send the outbound transaction. To adequately pay for network resources (TSS, compute, state storage) the fee is marked up from what nodes actually pay on-chain by an "Outbound Fee Multiplier" (OFM).&#x20;

The OFM moves between a `MaxOutboundFeeMultiplier` and a `MinOutboundFeeMultiplier`(defined as [Mimir Values](https://mayanode.mayachain.info/mayachain/mimir)), based on the network's current outbound fee "surplus" in relation to a "target surplus". The outbound fee "surplus" is the cumulative difference (in $CACAO) between what the users are charged for outbound fees and what the nodes actually pay. As the network books a "surplus" the OFM slowly decreases from the Max to the Min. Current values for the OFM can be found on the [Network Endpoint](https://mayanode.mayachain.info/mayachain/network).&#x20;

$$
outboundFee = txSize * gasRate * OFM
$$

The minimum Outbound Layer1 Fee the network will charge is on `/mayachain/mimir` and is priced in USD (based on MAYAChain's USD pool prices). This means really cheap chains still pay their fair share. It is currently set to `10000000000` = $1.00

See [Outbound Fee](../../deep-dive/how-it-works/fees.md#outbound-fee) for more information.&#x20;

## Fee Ordering for Swaps

Fees are taken in the following order when conducting a swap.

1. Inbound Fee (user wallet controlled, not MAYAChain controlled)
2. Affiliate Fee (if any) - skimmed from the input.
3. Swap Fee (denoted in output asset)
4. Outbound Fee (taken from the swap output)

To work out the total fees, fees should be converted to a common asset (e.g. CACAO or USD) then added up. Total fees should be less than the input else it is likely to result in a refund.

### Refunds and Minimum Swappable Amount

If a transaction fails, it is refunded, thus it will pay the `outboundFee` for the **SourceChain** not the DestinationChain. Thus devs should always swap an amount that is a maximum of the following, multiplier by a buffer of at least 4x to allow for sudden gas spikes:

1. The Destination Chain outbound\_fee
2. The Source Chain outbound\_fee
3. $1.00 (the minimum)

The outbound\_fee for each chain is returned on the [Inbound Addresses](https://mayanode.mayachain.info/mayachain/inbound\_addresses) endpoint, priced in the gas asset.&#x20;

It is strongly recommended to use the `recommended_min_amount_in` value that is included on the [Swap Quote](../introduction/swapping-guide/quickstart-guide.md) endpoint, which is the calculation described above. This value is priced in the inbound asset of the quote request (in 1e8). This should be the minimum-allowed swap amount for the requested quote.&#x20;

_Remember, if the swap limit is not met or the swap is otherwise refunded the outbound\_fee of the Source Chain will be deducted from the input amount, so give your users enough room._

### Understanding gas\_rate

MAYANode keeps track of current gas prices. Access these at the `/inbound_addresses` endpoint of the [MAYANode API](connecting-to-mayachain.md). The response is an array of objects like this:

```json
{
		"address": "0x87a8d8abd8086173e2f15a90a1938d8077e02ecb",
		"chain": "ETH",
		"chain_lp_actions_paused": false,
		"chain_trading_paused": false,
		"gas_rate": "130",
		"gas_rate_units": "gwei",
		"global_trading_paused": false,
		"halted": false,
		"outbound_fee": "1080000",
		"outbound_tx_size": "80000",
		"pub_key": "mayapub1addwnpepqvz6vkw4nzcp5sl9ktx63kz7v0n754r99uehfnzv42eauc644yzgvmuun3k",
		"router": "0xe3985E6b61b814F7Cdb188766562ba71b446B46d"
	},
```

The `gas_rate` property can be used to estimate network fees for each chain the swap interacts with. For example, if the swap is `BTC -> ETH` the swap will incur fees on the bitcoin network and Ethereum network. The `gas_rate` property works differently on each chain "type" (e.g. EVM, UTXO, COSMOS).

The `gas_rate_units` explain what the rate is for chain, as a prompt to the developer.&#x20;

The `outbound_tx_size` is what MAYAChain internally budgets as a typical transaction size for each chain.&#x20;

The `outbound_fee` is `gas_rate * outbound_tx_size * OFM` and developers can use this to budget for the fee to be charged to the user. The current Outbound Fee Multiplier (OFM) can be found on the [Network Endpoint](https://mayanode.mayachain.info/mayachain/network).

Keep in mind the `outbound_fee` is priced in the gas asset of each chain. For chains with tokens, be sure to convert the `outbound_fee` to the outbound token to determine how much will be taken from the outbound amount. To do this, use the `getValueOfAsset1InAsset2` formula described in the [`Math`](math.md) section.

## Fee Calculation by Chain&#x20;

### **MAYAChain (Cacao)**

The MAYAChain blockchain has a set 0.5 CACAO fee. This is set within the MAYAChain [Mimir](https://mayanode.mayachain.info/mayachain/mimir) by `NativeTransactionFee`. As MAYAChain is 1e10, `5000000000 TOR = 0.5 CACAO`

{% hint style="warning" %}
CACAO is the only asset expressed in 1e10 format. All other asset are expressed in 1e8 format.
{% endhint %}

### UTXO Chains like Bitcoin

For UXTO chains link Bitcoin, `gas_rate`is denoted in Satoshis. The `gas_rate` is calculated by looking at the average previous block fee seen by the MAYANodes.

All MAYAChain transactions use BECH32 so a standard tx size of 250 bytes can be used. The standard UTXO fee is then `gas_rate`\* 250.

### EVM Chains like Ethereum

For EVM chains like Ethereum, `gas_rate`is denoted in GWEI. The `gas_rate` is calculated by looking at the average previous block fee seen by the MAYANodes

An Ether Tx fee is: `gasRate * 10^9 (GWEI) * 21000 (units).`

An ERC20 Tx is larger: `gasRate * 10^9 (GWEI) * 70000 (units)`

{% hint style="success" %}
MAYAChain calculates and posts gas fee rates at [`https://mayanode.mayachain.info/mayachain/inbound_addresses`](https://mayanode.mayachain.info/mayachain/inbound\_addresses)&#x20;
{% endhint %}

