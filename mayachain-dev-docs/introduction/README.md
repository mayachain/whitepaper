# Introduction

MAYAChain is a decentralised cross-chain liquidity protocol that allows users to add liquidity or swap over that liquidity. It does not peg or wrap assets. Swaps are processed as easily as making a single on-chain transaction.

MAYAChain works by observing transactions to its vaults across all the chains it supports. When the majority of nodes observe funds flowing into the system, they agree on the user's intent (usually expressed through a [memo](../../.gitbook/assets/memos) within a transaction) and take the appropriate action.

{% hint style="info" %}
For more information see [Technology](../../deep-dive/how-it-works/technology.md) or [Concepts](../concepts/).
{% endhint %}

For wallets/interfaces to interact with MAYAChain, they need to:

1. Connect to MAYAChain to obtain information from one or more endpoints.
2. Construct transactions with the correct memos.
3. Send the transactions to MAYAChain Inbound Vaults.

{% hint style="info" %}
[Front-end](./#front-end-development-guides) guides have been developed for fast and simple implementation.
{% endhint %}

## Front-end Development Guides <a href="#front-end-development-guides" id="front-end-development-guides"></a>

### Native Swaps Guide

Frontend developers can use MAYAChain to access decentralised layer1 swaps between BTC, ETH, DASH, KUJI and more.

{% content-ref url="swapping-guide/quickstart-guide.md" %}
[quickstart-guide.md](swapping-guide/quickstart-guide.md)
{% endcontent-ref %}

### Native Savings Guide

MAYAChain offers a Savings product, which earns yield from Swap fees. Deposit Layer1 Assets to earn in-kind yield. No lockups, penalties, impermanent loss, minimums, maximums or KYC.

{% content-ref url="swapping-guide/quickstart-guide.md" %}
[quickstart-guide.md](swapping-guide/quickstart-guide.md)
{% endcontent-ref %}

### Aggregators

Aggregators can deploy contracts that use custom `swapIn` and `swapOut` cross-chain aggregation to perform swaps before and after MAYAChain.

Eg, swap from an asset on Sushiswap, then MAYAChain, to an asset on Fin in one transaction.

{% content-ref url="../aggregators/" %}
[aggregators](../aggregators/)
{% endcontent-ref %}

### Concepts

In-depth guides to understand MAYAChain's implementation have been created.

{% content-ref url="../concepts/" %}
[concepts](../concepts/)
{% endcontent-ref %}

### Libraries

Several libraries exist to allow for rapid integration. xchainjs has seen the most development is recommended.

{% content-ref url="../examples/" %}
[examples](../examples/)
{% endcontent-ref %}

Eg, swap from layer 1 ETH to BTC and back.

[https://docs.xchainjs.org/overview/](https://docs.xchainjs.org/overview/)

### Analytics

Analysts can build on Midgard or Flipside to access cross-chain metrics and analytics.

Eg, gather information on cross-chain liquidity

### Connecting to MAYAChain

MAYAChain has several APIs with Swagger documentation.

* Midgard - [https://midgard.ninerealms.com/v2/doc](https://midgard.mayachain.info/v2/doc)
* MAYANode - [https://mayanode.mayachain.info/mayachain/doc](https://mayanode.mayachain.info/mayachain/doc)
* Cosmos RPC -[ ](https://tendermint.mayachain.info)[https://tendermint.mayachain.info](https://tendermint.mayachain.info)

See [Connecting to MAYAChain ](../concepts/connecting-to-mayachain.md)for more information.
