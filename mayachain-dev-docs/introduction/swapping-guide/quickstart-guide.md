---
description: Make a cross-chain swap on MAYAChain in less than 5 minutes.
---

# Quickstart Guide

### **Introduction** <a href="#introduction" id="introduction"></a>

MAYAChain allows native L1 Swaps. On-chain [Memos](../../concepts/transaction-memos.md) are used instruct MAYAChain how to swap, with the option to add [price limits](quickstart-guide.md#price-limits) and [affiliate fees](quickstart-guide.md#affiliate-fees). MAYAChain nodes observe the inbound transactions and when the majority have observed the transactions, the transaction is processed by threshold-signature transactions from MAYAChain vaults.&#x20;

Let's demonstrate decentralized, non-custodial cross-chain swaps. In this example, we will build a transaction that instructs MAYAChain to swap native Bitcoin to native Ethereum in one transaction.

{% hint style="info" %}
The following examples use a free, hosted API provided by the Maya Protocol team. If you want to run your own full node, please see [Connecting to MAYAChain.](../../concepts/connecting-to-mayachain.md)
{% endhint %}

### 1. Determine the correct asset name. <a href="#id-1.-determine-the-correct-asset-name" id="id-1.-determine-the-correct-asset-name"></a>

MAYAChain uses a specific [asset notation.](../../concepts/asset-notation.md) Available assets are at: [​Pools Endpoint](https://mayanode.mayachain.info/mayachain/pools).

BTC => `BTC.BTC`&#x20;

ETH => `ETH.ETH`

{% hint style="info" %}
Only available pools can be used. (`where 'status' == Available)`
{% endhint %}

### 2. Query for a swap quote. <a href="#id-2.-query-for-a-swap-quote" id="id-2.-query-for-a-swap-quote"></a>

{% hint style="info" %}
All amounts are 1e8. Multiply native asset amounts by 100000000 when dealing with amounts in THORChain. 1 BTC = 100,000,000.
{% endhint %}

**Request**: _Swap 1 BTC to ETH and send the ETH to_ `0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`.

[https://mayanode.mayachain.info/mayachain/quote/swap?from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&amount=100000000\&destination=0x86d526d6624AbC0178cF7296cD538Ecc080A95F1](https://mayanode.mayachain.info/mayachain/quote/swap?from_asset=BTC.BTC\&to_asset=ETH.ETH\&amount=100000000\&destination=0x86d526d6624AbC0178cF7296cD538Ecc080A95F1)

​​**Response**:

```bison
{
  "expected_amount_out": "1855545107",
  "fees": {
    "affiliate": "0",
    "asset": "ETH.ETH",
    "outbound": "840000"
  },
  "inbound_address": "bc1qqtemwlu9ju3ts3da5l82qejnzdl3xfs3lcl4wg",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:ETH.ETH:0x86d526d6624AbC0178cF7296cD538Ecc080A95F1",
  "outbound_delay_blocks": 179,
  "outbound_delay_seconds": 2685,
  "slippage_bps": 168
}
```

_If you send 1 BTC to `bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8` with the memo `=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`, you can expect to receive_ `18.55545107` _ETH._

_For security reasons, your inbound transaction will be delayed by 600 seconds (1 BTC Block) and 2685 seconds (or 179 native MAYAChain blocks) for the outbound transaction, 3285_ seconds all u&#x70;_. You will pay an outbound gas fee of 0.0085 ETH and will incur 168 basis points (1.68%) of slippage._

{% hint style="info" %}
Full quote swap endpoint specification can be found here: ​[https://mayanode.mayachain.info/mayachain/doc](https://mayanode.mayachain.info/mayachain/doc).

See an example implementation LINK HERE
{% endhint %}

&#x20;​If you'd prefer to calculate the swap yourself, see the [Fees](../../../deep-dive/how-it-works/fees.md) section to understand what fees need to be accounted for in the output amount. Also, review the [Transaction Memos](../../concepts/transaction-memos.md) section to understand how to create the swap memos.

### 3. Sign and send transactions on the from\_asset chain. <a href="#id-3.-sign-and-send-transactions-on-the-from_asset-chain" id="id-3.-sign-and-send-transactions-on-the-from_asset-chain"></a>

Construct, sign and broadcast a transaction on the BTC network with the following parameters:

Amount => `1.0`

Recipient => `bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8`

Memo => `=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`

{% hint style="warning" %}
Never cache inbound addresses! Quotes should only be considered valid for 10 minutes. Sending funds to an old inbound address will result in loss of funds.
{% endhint %}

{% hint style="info" %}
Learn more about how to construct inbound transactions for each chain type here: ​[Sending Transactions](../../concepts/sending-transactions.md)
{% endhint %}

### 4. Receive tokens. <a href="#id-4.-receive-tokens" id="id-4.-receive-tokens"></a>

Once a majority of nodes have observed your inbound BTC transaction, they will sign the Ethereum funds out of the network and send them to the address specified in your transaction. You have just completed a non-custodial, cross-chain swap by simply sending a native L1 transaction.

### Additional Considerations <a href="#additional-considerations" id="additional-considerations"></a>

{% hint style="warning" %}
There is a rate limit of 1 request per second per IP address on /quote endpoints. It is advised to put a timeout on frontend components input fields, so that a request for quote only fires at most once per second. If not implemented correctly, you will receive 503 errors.
{% endhint %}

{% hint style="success" %}
For best results, request a new quote right before the user submits a transaction. This will tell you whether the _expected\_amount\_out_ has changed or if the _inbound\_address_ has changed. Ensuring that the _expected\_amount\_out_ is still valid will lead to better user experience and less frequent failed transactions.
{% endhint %}

#### Price Limits <a href="#price-limits" id="price-limits"></a>

Specify _tolerance\_bps_ to give users control over the maximum slip they are willing to experience before canceling the trade. If not specified, users will pay an unbounded amount of slip.​​

[https://mayanode.mayachain.info/mayachain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&tolerance\_bps=500](https://mayanode.mayachain.info/mayachain/quote/swap?amount=100000000\&from_asset=BTC.BTC\&to_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&tolerance_bps=500)

Notice how a minimum amount (1822007296/ \~18.22 ETH) has been appended to the end of the memo. This tells THORChain to revert the transaction if the transacted amount is more than 500 basis points less than what the expected\_amount\_out returns.

### Affiliate Fees <a href="#affiliate-fees" id="affiliate-fees"></a>

Specify affiliate\__address_ and _affiliate\_bps &#x74;_&#x6F; skim a percentage of the _expected_\__amount\_out._&#x200B;​

[https://mayanode.mayachain.info/mayachain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&affiliate=wr\&affiliate\_bps=10](https://mayanode.mayachain.info/mayachain/quote/swap?amount=100000000\&from_asset=BTC.BTC\&to_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&affiliate=wr\&affiliate_bps=10)

```json
{
  "expected_amount_out": "1851200268",
  "fees": {
    "affiliate": "1854014",
    "asset": "ETH.ETH",
    "outbound": "960000"
  },
  "inbound_address": "bc1qqtemwlu9ju3ts3da5l82qejnzdl3xfs3lcl4wg",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430::wr:10",
  "outbound_delay_blocks": 178,
  "outbound_delay_seconds": 2670,
  "slippage_bps": 168
}
```

Notice how `wr:10` has been appended to the end of the memo. This instructs MAYAChain to skim 10 basis points from the swap. The user should still expect to receive the _expected\_amount\_out,_ meaning the affiliate fee has already been subtracted from this number.&#x20;

For more information on affiliate fees: [fees.md](../../concepts/fees.md "mention")

### ~~Streaming Swaps~~ <a href="#streaming-swaps" id="streaming-swaps"></a>

[~~&#x200B;_&#x53;treaming Swaps_~~ ](streaming-swaps.md)~~_can be used to break up the trade to reduce slip fees._~~

~~Specify `streaming_interval` to define the interval in which streaming swaps are swapped.​​~~

```json
TBA
```

~~Notice how `approx_streaming_savings` shows the savings by using streaming swaps. `total_swap_seconds` also shows the amount of time the swap will take.~~

#### Error Handling <a href="#error-handling" id="error-handling"></a>

TBA

#### Support <a href="#support" id="support"></a>

Developers experiencing issues with these APIs can go to the Maya Protocol Discord server (LINK) for assistance. Interface developers should subscribe to the #interface-alerts channel for information pertinent to the endpoints and functionality discussed here.
