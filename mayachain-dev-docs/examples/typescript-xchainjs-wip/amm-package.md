---
description: API for performing actions with THORChain
---

# AMM Package

While the Query package allows quick and powerful information retrieval from THORChain such as a swap estimate., this package performs the actions (sends the transactions), such as a swap, add and remove.As a general rule, this package should be used in conjunction with the query package to first check if an action is going to be possible be performing the action.Example: call estimateSwap first to see if the swap is going to be successful before calling doSwap, as doSwap will not check.

**Code examples in Replit**

Currently implemented functions are listed below with code examples. Press the Run button to run the code and see the output. Press,`Show Files`, and select `index.ts` to see the code. Select `package.json` to see all the package dependencies. and .

#### DoSwap <a href="#doswap" id="doswap"></a>

Executes a swap from a given wallet. This will result in the inbound transaction into THORChain.DoSwap runs first then if successful with a constructed using a . Do swap can be used with an existing xchain client implementation or a custom wallet and will return the transaction hash of the inbound transaction.A seed is provided in the example but it has no funds so it will error.

#### Savers <a href="#savers" id="savers"></a>

Adds and removed liquidity from Savers. Requires a seed with funds.

#### Add Liquidity <a href="#add-liquidity" id="add-liquidity"></a>

Adds liquidity to a pool. Provide both assets for the pool. lp type is determined from the amount of the asset. The example is a single-sided rune deposit. A seed is provided in the example but it has no funds so it will error.

#### Remove Liquidity <a href="#remove-liquidity" id="remove-liquidity"></a>

Removes Liquidity from a pool. The opposite of adding liquidity.\
\
