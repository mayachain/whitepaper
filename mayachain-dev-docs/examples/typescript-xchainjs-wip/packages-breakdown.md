# Packages Breakdown

### How xChainjs is constructed

### xchain-\[chain] clients

Each blockchain that is integrated into MAYAChain has a corresponding xchain client with a suite of functionality to work with that chain. They all extend the `xchain-client` class.

### xchain-mayachain-amm

MAYAChain automatic market maker that uses MAYANode & Midgard Api's AMM functions like swapping, adding and removing liquidity. It wraps xchain clients and creates a new wallet class and balance collection.

### xchain-mayachain-query

Uses midgard and mayanode Api's to query MAYAChain for information. This module should be used as the starting place get any MAYAChain information that resides in MAYANode or Midgard as it does the heaving lifting and configuration.&#x20;

Default endpoints are provided with redundancy, custom MAYANode or Midgard endpoints can be provided in the constructor.&#x20;

### **xchain-midgard**&#x20;

This package is built from OpenAPI-generator. It is used by the mayachain-query.&#x20;

Thorchain-query contains midgard class that uses xchain-midgard and the following end points:&#x20;

* /v2/mayachain/mimir&#x20;
* /v2/mayachain/inbound\_addresses&#x20;
* /v2/mayachain/constants&#x20;
* /v2/mayachain/queue

For simplicity, is recommended to use the midgard class within mayachain-query instead of using the midgard package directly.&#x20;

#### Midgard Configuration in mayachain-query

Default endpoints `defaultMidgardConfig` are provided with redundancy within the Midgard class.&#x20;

```typescript
// How mayachain-query constructs midgard
const defaultMidgardConfig: Record<Network, MidgardConfig> = {
  mainnet: {
    apiRetries: 3,
    midgardBaseUrls: [
      'https://midgard.ninerealms.com',
      'https://midgard.thorchain.info',
      'https://midgard.thorswap.net',
    ],
  },
  ...
  export class Midgard {
  private config: MidgardConfig
  readonly network: Network
  private midgardApis: MidgardApi[]

  constructor(network: Network = Network.Mainnet, config?: MidgardConfig) {
    this.network = network
    this.config = config ?? defaultMidgardConfig[this.network]
    axiosRetry(axios, { retries: this.config.apiRetries, retryDelay: axiosRetry.exponentialDelay })
    this.midgardApis = this.config.midgardBaseUrls.map((url) => new MidgardApi(new Configuration({ basePath: url })))
  }
```

Custom Midgard endpoints can be provided in the constructor using the `MidgardConfig` type.&#x20;

```typescript
// adding custom endpoints
  const network = Network.Mainnet
  const customMidgardConfig: MidgardConfig = {
    apiRetries: 3,
    midgardBaseUrls: [
      'https://midgard.customURL.com',
    ],
  }  
  const midgard = new Midgard(network, customMidgardConfig)  
}
```

See [ListPools ](broken-reference)for a working example.&#x20;

### xchain-mayanode

This package is built from OpenAPI-generator and is also used by the thorchain-query. The design is similar to the midgard. MAYANode should only be used when time-sensitive data is required else midgard should be used.&#x20;

```typescript
// How thorchain-query constructs thornode
const defaultThornodeConfig: Record<Network, ThornodeConfig> = {
  mainnet: {
    apiRetries: 3,
    thornodeBaseUrls: [
      `https://thornode.ninerealms.com`,
      `https://thornode.thorswap.net`,
      `https://thornode.thorchain.info`,
    ],
  },
  ...
  export class Thornode {
  private config: ThornodeConfig
  private network: Network
 ...
  constructor(network: Network = Network.Mainnet, config?: ThornodeConfig) {
    this.network = network
    this.config = config ?? defaultThornodeConfig[this.network]
    axiosRetry(axios, { retries: this.config.apiRetries, retryDelay: axiosRetry.exponentialDelay })
    this.transactionsApi = this.config.thornodeBaseUrls.map(
      (url) => new TransactionsApi(new Configuration({ basePath: url })),
    )
    this.queueApi = this.config.thornodeBaseUrls.map((url) => new QueueApi(new Configuration({ basePath: url })))
    this.networkApi = this.config.thornodeBaseUrls.map((url) => new NetworkApi(new Configuration({ basePath: url })))
    this.poolsApi = this.config.thornodeBaseUrls.map((url) => new PoolsApi(new Configuration({ basePath: url })))
    this.liquidityProvidersApi = this.config.thornodeBaseUrls.map(
      (url) => new LiquidityProvidersApi(new Configuration({ basePath: url })),
    )
  }
```

#### MAYANode Configuration in mayachain-query

As with the midgard package, MAYANode can also be given custom end points via the`MayanodeConfig` type.&#x20;

**xchain-util**

A helper packager used by all the other packages. It has the following modules:

* `asset` - Utilities for handling assets
* `async` - Utitilies for `async` handling
* `bn` - Utitilies for using `bignumber.js`
* `chain` - Utilities for multi-chain
* `string` - Utilities for strings
