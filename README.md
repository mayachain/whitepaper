---
description: Introduction
cover: .gitbook/assets/1500x500 (1).jpeg
coverY: 0
---

# 🍫 What is Maya Protocol?

Maya Protocol is the ecosystem that encompasses both MAYAChain and AZTECChain. These two entities, when combined, are capable of demonstrating the power & advantages of both centralized and decentralized exchanges without inheriting the associated drawbacks.

{% embed url="https://www.youtube.com/watch?v=s8dDii4A8io" %}

### MAYAChain (Live)

**MAYAChain** is an Automated Market Maker, similar to Uniswap, but utilizes cross-chain liquidity. Conventionally, cross-chain swaps have been achievable through wrapping assets and bridging, which pose a serious security risk. According to Chainalysis, $2 billion were lost to bridge hacks in 2022 alone, representing 69% of all DeFi hacks.

**MAYAChain** is a friendly fork of THORChain. Similarly, it does not rely on pegging or wrapping assets, instead managing funds directly in on-chain vaults and safeguarding them through economic security. This is achieved using the Tendermint consensus engine, Cosmos-SDK state machine, and GG20 Threshold Signature Scheme (TSS).

**Interact with MAYAChain** using one of the frontend [**integrations**](https://docs.mayaprotocol.com/integrations).

### AZTECChain (Under Development)

**AZTECChain** is a Smart Contract chain. It exemplifies the potential of Maya Protocol, combining MAYAChain cross-chain liquidity with Smart Contracts and economic capabilities. Smart Contracts provide flexibility for crypto economies, allowing for the creation & use of Algorithmic stablecoins, derivatives like Synths, CEX style order book trading, and many more capabilities.\
\
**AZTECChain** is a fork of the Cosmos Hub (Gaia), allowing for the immediate utilization of the Cosmos mature infrastructure and Smart Contract development ecosystem. Algorithmic stablecoins will be delayed upon launch in order to complete economic design and run bounties. Neither the Maya nor the Aztec Chains will subsidize yield in order to inflate demand for their respective stablecoins or derivatives.

### Tokenomics Structure

Maya Protocol ecosystem utilizes 3 distinct tokens within its framework:

{% hint style="info" %}
The CACAO & $MAYA tokens are already in circulation, while AZTEC is still under development.
{% endhint %}

**CACAO** (Live) is the primary token of the ecosystem, which is utilized to pay for all transaction fees on both the Maya and Aztec Chains. CACAO is also the settlement token, used in pools in MAYAChain.

**MAYA** (Live) is a revenue share token. 10% of all swap & transaction fees on MAYAChain is distributed to $MAYA holders in the form of daily CACAO rewards.

**AZTEC** (under development), like MAYA, is a revenue share token. 10% of all transaction fees on Aztec Chain is distributed to AZTEC holders.

{% hint style="info" %}
All CACAO has been allocated fairly in the liquidity auction. The team has not been allocated any CACAO, and any CACAO acquired by the team was acquired in the same manner as the community. Rather, the team has opted to have allocations of MAYA and AZTEC. This ensures that the team is in alignment with the vision of building and delivering, as they will not be able to gain any benefit unless Maya Protocol is profitable.
{% endhint %}

### MAYAChain Innovations

MAYAChain shares numerous innovations with THORChain that were derived from the fundamental principles of being decentralized, resistant to capture, and as sustainable as possible.

**Capped Proof of Bond** validator selection helps to maintain the decentralization of the network and a high Nakamoto Coefficient.

**Periodic Validator Churning** process serves to prevent validator stagnation, verify the spendability of funds, and enhance the overall performance of the network with minimal governance requirements.

{% hint style="info" %}
A churn in MAYACHAIN happens once every 5 days, if no issues present themselves.
{% endhint %}

**Asynchronous Network Upgrades** allow validators to transition to a new protocol version at their own pace, while the network remains in consensus without disruption.

**Chain-agnostic Bifrost Protocol** can manage UTXO, EVM, BFT and Cryptonote chain connections with minimal differences in its core logic.

**Incentive Pendulum** system provides rewards to Validators and Liquidity Providers in order to maintain a Network Security ratio that ensures the security of funds at all times.

**Continuous Liquidity Pools** enable single-sided liquidity provision and employ liquidity-sensitive fees to thwart price attacks.

**Swap Queue** has been implemented which orders swaps based on the price impact in each block, thereby preventing sandwich attacks and most other forms of Miner Extractable Value (MEV).

**Liquidity Synths** have been developed to facilitate rapid, low-cost exchanges between L1 pool. Synths are a hybrid collateralized-pegged asset design that contribute to the liquidity of the market.

### MAYANodes

MAYANodes secure MAYAChain. They are intended to initially number 100, but can scale up to 250+. The design of MAYAChain is such that anyone with the necessary funds can join the network anonymously and securely, without requiring permission. Furthermore, MAYAChain takes this a step further by having a high churn rate, which ensures that the network is censorship-resistant, evades capture and resists centralization.&#x20;

Each MAYANode is composed of multiple independent servers in a cluster, which run full-nodes for each linked chain.

### The 3-Pillars of MAYAChain:

1. **Enhance the Security** of the network through Functional (Solvency Checker, Node Pause, TxOut Throttler), Procedural (Stagenet testing, PR reviews) and Economic (LP units in the bond, or value of the LP units in the bond) Security measures.\

2. **Increase the Liquidity** of the network, via Total Value Locked (TVL), or better UX around providing liquidity.\

3. **Boost the Volume** of the network, via Swap UX (Synths, Order Books), or wallet Integrations (Quotes Endpoint, Dev UX, Business Development).
