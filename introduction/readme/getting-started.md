---
description: Maya Protocol Quick Guide
---

# Getting Started

## Using MAYAChain

Users are not required to use CACAO in order to swap/trade Assets (e.g. BTC, ETH, RUNE, DASH, KUJI or ARB). Users can simply connect to any [**Frontend that supports MAYAChain**](getting-started.md#what-wallets-platforms-support-native-cacao), and trade away.

However, should users desire to perform other types of transactions, CACAO fees may apply.

### How to Buy Cacao?

1. Head to [one of the interfaces supporting Maya Protocol](getting-started.md#what-frontends-support-maya-protocol) and connect your [wallet](getting-started.md#what-wallets-support-maya-protocol).
2. Transfer any of the MAYAChain supported Assets (e.g. BTC, ETH, RUNE, DASH, XRD, KUJI or ARB) to your newly created wallet.
3. Head to the swap page, select the tokens and quantity, and perform the swap.

### What Wallets support Maya Protocol?

* [THORWallet](https://app.thorwallet.org/referrals?code=338919)
* [Leap Wallet](https://www.leapwallet.io/)
* [Ctrl Wallet ](https://chromewebstore.google.com/detail/ctrl-wallet/hmeobnfnfcmdkdcmlblgagmfpfboieaf)
* [KeepKey](https://www.keepkey.com)
* [Ledger](https://www.ledger.com/)&#x20;
* [Edge](https://edge.app)
* [Caviar Nine](https://www.caviarnine.com/tokens)
* [Moca](https://home.moca.app)
* [Shard Space](https://shardspace.app)
* [Clio Swap](https://www.clioswap.com/swap)\


### **What Frontends support Maya Protocol?**

* [El Dorito Swap](https://eldorito.club)
* [THORWallet](https://app.thorwallet.org/)
* [Rango Exchange](https://app.rango.exchange/)
* [Defispot](https://www.defispot.com)
* [Asgardex](https://www.asgardex.com/installer)
* [CacaoSwap](https://cacaoswap.app)
* [ThorSwap](https://www.thorswap.finance)
* [LeoDEX](https://leodex.io/)
* [Rango Exchange](https://rango.exchange/es)

### What are the Maya Protocol Blockchain Explorers?

* [MayaScan](https://www.mayascan.org/)&#x20;

##
