# Roles

There are four key roles in the system:

1. [**Liquidity providers**](https://docs.mayaprotocol.com/introduction/readme/roles/liquidity-providers) who add liquidity to pools and earn fees and rewards
2. [**Swappers**](https://docs.mayaprotocol.com/introduction/readme/roles/swapping) who use the liquidity to swap assets ad-hoc, paying fees
3. [**Arbitrageurs**](https://docs.mayaprotocol.com/introduction/readme/roles/trading) who monitor pools and rebalance continually, paying fees but with the intent to earn a profit.
4. [**Node Operators**](https://docs.mayaprotocol.com/introduction/readme/roles/node-operators) who provide a bond and are paid to secure the system
