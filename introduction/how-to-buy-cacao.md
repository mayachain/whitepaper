---
description: 'Unlocking CACAO: A Step-by-Step Guide to Seamless Token Acquisition'
---

# 🍫 How to buy CACAO?

{% hint style="info" %}
The CACAO token is not listed on any Centralized Exchange.&#x20;
{% endhint %}

You can buy CACAO by swapping BTC, ETH, USDT, USDC, wstETH, RUNE, DASH, KUJI, or USK to CACAO on one of the following interfaces:

* #### [ThorWallet](https://app.thorwallet.org/referrals?code=338919)
* #### [El Dorito Market](https://www.eldorado.market/)&#x20;
* #### [Rango Exchange](https://app.rango.exchange/) &#x20;
* [Asgardex](https://twitter.com/asgardex)&#x20;
* [Swapper](https://app.swapper.market/)&#x20;
* [Defispot](https://app.defispot.com/trade?from=BTC.BTC\&to=ETH.ETH)&#x20;
* [CacaoSwap](https://cacaoswap.app)
* [LeoDex](https://leodex.io)
* [THORSwap](https://www.thorswap.finance/)

**Step by Step:**

1. Create your wallet on your chosen interface.
2. Load your wallet with your chosen asset (BTC, ETH, USDT, USDC, wstETH, RUNE, DASH, KUJI, USK or AETH).&#x20;
3. Select Swap.
4. Enter the amount you wish to swap and the asset you want to get; in this case, the asset you want to get is CACAO.

You are ready!

Resources:

We have developed guides that explain step by step how to use each user interface:

* [Guides to set up your MAYAChain wallet ](https://docs.mayaprotocol.com/deep-dive/guides/swapping-assets)
* [Swap assets guide](https://docs.mayaprotocol.com/deep-dive/guides/swapping-assets)

### **CACAO** Price:

Follow up on CACAO behavior on [CoinGecko](https://www.coingecko.com/en/coins/maya-protocol) or [MayaScan](https://www.mayascan.org/).

\
