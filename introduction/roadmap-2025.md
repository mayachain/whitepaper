# 🛣️ Roadmap 2025

<figure><img src="../.gitbook/assets/Maya photoAsset 6-100 (2).jpg" alt=""><figcaption></figcaption></figure>

## Mimir V2

Mimir v2 shifts control to **node operators**, letting them adjust permissions and halt chains when needed. It boosts **decentralization** and empowers the community to propose and vote on upgrades, ensuring Maya evolves with transparency.

## Aztec&#x20;

Enables users to access Smart Contracts and build advanced DeFi tools like derivatives and algorithmic stablecoins within the Maya ecosystem.

## Zcash Integration

Zcash $ZEC is depositable into Maya for Swaps & Liquidity actions.

## Taproot

Maya integrates Taproot+, bringing enhanced privacy and scalability to Bitcoin transactions. This unlocks exciting new DeFi use cases for BTC, making it more versatile and efficient in the Maya ecosystem.

## Kaspa

Kaspa $KAS is depositable into Maya for Swaps & Liquidity actions.

## Swapper Clout

## THORChain Dex Aggregation

Enabling swaps between THORChain & MAYAChain assets.

## Cardano Integration

Cardano $ADA is depositable into Maya for Swaps & Liquidity actions.

## More Chains

Will be announced accordingly.
