---
description: List of all Wallets and UIs integrations with Maya Protocol.
---

# User interfaces & Wallets

Maya Protocol is a backend infrastructure. Therefore, you will need a user interface to interact with and use Maya Protocol. Different user interfaces are available. Here’s the complete list.

## <mark style="color:blue;">User Interfaces</mark>

### Thorwallet DEX

**Type:** Decentralized Exchange

**Accessibility:** Web App and Mobile

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya and THORChain-supported L1

**Supported wallets Webapp:** Ledger, XDEFI, Metamask, Keystore

{% hint style="success" %}
Go to [Thorwallet DEX](https://www.thorwallet.org/)&#x20;
{% endhint %}

### El Dorito Swap&#x20;

**Type:** Decentralized Exchange

**Accessibility:** Web App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1, Polkadot

**Supported wallets:** Ctrl Wallet, Keystore

{% hint style="success" %}
Go to[ El Dorito Swap](https://www.eldorado.market)
{% endhint %}

### CacaoSwap&#x20;

**Type:** Decentralized Exchange

**Accessibility:** Web App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Keystore, MetaMask, Ctrl Wallet, Keplr, Leap

{% hint style="success" %}
Go to [Cacao Swap](https://cacaoswap.app/?assetId=dash.dash)
{% endhint %}

### Asgardex

**Type:** Decentralized Exchange and Wallet

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Streaming Swaps&#x20;
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1, and THORChain

**Supported wallets:** Keystore, Ledger

{% hint style="success" %}
Go to [Asgardex](https://t.co/tmfEi5x0Lf)
{% endhint %}

### ThorSwap

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Streaming Swaps
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Ctrl Wallet, Metamask, KeepKey, Ledger, Brave Wallet, Trustwallet, and Trezor.

{% hint style="success" %}
Go to [ThorSwap ](https://www.thorswap.finance/)
{% endhint %}

### InstaSwap

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Ctrl Walle&#x74;**,** Metamask, Trust Wallet, Phantom Wallet, Trezor Wallet, Ledger, OKX Wallet, Wallet Connect, Keplr Wallet, Brave Wallet, Radix Wallet, KeepKey.

{% hint style="success" %}
Go to [InstaSWap](https://www.instaswap.com/)
{% endhint %}

### Astrolescent

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Radix Wallet

{% hint style="success" %}
Go to [Astrolescent](https://astrolescent.com/bridge)&#x20;
{% endhint %}

### FortunaDEX

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Ctrl Wallet, Metamask.

### LeoDEX

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Ctrl Wallet, Metamask.\


{% hint style="success" %}
Go to [LeoDEX](https://leodex.io/)
{% endhint %}

### Pulsar Finance&#x20;

**Type:**  Portfolio tracker and analytics tool

**Accessibility:** Desktop App\
\
**Features**:&#x20;

* **Portfolio Tracking for Maya Assets**: Users can seamlessly monitor their $CACAO holdings and other assets across chains supported by Maya Protocol.
* **Cross-Chain Insights**: Pulsar provides detailed analytics for swaps and liquidity pools within Maya Protocol, offering valuable insights for both traders and liquidity providers.
* **Real-Time Data**: Access up-to-date stats and performance metrics from Maya Protocol to stay ahead of the market.
* **Enhanced User Experience**: By integrating with Maya Protocol, Pulsar makes it easier for users to track and manage their cross-chain activities in one place.

{% hint style="success" %}
Go to [Pulsar Finance](https://app.pulsar.finance/ecosystem)
{% endhint %}

### DefiSpot&#x20;

**Type:** Decentralized Exchange&#x20;

**Accessibility:** Desktop App

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps powered by Maya
* Manage your liquidity and saver's position on Maya

**Blockchain support:** All Maya-supported L1

**Supported wallets:** Ctrl Wallet, Metamask, Keplr, Phantom, Walletconnect, Leap Wallet, Argeentx, Braavos, Trustwallet, and Rabby.

{% hint style="success" %}
Go to [Defispot](https://www.defispot.com/tokens/ETH.ETH?from=BTC.BTC\&to=ETH.ETH)
{% endhint %}

***

## <mark style="color:blue;">Wallets</mark>

### Ctrl Wallet&#x20;

Ctrl Wallet is a multi-ecosystem self-custody wallet with support for 30+ native blockchains, and all EVM and Cosmos chains, including Bitcoin, Ethereum, Solana, THORChain, Maya Protocol, TRON, and more.

**Type:** Multichain non-custodial wallet

**Available on:** Web extension

**Features:**&#x20;

* Send and receive CACAO and MAYA

Blockchain support: Bitcoin, Solana, all EVM, all Cosmos, THORChain, MAYA, TRON, DOGE, Litecoin, NEAR, Binance Smart Chain&#x20;

Hardware wallet integration: Ledger, Trezor.

{% hint style="success" %}
Go to [Ctrl Wallet](https://ctrl.xyz)&#x20;
{% endhint %}

### Caviar Nine&#x20;

CaviarNine is a platform that provide web3 users with advanced and user-friendly DeFi trading products built on the Radix platform.\
\
**Type:** Multichain non-custodial wallet

**Available on:** WebAPP

Blockchain support:  Radix Tokens.

{% hint style="success" %}
Go to [Caviar Nine ](https://www.caviarnine.com)
{% endhint %}

### Moca

Moca is a cryptocurrency wallet for easily managing your balances and tokens across all platforms – no prior experience required.

**Type:** Multichain non-custodial wallet

**Available on:** WebAPP

**Features:**&#x20;

* Send and receive CACAO and MAYA

Blockchain support:  BTC, ETH, LTC, DASH, MATIC, ADA, AVAX, SOL, ATOM, DOGE, BNB.

{% hint style="success" %}
Go to [Moca ](https://home.moca.app/#Home)
{% endhint %}

### Vultisig&#x20;

**Vultisig** is the first institutional-grade, privacy-focused, multi-chain wallet on the market, built on **THORChain’s vault technology**. Designed for both individuals and organizations, it offers unparalleled security, privacy, and functionality for managing digital assets seamlessly across multiple blockchains.

**Available on:**

* Web
* iOS
* Android

**Features:**&#x20;

* Send and receive CACAO and MAYA

Blockchain support: &#x20;

* Arbitrum, Avalanche, BSC, Base, Bitcoin, Bitcoin-Cash, Blast, Cosmos, CronosChain, Dash, Dogecoin, dydx, Ethereum, Kujira, Litecoin, Maya Protocol, Optimisim, Polkadot, Polygon, Solana, Sui, THORChain, Ton, Zksync

{% hint style="success" %}
Go to [Vultisig ](https://vultisig.com/docs)
{% endhint %}

### WinBit32&#x20;

A retro-inspired, no-frills crypto wallet designed for enthusiasts who value simplicity and control. With its Windows 3.11 aesthetic and focus on direct phrase-based authentication, it keeps things intuitive while offering powerful tools for managing cross-chain wallets, swapping assets, and enhancing security.

**Type:** Multichain non-custodial wallet

**Available on:** Web extension

**Features:**&#x20;

* Send and receive CACAO and MAYA

{% hint style="success" %}
Go to [WinBit32](https://winbit32.com/)
{% endhint %}

### Edge

Edge is a powerful and easy to use cryptocurrency wallet that allows users to easily control their own private keys with the familiarity and ease of mobile banking.&#x20;

**Type:** Multichain non-custodial wallet

**Available on:** Web extension

**Features:**&#x20;

* Send and receive CACAO and MAYA

Blockchain support:  Bitcoin, Ethereum, Litecoin, EOS, Stellar, XRP, Dash, Monero.

{% hint style="success" %}
Go to [Edge](https://edge.app/?af=edge-app)
{% endhint %}

***

## <mark style="color:blue;">Hardware wallet</mark>

### KeepKey&#x20;

KeepKey is a hardware wallet for securely storing digital assets.

**Type:** Hardware Wallet

**Features:**&#x20;

* Send and receive CACAO and MAYA
* Cross-chain native swaps: Yes (In firmware, no dApps yet)
* Manage liquidity and saver's position:  Yes (in firmware, no dApps yet)

**Blockchain support:** All Maya L1's, THORChain’s L1, and several EVMs and Cosmos chains.

{% hint style="success" %}
Go to [https://keepkey.com/](https://keepkey.com/)\

{% endhint %}
