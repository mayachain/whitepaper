---
description: >-
  Explore all user interfaces, wallets, and tools,  that will enrich your
  decentralized Maya experience to the fullest!
---

# 🌐 Maya Ecosystem

{% hint style="info" %}
Maya Protocol is permissionless. Everyone can integrate Maya into their dapp, wallet, or protocol. We support all interfaces integrating Maya. Contact our marketing team on [Discord ](http://discord.gg/mayaprotocol)to discuss co-marketing opportunities.&#x20;
{% endhint %}

{% hint style="danger" %}
**Disclaimer:** Maya Protocol will not support swapping interfaces and wallets containing ‘Maya’ in their name. We’ve learned from other protocols that this could have negative side effects. Hence, if an interface chooses to have ‘Maya’ in its name, Maya Protocol will not engage in co-marketing and will not mention the interface on its [website,](https://www.mayaprotocol.com/) docs, or [Discord.](http://discord.gg/mayaprotocol)
{% endhint %}
