---
description: Discover a complete toolkit to tailor your Maya experience
---

# Tools

### Maya Scan&#x20;

MayaScan is a cutting-edge blockchain explorer designed for speed, simplicity, user-friendliness, stunning UI, and efficiency. It offers a streamlined and intuitive experience for users seeking to explore blockchain data from the Maya Protocol.

**Features:** Maya Protocol monitors for transactions, addresses, LP, analytics and networks stats, MRC-20 & M-NFT indexer powered by GLD.

**Accessibility:** Web and mobile responsive.

{% hint style="success" %}
Go to [Maya Scan](https://www.mayascan.org/)
{% endhint %}

### Maya Swap&#x20;

Maya Swap is a seamless trading platform for MAYA and CACAO tokens with a tailored order book. By holding MAYA tokens, users automatically earn daily CACAO rewards, enriching their portfolios. It was built by the MayaScan team.

**Features:** Buy and sell $MAYA tokens in the order book. Track prices and volume in real time.

{% hint style="success" %}
Go to [Maya Swap](https://www.mayaswap.org/)
{% endhint %}

### Mayans.app&#x20;

Connect, Trade, Earn, Thrive. Mayans brings the worlds of social media and decentralized finance together through the Maya Protocol. Trade your MRC-20 tokens and M-NFTs. Built by the MayaScan team.

**Features:** Secure and private messaging, MRC-20 trading and staking, signals keeping you informed of the latest trends, playing DeFi games, and more.

{% hint style="success" %}
Go to [Mayans.app](https://mayans.app/)
{% endhint %}

### Maya Info Bot&#x20;

The Maya Info Bot tracks significant transactions and crucial changes to the Maya Protocol network. Subscribers are promptly notified via Telegram, Discord, and X channels.

**Features:**&#x20;

Tracked event types

&#x20;1\) Large transfers of native assets

&#x20;2\) Significant swap and liquidity addition/withdrawal transactions

&#x20;3\) Liquidity pool statistics

&#x20;4\) Cacao price fluctuations

&#x20;5\) Mimir parameter adjustments

**Accessibility:** Any mobile or desktop device with Telegram/Discord/X app installed or web browser.

{% hint style="success" %}
Keep track of price, large txs, Mimir, and other alerts [here](https://twitter.com/maya\_infobot)!
{% endhint %}
