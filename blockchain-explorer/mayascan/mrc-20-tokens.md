---
description: MAYAChain Native Tokens
---

# 🪙 MRC-20 Tokens

**MRC-20 tokens** are a revolutionary step forward in the Maya Protocol, and are powered by the MAYAChain and indexed by MayaScan. This new ecosystem is designed to make token deployment, minting, transfer, selling, and buying effortless and efficient. It is based on the concept of memos and ordinal theory, which draws inspiration from both the ERC-20 standard on Ethereum and Bitcoin's ordinals.&#x20;

**MRC-20** combines these elements to create a comprehensive and secure framework for managing token interactions, ensuring the preservation of network integrity. By providing this new standardized approach to token operations, MRC-20 is revolutionizing the way users interact with tokens on the blockchain and reinforcing Maya's position as a leader in blockchain innovation.

For more info on how to interact with MRC-20 Tokens, or even create your own, [**click here**](https://www.mayascan.org/standards/mrc-20/docs).
