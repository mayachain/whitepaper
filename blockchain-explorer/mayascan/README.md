---
description: Maya Protocol's Blockchain Explorer
---

# 🔎 MayaScan

[**MayaScan**](https://www.mayascan.org/) is the main blockchain explorer offered by Maya Protocol, offering an extensive range of services for users to track their transactions, swaps, liquidity, network status, nodes and more.&#x20;

**By entering** any of the supported MAYAChain addresses (Bitcoin, Ethereum, THORChain, Dash, Kujira, and of course MAYAChain) into the search field, users can easily view all the relevant information regarding the address.&#x20;

**MayaScan** provides a secure platform for on-chain p2p messaging, allowing for anonymous communications between nodes.&#x20;

As if this wasn’t enough, MayaScan also uses cutting-edge Ordinals technology and Memos to create and trade Native MAYAChain Tokens and Non-Fungible Tokens (NFTs).
